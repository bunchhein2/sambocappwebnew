﻿using System.Web;
using System.Web.Mvc;

namespace sambocappweb
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new AuthorizeAttribute());
            //filters.Add(new ContentSecurityPolicyFilterAttribute());
        }
    }

    //public class ContentSecurityPolicyFilterAttribute : ActionFilterAttribute
    //{
    //    public override void OnActionExecuting(ActionExecutingContext filterContext)
    //    {
    //        HttpResponseBase response = filterContext.HttpContext.Response;
    //        var header = response.Headers["Content-Security-Policy"];
    //        if (header == null)
    //        {
    //            response.AddHeader("Content-Security-Policy", "default-src 'none'; script-src 'self';style-src 'self' 'unsafe-inline';img-src 'self';font-src 'self' fonts.gstatic.com; connect-src 'self'; form-action 'self';");
    //        }
    //        base.OnActionExecuting(filterContext);
    //    }
    //}
}
 