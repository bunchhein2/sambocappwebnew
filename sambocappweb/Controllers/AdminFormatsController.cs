﻿using Microsoft.Reporting.WebForms;
using sambocappweb.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using ZXing;
using ZXing.Common;
using Microsoft.AspNet.Identity;
using System.Configuration;
using System.Data;
using sambocappweb.ViewModels;

namespace sambocappweb.Controllers
{
    public class AdminFormatsController : Controller
    {
        private ApplicationDbContext _context;
        public AdminFormatsController()
        {
            _context = new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // GET: adminformat
        [Route("administrative-format/report={report}")]
        public ActionResult Index(int Report)
        {
            if (Report > 0)
            {
                var userId = User.Identity.GetUserId();
                var user = _context.Users.SingleOrDefault(c => c.Id == userId);
                var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                SqlConnection con = new SqlConnection(connectionString);

                string base64String = "";
                var qrWriter = new BarcodeWriter();
                qrWriter.Format = BarcodeFormat.QR_CODE;
                qrWriter.Options = new EncodingOptions() { Height = 80, Width = 80, Margin = 0 };

                using (var q = qrWriter.Write("Created By: " + user.FirstName + " " + user.LastName + "\nCreated on: " + DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss tt")))
                {
                    using (var ms = new MemoryStream())
                    {
                        q.Save(ms, ImageFormat.Png);
                        byte[] imageBytes = ms.ToArray();
                        base64String = Convert.ToBase64String(imageBytes);
                    }
                }

                DataTable dt = new DataTable();
                con.Open();
                var dap = new SqlDataAdapter("select * from GenerateReport_V Where Id = '" + Report + "'", con);
                dap.Fill(dt);
                con.Close();

                con.Open();
                SqlCommand cmd = new SqlCommand("SELECT dbo.F_TO_LUNAR_DATE(@Date)", con);
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@Date", DateTime.Today.Date);
                var KhmerDate = cmd.ExecuteScalar();

                cmd = new SqlCommand("SELECT dbo.F_TO_KHMER_NUMBER(YEAR(@Date))", con);
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@Date", DateTime.Today.Date);
                var KhmerYear = "រាជធានីភ្នំពេញ, ថ្ងៃទី        ខែ          ឆ្នាំ" + cmd.ExecuteScalar();

                cmd = new SqlCommand("SELECT dbo.F_TO_LUNAR_DATE_EN_STYLE(@Date)", con);
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@Date", (DateTime)dt.Rows[0]["StartDate"]);
                var startDateFromOrOn = cmd.ExecuteScalar();

                cmd = new SqlCommand("SELECT dbo.F_TO_LUNAR_DATE_EN_STYLE(@Date)", con);
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@Date", (DateTime)dt.Rows[0]["EndDate"]);
                var endDateFromOrOn = cmd.ExecuteScalar();
                con.Close();

                int fromRanking = (int)dt.Rows[0]["RKFromRanking"];
                int toRanking = (int)dt.Rows[0]["RKToRanking"];
                int duration = (int)dt.Rows[0]["Durations"];
                string reportReference = dt.Rows[0]["Reference"].ToString();
                string reportVia = dt.Rows[0]["Via"].ToString();
                ReportParameter topHeaderSalute = new ReportParameter();
                ReportParameter topBodyContentSalute = new ReportParameter();
                ReportParameter topBodyContentSalute1 = new ReportParameter();
                ReportParameter conclusionSalute = new ReportParameter();
                ReportParameter conclusionSalute2 = new ReportParameter();
                ReportParameter conclusionSalute3 = new ReportParameter();
                ReportParameter fromOrOn = new ReportParameter();
                ReportParameter fromOrOnDate = new ReportParameter("fromOrOnDate", "");
                if (duration > 1)
                {
                    fromOrOn = new ReportParameter("fromOrOn", "ចាប់ពី" + startDateFromOrOn + " ដល់" + endDateFromOrOn);
                }
                else
                {
                    fromOrOn = new ReportParameter("fromOrOn", "កាលពី" + startDateFromOrOn);
                }

                if (fromRanking < toRanking)
                {
                    topHeaderSalute = new ReportParameter("topHeaderSalute", "ជម្រាបជូន");
                    topBodyContentSalute = new ReportParameter("topBodyContentSalute", "សូមជម្រាបជូន");
                    topBodyContentSalute1 = new ReportParameter("topBodyContentSalute1", "ជ្រាបថា");
                    conclusionSalute = new ReportParameter("conclusionSalute", "ជម្រាបជូន");
                    conclusionSalute2 = new ReportParameter("conclusionSalute2", "ជ្រាប");
                    conclusionSalute3 = new ReportParameter("conclusionSalute3", "ទទួលនូវការរាប់អានពីខ្ញុំ");
                }
                else if (fromRanking > toRanking)
                {
                    topHeaderSalute = new ReportParameter("topHeaderSalute", "សូមគោរពជូន");
                    topBodyContentSalute = new ReportParameter("topBodyContentSalute", "សូមគោរពជម្រាបជូន");
                    topBodyContentSalute1 = new ReportParameter("topBodyContentSalute1", "មេត្តាជ្រាបដ៏ខ្ពង់ខ្ពស់ថា");
                    conclusionSalute = new ReportParameter("conclusionSalute", "គោរពជម្រាបជូន");
                    conclusionSalute2 = new ReportParameter("conclusionSalute2", "មេត្តាជ្រាប");
                    conclusionSalute3 = new ReportParameter("conclusionSalute3", "មេត្តាទទួលនូវគារវកិច្ចដ៏ខ្ពង់ខ្ពស់ពីខ្ញុំ");
                }
                else
                {
                    topHeaderSalute = new ReportParameter("topHeaderSalute", "គោរពជូន");
                    topBodyContentSalute = new ReportParameter("topBodyContentSalute", "សូមជម្រាបជូន");
                    topBodyContentSalute1 = new ReportParameter("topBodyContentSalute1", "ជ្រាបថា");
                    conclusionSalute = new ReportParameter("conclusionSalute", "ជម្រាបជូន");
                    conclusionSalute2 = new ReportParameter("conclusionSalute2", "ជ្រាប");
                    conclusionSalute3 = new ReportParameter("conclusionSalute3", "ទទួលនូវគារវកិច្ចពីខ្ញុំ");
                }

                ReportParameter khmerDate = new ReportParameter("khmerDate", KhmerDate.ToString());
                ReportParameter khmerYear = new ReportParameter("khmerYear", KhmerYear.ToString());
                ReportParameter toFullTitle = new ReportParameter("toFullTitle", dt.Rows[0]["CSToFullTitle"].ToString());
                ReportParameter reportTypeName = new ReportParameter("reportTypeName", dt.Rows[0]["ReportTypeName"].ToString());
                ReportParameter objective = new ReportParameter("objective", dt.Rows[0]["Objective"].ToString());
                ReportParameter via = new ReportParameter("via", dt.Rows[0]["Via"].ToString());
                ReportParameter content = new ReportParameter("content", dt.Rows[0]["Content"].ToString());
                ReportParameter location = new ReportParameter("location", dt.Rows[0]["Location"].ToString());
                ReportParameter reference = new ReportParameter("reference", dt.Rows[0]["Reference"].ToString());
                ReportParameter toShortTitle = new ReportParameter("toShortTitle", dt.Rows[0]["CSToShortTitle"].ToString());
                ReportParameter fromFullTitle = new ReportParameter("fromFullTitle", dt.Rows[0]["CSFromFullTitle"].ToString());
                ReportParameter qrCODE = new ReportParameter("qrCODE", base64String);
                ReportViewer reportViewer = new ReportViewer();
                reportViewer.ProcessingMode = ProcessingMode.Local;
                reportViewer.SizeToReportContent = true;
                reportViewer.Width = Unit.Percentage(100);
                reportViewer.Height = Unit.Percentage(100);
                //reportViewer.AsyncRendering = false;
                //reportViewer.SizeToReportContent = true;
                //reportViewer.ZoomMode = ZoomMode.FullPage;
                reportViewer.LocalReport.EnableExternalImages = true;
                if (reportReference == "")
                {
                    if (reportVia == "")
                    {
                        reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Reports\ReportNoReferenceReport.rdlc";
                    }
                    else
                    {
                        reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Reports\ReportViaNoReference.rdlc";
                    }
                }
                else
                {
                    if (reportVia == "")
                    {
                        reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Reports\ReportReferenceReport.rdlc";
                    }
                    else
                    {
                        reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Reports\ReportReferenceAndVia.rdlc";
                    }
                }
                reportViewer.LocalReport.SetParameters(new ReportParameter[] { qrCODE, khmerDate, khmerYear, topHeaderSalute, toFullTitle, reportTypeName, objective, content, via, fromOrOn, fromOrOnDate, location, reference, topBodyContentSalute, toShortTitle, topBodyContentSalute1, conclusionSalute, conclusionSalute2, conclusionSalute3, fromFullTitle });
                reportViewer.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", dt));
                ViewBag.ReportViewer = reportViewer;
            }
            else
            {
                DataTable dt = new DataTable();
                ReportViewer reportViewer = new ReportViewer();
                reportViewer.ProcessingMode = ProcessingMode.Local;
                reportViewer.SizeToReportContent = true;
                reportViewer.Width = Unit.Percentage(100);
                reportViewer.Height = Unit.Percentage(100);
                reportViewer.LocalReport.EnableExternalImages = true;
                reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Reports\ReportReferenceReport.rdlc";
                reportViewer.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", dt));
                ViewBag.ReportViewer = reportViewer;
            }


            var viewModel = new AdminFormatViewModel()
            {
                Ranks = _context.Ranks.ToList(),
               // CivilServants = _context.CivilServants.ToList(),
                ReportTypes = _context.ReportTypes.ToList()
            };
            return View(viewModel);
        }

        // GET: adminformat
        [Route("administrative-format/invitation={invitation}")]
        public ActionResult InvitationLetter(int Invitation)
        {
            if (Invitation > 0)
            {
                var userId = User.Identity.GetUserId();
                var user = _context.Users.SingleOrDefault(c => c.Id == userId);
                var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                SqlConnection con = new SqlConnection(connectionString);

                string base64String = "";
                var qrWriter = new BarcodeWriter();
                qrWriter.Format = BarcodeFormat.QR_CODE;
                qrWriter.Options = new EncodingOptions() { Height = 80, Width = 80, Margin = 0 };

                using (var q = qrWriter.Write("Created By: " + user.FirstName + " " + user.LastName + "\nCreated on: " + DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss tt")))
                {
                    using (var ms = new MemoryStream())
                    {
                        q.Save(ms, ImageFormat.Png);
                        byte[] imageBytes = ms.ToArray();
                        base64String = Convert.ToBase64String(imageBytes);
                    }
                }

                DataTable dt = new DataTable();
                con.Open();
                var dap = new SqlDataAdapter("select * from GenerateInvitationLetter_V Where Id = '" + Invitation + "'", con);
                dap.Fill(dt);
                con.Close();

                con.Open();
                SqlCommand cmd = new SqlCommand("SELECT dbo.F_TO_LUNAR_DATE(@Date)", con);
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@Date", DateTime.Today.Date);
                var KhmerDate = cmd.ExecuteScalar();

                cmd = new SqlCommand("SELECT dbo.F_TO_KHMER_NUMBER(YEAR(@Date))", con);
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@Date", DateTime.Today.Date);
                var KhmerYear = "រាជធានីភ្នំពេញ, ថ្ងៃទី        ខែ          ឆ្នាំ" + cmd.ExecuteScalar();
                con.Close();

                string invitationReference = dt.Rows[0]["Reference"].ToString();

                ReportParameter khmerDate = new ReportParameter("khmerDate", KhmerDate.ToString());
                ReportParameter khmerYear = new ReportParameter("khmerYear", KhmerYear.ToString());
                ReportParameter topHeaderSalute = new ReportParameter();
                ReportParameter topBodyContentSalute = new ReportParameter();
                ReportParameter topBodyContentSalute1 = new ReportParameter();
                ReportParameter conclusionSalute = new ReportParameter();
                ReportParameter conclusionSalute2 = new ReportParameter();
                ReportParameter conclusionSalute3 = new ReportParameter();
                ReportParameter fromOrOn = new ReportParameter();
                ReportParameter fromOrOnDate = new ReportParameter("fromOrOnDate", "");
                ReportParameter qrCODE = new ReportParameter("qrCODE", base64String);
                ReportViewer reportViewer = new ReportViewer();
                reportViewer.ProcessingMode = ProcessingMode.Local;
                reportViewer.SizeToReportContent = true;
                reportViewer.Width = Unit.Percentage(100);
                reportViewer.Height = Unit.Percentage(100);
                reportViewer.LocalReport.EnableExternalImages = true;
                if (invitationReference == "")
                {
                    reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Reports\InvitationLetterNoReference.rdlc";
                }
                else
                {
                    reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Reports\InvitationLetter.rdlc";
                }
                
                reportViewer.LocalReport.SetParameters(new ReportParameter[] { qrCODE, khmerDate, khmerYear });
                reportViewer.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", dt));
                ViewBag.ReportViewer = reportViewer;
            }
            else
            {
                DataTable dt = new DataTable();
                ReportViewer reportViewer = new ReportViewer();
                reportViewer.ProcessingMode = ProcessingMode.Local;
                reportViewer.SizeToReportContent = true;
                reportViewer.Width = Unit.Percentage(100);
                reportViewer.Height = Unit.Percentage(100);
                reportViewer.LocalReport.EnableExternalImages = true;
                reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Reports\InvitationLetter.rdlc";
                reportViewer.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", dt));
                ViewBag.ReportViewer = reportViewer;
            }


            var viewModel = new AdminFormatViewModel()
            {
                Ranks = _context.Ranks.ToList(),
               // CivilServants = _context.CivilServants.ToList(),
                ReportTypes = _context.ReportTypes.ToList()
            };
            return View("Index", viewModel);
        }

        // GET: adminformat
        [Route("administrative-format/principle={principle}")]
        public ActionResult PrincipleLetter(int Principle)
        {
            if (Principle > 0)
            {
                var userId = User.Identity.GetUserId();
                var user = _context.Users.SingleOrDefault(c => c.Id == userId);
                var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                SqlConnection con = new SqlConnection(connectionString);

                string base64String = "";
                var qrWriter = new BarcodeWriter();
                qrWriter.Format = BarcodeFormat.QR_CODE;
                qrWriter.Options = new EncodingOptions() { Height = 80, Width = 80, Margin = 0 };

                using (var q = qrWriter.Write("Created By: " + user.FirstName + " " + user.LastName + "\nCreated on: " + DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss tt")))
                {
                    using (var ms = new MemoryStream())
                    {
                        q.Save(ms, ImageFormat.Png);
                        byte[] imageBytes = ms.ToArray();
                        base64String = Convert.ToBase64String(imageBytes);
                    }
                }

                DataTable dt = new DataTable();
                con.Open();
                var dap = new SqlDataAdapter("select * from GeneratePrinciple_V Where Id = '" + Principle + "'", con);
                dap.Fill(dt);
                con.Close();

                con.Open();
                SqlCommand cmd = new SqlCommand("SELECT dbo.F_TO_LUNAR_DATE(@Date)", con);
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@Date", DateTime.Today.Date);
                var KhmerDate = cmd.ExecuteScalar();

                cmd = new SqlCommand("SELECT dbo.F_TO_KHMER_NUMBER(YEAR(@Date))", con);
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@Date", DateTime.Today.Date);
                var KhmerYear = "រាជធានីភ្នំពេញ, ថ្ងៃទី        ខែ          ឆ្នាំ" + cmd.ExecuteScalar();
                con.Close();

                int fromRanking = (int)dt.Rows[0]["RKFromRanking"];
                int toRanking = (int)dt.Rows[0]["RKToRanking"];
                string principleReference = dt.Rows[0]["Reference"].ToString();
                string principleVia = dt.Rows[0]["Via"].ToString();
                ReportParameter topHeaderSalute = new ReportParameter();
                ReportParameter topBodyContentSalute = new ReportParameter();
                ReportParameter topBodyContentSalute1 = new ReportParameter();
                ReportParameter conclusionSalute = new ReportParameter();
                ReportParameter conclusionSalute2 = new ReportParameter();
                ReportParameter conclusionSalute3 = new ReportParameter();

                if (fromRanking < toRanking)
                {
                    topHeaderSalute = new ReportParameter("topHeaderSalute", "ជម្រាបជូន");
                    topBodyContentSalute = new ReportParameter("topBodyContentSalute", "សូមជម្រាបជូន");
                    topBodyContentSalute1 = new ReportParameter("topBodyContentSalute1", "ជ្រាបថា");
                    conclusionSalute = new ReportParameter("conclusionSalute", "ជម្រាបជូន");
                    conclusionSalute2 = new ReportParameter("conclusionSalute2", "ពិនិត្យ និងសម្រេចតាមការគួរ");
                    conclusionSalute3 = new ReportParameter("conclusionSalute3", "ទទួលនូវការរាប់អានពីខ្ញុំ");
                }
                else if (fromRanking > toRanking)
                {
                    topHeaderSalute = new ReportParameter("topHeaderSalute", "សូមគោរពជូន");
                    topBodyContentSalute = new ReportParameter("topBodyContentSalute", "សូមគោរពជម្រាបជូន");
                    topBodyContentSalute1 = new ReportParameter("topBodyContentSalute1", "មេត្តាជ្រាបដ៏ខ្ពង់ខ្ពស់ថា");
                    conclusionSalute = new ReportParameter("conclusionSalute", "គោរពជម្រាបជូន");
                    conclusionSalute2 = new ReportParameter("conclusionSalute2", "មេត្តាពិនិត្យ និងសម្រេចដ៍ខ្ពង់ខ្ពស់");
                    conclusionSalute3 = new ReportParameter("conclusionSalute3", "មេត្តាទទួលនូវគារវកិច្ចដ៏ខ្ពង់ខ្ពស់ពីខ្ញុំ");
                }
                else
                {
                    topHeaderSalute = new ReportParameter("topHeaderSalute", "គោរពជូន");
                    topBodyContentSalute = new ReportParameter("topBodyContentSalute", "សូមជម្រាបជូន");
                    topBodyContentSalute1 = new ReportParameter("topBodyContentSalute1", "ជ្រាបថា");
                    conclusionSalute = new ReportParameter("conclusionSalute", "ជម្រាបជូន");
                    conclusionSalute2 = new ReportParameter("conclusionSalute2", "ពិនិត្យ និងសម្រេចតាមការគួរ");
                    conclusionSalute3 = new ReportParameter("conclusionSalute3", "ទទួលនូវគារវកិច្ចពីខ្ញុំ");
                }

                ReportParameter khmerDate = new ReportParameter("khmerDate", KhmerDate.ToString());
                ReportParameter khmerYear = new ReportParameter("khmerYear", KhmerYear.ToString());
                ReportParameter toFullTitle = new ReportParameter("toFullTitle", dt.Rows[0]["CSToFullTitle"].ToString());
                ReportParameter objective = new ReportParameter("objective", dt.Rows[0]["Objective"].ToString());
                ReportParameter via = new ReportParameter("via", dt.Rows[0]["Via"].ToString());
                ReportParameter content = new ReportParameter("content", dt.Rows[0]["Content"].ToString());
                ReportParameter location = new ReportParameter("location", dt.Rows[0]["Location"].ToString());
                ReportParameter reference = new ReportParameter("reference", dt.Rows[0]["Reference"].ToString());
                ReportParameter toShortTitle = new ReportParameter("toShortTitle", dt.Rows[0]["CSToShortTitle"].ToString());
                ReportParameter fromFullTitle = new ReportParameter("fromFullTitle", dt.Rows[0]["CSFromFullTitle"].ToString());
                ReportParameter qrCODE = new ReportParameter("qrCODE", base64String);
                ReportViewer reportViewer = new ReportViewer();
                reportViewer.ProcessingMode = ProcessingMode.Local;
                reportViewer.SizeToReportContent = true;
                reportViewer.Width = Unit.Percentage(100);
                reportViewer.Height = Unit.Percentage(100);
                reportViewer.LocalReport.EnableExternalImages = true;
                if (principleReference == "")
                {
                    if (principleVia == "")
                    {
                        if (fromRanking > 7)
                        {
                            reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Reports\PrincipleNoReferenceT.rdlc";
                        }
                        else
                        {
                            reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Reports\PrincipleNoReference.rdlc";
                        }
                    }
                    else
                    {
                        if (fromRanking > 7)
                        {
                            reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Reports\PrincipleViaNoReferenceT.rdlc";
                        }
                        else
                        {
                            reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Reports\PrincipleViaNoReference.rdlc";
                        }
                    }
                }
                else
                {
                    if (principleVia == "")
                    {
                        if (fromRanking > 7)
                        {
                            reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Reports\PrincipleReferenceT.rdlc";
                        }
                        else
                        {
                            reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Reports\PrincipleReference.rdlc";
                        }
                    }
                    else
                    {
                        if (fromRanking > 7)
                        {
                            reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Reports\PrincipleReferenceAndViaT.rdlc";
                        }
                        else
                        {
                            reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Reports\PrincipleReferenceAndVia.rdlc";
                        }
                    }
                }
                reportViewer.LocalReport.SetParameters(new ReportParameter[] { qrCODE, khmerDate, khmerYear, topHeaderSalute, toFullTitle, objective, content, via, location, reference, topBodyContentSalute, toShortTitle, topBodyContentSalute1, conclusionSalute, conclusionSalute2, conclusionSalute3, fromFullTitle });
                reportViewer.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", dt));
                ViewBag.ReportViewer = reportViewer;
            }
            else
            {
                DataTable dt = new DataTable();
                ReportViewer reportViewer = new ReportViewer();
                reportViewer.ProcessingMode = ProcessingMode.Local;
                reportViewer.SizeToReportContent = true;
                reportViewer.Width = Unit.Percentage(100);
                reportViewer.Height = Unit.Percentage(100);
                reportViewer.LocalReport.EnableExternalImages = true;
                reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Reports\ReportReferenceReport.rdlc";
                reportViewer.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", dt));
                ViewBag.ReportViewer = reportViewer;
            }


            var viewModel = new AdminFormatViewModel()
            {
                Ranks = _context.Ranks.ToList(),
                //CivilServants = _context.CivilServants.ToList(),
                ReportTypes = _context.ReportTypes.ToList()
            };
            return View("Index", viewModel);
        }
    }
}