﻿using AutoMapper;
using sambocappweb.Dtos;
using sambocappweb.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Microsoft.AspNet.Identity;


namespace sambocappweb.Controllers.Api
{
    [Authorize]
    public class BannersController : ApiController
    {
        private ApplicationDbContext _context;
        public BannersController()
        {
            _context = new ApplicationDbContext();

        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        //GET : /api/Banner?departmentid={de..id}  for get all record
        [HttpGet]
        [Route("api/Banners")]
        public IHttpActionResult Getbanner()
        {
            
            var banner = (from b in _context.Banners
                          join u in _context.Users on b.userid equals u.Id
                          join s in _context.Shops on b.shopid equals s.id
                          select new
                          {
                              id = b.id,
                              userid = u.FirstName + " " + u.LastName,
                              date = b.date,
                              shopid = s.shopid,
                              shopname = s.shopName,
                              exireddate = b.exireddate,
                              bannerimage = b.bannerimage,
                              bannerstatus = b.bannerstatus
                          }).ToList();
            return Ok(banner);
           
        }
       

        //GET : /api/Bannners/{id} for get record by id
        [HttpGet]
        [Route("api/Banners/{id}")]
        public IHttpActionResult Getbanner(int id)
        {
            var banerstatus = _context.Banners.SingleOrDefault(c => c.id == id);
            if (banerstatus == null)
                return NotFound();

            return Ok(Mapper.Map<Banner, bannerDto>(banerstatus));
        }

        [HttpGet]
        [Route("api/Banners/Aprove/{id}")]
        public IHttpActionResult Aprove(int id)
        {
            var empInDb = _context.Banners.SingleOrDefault(c => c.id == id);
            var bannerstatus = _context.Banners.SingleOrDefault(c => c.id == id);
            if (bannerstatus == null)
                return BadRequest();
            if (bannerstatus.bannerstatus == "New")
            {
                bannerstatus.bannerstatus = "Disable";
                var photoPart = Path.Combine(HttpContext.Current.Server.MapPath("~/Images"), empInDb.bannerimage);
                if (File.Exists(photoPart))
                {
                    File.Delete(photoPart);
                }
            }
            else if (bannerstatus.bannerstatus == "Disable")
            {
                bannerstatus.bannerstatus = "New";
            }

            _context.SaveChanges();

            return Ok(new { });

        }




        //POS : /api/Banner   for Insert record
        [HttpPost]
        [Route("api/Banners")]
        public IHttpActionResult CreateBanner()
        {

            //var id = HttpContext.Current.Request.Form["Id"];
            //var id = HttpContext.Current.Request.Form["bannerid"];
            var date = HttpContext.Current.Request.Form["bannerdate"];
            var shopid = HttpContext.Current.Request.Form["bannershopid"];
            var exireddate = HttpContext.Current.Request.Form["bannerexireddate"];
            var bannerimage = HttpContext.Current.Request.Files["bannerimage"];
            var bannerstatus = HttpContext.Current.Request.Form["bannerstatus"];
            var qtymonth = HttpContext.Current.Request.Form["qtymonth"];
            //var createby = User.Identity.GetUserName();
            var createdate = DateTime.Today;
            //var BannerInDb = _context.Banners.SingleOrDefault(c => c.bannerstatus ==true);

            //if (BannerInDb != null)
            //    return BadRequest();

            string photoName = "";
            if (bannerimage != null)
            {
                photoName = Path.Combine(Path.GetDirectoryName(bannerimage.FileName)
                    , string.Concat(Path.GetFileNameWithoutExtension(bannerimage.FileName)
                    , DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss")
                    , Path.GetExtension(bannerimage.FileName)
                    ));
                var fileSavePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Images"), photoName);
                bannerimage.SaveAs(fileSavePath);

            }


            var bannerDto = new bannerDto()
            {
                //Id = Int32.Parse(id),
                date = DateTime.Parse(date),
                userid = User.Identity.GetUserId(),
                shopid = Int32.Parse(shopid),
                exireddate = DateTime.Parse(exireddate),
                bannerimage = photoName,
                qtymonth =Int32.Parse(qtymonth),
                bannerstatus = bannerstatus,
            };


            try
            {
                var banner = Mapper.Map<bannerDto, Banner>(bannerDto);
                if (photoName.Length < 1000000)
                {
                _context.Banners.Add(banner);
                _context.SaveChanges();

                bannerDto.id = banner.id;
                }
                else
                {
                    
                }
                

                return Created(new Uri(Request.RequestUri + "/" + bannerDto.id), banner);
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
        }

        //PUT : /api/banner/{id}  for Update record
        [HttpPut]
        [Route("api/Banners/{id}")]
        public IHttpActionResult UpdateBanner(int id)
        {
            var date = HttpContext.Current.Request.Form["bannerdate"];
            var shopid = HttpContext.Current.Request.Form["bannershopid"];
            var exireddate = HttpContext.Current.Request.Form["bannerexireddate"];
            var bannerimage = HttpContext.Current.Request.Files["bannerimage"];
            var qtymonth = HttpContext.Current.Request.Form["qtymonth"];
            
            var banner_old = HttpContext.Current.Request.Form["banner_old"];
            var bannerstatus = HttpContext.Current.Request.Form["bannerstatus"];

            var BannerInDb = _context.Banners.SingleOrDefault(c => c.id == id);

            string photoName = "";
            photoName = banner_old;
            if (bannerimage != null)
            {
                photoName = Path.Combine(Path.GetDirectoryName(bannerimage.FileName)
                        , string.Concat(Path.GetFileNameWithoutExtension(bannerimage.FileName)
                        , DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss")
                        , Path.GetExtension(bannerimage.FileName)
                        ));
                var fileSavePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Images"), photoName);
                bannerimage.SaveAs(fileSavePath);

                //Delete OldPhoto
                var oldPhotoPath = Path.Combine(HttpContext.Current.Server.MapPath("~/Images"), BannerInDb.bannerimage);
                if (File.Exists(oldPhotoPath))
                {
                    File.Delete(oldPhotoPath);
                }

            }
                var customerDto = new bannerDto()
                {
                    id = id,
                    date = DateTime.Parse(date),
                    userid = User.Identity.GetUserId(),
                    shopid = Int32.Parse(shopid),
                    exireddate = DateTime.Parse(exireddate),
                    bannerimage = photoName,
                    qtymonth = Int32.Parse(qtymonth),
                    bannerstatus = bannerstatus,

                };
                Mapper.Map(customerDto, BannerInDb);
                _context.SaveChanges();
                return Ok(new { });

        }
        //DELETE : /api/Employees/{id}  for Delete record
        [HttpDelete]
        [Route("api/Banners/{id}")]
        public IHttpActionResult BannerDelete(int id)
        {
            var empInDb = _context.Banners.SingleOrDefault(c => c.id == id);
            if (empInDb == null)
                return BadRequest();
            _context.Banners.Remove(empInDb);
            //empInDb.bannerstatus = "Disable";
            _context.SaveChanges();

            var photoPart = Path.Combine(HttpContext.Current.Server.MapPath("~/Images"), empInDb.bannerimage);
            if (File.Exists(photoPart))
            {
                File.Delete(photoPart);
            }
            return Ok(new { });

           

        }
        
    }

}
