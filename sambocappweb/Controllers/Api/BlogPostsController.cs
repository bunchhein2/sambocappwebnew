﻿using AutoMapper;
using sambocappweb.Dtos;
using sambocappweb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using System.Data.Entity;

namespace sambocappweb.Controllers.Api
{
    [Authorize]
    public class BlogPostsController : ApiController
    {
        private ApplicationDbContext _context;
        public BlogPostsController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // GET: /api/blogPosts
        [HttpGet]
        public IHttpActionResult GetBlogPosts()
        {
            return Ok(_context.BlogPosts.Include(c => c.BlogCategory).ToList().Select(Mapper.Map<BlogPost, BlogPostDto>));
        }

        // GET: /api/blogPosts/{id}
        [HttpGet]
        public IHttpActionResult GetBlogPost(int id)
        {
            var blogPost = _context.BlogPosts.SingleOrDefault(c => c.Id == id);

            if (blogPost == null)
                return NotFound();

            return Ok(Mapper.Map<BlogPost, BlogPostDto>(blogPost));
        }

        // POST: /api/blogPosts
        [HttpPost]
        public IHttpActionResult CreatePost(BlogPostDto blogPostDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var userId = User.Identity.GetUserId();

            var user = _context.Users.FirstOrDefault(x => x.Id == userId);

            //var isExist = _context.BlogPosts.SingleOrDefault(c => c.Title == blogPostDto.Title);

            //if (isExist != null)
            //    return BadRequest();

            blogPostDto.Author = user.FirstName + " " + user.LastName;
            blogPostDto.PostDate = DateTime.Now;
            blogPostDto.ViewCounter = 0;

            var blogPost = Mapper.Map<BlogPostDto, BlogPost>(blogPostDto);

            _context.BlogPosts.Add(blogPost);

            _context.SaveChanges();

            blogPostDto.Id = blogPost.Id;

            return Created(new Uri(Request.RequestUri + "/" + blogPostDto.Id), blogPostDto);
        }

        // PUT: /api/blogPosts/{id}
        [HttpPut]
        public IHttpActionResult UpdatePost(int id, BlogPostDto blogPostDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            //var isExist = _context.BlogPosts.SingleOrDefault(c => c.Title == blogPostDto.Title && c.Id != blogPostDto.Id);

            //if (isExist != null)
            //    return BadRequest();

            var blogPostInDb = _context.BlogPosts.SingleOrDefault(c => c.Id == id);

            if (blogPostInDb == null)
                return NotFound();

            blogPostDto.Author = blogPostInDb.Author;
            blogPostDto.PostDate = blogPostInDb.PostDate;
            blogPostDto.ViewCounter = blogPostInDb.ViewCounter;

            Mapper.Map(blogPostDto, blogPostInDb);

            _context.SaveChanges();

            return Ok(blogPostDto);
        }

        // DELETE: /api/blogPosts/{id}
        [HttpDelete]
        public IHttpActionResult DeleteDepartment(int id)
        {
            var blogPostInDb = _context.BlogPosts.SingleOrDefault(c => c.Id == id);

            if (blogPostInDb == null)
                return NotFound();

            _context.BlogPosts.Remove(blogPostInDb);

            _context.SaveChanges();

            return Ok(new { });
        }
    }
}
