﻿using AutoMapper;
using sambocappweb.Dtos;
using sambocappweb.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace sambocappweb.Controllers.Api
{
    [Authorize]
    public class CurrenciesController : ApiController
    {
        private ApplicationDbContext _context;
        public CurrenciesController()
        {
            _context = new ApplicationDbContext();

        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        //GET : /api/Currency?Currenciesid={de..id}  for get all record
        [HttpGet]
        public IHttpActionResult GetCurrency()
        {
            //return Ok(_context.Currencies.ToList().Select(Mapper.Map<Currency, CurrencyDto>));
            return Ok(_context.Currencies.ToList().Select(Mapper.Map<Currency, CurrencyDto>).Where(c => c.status == "Active" || c.status == "Inactive"));
        }


        //GET : /api/Currency/{id} for get record by id
        [HttpGet]
        public IHttpActionResult GetCurrency(int id)
        {
            var status = _context.Currencies.SingleOrDefault(c => c.id == id);
            if (status == null)
                return NotFound();

            return Ok(Mapper.Map<Currency, CurrencyDto>(status));
        }

        //POS : /api/Currency   for Insert record
        [HttpPost]
        public IHttpActionResult CreateCurrency()
        {

            var id = HttpContext.Current.Request.Form["Id"];
            var currencyname = HttpContext.Current.Request.Form["currencyname"];
            var sign = HttpContext.Current.Request.Form["currencysign"];
            var status = HttpContext.Current.Request.Form["currencystatus"];
        
            var CurrencyDto = new CurrencyDto()
            {
                //Id = Int32.Parse(id),
                currencyname = currencyname,
                sign = sign,
                status = status,
            };


            try
            {
                var Currency = Mapper.Map<CurrencyDto, Currency>(CurrencyDto);
                _context.Currencies.Add(Currency);
                _context.SaveChanges();

                CurrencyDto.id = Currency.id;

                return Created(new Uri(Request.RequestUri + "/" + CurrencyDto.id), Currency);
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
        }

        //PUT : /api/Currency/{id}  for Update record
        [HttpPut]
        public IHttpActionResult UpdateCurrency(int id)
        {
            var currencyname = HttpContext.Current.Request.Form["currencyname"];
            var sign = HttpContext.Current.Request.Form["currencysign"];
            var status = HttpContext.Current.Request.Form["currencystatus"];


            var Currency = _context.Currencies.SingleOrDefault(c => c.id == id);

            var CurrencyDto = new CurrencyDto()
            {
                id = id,
                //Showid = Int32.Parse(showid),
                currencyname = currencyname,
                sign = sign,
                status = status,

            };
            Mapper.Map(CurrencyDto, Currency);
            _context.SaveChanges();

            return Ok(new { });

        }



        //DELETE : /api/Currency/{id}  for Delete record
        [HttpDelete]
        public IHttpActionResult DeleteCurrency(int id)
        {
            var CuInDb = _context.Currencies.SingleOrDefault(c => c.id == id);
            if (CuInDb == null)
                return BadRequest();

            CuInDb.status = "Inactive";
            _context.SaveChanges();
            return Ok(new { });
        }
    }
}