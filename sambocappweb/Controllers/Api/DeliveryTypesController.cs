﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using AutoMapper;
using sambocappweb.Dtos;
using sambocappweb.Models;

namespace sambocappweb.Controllers.Api
{
    public class DeliveryTypesController : ApiController
    {
        private ApplicationDbContext _context;
        public DeliveryTypesController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // GET: /api/DeliveryTypes
        [HttpGet]
        public IHttpActionResult GetDeliveryTypes()
        {
            return Ok(_context.DeliveryTypes.ToList().Select(Mapper.Map<DeliveryType, DeliveryTypeDto>));
        }

        // GET: /api/DeliveryTypes/{id}
        [HttpGet]
        public IHttpActionResult GetFeedback(int id)
        {
            var feedback = _context.DeliveryTypes.SingleOrDefault(c => c.id == id);

            if (feedback == null)
                return NotFound();

            return Ok(Mapper.Map<DeliveryType, DeliveryTypeDto>(feedback));
        }

        // POST: /api/feedbacks
        [HttpPost]
        public IHttpActionResult CreateFeedback()
        {
            var id = HttpContext.Current.Request.Form["id"];
            var deliveryName = HttpContext.Current.Request.Form["delivery_name"];
            var status = HttpContext.Current.Request.Form["status"];


            if (!ModelState.IsValid)
                return BadRequest();

            var deliveryDto = new DeliveryTypeDto()
            {
                delivery_name = deliveryName,
                status = status
            };

            var feedback = Mapper.Map<DeliveryTypeDto, DeliveryType>(deliveryDto);

            _context.DeliveryTypes.Add(feedback);

            _context.SaveChanges();

            deliveryDto.id = feedback.id;

            return Created(new Uri(Request.RequestUri + "/" + deliveryDto.id), deliveryDto);
        }

        // PUT: /api/DeliveryTypes/{id}
        [HttpPut]
        
        public IHttpActionResult UpdateDelvery()
        {
            var id = HttpContext.Current.Request.Form["id"];
            var deliveryName = HttpContext.Current.Request.Form["delivery_name"];
            var status = HttpContext.Current.Request.Form["status"];

            int deliveryID = 0;
            if (id != null)
            {
                deliveryID = int.Parse(id);
            }

            var isExist = _context.DeliveryTypes.SingleOrDefault(c => c.id == deliveryID);
          

            var deliveryDto = new DeliveryTypeDto()
            {
                id = Int32.Parse(id),
                delivery_name = deliveryName,
                status = status
            };

            Mapper.Map(deliveryDto, isExist);
            _context.SaveChanges();
            return Ok(new { });
        }

        // DELETE: /api/feedbacks/{id}
        [HttpDelete]
        public IHttpActionResult DeleteFeedback(int id)
        {
            var feedbackInDb = _context.DeliveryTypes.SingleOrDefault(c => c.id == id);

            if (feedbackInDb == null)
                return NotFound();

            _context.DeliveryTypes.Remove(feedbackInDb);

            _context.SaveChanges();

            return Ok(new { });
        }
    }
}