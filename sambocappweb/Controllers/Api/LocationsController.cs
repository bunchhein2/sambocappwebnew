﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using sambocappweb.Dtos;
using sambocappweb.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace sambocappweb.Controllers.Api
{
    [Authorize]
    public class LocationsController : ApiController
    {
        private ApplicationDbContext _context;
        public LocationsController()
        {
            _context = new ApplicationDbContext();

        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        //GET : /api/Locations
        [HttpGet]
        public IHttpActionResult Getbank()
        {
            return Ok(_context.Locations.ToList().Select(Mapper.Map<Location, LocationDto>).Where(c => c.active == "true"));
        }


        //GET : /api/Locations/{id} for get record by id
        [HttpGet]
        public IHttpActionResult Getlocation(int id)
        {
            var Edit = _context.Locations.SingleOrDefault(c => c.id == id);

            if (Edit == null)
                return NotFound();

            return Ok(Mapper.Map<Location, LocationDto>(Edit));

        }

        //POS : /api/Locations   for Insert record
        [HttpPost]
        public IHttpActionResult Createlocation()
        {

            var id = HttpContext.Current.Request.Form["id"];
            var location = HttpContext.Current.Request.Form["location"];
            var active = HttpContext.Current.Request.Form["active"];
            
            var location1 = new LocationDto()
            {
                //Id = Int32.Parse(id),
                location = location,
                active = "true",
            };


            try
            {
                var location2 = Mapper.Map<LocationDto, Location>(location1);
                _context.Locations.Add(location2);
                _context.SaveChanges();

                location1.id = location1.id;

                return Created(new Uri(Request.RequestUri + "/" + location1.id), location2);
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
        }

        //PUT : /api/Locations/{id}  for Update record
        [HttpPut]
        public IHttpActionResult UpdateCurrency(int id)
        {
            //var id = HttpContext.Current.Request.Form["id"];
            var location = HttpContext.Current.Request.Form["location"];
            var active = HttpContext.Current.Request.Form["active"];
            var locationInDB = _context.Locations.SingleOrDefault(c => c.id == id);
            var location3 = new LocationDto()
            {
                id = id,
                location = location,
                active = active,
            };

            Mapper.Map(location3, locationInDB);
            _context.SaveChanges();

            return Ok(new { });

        }



        //DELETE : /api/Locations/{id}  for Delete record
        [HttpDelete]
        public IHttpActionResult DeleteCurrency(int id)
        {
            var InDb = _context.Locations.SingleOrDefault(c => c.id == id);
            if (InDb == null)
                return BadRequest();

            InDb.active = "false";
            _context.SaveChanges();

            return Ok(new { });
        }
    }
}
