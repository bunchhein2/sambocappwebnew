﻿using AutoMapper;

using sambocappweb.Dtos;
using sambocappweb.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Validation;
using System.Data.Entity;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using sambocappweb.Dtos.BO;

namespace sambocappweb.Controllers.Api
{
    [Authorize]
    public class OrderdetailsController : ApiController
    {
        private ApplicationDbContext _context;
        public OrderdetailsController()
        {
            _context = new ApplicationDbContext();

        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        //GET : /api/Orderdetails/id={di} for get all record
        [HttpGet]
        [Route("api/Orderdetails/{id}")]
        public IHttpActionResult GetFilter(string id)
        {
            if (id == "id")
            {
                var request = _context.Orderdetails.Select(Mapper.Map<Orderdetail, OrderdetailDto>);
                return Ok(request);
            }
            else
            {
                var request = _context.Orderdetails.Select(Mapper.Map<Orderdetail, OrderdetailDto>)
                                            .Where(c => c.id ==int.Parse(id));
                return Ok(request);
            }

        }
        //GET : /api/Orderdetails?id={de..id}  for get all record
        [HttpGet]
        [Route("api/Orderdetails/{orderid}/detail")]
        public IHttpActionResult GetOrderdetail(int orderid)
        {
            var orderdetail = (from b in _context.Orderdetails
                               join u in _context.Orders on b.orderid equals u.Id
                               join s in _context.Products on b.productid equals s.Id
                               //join c in _context.Customers on u.CustomerId equals c.id
                               //join sho in _context.Shops on s.ShopId equals sho.id
                               where b.orderid==orderid
                               select new OrderDetailRes()
                               {
                                   id = b.id,
                                   orderid = b.orderid,
                                   date = u.Date,
                                   productid = b.productid,
                                   productname = s.ProductName,
                                   //shopName = sho.shopName,
                                   qty = b.qty,
                                   price = b.price,
                                   discount = b.discount,
                               }).ToList();


            return Ok(orderdetail);
            //return Ok(_context.Orderdetails.ToList().Select(Mapper.Map<Orderdetail, OrderdetailDto>));

        }

        //GET : /api/Orderdetails/{id} for get record by id
        [HttpGet]
        //public IHttpActionResult GetOrderdetail(int id)
        //{
        //    var Orderdetailid = _context.Orderdetails.SingleOrDefault(c => c.id == id);
        //    if (Orderdetailid == null)
        //        return NotFound();

        //    return Ok(Mapper.Map<Orderdetail, OrderdetailDto>(Orderdetailid));
        //}




        //POS : /api/Orderdetails  for Insert record
        [HttpPost]
        public IHttpActionResult CreateOrderdetail()
        {

            //var id = HttpContext.Current.Request.Form["id"];
            var orderid = HttpContext.Current.Request.Form["orderid"];
            var productid = HttpContext.Current.Request.Form["orderproductid"];
            var qty = HttpContext.Current.Request.Form["orderqty"];
            var price = HttpContext.Current.Request.Form["orderprice"];
            var discount = HttpContext.Current.Request.Form["orderdiscount"];

            var OrderdetailDto = new OrderdetailDto()
            {
                //id = Int32.Parse(id),
                orderid = int.Parse(orderid),
                //orderid = User.Identity.GetUserId(),
                productid = int.Parse(productid),
                qty = int.Parse(qty),
                price = decimal.Parse(price),
                discount = decimal.Parse(discount),
            };


            try
            {
                var Orderdetail = Mapper.Map<OrderdetailDto, Orderdetail>(OrderdetailDto);
                _context.Orderdetails.Add(Orderdetail);
                _context.SaveChanges();

                OrderdetailDto.id = Orderdetail.id;

                return Created(new Uri(Request.RequestUri + "/" + OrderdetailDto.id), Orderdetail);
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
        }


        //PUT : /api/Orderdetails/{id}  for Update record
        [HttpPut]
        public IHttpActionResult UpdateOrderdetail(int id)
                    {
            //var id = HttpContext.Current.Request.Form["id"];
            var orderid = HttpContext.Current.Request.Form["orderid"];
            var productid = HttpContext.Current.Request.Form["orderproductid"];
            var qty = HttpContext.Current.Request.Form["orderqty"];
            var price = HttpContext.Current.Request.Form["orderprice"];
            var discount = HttpContext.Current.Request.Form["orderdiscount"];

            var OrderdetailInDb = _context.Orderdetails.SingleOrDefault(c => c.id == id);
            var OrderdetailDto = new OrderdetailDto()
            {
                id = id,
                orderid = Int32.Parse(orderid),
                productid = Int32.Parse(productid),
                qty = Int32.Parse(qty),
                price = decimal.Parse(price),
                discount = decimal.Parse(discount),

            };
            Mapper.Map(OrderdetailDto, OrderdetailInDb);
            _context.SaveChanges();

            return Ok(new { });

        }

        //DELETE : /api/Orderdetails/{id}  for Delete record
        [HttpDelete]
        public IHttpActionResult DeleteExchange(int id)
        {
            var OrderdetailInDb = _context.Orderdetails.SingleOrDefault(c => c.id == id);
            if (OrderdetailInDb == null)
                return NotFound();

            _context.Orderdetails.Remove(OrderdetailInDb);
            _context.SaveChanges();

            return Ok(new { });
        }
    }
}
