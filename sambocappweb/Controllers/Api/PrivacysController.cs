﻿using AutoMapper;
using sambocappweb.Dtos;
using sambocappweb.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace sambocappweb.Controllers.Api
{
    [Authorize]
    public class PrivacysController : ApiController
    {
        private ApplicationDbContext _context;
        public PrivacysController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // GET: /api/departments
        [HttpGet]
        public IHttpActionResult GetPrivacy()
        {
            return Ok(_context.Privacies.ToList().Select(Mapper.Map<Privacy, PrivacyDto>));
        }


        // GET: /api/departments/{id}
        [HttpGet]
        public IHttpActionResult GetDepartment(int id)
        {
            var department = _context.Privacies.SingleOrDefault(c => c.id == id);

            if (department == null)
                return NotFound();

            return Ok(Mapper.Map<Privacy, PrivacyDto>(department));
        }

        // POST: /api/departments
        [HttpPost]
        public IHttpActionResult CreateCase(PrivacyDto departmentDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var isExist = _context.Privacies.SingleOrDefault(c => c.description == departmentDto.description);
            if (isExist != null)
                return BadRequest();

            var department = Mapper.Map<PrivacyDto, Privacy>(departmentDto);
            _context.Privacies.Add(department);
            _context.SaveChanges();
            departmentDto.id = department.id;
            return Created(new Uri(Request.RequestUri + "/" + departmentDto.id), departmentDto);
        }

        // PUT: /api/departments/{id}
        [HttpPut]
        public IHttpActionResult UpdateCase(int id, PrivacyDto departmentDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var isExist = _context.Privacies.SingleOrDefault(c => c.description == departmentDto.description && c.id != departmentDto.id);
            if (isExist != null)
                return BadRequest();

            var departmentInDb = _context.Privacies.SingleOrDefault(c => c.id == id);
            if (departmentInDb == null)
                return NotFound();

            Mapper.Map(departmentDto, departmentInDb);
            _context.SaveChanges();

            return Ok(departmentDto);
        }

        // DELETE: /api/departments/{id}
        [HttpDelete]
        public IHttpActionResult DeleteCase(int id)
        {

            var departmentInDb = _context.Privacies.SingleOrDefault(c => c.id == id);
            if (departmentInDb == null)
                return NotFound();

            _context.Privacies.Remove(departmentInDb);
            _context.SaveChanges();

            return Ok(new { });
        }
    }
}