﻿using AutoMapper;
using sambocappweb.Dtos;
using sambocappweb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;
using Microsoft.AspNet.Identity;
using System.Data.Entity;
using System.IO;

namespace sambocappweb.Controllers.Api
{
    [Authorize]
    public class ProductsController : ApiController
    {
        private ApplicationDbContext _context;

        public ProductsController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // GET: /api/products
        [HttpGet]
        public IHttpActionResult GetProduct()
        {

            var product = (from p in _context.Products
                            join s in _context.Shops on p.ShopId equals s.id
                           //join pm in _context.ProductImages on p.Id equals pm.productid
                           join cu in _context.Currencies on p.CurrencyId equals cu.id

                            select new
                            {
                                id = p.Id,
                                Sid = p.ShopId,
                                shopName = s.shopName,
                                Productcode = p.ProductCode,
                                productname = p.ProductName,
                                Description = p.Description,
                                QtyInStock = p.QtyInStock,
                                Price = p.Price,
                                CurrencyId = cu.currencyname,
                                CutStockType = p.CutStockType,
                                ExpiredDate = p.ExpiredDate,
                                LinkVideo = p.LinkVideo,
                                ImageThumbnail = p.ImageThumbnail,
                                Status = p.Status,
                            }).ToList();

            return Ok(product);
            //var product1 = _context.Products.Include(c => c.Shop).Select(Mapper.Map<Product, ProductDto>);
            //return Ok(product1);
           
        }
        // GET: /api/products/{id}
        [HttpGet]
        public IHttpActionResult GetProduct(int id)
        {
           
            var prouducts = _context.Products.SingleOrDefault(c => c.Id == id);

            if (prouducts == null)
                return NotFound();

            return Ok(Mapper.Map<Product, ProductDto>(prouducts));
            
        }
        
        // POST: /api/products
        [HttpPost]
        public IHttpActionResult CreateProducts()
        {

            var shopid = HttpContext.Current.Request.Form["ShopId"];
            var productcode = HttpContext.Current.Request.Form["ProductCode"];
            var productname = HttpContext.Current.Request.Form["ProductName"];
            var description = HttpContext.Current.Request.Form["Description"];
            var qtyinstock = HttpContext.Current.Request.Form["QtyInStock"];
            var price = HttpContext.Current.Request.Form["Price"];
            var currencyid = HttpContext.Current.Request.Form["CurrencyId"];
            var productPhoto = HttpContext.Current.Request.Files["ImageThumbnail"];
            var cutstocktype = HttpContext.Current.Request.Form["CutStockType"];
            var expireddate = HttpContext.Current.Request.Form["ExpiredDate"];
            //var linkvideo = HttpContext.Current.Request.Form["LinkVideo"];
            var linkvideo = HttpContext.Current.Request.Files["LinkVideo"];
            var status = HttpContext.Current.Request.Form["Status"];

            //Check if Simeler
            //var procutInDB = _context.Products.FirstOrDefault(c => c.ProductCode == productcode);
            //if (procutInDB != null)
            //    return BadRequest();
            string VideoName = "";

            if (linkvideo != null)
            {
                VideoName = Path.Combine(Path.GetDirectoryName(linkvideo.FileName)
                    , string.Concat(Path.GetFileNameWithoutExtension(linkvideo.FileName)
                    , DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss")
                    , Path.GetExtension(linkvideo.FileName)
                    ));
                var savePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Video"), VideoName);
                linkvideo.SaveAs(savePath);
            }
            string photoName = "";

            if (productPhoto != null)
            {
                photoName = Path.Combine(Path.GetDirectoryName(productPhoto.FileName)
                    , string.Concat(Path.GetFileNameWithoutExtension(productPhoto.FileName)
                    , DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss")
                    , Path.GetExtension(productPhoto.FileName)
                    ));
                var savePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Images"), photoName);
                productPhoto.SaveAs(savePath);
            }

            var productDto = new ProductDto()
            {
                ShopId = Int32.Parse(shopid),
                ExpiredDate = DateTime.Parse(expireddate),
                ProductCode = productcode,
                ProductName = productname,
                Description = description,
                QtyInStock = Int32.Parse( qtyinstock),
                Price = decimal.Parse( price),
                CurrencyId = Int32.Parse(currencyid),
                ImageThumbnail = photoName,
                CutStockType = cutstocktype,
                LinkVideo = VideoName,
                Status = status
                
            };
            var product = Mapper.Map<ProductDto, Product>(productDto);
            _context.Products.Add(product);
            _context.SaveChanges();

            return Created(new Uri(Request.RequestUri + "/" + productDto.Id), productDto);

        }

        // PUT: /api/products/{id}
        [HttpPut]
        public IHttpActionResult UpdateProduct(int id)
        {

            //var Id = HttpContext.Current.Request.Form["Id"];
            var shopid = HttpContext.Current.Request.Form["ShopId"];
            var productcode = HttpContext.Current.Request.Form["ProductCode"];
            var productname = HttpContext.Current.Request.Form["ProductName"];
            var description = HttpContext.Current.Request.Form["Description"];
            var qtyinstock = HttpContext.Current.Request.Form["QtyInStock"];
            var price = HttpContext.Current.Request.Form["Price"];
            var currencyid = HttpContext.Current.Request.Form["CurrencyId"];
            var cutstocktype = HttpContext.Current.Request.Form["CutStockType"];
            var expireddate = HttpContext.Current.Request.Form["ExpiredDate"];
            var status = HttpContext.Current.Request.Form["Status"];

            var productPhoto = HttpContext.Current.Request.Files["ImageThumbnail"];
            var linkvideo = HttpContext.Current.Request.Files["LinkVideo"];
            var file_old = HttpContext.Current.Request.Form["file_old"];
            var video_old = HttpContext.Current.Request.Form["video_old"];


            var procutInDB = _context.Products.FirstOrDefault(c => c.Id == id);
            if (procutInDB == null)
                return BadRequest();

            string VideoName_New = "", photoName_new="";
            photoName_new = file_old;
            VideoName_New = video_old;

            if (linkvideo != null)
            {
                VideoName_New = Path.Combine(Path.GetDirectoryName(linkvideo.FileName)
                    , string.Concat(Path.GetFileNameWithoutExtension(linkvideo.FileName)
                    , DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss")
                    , Path.GetExtension(linkvideo.FileName)
                    ));
                var savePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Video"), VideoName_New);
                linkvideo.SaveAs(savePath);
                //Delete Old Image
                var oldFilePathV = Path.Combine(HttpContext.Current.Server.MapPath("~/Video"), procutInDB.LinkVideo);

                if (File.Exists(oldFilePathV))
                {
                    File.Delete(oldFilePathV);
                }

            }

            if (productPhoto != null)
            {
                photoName_new = Path.Combine(Path.GetDirectoryName(productPhoto.FileName)
                    , string.Concat(Path.GetFileNameWithoutExtension(productPhoto.FileName)
                    , DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss")
                    , Path.GetExtension(productPhoto.FileName)
                    ));

                var savePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Images"), photoName_new);
                productPhoto.SaveAs(savePath);

                //Delete Old Image
                var oldFilePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Images"), procutInDB.ImageThumbnail);

                if (File.Exists(oldFilePath))
                {
                    File.Delete(oldFilePath);
                }

            }
            var user = _context.Products.SingleOrDefault(c => c.Id == id);
            var productDto = new ProductDto()
            {
                    //Id = id,
                    ShopId = Int32.Parse(shopid),
                    ExpiredDate = DateTime.Parse(expireddate),
                    ProductCode = productcode,
                    ProductName = productname,
                    Description = description,
                    QtyInStock = Int32.Parse(qtyinstock),
                    Price = decimal.Parse(price),
                    CurrencyId = Int32.Parse(currencyid),
                    ImageThumbnail = photoName_new,
                    CutStockType = cutstocktype,
                    LinkVideo = VideoName_New,
                    Status = status

            };

                Mapper.Map(productDto, procutInDB);

                _context.SaveChanges();
            

                return Ok(new { });
        }

        // DELETE: /api/products/{id}
        [HttpDelete]
        public IHttpActionResult DeleteProduct(int id)
        {
            var productInDb = _context.Products.SingleOrDefault(c => c.Id == id);

            if (productInDb == null)
                return NotFound();

            _context.Products.Remove(productInDb);

            _context.SaveChanges();

            return Ok(new { });
        }

    }
}
