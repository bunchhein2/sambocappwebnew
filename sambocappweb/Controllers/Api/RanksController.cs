﻿using AutoMapper;
using sambocappweb.Dtos;
using sambocappweb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace sambocappweb.Controllers.Api
{
    [Authorize]
    public class RanksController : ApiController
    {
        private ApplicationDbContext _context;
        public RanksController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // GET: /api/ranks
        [HttpGet]
        public IHttpActionResult GetRanks()
        {
            return Ok(_context.Ranks.ToList().Select(Mapper.Map<Rank, RankDto>));
        }

        // GET: /api/ranks/{id}
        [HttpGet]
        public IHttpActionResult GetRank(int id)
        {
            var rank = _context.Ranks.SingleOrDefault(c => c.Id == id);

            if (rank == null)
                return NotFound();

            return Ok(Mapper.Map<Rank, RankDto>(rank));
        }

        // POST: /api/ranks
        [HttpPost]
        public IHttpActionResult CreateRank(RankDto rankDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var isExist = _context.Ranks.SingleOrDefault(c => c.Name == rankDto.Name);

            if (isExist != null)
                return BadRequest();

            var rank = Mapper.Map<RankDto, Rank>(rankDto);

            _context.Ranks.Add(rank);

            _context.SaveChanges();

            rankDto.Id = rank.Id;

            return Created(new Uri(Request.RequestUri + "/" + rankDto.Id), rankDto);
        }

        // PUT: /api/ranks/{id}
        [HttpPut]
        public IHttpActionResult UpdateRank(int id, RankDto rankDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var isExist = _context.Ranks.SingleOrDefault(c => c.Name == rankDto.Name && c.Id != rankDto.Id);

            if (isExist != null)
                return BadRequest();

            var rankInDb = _context.Ranks.SingleOrDefault(c => c.Id == id);

            if (rankInDb == null)
                return NotFound();

            Mapper.Map(rankDto, rankInDb);

            _context.SaveChanges();

            return Ok(rankDto);
        }

        // DELETE: /api/ranks/{id}
        [HttpDelete]
        public IHttpActionResult DeleteRank(int id)
        {
            var rankInDb = _context.Ranks.SingleOrDefault(c => c.Id == id);

            if (rankInDb == null)
                return NotFound();

            _context.Ranks.Remove(rankInDb);

            _context.SaveChanges();

            return Ok(new { });
        }
    }
}
