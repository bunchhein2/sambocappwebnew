﻿using AutoMapper;
using sambocappweb.Dtos;
using sambocappweb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace sambocappweb.Controllers.Api
{
    [Authorize]
    public class ReportTypesController : ApiController
    {
        private ApplicationDbContext _context;
        public ReportTypesController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // GET: /api/reporttypes
        [HttpGet]
        public IHttpActionResult GetReportTypes()
        {
            return Ok(_context.ReportTypes.ToList().Select(Mapper.Map<ReportType, ReportTypeDto>));
        }

        // GET: /api/reporttypes/{id}
        [HttpGet]
        public IHttpActionResult GetReportType(int id)
        {
            var reportType = _context.ReportTypes.SingleOrDefault(c => c.Id == id);

            if (reportType == null)
                return NotFound();

            return Ok(Mapper.Map<ReportType, ReportTypeDto>(reportType));
        }

        // POST: /api/reporttypes
        [HttpPost]
        public IHttpActionResult CreateReportType(ReportTypeDto reportTypeDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var isExist = _context.ReportTypes.SingleOrDefault(c => c.Name == reportTypeDto.Name);

            if (isExist != null)
                return BadRequest();

            var reportType = Mapper.Map<ReportTypeDto, ReportType>(reportTypeDto);

            _context.ReportTypes.Add(reportType);

            _context.SaveChanges();

            reportTypeDto.Id = reportType.Id;

            return Created(new Uri(Request.RequestUri + "/" + reportTypeDto.Id), reportTypeDto);
        }

        // PUT: /api/reporttypes/{id}
        [HttpPut]
        public IHttpActionResult UpdateReportType(int id, ReportTypeDto reportTypeDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var isExist = _context.ReportTypes.SingleOrDefault(c => c.Name == reportTypeDto.Name && c.Id != reportTypeDto.Id);

            if (isExist != null)
                return BadRequest();

            var reportTypeInDb = _context.ReportTypes.SingleOrDefault(c => c.Id == id);

            if (reportTypeInDb == null)
                return NotFound();

            Mapper.Map(reportTypeDto, reportTypeInDb);

            _context.SaveChanges();

            return Ok(reportTypeDto);
        }

        // DELETE: /api/reporttypes/{id}
        [HttpDelete]
        public IHttpActionResult DeleteReportType(int id)
        {
            var reportTypeInDb = _context.ReportTypes.SingleOrDefault(c => c.Id == id);

            if (reportTypeInDb == null)
                return NotFound();

            _context.ReportTypes.Remove(reportTypeInDb);

            _context.SaveChanges();

            return Ok(new { });
        }
    }
}
