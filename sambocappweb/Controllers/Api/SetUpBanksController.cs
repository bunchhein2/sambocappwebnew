﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using sambocappweb.Dtos;
using sambocappweb.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
namespace sambocappweb.Controllers.Api
{
    [Authorize]
    public class SetUpBanksController : ApiController
    {
        private ApplicationDbContext _context;
        public SetUpBanksController()
        {
            _context = new ApplicationDbContext();

        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        //GET : /api/SetUpBanks
        [HttpGet]
        public IHttpActionResult Getbank()
        {
            //return Ok(_context.P.ToList().Select(Mapper.Map<Currency, CurrencysDto>));.Where(c => c.status == true)
            return Ok(_context.PaymentMethods.ToList().Select(Mapper.Map<PaymentMethod, PaymentMethodDto>).Where(c => c.status == true));
        }


        //GET : /api/SetUpBanks/{id} for get record by id
        [HttpGet]
        public IHttpActionResult GetCurrency(int id)
        {
            var Edit = _context.PaymentMethods.SingleOrDefault(c => c.id == id);

            if (Edit == null)
                return NotFound();

            return Ok(Mapper.Map<PaymentMethod, PaymentMethodDto>(Edit));
            
        }

        //POS : /api/CurSetUpBanksrency   for Insert record
        [HttpPost]
        public IHttpActionResult CreateBank()
        {

            //var id = HttpContext.Current.Request.Form["Id"];
            var methodname = HttpContext.Current.Request.Form["methodname"];
            var status = HttpContext.Current.Request.Form["currencystatus"];
            var createdate = DateTime.Today;

            var PaymentMethod = new PaymentMethodDto()
            {
                //Id = Int32.Parse(id),
                methodname = methodname,
                createby = User.Identity.GetUserId(),
                createdate = DateTime.Today,
                status = true,
            };


            try
            {
                var paymentmethod = Mapper.Map<PaymentMethodDto, PaymentMethod>(PaymentMethod);
                _context.PaymentMethods.Add(paymentmethod);
                _context.SaveChanges();

                PaymentMethod.id = PaymentMethod.id;

                return Created(new Uri(Request.RequestUri + "/" + PaymentMethod.id), paymentmethod);
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
        }

        //PUT : /api/SetUpBanks/{id}  for Update record
        [HttpPut]
        [Route("api/SetUpBanks/{id}")]
        public IHttpActionResult UpdateCurrency(int id)
        {
            //var id = HttpContext.Current.Request.Form["Id"];
            var methodname = HttpContext.Current.Request.Form["methodname"];
            var status = HttpContext.Current.Request.Form["currencystatus"];
            var createdate = DateTime.Today;
            var PaymentMethodInDB = _context.PaymentMethods.SingleOrDefault(c => c.id == id);
            var PaymentMethod = new PaymentMethodDto()
            {
                id = id,
                methodname = methodname,
                createby = User.Identity.GetUserId(),
                createdate = DateTime.Today,
                status = true,
            };
            Mapper.Map(PaymentMethod, PaymentMethodInDB);
            _context.SaveChanges();

            return Ok(new { });

        }



        //DELETE : /api/SetUpBanks/{id}  for Delete record
        [HttpDelete]
        public IHttpActionResult DeleteCurrency(int id)
        {
            var InDb = _context.PaymentMethods.SingleOrDefault(c => c.id == id);
            if (InDb == null)
                return BadRequest();

            InDb.status =false;
            _context.SaveChanges();

            return Ok(new { });
        }
    }
}
