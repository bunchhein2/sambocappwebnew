﻿using AutoMapper;
using sambocappweb.Dtos;
using sambocappweb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.IO;
using System.Data.Entity;

namespace sambocappweb.Controllers.Api
{
    public class ShopPaymentsController : ApiController
    {
        private ApplicationDbContext _context;

        public ShopPaymentsController()
        {
            _context = new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        [HttpGet]
        [Route("api/ShopPayment")]
        //Get : api/ShopPayment
        public IHttpActionResult GetShopPayment()
        {
            var shopPayment = _context.ShopPayments.Include(c => c.shop).ToList().Select(Mapper.Map<ShopPayment, ShopPaymentDto>);

            //var shopPayment = (from sp in _context.ShopPayments
            //                join s in _context.Shops on sp.shopid equals s.id
            //                select new ShopPaymentV
            //                {
            //                    id = sp.id,
            //                    date=sp.date,
            //                    shopid=sp.shopid,
            //                    shop=s.shopName,
            //                    paytype=sp.paytype,
            //                    startdate=sp.startdate,
            //                    enddate=sp.enddate,
            //                    amount=sp.amount,
            //                    note=sp.note
            //                }).ToList();
            return Ok(shopPayment);
        }
        [HttpGet]
        [Route("api/ShopPayment/{id}")]
        //Get : api/ShopPayment
        public IHttpActionResult GetShopPaymentById(int id)
        {
            var shopPayment = _context.ShopPayments.Include(c => c.shop).SingleOrDefault(c => c.id == id);
            if (shopPayment == null)
                return NotFound();
            return Ok(shopPayment);
        }
        // POST: /api/departments
        [HttpPost]
        [Route("api/ShopPayment")]
        public IHttpActionResult CreateShopPayment()
        {
            var date = HttpContext.Current.Request.Form["date"];
            var shopId = HttpContext.Current.Request.Form["shopid"];
            var payType = HttpContext.Current.Request.Form["paytype"];
            var startDate = HttpContext.Current.Request.Form["startdate"];
            var endDate = HttpContext.Current.Request.Form["enddate"];
            var Amount = HttpContext.Current.Request.Form["amount"];
            var Note = HttpContext.Current.Request.Form["note"];

            var shoppaymentdto = new ShopPaymentDto()
            {
                //id = Int32.Parse(id),
                date = DateTime.Parse(date),
                shopid = Int32.Parse(shopId),
                paytype = payType,
                startdate = DateTime.Parse(startDate),
                enddate = DateTime.Parse(endDate),
                amount = decimal.Parse(Amount),
                note = Note,
            };

            var shopPayment = Mapper.Map<ShopPaymentDto, ShopPayment>(shoppaymentdto);
            //shopPayment.date = DateTime.Today;
            _context.ShopPayments.Add(shopPayment);
            _context.SaveChanges();

            shoppaymentdto.id = shopPayment.id;

            return Created(new Uri(Request.RequestUri + "/" + shoppaymentdto.id), shopPayment);


        }
        // PUT: /api/departments/{id}
        [HttpPut]
        [Route("api/ShopPayment/{id}")]
        public IHttpActionResult UpdateShopPayment(int id)
        {

            //var id = HttpContext.Current.Request.Form["shopPaymentid"];
            var date = HttpContext.Current.Request.Form["date"];
            var shopid = HttpContext.Current.Request.Form["shopid"];
            var paytype = HttpContext.Current.Request.Form["payType"];
            var startdate = HttpContext.Current.Request.Form["startDate"];
            var enddate = HttpContext.Current.Request.Form["endDate"];
            var amount = HttpContext.Current.Request.Form["amount"];
            var note = HttpContext.Current.Request.Form["note"];

            var shoppaymentdto = new ShopPaymentDto()
            {
                //id = id,
                date = DateTime.Parse(date),
                shopid = Int32.Parse(shopid),
                paytype = paytype,
                startdate = DateTime.Parse(startdate),
                enddate = DateTime.Parse(enddate),
                amount = decimal.Parse(amount),
                note = note,
            };
            var shopPayment = _context.ShopPayments.SingleOrDefault(c => c.id == id);
            //shopPayment.date = DateTime.Today;
            Mapper.Map(shoppaymentdto, shopPayment);
            _context.SaveChanges();

            shoppaymentdto.id = shopPayment.id;

            return Ok(new { });
        }
        // DELETE: /api/departments/{id}
        [HttpDelete]
        [Route("api/ShopPayment/{id}")]
        public IHttpActionResult DeleteShopPayment(int id)
        {

            var shopPayment = _context.ShopPayments.SingleOrDefault(c => c.id == id);
            if (shopPayment == null)
                return NotFound();

            _context.ShopPayments.Remove(shopPayment);
            _context.SaveChanges();

            return Ok(new { });
        }
    }
}
