﻿using AutoMapper;
using sambocappweb.Dtos;
using sambocappweb.Models;
using sambocappweb.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Microsoft.AspNet.Identity;

namespace sambocappweb.Controllers.Api
{
    public class ShopsController : ApiController
    {
        private ApplicationDbContext _context;

        public ShopsController()
        {
            _context = new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }


        [HttpGet]
        //Get : api/Shops
        public IHttpActionResult GetCustomer()
        {
            //var getCompanyType = _context.Shops.Select(Mapper.Map<Shop, ShopDto>);
            ////var gettype = _context.Shops.ToList().Select(Mapper.Map<Shop, ShopDto>).Where(c => c.status == "Register");
            //return Ok(getCompanyType);
            var getCompanyType = (from s in _context.Shops
                                  join b in _context.PaymentMethods on s.bankNameid equals b.id
                                  join p in _context.Privacies on s.id equals p.id
                                  select new
                                  {
                                      id = s.id,
                                      shopid = s.shopid,
                                      shopName = s.shopName,
                                      gender = s.gender,
                                      dob = s.dob,
                                      nationality = s.nationality,
                                      ownerName = s.ownerName,
                                      phone = s.phone,
                                      password = s.password,
                                      tokenid = s.tokenid,
                                      facebookPage = s.facebookPage,
                                      location = s.location,
                                      logoShop = s.logoShop,
                                      paymentType = s.paymentType,
                                      qrCodeImage = s.qrCodeImage,
                                      bankNameid = b.methodname,
                                      methodName = b.methodname,
                                      accountNumber = s.accountNumber,
                                      accountName = s.accountName,
                                      feetype = s.feetype,
                                      feecharge = s.feecharge,
                                      shophistorydate = s.shophistorydate,
                                      note = s.note,
                                      status = s.status,
                                      description = p.description
                                  }).ToList().Where(c => c.status == "Active");
            return Ok(getCompanyType);
        }


        [HttpGet]
        //Get : api/Shops{id}
        //[Route("api/Shops{id}")]
        public IHttpActionResult GetCustomer(int id)
        {
            var department = _context.Privacies.SingleOrDefault(c => c.id == id);
            var getCustomerById = _context.Shops.SingleOrDefault(c => c.id == id);

            if (getCustomerById == null)
                return NotFound();

            return Ok(Mapper.Map<Shop, ShopDto>(getCustomerById));

        }

        //GET : /api/Shops/ShopId={ShopId}  for get all record
        [HttpGet]
        [Route("api/Shops/{ShopId}/{status}")]
        public IHttpActionResult GetShops(string ShopId, string status)
        {

            if (ShopId == "all")
            {
                var manageShop = _context.Shops.Select(Mapper.Map<Shop, ShopDto>);
                return Ok(manageShop);
            }
            else
            {
                var manageShop = _context.Shops.Select(Mapper.Map<Shop, ShopDto>)
                                            .Where(c => c.status== status); //(c => c.is_active == true);
                return Ok(manageShop);
            }
        }
        //GET : /api/Shops/None={None}  for get all record
        //[HttpGet]
        //[Route("api/Shops/active/inactive/process/{None}")]
        //public IHttpActionResult GetShopNone(string None)
        //{

        //    if (None == "Inactive")
        //    {
        //        var NoneACT = _context.Shops.Select(Mapper.Map<Shop, ShopDto>);
        //        return Ok(NoneACT);
        //    }
        //    else
        //    {
        //        var NoneACT = _context.Shops.Select(Mapper.Map<Shop, ShopDto>)
        //                                    .Where(c => c.status == "Inactive"); //(c => c.is_active == true);
        //        return Ok(NoneACT);
        //    }
        //}
       
        //[Route("api/Shops/{id}/{a}/{b}")]
        public IHttpActionResult Aprove(int id)
        {
            var shopInDb = _context.Shops.SingleOrDefault(c => c.id == id);
            if (shopInDb == null)
                return BadRequest();
            if (shopInDb.status == "Active")
            {
                shopInDb.status = "Inactive";
            }
            else if (shopInDb.status == "Inactive")
            {
                shopInDb.status = "Active";
            }

            _context.SaveChanges();

            return Ok(new { });

        }

        [HttpPost]
        //POST : api/Shops

        public IHttpActionResult CreateShop( )
        {
            var id = HttpContext.Current.Request.Form["id"];
            var shopid = HttpContext.Current.Request.Form["shopidm"];
            var shopName = HttpContext.Current.Request.Form["shopName"];
            var ownerName = HttpContext.Current.Request.Form["ownerName"];
            var gender = HttpContext.Current.Request.Form["gender"];
            var dob = HttpContext.Current.Request.Form["dob"];
            var nationality = HttpContext.Current.Request.Form["nationality"];
            var phone = HttpContext.Current.Request.Form["phone"];
            var password = HttpContext.Current.Request.Form["password"];
            var tokenid = HttpContext.Current.Request.Form["tokenId"];
            var facebookPage = HttpContext.Current.Request.Form["facebookPage"];
            var location = HttpContext.Current.Request.Form["location"];
            var logoShop = HttpContext.Current.Request.Files["logoShop"];
            var paymentType = HttpContext.Current.Request.Form["paymentType"];
            var qrCodeImages = HttpContext.Current.Request.Files["qrCodeImage"];
            var bankNameid = HttpContext.Current.Request.Form["bankNameid"];
            var accountNumber = HttpContext.Current.Request.Form["accountNumber"];
            var accountName = HttpContext.Current.Request.Form["accountName"];
            var feetype = HttpContext.Current.Request.Form["feetype"];
            var feecharge = HttpContext.Current.Request.Form["feecharge"];
            var shophistorydate = HttpContext.Current.Request.Form["shophistorydate"];
            var note = HttpContext.Current.Request.Form["note"];
            var status = HttpContext.Current.Request.Form["status"];

            //if (Image != null)
            //    return BadRequest();
            
           
            string Logo = "";

            if (logoShop != null)
            {
                Logo = Path.Combine(Path.GetDirectoryName(logoShop.FileName)
                    , string.Concat(Path.GetFileNameWithoutExtension(logoShop.FileName)
                    , DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss")
                    , Path.GetExtension(logoShop.FileName)
                    ));
                var savePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Images"), Logo);
                logoShop.SaveAs(savePath);
            }
            string photoName = "";

            if (qrCodeImages != null)
            {
                photoName = Path.Combine(Path.GetDirectoryName(qrCodeImages.FileName)
                    , string.Concat(Path.GetFileNameWithoutExtension(qrCodeImages.FileName)
                    , DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss")
                    , Path.GetExtension(qrCodeImages.FileName)
                    ));
                var savePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Images"), photoName);
                qrCodeImages.SaveAs(savePath);
            }
            var shopDto = new ShopDto()
            {
                // id = int.Parse(id),
                shopid= shopid,
                shopName = shopName,
                gender=gender,
                dob= DateTime.Parse(dob),
                nationality=nationality,
                ownerName = ownerName,
                phone = phone,
                password = password,
                tokenid = tokenid,
                facebookPage = facebookPage,
                location = location,
                logoShop = Logo,
                paymentType = paymentType,
                qrCodeImage = photoName,
                bankNameid = int.Parse(bankNameid),
                accountNumber = accountNumber,
                accountName = accountName,
                feetype= feetype,
                feecharge = decimal.Parse(feecharge),
                shophistorydate =DateTime.Parse(shophistorydate),
                note =note,
                status = status

            };

            var shop = Mapper.Map<ShopDto, Shop>(shopDto);
            _context.Shops.Add(shop);
            _context.SaveChanges();

            return Created(new Uri(Request.RequestUri + "/" + shopDto.id), shopDto);

        }

        [HttpPut]
        //PUT : /api/Shops/{id}
        public IHttpActionResult EditCustomer(int id)
        {
            var shopid = HttpContext.Current.Request.Form["shopidm"];
            var shopName = HttpContext.Current.Request.Form["shopName"];
            var gender = HttpContext.Current.Request.Form["gender"];
            var dob = HttpContext.Current.Request.Form["dob"];
            var nationality = HttpContext.Current.Request.Form["nationality"];
            var ownerName = HttpContext.Current.Request.Form["ownerName"];
            var phone = HttpContext.Current.Request.Form["phone"];
            var password = HttpContext.Current.Request.Form["password"];
            var tokenid = HttpContext.Current.Request.Form["tokenId"];
            var facebookPage = HttpContext.Current.Request.Form["facebookPage"];
            var location = HttpContext.Current.Request.Form["location"];

            var logoShop = HttpContext.Current.Request.Files["logoShop"];

            var paymentType = HttpContext.Current.Request.Form["paymentType"];
            var qrCodeImages = HttpContext.Current.Request.Files["qrCodeImage"];
            var bankNameid = HttpContext.Current.Request.Form["bankNameid"];
            var accountNumber = HttpContext.Current.Request.Form["accountNumber"];
            var accountName = HttpContext.Current.Request.Form["accountName"];
            var feetype = HttpContext.Current.Request.Form["feetype"];
            var feecharge = HttpContext.Current.Request.Form["feecharge"];
            var shophistorydate = HttpContext.Current.Request.Form["shophistorydate"];
            var note = HttpContext.Current.Request.Form["note"];
            var status = HttpContext.Current.Request.Form["status"]; 

            var qrcode_old = HttpContext.Current.Request.Form["qrcode_old"];
            var logoShop_old = HttpContext.Current.Request.Form["logoShop_old"];



            var ShopDB = _context.Shops.SingleOrDefault(c => c.id == id);
            //string Logo = "";
            string logoShop_new = "", qrCodeImages_new="";

            logoShop_new = logoShop_old;
            qrCodeImages_new = qrcode_old;
            //////////
            if (logoShop != null)
            {
                logoShop_new = Path.Combine(Path.GetDirectoryName(logoShop.FileName)
                    , string.Concat(Path.GetFileNameWithoutExtension(logoShop.FileName)
                    , DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss")
                    , Path.GetExtension(logoShop.FileName)
                    ));
                var savePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Images"), logoShop_new);
                logoShop.SaveAs(savePath);
                //Delete OldPhoto
                var oldPhotoPath = Path.Combine(HttpContext.Current.Server.MapPath("~/Images"), ShopDB.logoShop);

                if (File.Exists(oldPhotoPath))
                {
                    File.Delete(oldPhotoPath);
                }
            
                
            }

            if (qrCodeImages != null)
            {
                qrCodeImages_new = Path.Combine(Path.GetDirectoryName(qrCodeImages.FileName)
                    , string.Concat(Path.GetFileNameWithoutExtension(qrCodeImages.FileName)
                    , DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss")
                    , Path.GetExtension(qrCodeImages.FileName)
                    ));
                var savePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Images"), qrCodeImages_new);
                qrCodeImages.SaveAs(savePath);
                //Delete OldPhoto
                var oldPhotoPath = Path.Combine(HttpContext.Current.Server.MapPath("~/Images"), ShopDB.qrCodeImage);

                if (File.Exists(oldPhotoPath))
                {
                    File.Delete(oldPhotoPath);
                }

            }
                var shopDot = new ShopDto()
                {
                    id=id,
                    shopid=shopid,
                    shopName = shopName,
                    gender = gender,
                    dob = DateTime.Parse(dob),
                    nationality = nationality,
                    ownerName = ownerName,
                    phone = phone,
                    password = password,
                    tokenid = tokenid,
                    facebookPage = facebookPage,
                    location = location,
                    logoShop = logoShop_new,
                    paymentType = paymentType,
                    qrCodeImage = qrCodeImages_new,
                    bankNameid = int.Parse(bankNameid),
                    accountNumber = accountNumber,
                    accountName = accountName,
                    feetype=feetype,
                    feecharge = decimal.Parse(feecharge),
                    shophistorydate = DateTime.Parse(shophistorydate),
                    note = note,
                    status = status

                };
                Mapper.Map(shopDot, ShopDB);
                _context.SaveChanges();
            return Ok(new { });
        }
        [HttpDelete]
        //DELETE : /api/companyTypes/{id}
        //[Route("api/Shops/{id}/")]
        public IHttpActionResult DeleteShop(int id)
        {

            var shopInDb = _context.Shops.SingleOrDefault(c => c.id == id);
            if (shopInDb == null)
                return NotFound();
            _context.Shops.Remove(shopInDb);
            _context.SaveChanges();

            return Ok(new { });


            //var shopInDb = _context.Shops.SingleOrDefault(c => c.id == id);
            //if (shopInDb == null)
            //    return BadRequest();

            //shopInDb.status = "None_active";
            //_context.SaveChanges();

            //return Ok(new { });
        }
        
        //public IHttpActionResult Disable(int id)
        //{
        //    var shopInDb = _context.Shops.SingleOrDefault(c => c.id == id);
        //    if (shopInDb == null)
        //        return BadRequest();

        //    shopInDb.status = "Inactive";
        //    _context.SaveChanges();

        //    return Ok(new { });

        //}

    }
}
