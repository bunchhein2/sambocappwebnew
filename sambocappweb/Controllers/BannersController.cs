﻿using sambocappweb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sambocappweb.Controllers
{
    public class BannersController : Controller
    {
        private ApplicationDbContext _context;

        public BannersController()
        {
            _context = new ApplicationDbContext();
        }
        // GET: Banners
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Banner()
        {
            var shop = _context.Shops.ToList();
            return View("_Banner",shop);
        }

    }
}