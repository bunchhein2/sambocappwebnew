﻿using sambocappweb.Models;
using sambocappweb.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sambocappweb.Controllers
{
    public class BeginingBalanceController : Controller
    {
        private ApplicationDbContext _context;

        public BeginingBalanceController()
        {
            _context = new ApplicationDbContext();
        }

        // GET: Publications
        [Route("manage-beginingbalance")]
        public ActionResult Index()
        {
            var viewModel = new BeginingBalanceViewModel()
            {
                PaymentMethods = _context.PaymentMethods.ToList()
            };
            return View(viewModel);
        }
    }
}
