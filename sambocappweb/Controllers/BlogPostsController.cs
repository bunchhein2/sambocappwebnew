﻿using sambocappweb.Models;
using sambocappweb.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Script.Serialization;

namespace sambocappweb.Controllers
{
    [AllowAnonymous]
    public class BlogPostsController : Controller
    {
        private ApplicationDbContext _context;
        public BlogPostsController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // GET: BlogPosts
        [Route("manage-website")]
        public ActionResult Index()
        {
            var blogPostViewModel = new BlogPostViewModel()
            {
                BlogCategories = _context.BlogCategories.ToList()
                //Images = Directory.EnumerateFiles(Server.MapPath("~/Uploads")).Select(fn => "~/Uploads/" + Path.GetFileName(fn)).Take(30)
            };
            return View(blogPostViewModel);
        }

        public JsonResult filesinfolder()
        {
            DirectoryInfo salesFTPDirectory = new DirectoryInfo(Server.MapPath("~/Uploads"));
            IEnumerable<string> files = salesFTPDirectory.GetFiles()
              .Where(f => f.Extension == ".xls" || f.Extension == ".xml" || f.Extension == ".jps" || f.Extension == ".jpg" || f.Extension == ".jpeg" || f.Extension == ".png")
              .OrderByDescending(f => f.CreationTime)
              .Select(f => "/Uploads/" + f.Name)
              .Take(36);
            return Json(files, JsonRequestBehavior.AllowGet);
        }

        [Route("contact-us")]
        public ActionResult Contact()
        {
            var cookie = HttpContext.Request.Cookies["Language"].Value;
            if (cookie == "ca")
            {
                var widgetNews = _context.BlogPosts.Include(c => c.BlogCategory).Where(c => c.BlogCategory.Name == "News").OrderByDescending(c => c.Id).ToList().Take(5);

                var viewModel = new BlogPostViewModel()
                {
                    BlogPosts = widgetNews
                };
                return View(viewModel);
            }
            else
            {
                var widgetNews = _context.BlogPosts.Include(c => c.BlogCategory).Where(c => c.BlogCategory.Name == "News" && c.Title != "").OrderByDescending(c => c.Id).ToList().Take(5);

                var viewModel = new BlogPostViewModel()
                {
                    BlogPosts = widgetNews
                };
                return View(viewModel);
            }
        }

        [Route("network-implementation")]
        public ActionResult NetworkImplementation()
        {
            var cookie = HttpContext.Request.Cookies["Language"].Value;
            if (cookie == "ca")
            {
                var sa = _context.BlogPosts.Include(c => c.BlogCategory).FirstOrDefault(c => c.BlogCategory.Name == "Network Implementation");
                var widgetNews = _context.BlogPosts.Include(c => c.BlogCategory).Where(c => c.BlogCategory.Name == "News").OrderByDescending(c => c.Id).ToList().Take(5);

                var viewModel = new BlogPostViewModel()
                {
                    BlogPosts = widgetNews,
                    BlogPost = sa
                };
                return View(viewModel);
            }
            else
            {
                var sa = _context.BlogPosts.Include(c => c.BlogCategory).FirstOrDefault(c => c.BlogCategory.Name == "Network Implementation");
                var widgetNews = _context.BlogPosts.Include(c => c.BlogCategory).Where(c => c.BlogCategory.Name == "News" && c.Title != "").OrderByDescending(c => c.Id).ToList().Take(5);

                var viewModel = new BlogPostViewModel()
                {
                    BlogPosts = widgetNews,
                    BlogPost = sa
                };
                return View(viewModel);
            }
        }

        [Route("software-development")]
        public ActionResult SoftwareDevelopment()
        {
            var cookie = HttpContext.Request.Cookies["Language"].Value;
            if (cookie == "ca")
            {
                var sa = _context.BlogPosts.Include(c => c.BlogCategory).FirstOrDefault(c => c.BlogCategory.Name == "Software Development");
                var widgetNews = _context.BlogPosts.Include(c => c.BlogCategory).Where(c => c.BlogCategory.Name == "News").OrderByDescending(c => c.Id).ToList().Take(5);

                var viewModel = new BlogPostViewModel()
                {
                    BlogPosts = widgetNews,
                    BlogPost = sa
                };
                return View(viewModel);
            }
            else
            {
                var sa = _context.BlogPosts.Include(c => c.BlogCategory).FirstOrDefault(c => c.BlogCategory.Name == "Software Development");
                var widgetNews = _context.BlogPosts.Include(c => c.BlogCategory).Where(c => c.BlogCategory.Name == "News" && c.Title != "").OrderByDescending(c => c.Id).ToList().Take(5);

                var viewModel = new BlogPostViewModel()
                {
                    BlogPosts = widgetNews,
                    BlogPost = sa
                };
                return View(viewModel);
            }
        }

        [Route("website-development")]
        public ActionResult WebsiteDevelopment()
        {
            var cookie = HttpContext.Request.Cookies["Language"].Value;
            if (cookie == "ca")
            {
                var sa = _context.BlogPosts.Include(c => c.BlogCategory).FirstOrDefault(c => c.BlogCategory.Name == "Website Development");
                var widgetNews = _context.BlogPosts.Include(c => c.BlogCategory).Where(c => c.BlogCategory.Name == "News").OrderByDescending(c => c.Id).ToList().Take(5);

                var viewModel = new BlogPostViewModel()
                {
                    BlogPosts = widgetNews,
                    BlogPost = sa
                };
                return View(viewModel);
            }
            else
            {
                var sa = _context.BlogPosts.Include(c => c.BlogCategory).FirstOrDefault(c => c.BlogCategory.Name == "Website Development");
                var widgetNews = _context.BlogPosts.Include(c => c.BlogCategory).Where(c => c.BlogCategory.Name == "News" && c.Title != "").OrderByDescending(c => c.Id).ToList().Take(5);

                var viewModel = new BlogPostViewModel()
                {
                    BlogPosts = widgetNews,
                    BlogPost = sa
                };
                return View(viewModel);
            }
        }

        [Route("mobile-app-evelopment")]
        public ActionResult MobileAppDevelopment()
        {
            var cookie = HttpContext.Request.Cookies["Language"].Value;
            if (cookie == "ca")
            {
                var sa = _context.BlogPosts.Include(c => c.BlogCategory).FirstOrDefault(c => c.BlogCategory.Name == "Mobile App Development");
                var widgetNews = _context.BlogPosts.Include(c => c.BlogCategory).Where(c => c.BlogCategory.Name == "News").OrderByDescending(c => c.Id).ToList().Take(5);

                var viewModel = new BlogPostViewModel()
                {
                    BlogPosts = widgetNews,
                    BlogPost = sa
                };
                return View(viewModel);
            }
            else
            {
                var sa = _context.BlogPosts.Include(c => c.BlogCategory).FirstOrDefault(c => c.BlogCategory.Name == "Mobile App Development");
                var widgetNews = _context.BlogPosts.Include(c => c.BlogCategory).Where(c => c.BlogCategory.Name == "News" && c.Title != "").OrderByDescending(c => c.Id).ToList().Take(5);

                var viewModel = new BlogPostViewModel()
                {
                    BlogPosts = widgetNews,
                    BlogPost = sa
                };
                return View(viewModel);
            }
        }

        [Route("maintenance-services")]
        public ActionResult MaintenanceServices()
        {
            var cookie = HttpContext.Request.Cookies["Language"].Value;
            if (cookie == "ca")
            {
                var sa = _context.BlogPosts.Include(c => c.BlogCategory).FirstOrDefault(c => c.BlogCategory.Name == "Maintenance Services");
                var widgetNews = _context.BlogPosts.Include(c => c.BlogCategory).Where(c => c.BlogCategory.Name == "News").OrderByDescending(c => c.Id).ToList().Take(5);

                var viewModel = new BlogPostViewModel()
                {
                    BlogPosts = widgetNews,
                    BlogPost = sa
                };
                return View(viewModel);
            }
            else
            {
                var sa = _context.BlogPosts.Include(c => c.BlogCategory).FirstOrDefault(c => c.BlogCategory.Name == "Maintenance Services");
                var widgetNews = _context.BlogPosts.Include(c => c.BlogCategory).Where(c => c.BlogCategory.Name == "News" && c.Title != "").OrderByDescending(c => c.Id).ToList().Take(5);

                var viewModel = new BlogPostViewModel()
                {
                    BlogPosts = widgetNews,
                    BlogPost = sa
                };
                return View(viewModel);
            }
        }

        [Route("mis-privacy")]
        public ActionResult Structure()
        {
            var structure = _context.BlogPosts.Include(c => c.BlogCategory).FirstOrDefault(c => c.BlogCategory.Name == "Structure");
            var widgetNews = _context.BlogPosts.Include(c => c.BlogCategory).Where(c => c.BlogCategory.Name == "News").OrderByDescending(c => c.Id).ToList().Take(5);
            var viewModel = new BlogPostViewModel()
            {
                BlogPosts = widgetNews,
                BlogPost = structure
            };
            return View(viewModel);
        }

        [Route("management-information-system-members")]
        public ActionResult sambocappwebMembers()
        {
            var sambocappwebMembers = _context.BlogPosts.Include(c => c.BlogCategory).FirstOrDefault(c => c.BlogCategory.Name == "sambocappweb Members");
            var widgetNews = _context.BlogPosts.Include(c => c.BlogCategory).Where(c => c.BlogCategory.Name == "News").OrderByDescending(c => c.Id).ToList().Take(5);
            var viewModel = new BlogPostViewModel()
            {
                BlogPosts = widgetNews,
                BlogPost = sambocappwebMembers
            };
            return View(viewModel);
        }

        [Route("exco-members")]
        public ActionResult ExcoMembers()
        {
            var excoMembers = _context.BlogPosts.Include(c => c.BlogCategory).FirstOrDefault(c => c.BlogCategory.Name == "Exco Members");
            var widgetNews = _context.BlogPosts.Include(c => c.BlogCategory).Where(c => c.BlogCategory.Name == "News").OrderByDescending(c => c.Id).ToList().Take(5);
            var viewModel = new BlogPostViewModel()
            {
                BlogPosts = widgetNews,
                BlogPost = excoMembers
            };
            return View(viewModel);
        }

        [Route("news")]
        public ActionResult News()
        {
            var cookie = HttpContext.Request.Cookies["Language"].Value;
            if (cookie == "ca")
            {
                var news = _context.BlogPosts.Include(c => c.BlogCategory).FirstOrDefault(c => c.BlogCategory.Name == "News");
                var widgetNews = _context.BlogPosts.Include(c => c.BlogCategory).Where(c => c.BlogCategory.Name == "News").OrderByDescending(c => c.Id).ToList().Take(5);
                var viewModel = new BlogPostViewModel()
                {
                    BlogPosts = widgetNews,
                    BlogPost = news
                };
                return View(viewModel);
            }
            else
            {
                var news = _context.BlogPosts.Include(c => c.BlogCategory).FirstOrDefault(c => c.BlogCategory.Name == "News");
                var widgetNews = _context.BlogPosts.Include(c => c.BlogCategory).Where(c => c.BlogCategory.Name == "News" && c.Title != "").OrderByDescending(c => c.Id).ToList().Take(5);
                var viewModel = new BlogPostViewModel()
                {
                    BlogPosts = widgetNews,
                    BlogPost = news
                };
                return View(viewModel);
            }
        }

        [Route("news/{id}")]
        public ActionResult News(int id)
        {
            var cookie = HttpContext.Request.Cookies["Language"].Value;
            if (cookie == "ca")
            {
                var widgetNews = _context.BlogPosts.Include(c => c.BlogCategory).Where(c => c.BlogCategory.Name == "News").OrderByDescending(c => c.Id).ToList().Take(5);
                var news = _context.BlogPosts.Include(c => c.BlogCategory).FirstOrDefault(c => c.BlogCategory.Name == "News" && c.Id == id);
                news.ViewCounter += 1;
                _context.SaveChanges();
                var viewModel = new BlogPostViewModel()
                {
                    BlogPosts = widgetNews,
                    BlogPost = news
                };
                return View("NewsContent", viewModel);
            }
            else
            {
                var widgetNews = _context.BlogPosts.Include(c => c.BlogCategory).Where(c => c.BlogCategory.Name == "News" && c.Title != "").OrderByDescending(c => c.Id).ToList().Take(5);
                var news = _context.BlogPosts.Include(c => c.BlogCategory).FirstOrDefault(c => c.BlogCategory.Name == "News" && c.Id == id);
                news.ViewCounter += 1;
                _context.SaveChanges();
                var viewModel = new BlogPostViewModel()
                {
                    BlogPosts = widgetNews,
                    BlogPost = news
                };
                return View("NewsContent", viewModel);
            }
        }

        [Authorize]
        public ActionResult Upload()
        {
            var file = Request.Files["Filedata"];
            string extension = Path.GetExtension(file.FileName);
            string fileName = Path.Combine(Path.GetDirectoryName(file.FileName)
                                       , string.Concat(Path.GetFileNameWithoutExtension(file.FileName)
                                       , DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss")
                                       , Path.GetExtension(file.FileName)
                                       ));

            string savePath = Server.MapPath(@"~\Uploads\" + fileName);
            file.SaveAs(savePath);

            return Content(Url.Content(@"~\Uploads\" + fileName));
        }

        [Authorize]
        [HttpPost]
        public string News(FormCollection collection)
        {
            var pag_content = "";
            var pag_navigation = "";
            int page = Convert.ToInt32(collection["data[page]"]);
            int max = 6;
            string search = collection["data[search]"];
            int cur_page = page;
            page -= 1;
            int per_page = max > 1 ? max : 16;
            bool previous_btn = true;
            bool next_btn = true;
            bool first_btn = true;
            bool last_btn = true;
            int start = page * per_page;
            DataTable data_count = new DataTable();
            var sqlcount = "";
            var sql = "";

            DataTable data_products = new DataTable();
            var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            SqlConnection conx = new SqlConnection(connectionString);
            
            var cookie = HttpContext.Request.Cookies["Language"].Value;
            if (cookie == "ca")
            {
                sql = "SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY BP.Id DESC) AS Seq,BP.*, REPLACE(REPLACE(CONVERT(VARCHAR,PostDate,106), ' ','-'), ',','') AS PostedDate, BC.Name FROM BlogPosts BP INNER JOIN BlogCategories BC ON BC.Id=BP.BlogCategoryId WHERE BC.Name='News') C WHERE C.Seq Between " + (start + 1) + " and " + (start + per_page) + "";
                sqlcount = "SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY BP.Id DESC) AS Seq,BP.*, REPLACE(REPLACE(CONVERT(VARCHAR,PostDate,106), ' ','-'), ',','') AS PostedDate, BC.Name FROM BlogPosts BP INNER JOIN BlogCategories BC ON BC.Id=BP.BlogCategoryId WHERE BC.Name='News') C";
            }
            else
            {
                sql = "SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY BP.Id DESC) AS Seq,BP.*, REPLACE(REPLACE(CONVERT(VARCHAR,PostDate,106), ' ','-'), ',','') AS PostedDate, BC.Name FROM BlogPosts BP INNER JOIN BlogCategories BC ON BC.Id=BP.BlogCategoryId WHERE BC.Name='News' AND BP.title <> '') C WHERE C.Seq Between " + (start + 1) + " and " + (start + per_page) + "";
                sqlcount = "SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY BP.Id DESC) AS Seq,BP.*, REPLACE(REPLACE(CONVERT(VARCHAR,PostDate,106), ' ','-'), ',','') AS PostedDate, BC.Name FROM BlogPosts BP INNER JOIN BlogCategories BC ON BC.Id=BP.BlogCategoryId WHERE BC.Name='News' AND BP.title <> '') C";
            }

            SqlDataAdapter adp = new SqlDataAdapter(sql, conx);
            var all_items_query = adp.Fill(data_products);

            SqlDataAdapter adpcount = new SqlDataAdapter(sqlcount, conx);
            var count_row = adpcount.Fill(data_count);

            int count = data_count.Select().Count();

            if (count > 0)
            {
                foreach (DataRow row in data_products.Rows)
                {
                    var titleCharKh = row["titleKh"].ToString().Length;
                    var titleChar = row["title"].ToString().Length;
                    var titleKh = "";
                    var title = "";
                    if (titleCharKh >= 140)
                    {
                        titleKh = row["titleKh"].ToString().Substring(0, 140) + "...";
                    }
                    else
                    {
                        titleKh = row["titleKh"].ToString();
                    }
                    if (titleChar >= 140)
                    {
                        title = row["title"].ToString().Substring(0, 140) + "...";
                    }
                    else
                    {
                        title = row["title"].ToString();
                    }
                    
                    if (cookie == "ca")
                    {
                        pag_content += "<div class='col-md-6' style='padding-left:5px;padding-right:5px'>" +
                        "<div class='panel panel-default'>" +
                            "<div class='panel-body' id='cardPost'>" +
                                "<a style='text-decoration: none;' href='/news/" + row["id"] + "'>" +
                                    "<img src='/Uploads/" + row["thumbnail"] + "' width='100%' class='img-responsive item-featured' />" +
                                    "<hr>" +
                                    "<p id='pPost'>" + titleKh + "</p>" +
                                    //"<p id='cardDate' style='line-height: 27px;'><span class='glyphicon glyphicon-calendar'></span> " + @sambocappweb.Resources.Language.Date + ": " + row["postedDate"] + " | <span class='glyphicon glyphicon-eye-open'></span> ចំនួនអ្នកអាន: " + row["viewCounter"] + "</p>" +
                                "</a>" +
                            "</div>" +
                        "</div>" +
                    "</div>";
                    }
                    else
                    {
                        if (title != "")
                        {
                            pag_content += "<div class='col-md-6' style='padding-left:5px;padding-right:5px'>" +
                                "<div class='panel panel-default'>" +
                                    "<div class='panel-body' id='cardPost'>" +
                                        "<a style='text-decoration: none;' href='/news/" + row["id"] + "'>" +
                                            "<img src='/Uploads/" + row["thumbnail"] + "' width='100%' class='img-responsive item-featured' />" +
                                            "<hr>" +
                                            "<p id='pPost'>" + title + "</p>" +
                                            //"<p id='cardDate' style='line-height: 27px;'><span class='glyphicon glyphicon-calendar'></span> " + @sambocappweb.Resources.Language.Date + ": " + row["postedDate"] + " | <span class='glyphicon glyphicon-eye-open'></span> View: " + row["viewCounter"] + "</p>" +
                                        "</a>" +
                                    "</div>" +
                                "</div>" +
                            "</div>";
                        }
                        
                    }
                }
            }
            else
            {
                pag_content += "<p class='p-d bg-danger'>No content found</p>";
            }

            pag_content = pag_content + "<br class = 'clear' />";

            decimal nop_ceil = Decimal.Divide(count, per_page);
            int no_of_paginations = Convert.ToInt32(Math.Ceiling(nop_ceil));

            var start_loop = 1;
            var end_loop = no_of_paginations;

            if (cur_page >= 7)
            {
                start_loop = cur_page - 3;
                if (no_of_paginations > cur_page + 3)
                {
                    end_loop = cur_page + 3;
                }
                else if (cur_page <= no_of_paginations && cur_page > no_of_paginations - 6)
                {
                    start_loop = no_of_paginations - 6;
                    end_loop = no_of_paginations;
                }
            }
            else
            {
                if (no_of_paginations > 7)
                {
                    end_loop = 7;
                }
            }

            pag_navigation += "<ul>";

            if (first_btn && cur_page > 1)
            {
                pag_navigation += "<li p='1' class='active'>First</li>";
            }
            else if (first_btn)
            {
                pag_navigation += "<li p='1' class='inactive'>First</li>";
            }

            if (previous_btn && cur_page > 1)
            {
                var pre = cur_page - 1;
                pag_navigation += "<li p='" + pre + "' class='active'>Previous</li>";
            }
            else if (previous_btn)
            {
                pag_navigation += "<li class='inactive'>Previous</li>";
            }

            for (int i = start_loop; i <= end_loop; i++)
            {

                if (cur_page == i)
                    pag_navigation += "<li p='" + i + "' class = 'selected' >" + i + "</li>";
                else
                    pag_navigation += "<li p='" + i + "' class='active'>" + i + "</li>";
            }

            if (next_btn && cur_page < no_of_paginations)
            {
                var nex = cur_page + 1;
                pag_navigation += "<li p='" + nex + "' class='active'>Next</li>";
            }
            else if (next_btn)
            {
                pag_navigation += "<li class='inactive'>Next</li>";
            }

            if (last_btn && cur_page < no_of_paginations)
            {
                pag_navigation += "<li p='" + no_of_paginations + "' class='active'>Last</li>";
            }
            else if (last_btn)
            {
                pag_navigation += "<li p='" + no_of_paginations + "' class='inactive'>Last</li>";
            }

            pag_navigation = pag_navigation + "</ul>";

            var response = new Dictionary<string, string> {
                { "content", pag_content },
                { "navigation", pag_navigation }
            };

            string json = new JavaScriptSerializer().Serialize(response);
            return json;
        }

        [Authorize]
        [HttpPost]
        public string HomePage(FormCollection collection)
        {
            var pag_content = "";
            var pag_navigation = "";
            int page = Convert.ToInt32(collection["data[page]"]);
            int max = 6;
            string search = collection["data[search]"];
            int cur_page = page;
            page -= 1;
            int per_page = max > 1 ? max : 16;
            bool previous_btn = true;
            bool next_btn = true;
            bool first_btn = true;
            bool last_btn = true;
            int start = page * per_page;
            var sqlcount = "";
            var sql = "";

            DataTable data_products = new DataTable();
            var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            SqlConnection conx = new SqlConnection(connectionString);
            DataTable data_count = new DataTable();
            
            var cookie = HttpContext.Request.Cookies["Language"].Value;
            if (cookie == "ca")
            {
                sql = "SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY BP.Id DESC) AS Seq,BP.*, REPLACE(REPLACE(CONVERT(VARCHAR,PostDate,106), ' ','-'), ',','') AS PostedDate, BC.Name FROM BlogPosts BP INNER JOIN BlogCategories BC ON BC.Id=BP.BlogCategoryId WHERE BC.Name='News') C WHERE C.Seq Between " + (start + 1) + " and " + (start + per_page) + "";
                sqlcount = "SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY BP.Id DESC) AS Seq,BP.*, REPLACE(REPLACE(CONVERT(VARCHAR,PostDate,106), ' ','-'), ',','') AS PostedDate, BC.Name FROM BlogPosts BP INNER JOIN BlogCategories BC ON BC.Id=BP.BlogCategoryId WHERE BC.Name='News') C";
            }
            else
            {
                sql = "SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY BP.Id DESC) AS Seq,BP.*, REPLACE(REPLACE(CONVERT(VARCHAR,PostDate,106), ' ','-'), ',','') AS PostedDate, BC.Name FROM BlogPosts BP INNER JOIN BlogCategories BC ON BC.Id=BP.BlogCategoryId WHERE BC.Name='News' AND BP.title <> '') C WHERE C.Seq Between " + (start + 1) + " and " + (start + per_page) + "";
                sqlcount = "SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY BP.Id DESC) AS Seq,BP.*, REPLACE(REPLACE(CONVERT(VARCHAR,PostDate,106), ' ','-'), ',','') AS PostedDate, BC.Name FROM BlogPosts BP INNER JOIN BlogCategories BC ON BC.Id=BP.BlogCategoryId WHERE BC.Name='News' AND BP.title <> '') C";
            }

            SqlDataAdapter adp = new SqlDataAdapter(sql, conx);
            var all_items_query = adp.Fill(data_products);

            SqlDataAdapter adpcount = new SqlDataAdapter(sqlcount, conx);
            var count_row = adpcount.Fill(data_count);

            int count = data_count.Select().Count();

            if (count > 0)
            {
                foreach (DataRow row in data_products.Rows)
                {
                    var titleCharKh = row["titleKh"].ToString().Length;
                    var titleChar = row["title"].ToString().Length;
                    var titleKh = "";
                    var title = "";
                    if (titleCharKh >= 140)
                    {
                        titleKh = row["titleKh"].ToString().Substring(0, 140) + "...";
                    }
                    else
                    {
                        titleKh = row["titleKh"].ToString();
                    }
                    if (titleChar >= 140)
                    {
                        title = row["title"].ToString().Substring(0, 140) + "...";
                    }
                    else
                    {
                        title = row["title"].ToString();
                    }

                    if (cookie == "ca")
                    {
                        pag_content += "<div class='col-md-4' style='padding-left:5px;padding-right:5px'>" +
                        "<div class='panel panel-default'>" +
                            "<div class='panel-body' id='cardPost'>" +
                                "<a style='text-decoration: none;' href='/news/" + row["id"] + "'>" +
                                    "<img src='/Uploads/" + row["thumbnail"] + "' width='100%' class='img-responsive item-featured' />" +
                                    "<hr>" +
                                    "<p id='pPost'>" + titleKh + "</p>" +
                                    //"<p id='cardDate' style='line-height: 27px;'><span class='glyphicon glyphicon-calendar'></span> " + @sambocappweb.Resources.Language.Date + ": " + row["postedDate"] + " | <span class='glyphicon glyphicon-eye-open'></span> ចំនួនអ្នកអាន: " + row["viewCounter"] + "</p>" +
                                "</a>" +
                            "</div>" +
                        "</div>" +
                    "</div>";
                    }
                    else
                    {
                        if (title != "")
                        {
                            pag_content += "<div class='col-md-4' style='padding-left:5px;padding-right:5px'>" +
                                "<div class='panel panel-default'>" +
                                    "<div class='panel-body' id='cardPost'>" +
                                        "<a style='text-decoration: none;' href='/news/" + row["id"] + "'>" +
                                            "<img src='/Uploads/" + row["thumbnail"] + "' width='100%' class='img-responsive item-featured' />" +
                                            "<hr>" +
                                            "<p id='pPost'>" + title + "</p>" +
                                            //"<p id='cardDate' style='line-height: 27px;'><span class='glyphicon glyphicon-calendar'></span> " + @sambocappweb.Resources.Language.Date + ": " + row["postedDate"] + " | <span class='glyphicon glyphicon-eye-open'></span> View: " + row["viewCounter"] + "</p>" +
                                        "</a>" +
                                    "</div>" +
                                "</div>" +
                            "</div>";
                        }

                    }
                }
            }
            else
            {
                pag_content += "<p class='p-d bg-danger'>No content found</p>";
            }

            pag_content = pag_content + "<br class = 'clear' />";

            decimal nop_ceil = Decimal.Divide(count, per_page);
            int no_of_paginations = Convert.ToInt32(Math.Ceiling(nop_ceil));

            var start_loop = 1;
            var end_loop = no_of_paginations;

            if (cur_page >= 7)
            {
                start_loop = cur_page - 3;
                if (no_of_paginations > cur_page + 3)
                {
                    end_loop = cur_page + 3;
                }
                else if (cur_page <= no_of_paginations && cur_page > no_of_paginations - 6)
                {
                    start_loop = no_of_paginations - 6;
                    end_loop = no_of_paginations;
                }
            }
            else
            {
                if (no_of_paginations > 7)
                {
                    end_loop = 7;
                }
            }

            pag_navigation += "<ul>";

            if (first_btn && cur_page > 1)
            {
                pag_navigation += "<li p='1' class='active'>First</li>";
            }
            else if (first_btn)
            {
                pag_navigation += "<li p='1' class='inactive'>First</li>";
            }

            if (previous_btn && cur_page > 1)
            {
                var pre = cur_page - 1;
                pag_navigation += "<li p='" + pre + "' class='active'>Previous</li>";
            }
            else if (previous_btn)
            {
                pag_navigation += "<li class='inactive'>Previous</li>";
            }

            for (int i = start_loop; i <= end_loop; i++)
            {

                if (cur_page == i)
                    pag_navigation += "<li p='" + i + "' class = 'selected' >" + i + "</li>";
                else
                    pag_navigation += "<li p='" + i + "' class='active'>" + i + "</li>";
            }

            if (next_btn && cur_page < no_of_paginations)
            {
                var nex = cur_page + 1;
                pag_navigation += "<li p='" + nex + "' class='active'>Next</li>";
            }
            else if (next_btn)
            {
                pag_navigation += "<li class='inactive'>Next</li>";
            }

            if (last_btn && cur_page < no_of_paginations)
            {
                pag_navigation += "<li p='" + no_of_paginations + "' class='active'>Last</li>";
            }
            else if (last_btn)
            {
                pag_navigation += "<li p='" + no_of_paginations + "' class='inactive'>Last</li>";
            }

            pag_navigation = pag_navigation + "</ul>";

            var response = new Dictionary<string, string> {
                { "content", pag_content },
                { "navigation", pag_navigation }
            };

            string json = new JavaScriptSerializer().Serialize(response);
            return json;
        }
    }
}