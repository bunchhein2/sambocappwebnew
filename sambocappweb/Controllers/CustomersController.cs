﻿using sambocappweb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sambocappweb.Controllers
{
    public class CustomersController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        // GET: Customers
        public ActionResult Index()
        {
            ViewBag.CurrentlocationList = new SelectList(db.Locations, "id", "location");
            count();
            return View();
            
        }
        public ActionResult create(int id = 0)
        {
            var Pro = new Customer();
            var Code = db.Customers.OrderByDescending(c => c.id).FirstOrDefault();
            if (id != 0)
            {
                Code = db.Customers.Where(x => x.id == id).FirstOrDefault<Customer>();
            }
            else if (Code == null)
            {
                Pro.tokenid = "001";

            }
            else
            {
                Pro.tokenid = "" + (Convert.ToInt32(Pro.tokenid.Substring(9, Pro.tokenid.Length - 9)) + 1).ToString();
            }
            return View(Pro);
        }
        public void count()
        {
            var viewbage = db.Customers.Count();
            var view1 = viewbage + 1;
            ViewBag.Count = "00" + view1;

        }
    }
}