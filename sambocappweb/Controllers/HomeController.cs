﻿using AutoMapper;
using sambocappweb.Dtos;
using sambocappweb.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using sambocappweb.ViewModels;
using System.Data.Entity;
using System.Net;
using System.Net.Http;
namespace sambocappweb.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private ApplicationDbContext _context;
        public HomeController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        //[AllowAnonymous]
        ////[Authorize]
        //public ActionResult Index()
        //{
            
        //    //var cookie = HttpContext.Request.Cookies["Language"].Value;
        //    //if (cookie == "ca")
        //    //{
        //    //    var news = _context.BlogPosts.Include(c => c.BlogCategory).Where(c => c.BlogCategory.Name == "News").OrderByDescending(c => c.Id).ToList().Take(5);

        //    //    var viewModel = new BlogPostViewModel()
        //    //    {
        //    //        BlogPosts = news
        //    //    };
        //    //    return View("Blog", viewModel);
        //    //}
        //    //else
        //    //{
        //    //    var news = _context.BlogPosts.Include(c => c.BlogCategory).Where(c => c.BlogCategory.Name == "News" && c.Title != "").OrderByDescending(c => c.Id).ToList().Take(5);
        //    //    var viewModel = new BlogPostViewModel()
        //    //    {
        //    //        BlogPosts = news
        //    //    };
        //    //    return View("Login", viewModel);
        //    //}
        //    return RedirectToAction("Login", "Account"); 
        //}
        [Authorize]
        //[Route("admin")]
        public ActionResult Index()
        {
            count();
            shopcountN();
            Productcount();
            ProductcountN();
            addvertisecount();
            advertisecountN();
            ordercount();
            ordercountN();
            customercountN();
            customercount();

            //var userId = User.Identity.GetUserId();
            //DataTable department = new DataTable();
            //var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            //SqlConnection conx = new SqlConnection(connectionString);
            //SqlDataAdapter adp = new SqlDataAdapter("SELECT DS.Id,DS.Name FROM JobPermissions DP INNER JOIN Departments DS ON DS.Id=DP.DepartmentId WHERE DP.UserId='" + userId + "'", conx);
            //adp.Fill(department);

            //var jobViewModel = new JobViewModel()
            //{
            //    Departments = ConvertIEnumDepartment(department),
            //};

            return View("Index");
        }

        private IEnumerable<Department> ConvertIEnumDepartment(DataTable dataTable)
        {
            return dataTable.AsEnumerable().Select(row => new Department
            {
                Id = Convert.ToInt32(row["Id"]),
                Name = row["Name"].ToString()
            });
        }
        
        public void count()
        {
            var count = _context.Shops.ToList().Select(Mapper.Map<Shop, ShopDto>).Where(c => c.status == "Register").Count();
            //var viewbage =  _context.Shops.Count();
            ViewBag.Count = count;

        }
        public void shopcountN()
        {
            var count = _context.Shops.ToList().Select(Mapper.Map<Shop, ShopDto>).Count();
            //var viewbage =  _context.Shops.Count();
            ViewBag.CountN = count;

        }
        public void Productcount()
        {

            var CountPro = _context.Products.ToList().Select(Mapper.Map<Product, ProductDto>).Where(c => c.Status == "Inactive").Count();
            ViewBag.CountPro = CountPro;
            

        }
        public void ProductcountN()
        {
            var CountProN = _context.Products.ToList().Select(Mapper.Map<Product, ProductDto>).Count();
            ViewBag.CountProN = CountProN;

        }
        public void addvertisecount()
        {
            var Countaddvertise = _context.Banners.ToList().Select(Mapper.Map<Banner, bannerDto>).Where(c => c.bannerstatus == "New").Count();
            ViewBag.Countaddvertise = Countaddvertise;

        }
        public void advertisecountN()
        {
            var CountAdd = _context.Banners.ToList().Select(Mapper.Map<Banner, bannerDto>).Count();
            ViewBag.CountAdd = CountAdd;

        }
        public void ordercount()
        {
            var order = _context.Orders.ToList().Select(Mapper.Map<Order, OrderDto>).Where(c => c.Status == "Unpaid").Count();
            ViewBag.Countorder = order;

        }
        public void ordercountN()
        {
            var Countorder = _context.Orders.ToList().Select(Mapper.Map<Order, OrderDto>).Count();
            ViewBag.CountorderN = Countorder;

        }
        public void customercount()
        {
            var custom = _context.Customers.ToList().Select(Mapper.Map<Customer, CustomerDto>).Where(c => c.date == DateTime.Today).Count();
            ViewBag.Countcustomer = custom;

        }
        public void customercountN()
        {
            var customer = _context.Customers.ToList().Select(Mapper.Map<Customer, CustomerDto>).Count();
            ViewBag.CountcustomerN = customer;

        }
    }
}