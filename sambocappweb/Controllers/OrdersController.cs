﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using sambocappweb.Models;

namespace sambocappweb.Controllers
{
    
    public class OrdersController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Orders
        public ActionResult Index()
        {
            ViewBag.ShopList = new SelectList(db.Shops, "id", "shopName");
            ViewBag.CustomerList = new SelectList(db.Customers, "id", "customerName");
            //ViewBag.ExchangeList = new SelectList(db.Exchanges, "Id", "RateOut");
            ViewBag.ExchangeList = new SelectList(db.Exchanges, "id", "rate");
            ViewBag.DeliveryList = new SelectList(db.DeliveryTypes, "id", "delivery_name");
            ViewBag.PaymenttypeList = new SelectList(db.PaymentMethods, "id", "methodname");
            ViewBag.CurrentlocationList = new SelectList(db.Locations, "id", "location");

            //ViewBag.Total = new SelectList(db.Orders, "id", "InvoiceNo");
            count();
            ViewBag.Orderlist = new SelectList(db.Products, "Id", "ProductName");
            //ViewBag.Orderidlist = new SelectList(_context.Orders, "Id", "Date");
            
            return View();
        }
        public void count()
        {
            var viewbage = db.Orders.Count();
            var view1 = viewbage + 1;
            ViewBag.Count = "000" + view1;

        }
        
    }
    
}