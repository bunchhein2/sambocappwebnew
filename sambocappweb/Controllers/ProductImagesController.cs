﻿using sambocappweb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sambocappweb.Controllers
{
    public class ProductImagesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        // GET: ProductImages
        public ActionResult Index()
        {
            ViewBag.productList = new SelectList(db.Products, "Id", "ProductName");
            return View();
        }
    }
}