﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sambocappweb.Dtos.BO
{
    public class OrderDetailRes
    {
        public int id { get; set; }
        public DateTime date { get; set; }
        public int orderid { get; set; }
        public int productid { get; set; }
        public string productname { get; set; }
        public int qty { get; set; }
        public decimal price { get; set; }
        public decimal discount { get; set; }
    }
}