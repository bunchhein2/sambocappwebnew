﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace sambocappweb.Dtos
{
    public class BuyerDto
    {
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        public string buyerName { get; set; }
        public string buyerSex { get; set; }
        public string buyerAge { get; set; }
        public string buyerAddress { get; set; }
        public DateTime? buyerDob { get; set; }
        public string buyerIdentityno { get; set; }
        public string buyerPhone { get; set; }
        public string buyerPhoto { get; set; }

        public string buyerWithname { get; set; }
        public string buyerWithsex { get; set; }
        public string buyerWithage { get; set; }
        public string buyerWithaddress { get; set; }
        public DateTime? buyerWithdob { get; set; }
        public string buyerWithidentityno { get; set; }
        public string buyerWithphone { get; set; }
        public string buyerWithphoto { get; set; }
        public Boolean? status { get; set; }
    }
}