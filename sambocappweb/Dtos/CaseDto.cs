﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace sambocappweb.Dtos
{
    public class CaseDto
    {
        public int id { get; set; }
        [Required]
        [StringLength(50)]
        public string name { get; set; }
        public string note { get; set; }
        public Boolean status { get; set; }
    }
}