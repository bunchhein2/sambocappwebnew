﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Dtos
{
    [Table("CaseType_tbl")]
    public class CaseTypeDto
    {
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        public string name { get; set; }
        public string note { get; set; }
        public Boolean status { get; set; }

    }
}