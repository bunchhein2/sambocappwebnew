﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Dtos
{
    [Table("Civilcasetribunal_tbl")]
    public class CivilcaseTribunalDto
    {
        [Required]
        public int id { get; set; }
        public DateTime date { get; set; }
        public int civilcaseid { get; set; }
        public int tribunalid { get; set; }
        public int lawyerid { get; set; }
        public int judgeid { get; set; }
        public int clerkid { get; set; }
        public int officerid { get; set; }
        public string location { get; set; }
        public string note { get; set; }
        public Boolean status { get; set; }
    }
}