﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Dtos
{
    [Table("Civilcasepayment_tbl")]

    public class CivilcasepaymentDto
    {
        [Required]
        public int id { get; set; }
        public DateTime date { get; set; }
        public int civilcaseid { get; set; }
        public string paidby { get; set; }
        public decimal paidamount { get; set; }
        public string note { get; set; }
        public Boolean status { get; set; }
        public DateTime duedate { get; set; }
        public string referred { get; set; }
        public string paystatus { get; set; }
        public Boolean billgroup { get; set; }
        public DateTime paiddate { get; set; }
        public int paymentmethodid { get; set; }
    }
}