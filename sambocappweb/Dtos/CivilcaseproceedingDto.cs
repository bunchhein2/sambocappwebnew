﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Dtos
{
    [Table("Civilcaseproceeding_tbl")]
    public class CivilcaseproceedingDto
    {
        public int id { get; set; }
        public DateTime date { get; set; }
        public int civilcaseid { get; set; }
        public int lawyerid { get; set; }
        public int proccedingtypeid { get; set; }
        public Boolean status { get; set; }
    }
}