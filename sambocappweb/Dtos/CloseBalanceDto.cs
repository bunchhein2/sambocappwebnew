﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using sambocappweb.Models;

namespace sambocappweb.Dtos
{
    public class CloseBalanceDto
    {
        public int id { get; set; }
        public DateTime? date { get; set; }
        public decimal startbalance { get; set; }
        public decimal civilcasepaid { get; set; }
        //public Civilcasepayment civilcasepayment { get; set; }
        public decimal contractpaid { get; set; }
        //public ContractPayment contractPayment { get; set; }
        public decimal otherincome { get; set; }
        public decimal otherexpend { get; set; }
        //public OtherExpense otherExpense { get; set; }
        public decimal endbalance { get; set; }
    }
}