﻿using sambocappweb.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Dtos
{
    [Table("contract_tbl")]
    public class ContractDto
    {
		public int contractid { get; set; }
		public int shopid { get; set; }
		public Shop shopName { get; set; }
		public DateTime? date { get; set; }
		public string attachment { get; set; }
		public string note { get; set; }
		public bool status { get; set; }
	}
}