﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sambocappweb.Dtos
{
    public class ContractExpanceDetailDto
    {
        public int id { get; set; }
        public int contractexpenseid { get; set; }
        public string description { get; set; }
        public decimal amount { get; set; }
        public int paymentmethodid { get; set; }
    }
}