﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sambocappweb.Dtos
{
    public class ContractPositionDto
    {
        public int id { get; set; }
        public string name { get; set; }
        public string note { get; set; }
    }
}