﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sambocappweb.Dtos
{
    public class ContractProceedingDto
    {
		public int id { get; set; }
		public DateTime? date { get; set; }
		public int contractid { get; set; }
        public int contract { get; set; }
        public int lawyerid { get; set; }
        public string lawyername { get; set; }
        public int proceedingtypeid { get; set; }
		public string proccedingtype { get; set; }
		public bool status { get; set; }
	}
}