﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Dtos
{
    [Table("currency_tbl")]
    public class CurrencyDto
    {
        public int id { get; set; }

        [StringLength(50)]
        public string currencyname { get; set; }

        [StringLength(50)]
        public string sign { get; set; }
        public string status { get; set; }
    }
}