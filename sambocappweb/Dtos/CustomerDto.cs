﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace sambocappweb.Dtos
{

    public class CustomerDto
    {
        public int id { get; set; }
        public DateTime date { get; set; }
        public string phone { get; set; }
        public string tokenid { get; set; }
        public string currentLocation { get; set; }
        [Required]
        public string customerName { get; set; }
        public string gender { get; set; }
        public string imageProfile { get; set; }
        public string password { get; set; }
    }
}