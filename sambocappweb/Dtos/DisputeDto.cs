﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sambocappweb.Dtos
{
    public class DisputeDto
    {
        public int id { get; set; }
        public string deputenNme { get; set; }
        public string deputeNote { get; set; }
        public bool status { get; set; }
    }
}