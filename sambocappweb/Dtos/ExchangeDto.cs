﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Dtos
{

    [Table("exchange_tbl")]
    public class ExchangeDto
    {
        [Required]
        public int id { get; set; }
        [Required]
        public DateTime date { get; set; }
        [Required]
        public int currencyid { get; set; }
        [Required]
        public int shopid { get; set; }
        [Required]
        public decimal rate { get; set; }
    }
}