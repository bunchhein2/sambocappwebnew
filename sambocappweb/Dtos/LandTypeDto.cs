﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sambocappweb.Dtos
{
    public class LandTypeDto
    {
        public int id { get; set; }
        public string land { get; set; }
        public string note { get; set; }
    }
}