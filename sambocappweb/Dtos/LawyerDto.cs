﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Dtos
{
    [Table("Lawyer_tbl")]
    public class LawyerDto
    {
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        public string name { get; set; }
        public string sex { get; set; }
        public string age { get; set; }
        public string address { get; set; }
        public string identityno { get; set; }
        public string phone { get; set; }
        public string photo { get; set; }
        public Boolean status { get; set; }
    }
}