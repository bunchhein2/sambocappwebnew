﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sambocappweb.Dtos
{
    public class LetterTypeDto
    {
        public int id { get; set; }
        public string letter { get; set; }
        public string note { get; set; }
    }
}