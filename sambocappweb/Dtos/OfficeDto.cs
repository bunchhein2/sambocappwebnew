﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Dtos
{
    [Table("office_tbl")]
    public class OfficeDto
    {
        public int id { get; set; }
        [Required]
        [StringLength(50)]
        public string officename { get; set; }
        
        public string officenote { get; set; }
    }
}