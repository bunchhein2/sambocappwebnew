﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sambocappweb.Dtos
{
    public class OwnerShipDto
    {
        public int id { get; set; }
        public DateTime? date { get; set; }
        public string shiptype { get; set; }
        public string ownermalename { get; set; }
        public string ownermalephone { get; set; }
        public string ownerfemalename { get; set; }
        public string ownerfemalephone { get; set; }
        public string gotmalename { get; set; }
        public string gotmalephone { get; set; }
        public string gotfemalename { get; set; }
        public string gotfemalephone { get; set; }
        public int propertyid { get; set; }
        public int letterid { get; set; }
        public string letternumber { get; set; }
        public DateTime? letterdate { get; set; }
        public string location { get; set; }
        public int landtypeid { get; set; }
        public string landnote { get; set; }
        public string hometype { get; set; }
        public string createby { get; set; }
        public DateTime? createdate { get; set; }
    }
}