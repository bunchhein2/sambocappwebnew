﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sambocappweb.Dtos
{
    public class ShipProccessDto
    {
        public int id { get; set; }
        public string type { get; set; }
        public DateTime? date { get; set; }
        public DateTime? datein { get; set; }
        public DateTime? dateout { get; set; }
        public string note { get; set; }
        public string createby { get; set; }
        public DateTime? createdate { get; set; }
        public int ownershipid { get; set; }
    }
}