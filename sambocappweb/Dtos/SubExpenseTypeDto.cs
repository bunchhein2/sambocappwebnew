﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Dtos
{
    [Table("subexpensetype_tbl")]
    public class SubExpenseTypeDto
    {
        [Key]
        [Required]
        public int id { get; set; }
        public string subtypename { get; set; }
        public int subexpensetypeid { get; set; }

    }
}