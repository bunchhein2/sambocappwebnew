﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Dtos
{
        [Table("Tribunal_tbl")]
        public class TribunalDto
    {
            [Required]
            public int id { get; set; }
            public string tribunalname { get; set; }
            public string tribunalnote { get; set; }
            public Boolean tribunalstatus { get; set; }
        }
    }