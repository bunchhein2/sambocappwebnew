namespace sambocappweb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initProject : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.tbl_customer",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        date = c.DateTime(nullable: false),
                        phone = c.String(),
                        tokenid = c.String(),
                        currentLocation = c.String(),
                        customerName = c.String(nullable: false),
                        imageProfile = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.tbl_DeliveryType",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        delivery_name = c.String(nullable: false, maxLength: 100),
                        status = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
            //CreateTable(
            //    "dbo.tbl_feedBackSystem",
            //    c => new
            //        {
            //            id = c.Int(nullable: false, identity: true),
            //            feedbackDate = c.DateTime(nullable: false),
            //            memberId = c.Int(nullable: false),
            //            note = c.String(),
            //        })
            //    .PrimaryKey(t => t.id);
            
            //CreateTable(
            //    "dbo.tbl_generalEducation",
            //    c => new
            //        {
            //            id = c.Int(nullable: false, identity: true),
            //            userid = c.String(maxLength: 100),
            //            fromYear = c.Int(nullable: false),
            //            toYear = c.Int(nullable: false),
            //            note = c.String(),
            //        })
            //    .PrimaryKey(t => t.id);
            
            //CreateTable(
            //    "dbo.tbl_guideline",
            //    c => new
            //        {
            //            id = c.Int(nullable: false, identity: true),
            //            titleGuideline = c.String(nullable: false, maxLength: 100),
            //            guidelineNote = c.String(maxLength: 100),
            //            createBy = c.String(maxLength: 100),
            //            createDate = c.DateTime(nullable: false),
            //        })
            //    .PrimaryKey(t => t.id);
            
            //CreateTable(
            //    "dbo.tbl_newEvent",
            //    c => new
            //        {
            //            id = c.Int(nullable: false, identity: true),
            //            newEventDate = c.DateTime(nullable: false),
            //            newEventTitle = c.String(nullable: false, maxLength: 100),
            //            newEventImage = c.String(),
            //            newEventNote = c.String(maxLength: 100),
            //            status = c.String(maxLength: 100),
            //            createBy = c.String(maxLength: 100),
            //            createDate = c.DateTime(nullable: false),
            //        })
            //    .PrimaryKey(t => t.id);
            
            //CreateTable(
            //    "dbo.question_tbl",
            //    c => new
            //        {
            //            id = c.Int(nullable: false, identity: true),
            //            questiontypeid = c.Int(nullable: false),
            //            question = c.String(nullable: false, maxLength: 255),
            //            image = c.String(maxLength: 255),
            //            minute = c.Decimal(nullable: false, precision: 18, scale: 2),
            //            score = c.Decimal(nullable: false, precision: 18, scale: 2),
            //        })
            //    .PrimaryKey(t => t.id)
            //    .ForeignKey("dbo.questiontype_tbl", t => t.questiontypeid)
            //    .Index(t => t.questiontypeid);
            
            //CreateTable(
            //    "dbo.questiontype_tbl",
            //    c => new
            //        {
            //            id = c.Int(nullable: false, identity: true),
            //            questiontypenote = c.String(nullable: false, maxLength: 255),
            //        })
            //    .PrimaryKey(t => t.id);
            
            //CreateTable(
            //    "dbo.tbl_registration",
            //    c => new
            //        {
            //            id = c.Int(nullable: false, identity: true),
            //            date = c.DateTime(nullable: false),
            //            userid = c.String(maxLength: 100),
            //            firstName = c.String(maxLength: 100),
            //            lastName = c.String(maxLength: 100),
            //            firstNameInKh = c.String(maxLength: 100),
            //            lastNameInKh = c.String(maxLength: 100),
            //            dob = c.DateTime(nullable: false),
            //            gender = c.String(maxLength: 100),
            //            marrientalStatus = c.String(maxLength: 100),
            //            address = c.String(maxLength: 100),
            //            phone = c.String(maxLength: 100),
            //            email = c.String(maxLength: 100),
            //            workPlace = c.String(maxLength: 100),
            //            workAddress = c.String(maxLength: 100),
            //            genderalEducationId = c.Int(nullable: false),
            //            language = c.String(maxLength: 100),
            //            skillId = c.Int(nullable: false),
            //            status = c.String(maxLength: 100),
            //        })
            //    .PrimaryKey(t => t.id)
            //    .ForeignKey("dbo.tbl_skill", t => t.skillId)
            //    .Index(t => t.skillId);
            
            //CreateTable(
            //    "dbo.tbl_skill",
            //    c => new
            //        {
            //            id = c.Int(nullable: false, identity: true),
            //            skillTittle = c.String(nullable: false, maxLength: 100),
            //            skillDescription1 = c.String(maxLength: 100),
            //            status = c.String(maxLength: 100),
            //            createBy = c.String(maxLength: 100),
            //            createDate = c.DateTime(nullable: false),
            //        })
            //    .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.tbl_shop",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        shopName = c.String(nullable: false, maxLength: 100),
                        ownerName = c.String(nullable: false, maxLength: 100),
                        phone = c.String(nullable: false),
                        password = c.String(nullable: false),
                        tokenid = c.String(nullable: false),
                        facebookPage = c.String(),
                        location = c.String(nullable: false),
                        logoShop = c.String(),
                        paymentType = c.String(nullable: false),
                        qrCodeShopName = c.String(),
                        qrCodeImage = c.String(),
                        bankName = c.String(nullable: false),
                        accountNumber = c.String(nullable: false),
                        accountName = c.String(nullable: false, maxLength: 100),
                        status = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
            //CreateTable(
            //    "dbo.tbl_video",
            //    c => new
            //        {
            //            id = c.Int(nullable: false, identity: true),
            //            video_title = c.String(nullable: false, maxLength: 100),
            //            video = c.String(maxLength: 100),
            //            createBy = c.String(maxLength: 100),
            //            createDate = c.DateTime(nullable: false),
            //        })
            //    .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.tbl_registration", "skillId", "dbo.tbl_skill");
            DropForeignKey("dbo.question_tbl", "questiontypeid", "dbo.questiontype_tbl");
            DropIndex("dbo.tbl_registration", new[] { "skillId" });
            DropIndex("dbo.question_tbl", new[] { "questiontypeid" });
            DropTable("dbo.tbl_video");
            DropTable("dbo.tbl_shop");
            DropTable("dbo.tbl_skill");
            DropTable("dbo.tbl_registration");
            DropTable("dbo.questiontype_tbl");
            DropTable("dbo.question_tbl");
            DropTable("dbo.tbl_newEvent");
            DropTable("dbo.tbl_guideline");
            DropTable("dbo.tbl_generalEducation");
            DropTable("dbo.tbl_feedBackSystem");
            DropTable("dbo.tbl_DeliveryType");
            DropTable("dbo.tbl_customer");
        }
    }
}
