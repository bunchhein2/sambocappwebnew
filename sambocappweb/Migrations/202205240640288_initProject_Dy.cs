namespace sambocappweb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initProject_Dy : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Currencies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CurrencyName = c.String(maxLength: 50),
                        Sign = c.String(maxLength: 50),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Exchange",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RateIn = c.Decimal(nullable: false, precision: 18, scale: 2),
                        RateOut = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ExchangeDate = c.DateTime(nullable: false),
                        Description = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        InvoiceNo = c.Int(nullable: false),
                        Date = c.DateTime(nullable: false),
                        ShopId = c.Int(nullable: false),
                        CustomerId = c.Int(nullable: false),
                        DeliveryTypeIn = c.String(),
                        CurrentLocation = c.String(),
                        Phone = c.String(nullable: false, maxLength: 50),
                        PaymentType = c.String(),
                        QrcodeShopName = c.String(nullable: false, maxLength: 50),
                        BankName = c.String(nullable: false, maxLength: 50),
                        AccountNumber = c.String(nullable: false, maxLength: 50),
                        AccountName = c.String(nullable: false, maxLength: 50),
                        ReceiptUpload = c.String(nullable: false, maxLength: 100),
                        AmountTobePaid = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ExchangeId = c.Int(nullable: false),
                        Status = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.tbl_customer", t => t.CustomerId)
                .ForeignKey("dbo.Exchange", t => t.ExchangeId)
                .ForeignKey("dbo.tbl_shop", t => t.ShopId)
                .Index(t => t.ShopId)
                .Index(t => t.CustomerId)
                .Index(t => t.ExchangeId);
            
            CreateTable(
                "dbo.Privacies",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        description = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ShopId = c.Int(nullable: false),
                        ProductCode = c.String(nullable: false, maxLength: 50),
                        ProductName = c.String(nullable: false, maxLength: 50),
                        Description = c.String(nullable: false, maxLength: 50),
                        QtyInStock = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CurrencyId = c.Int(nullable: false),
                        CutStockType = c.String(nullable: false, maxLength: 50),
                        ExpiredDate = c.DateTime(nullable: false),
                        LinkVideo = c.String(nullable: false, maxLength: 100),
                        ImageThumbnail = c.String(nullable: false, maxLength: 100),
                        Status = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Currencies", t => t.CurrencyId)
                .ForeignKey("dbo.tbl_shop", t => t.ShopId)
                .Index(t => t.ShopId)
                .Index(t => t.CurrencyId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Products", "ShopId", "dbo.tbl_shop");
            DropForeignKey("dbo.Products", "CurrencyId", "dbo.Currencies");
            DropForeignKey("dbo.Orders", "ShopId", "dbo.tbl_shop");
            DropForeignKey("dbo.Orders", "ExchangeId", "dbo.Exchange");
            DropForeignKey("dbo.Orders", "CustomerId", "dbo.tbl_customer");
            DropIndex("dbo.Products", new[] { "CurrencyId" });
            DropIndex("dbo.Products", new[] { "ShopId" });
            DropIndex("dbo.Orders", new[] { "ExchangeId" });
            DropIndex("dbo.Orders", new[] { "CustomerId" });
            DropIndex("dbo.Orders", new[] { "ShopId" });
            DropTable("dbo.Products");
            DropTable("dbo.Privacies");
            DropTable("dbo.Orders");
            DropTable("dbo.Exchange");
            DropTable("dbo.Currencies");
        }
    }
}
