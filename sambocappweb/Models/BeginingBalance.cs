﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    [Table("beginingbalance_tbl")]
    public class BeginingBalance
    {
        [Required]
        public int id { get; set; }
        public DateTime date { get; set; }
        public int paymentmethodid { get; set; }
        public Decimal amount { get; set; }
    }
}