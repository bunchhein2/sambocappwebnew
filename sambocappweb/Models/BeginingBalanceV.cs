﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    public class BeginingBalanceV
    {
        [Required]
        public int id { get; set; }
        public DateTime date { get; set; }
        public int paymentmethodid { get; set; }
        public Decimal amount { get; set; }
        public string paymentmethodname { get; set; }
    }
}