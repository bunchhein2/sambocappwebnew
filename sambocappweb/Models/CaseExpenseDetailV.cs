﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    public class CaseExpenseDetailV
    {
        public int id { get; set; }
        public int caseexpenseid { get; set; }
        public int Caseexpense { get; set; }
        public string description { get; set; }
        public decimal amount { get; set; }
        public int paymentmethodid { get; set; }
        public string paymentMethod { get; set; }
    }
}