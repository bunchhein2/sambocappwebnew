﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    [Table("caseexpensedetail_tbl")]
    public class CaseexpenseDetail
    {
        public int id { get; set; }
        public int caseexpenseid { get; set; }
        public Caseexpense Caseexpense { get; set; }
        public string description { get; set; }
        public decimal amount { get; set; }
        public int paymentmethodid { get; set; }
        public PaymentMethod PaymentMethod { get; set; }

    }
}