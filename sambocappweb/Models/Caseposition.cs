﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    [Table("caseposition_tbl")]
    public class Caseposition
    {
        public int id { get; set; }
        [Required]
        [StringLength(50)]
        public string casepositionname { get; set; }
        public string casepositionnote { get; set; }
    }
}