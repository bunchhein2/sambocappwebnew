﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    [Table("Civilcase_tbl")]
    public class CivilCase
    {
        public int id { get; set; }
        public DateTime civilcasedate { get; set; }
        [Required]
        public int civilcaseno { get; set; }
        [Required]
        public int casetypeid { get; set; }
        [Required]
        public int defendentid { get; set; }
        [Required]
        public int plaintiffid { get; set; }
        [Required]
        public int caseid { get; set; }
        [Required]
        public int lawyerid { get; set; }
        public decimal charges { get; set; }
        public decimal services { get; set; }
        public string casestatus { get; set; }
        public string paidstatus { get; set; }
        public Boolean status { get; set; }
        public string createby { get; set; }
        public DateTime createdate { get; set; }
        public int lawrelateid { get; set; }
        public int customerid { get; set; }
        public string documentno { get; set; }
        public int casepositionid { get; set; }
        public int officeid { get; set; }

    }
}