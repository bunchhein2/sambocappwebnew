﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    public class CivilcaseProceedingV
    {
        [Required]
        public int id { get; set; }
        public DateTime date { get; set; }
        public int civilcaseid { get; set; }
        public int lawyerid { get; set; }
        public int proccedingtypeid { get; set; }
        public Boolean status { get; set; }
        public int civilcaseno { get; set; }
        public string lawyername { get; set; }
        public string proccedingtype { get; set; }
    }
}