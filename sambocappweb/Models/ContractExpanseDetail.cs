﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    [Table("Contractexpensedetail_tbl")]
    public class ContractExpanseDetail
    {
        public int id { get; set; }
        public int contractexpenseid { get; set; }
        public string description { get; set; }
        public decimal amount { get; set; }
        public int paymentmethodid { get; set; }
    }
}