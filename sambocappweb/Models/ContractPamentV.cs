﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    public class ContractPamentV
    {
        public int id { get; set; }
        public DateTime? date { get; set; }
        public int contractid { get; set; }
        public int contract { get; set; }
        public string paidby { get; set; }
        public decimal paidamount { get; set; }
        public string note { get; set; }
        public bool status { get; set; }
        public int paymentmethodid { get; set; }
        public string paymentMethod { get; set; }
    }
}