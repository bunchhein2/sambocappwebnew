﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
	[Table("Contractpayment_tbl")]
    public class ContractPayment
    {
		public int id { get; set; }
		public DateTime? date {get;set;}
		public int contractid { get; set; }
        public Contract contract { get; set; }
        public string paidby { get; set; }
		public decimal paidamount { get; set; }
		public string note { get; set; }
		public bool status { get; set; }
        public int paymentmethodid { get; set; }
		public PaymentMethod PaymentMethod { get; set; }

	}
}