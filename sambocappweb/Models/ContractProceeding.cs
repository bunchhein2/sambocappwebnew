﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
	[Table("Contractproceeding_tbl")]
	  public class ContractProceeding
	  {
		  public int id { get; set; }
		  public DateTime? date {get;set;}
		  public int contractid { get; set; }
		  public int lawyerid { get; set; }
		  public int proceedingtypeid { get; set; }
		  public bool status { get; set; }
	  }
}