﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    public class ContractV
    {
		public int id { get; set; }
		public DateTime? date { get; set; }
		public int officeid { get; set; }
        public string Officename { get; set; }
        public string contractid { get; set; }
		public int lawyerlateid { get; set; }
        public string Lawyerrelatename { get; set; }
        public int disputeid { get; set; }
        public string Disputename { get; set; }
        public int sellerid { get; set; }
        public string Sellername { get; set; }
        public int buyerid { get; set; }
        public string Buyername { get; set; }
        public int lawyerid { get; set; }
		public string Lawyername { get; set; }
		public int contractposition { get; set; }
        public string Contractpositionname { get; set; }
        public int customerid { get; set; }
        public string Customername { get; set; }
        public decimal? charges { get; set; }
		public decimal? service { get; set; }
		public string contractstatus { get; set; }
		public string paidstatus { get; set; }
		public string contractaddress { get; set; }
		public string datechankate { get; set; }
		public string datesorikate { get; set; }
		public string createby { get; set; }
		public DateTime? createdate { get; set; }
		public string updateby { get; set; }
		public DateTime? updatedate { get; set; }
		public bool status { get; set; }
	}
}