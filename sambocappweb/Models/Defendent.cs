﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    [Table("Defendent_tbl")]
    public class Defendent
    {
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        public string defendentName { get; set; }
        public string defendentSex { get; set; }
        public string defendentAge { get; set; }
        public string defendentAddress { get; set; }
        public DateTime defendentDob { get; set; }
        public string defendentIdentityno { get; set; }
        public string defendentPhone { get; set; }
        public string defendentPhoto { get; set; }
        public Boolean defendentStatus { get; set; }
        public int civilcaseid { get; set; }
    }
}