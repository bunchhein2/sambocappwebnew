﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity;

namespace sambocappweb.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        [Required]
        [Display(Name = "First Name")]
        [StringLength(255)]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        [StringLength(255)]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "Fullname In Khmer")]
        [StringLength(255)]
        public string FullNameInKhmer { get; set; }

        [Required]
        [Display(Name = "Contact Phone")]
        [StringLength(255)]
        public string ContactPhone { get; set; }

        [Required]
        [Display(Name = "Gender")]
        [StringLength(6)]
        public string Gender { get; set; }

        [Required]
        [Display(Name = "Position")]
        [StringLength(255)]
        public string Position { get; set; }

        [Required]
        [Display(Name = "Telegram ID")]
        [StringLength(255)]
        public string TelegramId { get; set; }

        [Display(Name = "Is Super User")]
        public bool IsSuperUser { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<ShopPayment> ShopPayments { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<QuestionType> QuestionTypes { get; set; }
        public DbSet<GuideLine> GuideLines { get; set; }
        public DbSet<FeedBackSystem> FeedBackSystems { get; set; }
        public DbSet<NewEvent> NewEvents { get; set; }
        public DbSet<Video> Videos { get; set; }
        public DbSet<GeneralEducation> Generaleducations { get; set; }
        public DbSet<Registration> Registrations { get; set; }
        public DbSet<Skill> Skills { get; set; }
        public DbSet<DeliveryType> DeliveryTypes { get; set; }
        public DbSet<Shop> Shops { get; set; }
        public DbSet<Customer> Customers { get; set; }
       
        
        public DbSet<Department> Departments { get; set; }
       
        
        public DbSet<LoginHistory> LoginHistories { get; set; }
        public DbSet<BlogCategory> BlogCategories { get; set; }
        public DbSet<BlogPost> BlogPosts { get; set; }
        public DbSet<Feedback> Feedbacks { get; set; }
        public DbSet<Rank> Ranks { get; set; }
        
        public DbSet<ReportType> ReportTypes { get; set; }
        public DbSet<Report> Reports { get; set; }
        
        
        
        public DbSet<Order> Orders { get; set; }        
        public DbSet<Product> Products { get; set; }

        public DbSet<Currency> Currencies { get; set; }
        public DbSet<Privacy> Privacies { get; set; }
        public DbSet<Orderdetail> Orderdetails { get; set; }
        public DbSet<Exchange> Exchanges { get; set; }
        public DbSet<Banner> Banners { get; set; }
        public DbSet<ProductImage> ProductImages { get; set; }
        public DbSet<PaymentMethod> PaymentMethods { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<Contract> Contracts { get; set; }



        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            base.OnModelCreating(modelBuilder);
        }

        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}