﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    public class InvitationDetail
    {
        public int Id { get; set; }

        public int InvitationId { get; set; }

        public Invitation Invitation { get; set; }

        [Required]
        public int CivilServantToId { get; set; }
        public CivilServant CivilServantTo { get; set; }
    }
}