﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
namespace sambocappweb.Models
{
    [Table("paymentmethodBalance_tbl")]
    public class PaymentMethodBalance
    {
        [Required]
        public int id { get; set; }
        public string paymentmethodname { get; set; }
    }
}