﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    [Table("Plaintiff_tbl")]
    public class Plaintiff
    {
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        public string plaintiffName { get; set; }
        public string plaintiffSex { get; set; }
        public string plaintiffAge { get; set; }
        public string plaintiffAddress { get; set; }
        public DateTime plaintiffDob { get; set; }
        public string plaintiffIdentityno { get; set; }
        public string plaintiffPhone { get; set; }
        public string plaintiffPhoto { get; set; }
        
        public Boolean status { get; set; }
        public int civilcaseid { get; set; }
    }
}