﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    [Table("Products")]
    public class Product
    {
        public int Id { get; set; }

        public int ShopId { get; set; }
        public Shop Shop { get; set; }

        [Required]
        [StringLength(50)]
        public string ProductCode { get; set; }

        [Required]
        [StringLength(50)]
        public string ProductName { get; set; }

        [Required]
        [StringLength(50)]
        public string Description { get; set; }

        public int QtyInStock { get; set; }

        public decimal Price { get; set; }
        public int CurrencyId { get; set; }
        public Currency Currency { get; set; }


        [Required]
        [StringLength(50)]
        public string CutStockType { get; set; }
        public DateTime ExpiredDate { get; set; }

        [StringLength(100)]
        public string LinkVideo { get; set; }

        [StringLength(100)]
        public string ImageThumbnail { get; set; }

        [StringLength(50)]
        public string Status { get; set; }

        ////Tbl_Pro-Image
        //[Required]
        //public int id { get; set; }
        //[Required]
        //public int productid { get; set; }
        //[Required]
        //public string productimage { get; set; }

    }

}