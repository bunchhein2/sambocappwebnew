﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    [Table("product_image_tbl")]
    public class ProductImage
    {
        [Required]
        public int id { get; set; }
        [Required]
        public int productid { get; set; }
        [Required]
        public string productimage { get; set; }
    }
}