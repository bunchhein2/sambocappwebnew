﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    public class Report
    {
        public int Id { get; set; }

        [Required]
        [StringLength(128)]
        public string UserId { get; set; }

        public ApplicationUser User { get; set; }

       

        

        [Required]
        public int ReportTypeId { get; set; }

        public ReportType ReportType { get; set; }

        [Required]
        [StringLength(1020)]
        public string Objective { get; set; }

        [StringLength(1020)]
        public string Via { get; set; }

        public string Content { get; set; }

        public int Durations { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        [StringLength(1020)]
        public string Location { get; set; }

        [StringLength(1020)]
        public string Reference { get; set; }
    }
}