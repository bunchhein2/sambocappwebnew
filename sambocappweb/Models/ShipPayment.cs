﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    [Table("shippayment_tbl")]
    public class ShipPayment
    {
        public int id { get; set; }
        public int ownershipid { get; set; }
        public DateTime? date { get; set; }
        public decimal amount { get; set; }
        public string note { get; set; }
        public string createby { get; set; }
        public DateTime? createdate { get; set; }
        public string updateby { get; set; }
        public DateTime? updatedate { get; set; }
    }
}