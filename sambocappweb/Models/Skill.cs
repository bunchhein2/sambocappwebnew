﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    [Table("tbl_skill")]
    public class Skill
    {
        public int id { get; set; }
        [Required]
        [StringLength(100)]
        public string skillTittle { get; set; }
        [StringLength(100)]
        public string skillDescription1 { get; set; }
        [StringLength(100)]
        public string status { get; set; }
        [StringLength(100)]
        public string createBy { get; set; }
        public DateTime createDate { get; set; }

    }
}