﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
 
    [Table("subincometype_tbl")]
    public class SubIncomeType
    {
        [Key]
        public int id { get; set; }
        [Required]
        public int subincometypeid { get; set; }
        [Required]
        public string subincometypename { get; set; }

    }
}