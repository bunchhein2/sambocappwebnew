﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    [Table("tbl_video")]
    public class Video
    {
        public int id { get; set; }
        [Required]
        [StringLength(100)]
        public string video_title { get; set; }
        [StringLength(100)]
        public string video { get; set; }
        [StringLength(100)]
        public string createBy { get; set; }
        public DateTime createDate { get; set; }

    }
}