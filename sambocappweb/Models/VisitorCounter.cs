﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    public class VisitorCounter
    {
        public int Id { get; set; }

        [Required]
        public int Counter { get; set; }

        [Required]
        public DateTime? ViewDate { get; set; }
    }
}