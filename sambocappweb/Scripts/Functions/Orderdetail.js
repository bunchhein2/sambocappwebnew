﻿
$(document).ready(function () {
    $("#OrderdetailFrm").on('show.modal.bs', function () {
        GetOrderdetail();
        oTable = $('#tblOrderdetail').DataTable();
        oTable.columns(1).search($('#iddiatal').val().trim());
        //hit search on server
        oTable.draw();
    });
    //GetOrderdetail();

});

//$("#btnbanner").click(function () {
//    $("#txtbtnbanner").val("save");
//    document.getElementById('btnorderdetail').innerText = "Save";
//});
var tableOrderdetail = [];
function GetOrderdetail() {
    var iddiatal = $("#iddiatal").val();
    tableOrderdetail = $('#tblOrderdetail').DataTable({
        ajax: {
            url: "/api/Orderdetails/" + iddiatal+"/detail",
            dataSrc: ""
        },
        columns: [
            
            {
                data: "id",
                render: function (data) {

                    return "" + ("");
                }
            },
            {
                data: "orderid"
            },
            {
                data: "date",
                render: function (data) {
                    return moment(new Date(data)).format('DD-MM-YYYY');
                }
            },
            {
                data: "productname"
            }
            , 
            {
                data: "qty"
            },
            {
                data: "price",
            },
            {
                data: "discount"
            },

            {
                data: "id",
                render: function (data) {
                    return "<button type='button' onclick='OrderdetailDelete(" + data + ")' class='btn btn-danger btn-xs' style='border-width: 0px;'><span class='glyphicon glyphicon-trash'></span> Delete</button>";
                    //+ "<button type='button' onclick='OrderdetailEdit(" + data + ")' class='btn btn-warning btn-xs' style='border-width: 0px; width: 65px; margin-right: 5px;'><span class='glyphicon glyphicon-edit'></span> Edit</button>";
                },
                "width": "130px"
            }
        ],
        destroy: true,
        "order": [[0, "desc"]],
        "info": true
    });
}

function CreateOrderdetail() {

    var action = '';
    action = $("#txtbtnbanner").val();
    if (action == "save") {
        Validate();

    } else if (action == "update") {
        Validate_Update();
    }

}
function OrderdetailEdit(id) {
    //action = document.getElementById('btnorderdetail').innerText = "Update";
    $("#txtbtnbanner").val("update");
    $.ajax({
        url: "/api/Orderdetails/" + id,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            var data = new FormData();
            
            $('#id').val(result.id);
            
            $('#orderid2').val(result.orderid);
            $('#orderproductid').val(result.productid);
            $('#orderqty').val(result.qty);
            $('#orderprice').val(result.price);
            $('#orderdiscount').val(result.discount);
            $("#OrderdetailModel").modal('show');
        },
        error: function (errormessage) {
            toastr.error("Something unexpected happenn.", "Server Response");
        }
    });
}
function OrderdetailDelete(id) {
    // alert('hi');
    bootbox.confirm({
        title: "Confirmation",
        message: "Are you sure you want to delete this?",
        buttons: {
            cancel: {
                label: "Cancel",
                className: "btn-default"
            },
            confirm: {
                label: "Delete",
                className: "btn-danger"
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: "/api/Orderdetails/" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        $('#tblExchange').DataTable().ajax.reload();
                        toastr.success("Item Deleted successfully!", "Service Response");
                        $('#tblOrderdetail').DataTable().ajax.reload();
                    },
                    error: function (errormessage) {
                        toastr.error("This Item is already exists in Database", "Service Response");
                    }
                });
            }
        }
    });

}
function Validate() {
    if ($('#orderid2').val().trim() === "") {
        $('#orderid2').css('border-color', 'red');
        $('#orderid2').focus();
        toastr.info("Please enter orderid", "Required");
    } else if ($('#orderproductid').val().trim() === "") {
        $('#orderproductid').css('border-color', 'red');
        $('#orderproductid').focus();
        toastr.info("Please enter orderproductid", "Required");
    } else if ($('#orderqty').val().trim() === "") {
        $('#orderqty').css('border-color', 'red');
        $('#orderqty').focus();
        toastr.info("Please enter qty", "Required");
    } else if ($('#orderprice').val().trim() === "") {
        $('#orderprice').css('border-color', 'red');
        $('#orderprice').focus();
        toastr.info("Please enter price", "Required");
    } else if ($('#orderdiscount').val().trim() === "") {
        $('#orderdiscount').css('border-color', 'red');
        $('#orderdiscount').focus();
        toastr.info("Please enter discount", "Required");
    } else {
                $('#orderid2').css('border-color', '#cccccc');
                $('#orderproductid').css('border-color', '#cccccc');
                $('#orderqty').css('border-color', '#cccccc');
                $('#orderprice').css('border-color', '#cccccc');
                $('#orderdiscount').css('border-color', '#cccccc');
                var data = new FormData();
                data.append("orderid", $('#orderid2').val());
                data.append("orderproductid", $('#orderproductid').val());
                data.append("orderqty", $('#orderqty').val());
                data.append("orderprice", $('#orderprice').val());
                data.append("orderdiscount", $('#orderdiscount').val());
                $.ajax({
                    url: "/api/Orderdetails",
                    data: data,
                    type: "POST",
                    contentType: false,
                    processData: false,
                    success: function (result) {
                        toastr.success("New Banner has been Created", "Server Respond");
                        $('#tblOrderdetail').DataTable().ajax.reload();
                        //$("#OrderdetailModel").modal('hide');
                        ClearControl();


                    },
                    error: function (errormesage) {
                        $('#orderid').focus();
                        toastr.error("This Name is exist in Database", "Server Respond")
                    }

                });
           }
}
function Validate_Update(){
    if ($('#orderid2').val().trim() === "") {
        $('#orderid2').css('border-color', 'red');
        $('#orderid2').focus();
        toastr.info("Please enter orderid", "Required");
    } else if ($('#orderproductid').val().trim() === "") {
        $('#orderproductid').css('border-color', 'red');
        $('#orderproductid').focus();
        toastr.info("Please enter orderproductid", "Required");
    } else if ($('#orderqty').val().trim() === "") {
        $('#orderqty').css('border-color', 'red');
        $('#orderqty').focus();
        toastr.info("Please enter qty", "Required");
    } else if ($('#orderprice').val().trim() === "") {
        $('#orderprice').css('border-color', 'red');
        $('#orderprice').focus();
        toastr.info("Please enter price", "Required");
    } else if ($('#orderdiscount').val().trim() === "") {
        $('#orderdiscount').css('border-color', 'red');
        $('#orderdiscount').focus();
        toastr.info("Please enter discount", "Required");
    } else {
        $('#orderid2').css('border-color', '#cccccc');
        $('#orderproductid').css('border-color', '#cccccc');
        $('#orderqty').css('border-color', '#cccccc');
        $('#orderprice').css('border-color', '#cccccc');
        $('#orderdiscount').css('border-color', '#cccccc');
        var data = new FormData();
        data.append("id", $('#id').val());
        data.append("orderid", $('#orderid2').val());
        data.append("orderproductid", $('#orderproductid').val());
        data.append("orderqty", $('#orderqty').val());
        data.append("orderprice", $('#orderprice').val());
        data.append("orderdiscount", $('#orderdiscount').val());
        $.ajax({
            url: "/api/Orderdetails/" + $("#id").val(),
                data: data,
                type: "PUT",
                contentType: false,
                processData: false,
                dataType: "json",
                success: function (result) {
                    toastr.success("exchang has been Updated", "Server Respond");
                    action = document.getElementById('btnorderdetail').innerText = "Save";
                    $('#tblOrderdetail').DataTable().ajax.reload();
                    $("#OrderdetailModel").modal('hide');
                    ClearControl();

                },
                error: function (errormesage) {
                    toastr.error("exchang hasn't Updated in Database", "Server Respond")
                }
           });

        }
}
function ClearControl() {
    $('#id').val('');
    /*$('#orderid').val('');*/
    $('#orderproductid').val('');
    $('#orderqty').val('');
    $('#orderprice').val('');
    $('#orderdiscount').val('');
    document.getElementById('btnorderdetail').innerHTML = "Save";
    $("#txtbtnbanner").val("save");
}