﻿

$(document).ready(function () {
    oTable = $('#uplaodimageTable').DataTable();
    $("#Addmoreimages").on('show.modal.bs', function () {
        GetProductimage();
        oTable = $('#uplaodimageTable').DataTable();
        oTable.columns(1).search($('#productimageid').val().trim());
        //hit search on server
        oTable.draw();
    });
    GetProductimage();

});

var tableProductimage = [];
function GetProductimage() {
    tableProducts = $('#uplaodimageTable').DataTable({
        ajax: {
            url: "/api/ProductImages",
            dataSrc: ""
        },
        columns: [
            {
                data: "id",
                render: function (data) {

                    return "" + ("").slice(-4);
                }
            },
            {
                data: "productid"
            },
            {
                data: "productname"
            },
            
            {
                data: "productimage",
                render: function (data) {

                    return '<image width="40%" src="../Images/' + data + '"/>';
                }
            },
            
            {
                data: "id",
                render: function (data) {
                    return "<button onclick='Delete(" + data + ")' class='btn btn-danger btn-xs' style='border-width: 0px;'><span class='glyphicon glyphicon-trash'></span>Delete</button>";    
                }
            }
        ],
        destroy: true,
        responsive: true,
        "order": [[0, "desc"]]

    });
}
//function ProductDetail(id) {
    
//    $.ajax({
//        url: "/api/ProductImages/" + id,
//        type: "GET",
//        contentType: "application/json;charset=UTF-8",
//        dataType: "json",
//        success: function (result) {

//            $('#productname').val(result.productid);


//            $('#Addmoreimages').modal('show');

//            action = document.getElementById('btnuplaodimage').innerText = "Save";

//        }
//    });
//    return false;
//}

//function ProductImgDelete(id) {
//    bootbox.confirm({
//        title: "Confirmation",
//        message: "Are you sure you want to delete this?",
//        buttons: {
//            cancel: {
//                label: "Cancel",
//                className: "btn-default"
//            },
//            confirm: {
//                label: "Delete",
//                className: "btn-danger"
//            }
//        },
//        callback: function (result) {
//            if (result) {
//                $.ajax({
//                    url: "/api/ProductImages/" + id,
//                    type: "DELETE",
//                    contentType: "application/json;charset=utf-8",
//                    datatype: "json",
//                    success: function (result) {
//                        $('#tableProducts').DataTable().ajax.reload();
//                        toastr.success("Item Deleted successfully!", "Service Response");
//                        tableProducts.ajax.reload();
//                    },
//                    error: function (errormessage) {
//                        toastr.error("This Item is already exists in Database", "Service Response");
//                    }
//                });
//            }
//        }
//    });
    
//}
//function ProductEdits(id) {

//    $.ajax({
//        url: "/api/ProductImages/" + id,
//        type: "GET",
//        contentType: "application/json;charset=UTF-8",
//        dataType: "json",
//        success: function (result) {

//            $('#productname').val(result.productid);

//            //$('#Addmoreimages').modal('show');

//            var action = '';
//            action = document.getElementById('btnuplaodimage').innerText = "Update";
//        }
//    });
//    return false;
//}
//function ProductEdits(id) {
//    var action = '';
//    action = document.getElementById('btnuplaodimage').innerText = "Update";
    
//    $.ajax({
//        url: "/api/ProductImages/" + id,
//        type: "GET",
//        contentType: "application/json;charset=UTF-8",
//        dataType: "json",
//        success: function (result) {
//            $('#Imamgeid').val(result.id);
            
//            $('#productname').val(result.productid);
            
//            if (result.productimage == "") {
//                $('#productimage').attr('src', '../Content/product.jpg');
//            }
//            else {
//                $('#productimage').attr('src', '../Images/' + result.productimage);
//            }
//            //$('#Addmoreimages').modal('show');
//        },
//        statusCode: {
//            400: function () {
//                toastr.error("This product is already exists in database.");
//            }
//        }

//    });

//    return false;

//}

function EditsPro(id) {
    $.ajax({
        url: "/api/ProductImages/" + id,
        type: "GET",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        success: function (result) {
            $('#Imamgeid').val(result.id);

            $('#productname').val(result.productid);

            if (result.productimage == "") {
                $('#productAddIMG').attr('src', '../Content/product.jpg');
            }
            else {
                $('#productAddIMG').attr('src', '../Images/' + result.productimage);
            }
            $('#file_old').val(result.productimage);
            action = document.getElementById('btnuplaodimage').innerText = "Update";
        },
        error: function (errormessage) {
            toastr.error("Something unexpected happenn.", "Server Response");
        }
    });
}

function Delete(id) {
    bootbox.confirm({
        title: "",
        message: "Are you sure want to delete this?",
        button: {
            cancel: {
                label: "Cancel",
                ClassName: "btn btn-default",
            },
            confirm: {
                label: "Delete",
                ClassName: "btn btn-danger"
            }
        },
        callback: function (result) {
            //alert(id);
            if (result) {
                $.ajax({
                    url: "/api/ProductImages/" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        $('#uplaodimageTable').DataTable().ajax.reload();
                        toastr.success("Item Deleted successfully!", "Service Response");
                    },
                    error: function (errormessage) {
                        toastr.error("This Item is already exists in Database", "Service Response");
                    }
                });
            }
        }
    });
}
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#productImg').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

