﻿$(document).ready(function () {
    $(document).ajaxStart(function () {
        Pace.start();
        $("div#divLoadingModal").addClass('show');
    }).ajaxStop(function () {
        Pace.stop();
        $("div#divLoadingModal").removeClass('show');
    });
    $('#fromdate').val(moment().format('YYYY-MM-DD'));
    $('#todate').val(moment().format('YYYY-MM-DD'));
        GetBeginingBalance();
        document.getElementById('balancedate').disabled = true;
        document.getElementById('btnBegining').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
        //$('#balanceamount').val('0');
    //$('#paymentmethodModel').on('show.bs.modal', function () {
    //    GetPaymentMethod();
    //    document.getElementById('balanceamount').disabled = true;
    //    document.getElementById('btnBeginning').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
    //    $('#balanceamount').val('');
    //});
});

var tableCase = [];

function GetStartBalance() {
    var fromdate = $('#fromdate').val();
    var paymentmethodid = $('#paymentmethodids').val();
    $.get("/api/beginingbalances/?fromdate=" + fromdate + "&paymentmethodid=" + paymentmethodid + "&a=1&b=2", function (data, status) {
        // alert("Data: " + data + "\nStatus: " + status);
        $('#beginingbalances').val(data);
        //var arrayData = data.split(',');
        //alert(arrayData[0]);
        //alert(arrayData[1]);
        //$('#civilcaseno').val(arrayData[1]);
        //$('#idnoformat').val(arrayData[0]);

    });
}

function GetEndBalance() {
    var fromdate = $('#fromdate').val();
    var todate = $('#todate').val();
    var paymentmethodid = $('#paymentmethodids').val();
    $.get("/api/beginingbalances/?fromdate=" + fromdate + "&todate=" + todate + "&paymentmethodid=" + paymentmethodid + "&a=1&b=2", function (data, status) {
        // alert("Data: " + data + "\nStatus: " + status);
        $('#endbalances').val(data);
        //var arrayData = data.split(',');
        //alert(arrayData[0]);
        //alert(arrayData[1]);
        //$('#civilcaseno').val(arrayData[1]);
        //$('#idnoformat').val(arrayData[0]);

    });
}

function GetCashflow() {
    var fromdate = $('#fromdate').val();
    var todate = $('#todate').val();

    tableCase = $('#cashflowTable').DataTable({
        ajax: {
            url: "/api/beginingbalances/?fromdate="+ fromdate +"&todate="+todate ,
            dataSrc: ""
        },
        columns: [
             
            {
                data: "paiddate",
                render: function (data) {
                    return moment(new Date(data)).format('DD-MMM-YYYY');
                }
            },
            {
                data: "note"
            },
            {
                data: "paymentmethodname"
            },
            {
                data: "saleamount"
            },
            {
                data: "expenseamount"
            },
            {
                data: "incomeamount"
            },
            {
                data: "transferamountin"
            },
            {
                data: "transferamountout"
            },
            {
                data: "transactiontype"
            }
        ],
        destroy: true,
        "order": [[0, "desc"]],
        "info": false
    });
}

function GetCashflowByPaymentMethod() {
    var fromdate = $('#fromdate').val();
    var todate = $('#todate').val();
    var paymentmethodid = $('#paymentmethodids').val();
    tableCase = $('#cashflowTable').DataTable({
        ajax: {
            url: "/api/beginingbalances/?fromdate=" + fromdate + "&todate=" + todate + "&paymentmethodid=" + paymentmethodid,
            dataSrc: ""
        },
        columns: [

            {
                data: "paiddate",
                render: function (data) {
                    return moment(new Date(data)).format('DD-MMM-YYYY');
                }
            },
            {
                data: "note"
            },
            {
                data: "paymentmethodname"
            },
            {
                data: "saleamount"
            },
            {
                data: "expenseamount"
            },
            {
                data: "incomeamount"
            },
            {
                data: "transferamountin"
            },
            {
                data: "transferamountout"
            },
            {
                data: "transactiontype"
            }
        ],
        destroy: true,
        "order": [[0, "desc"]],
        "info": false
    });
    GetStartBalance();
    GetEndBalance();
}

function GetBeginingBalance() {
    tableCase = $('#beginningBalanceTable').DataTable({
        ajax: {
            url: "/api/beginingbalances",
            dataSrc: ""
        },
        columns: [
             {
                 data: "id"
             },
            {
                data: "date",
                render: function (data) {
                    return moment(new Date(data)).format('DD-MMM-YYYY');
                }
            },
            {
                data: "paymentmethodname"
            },
            {
                data: "amount"
            },
            {
                data: "id",
                render: function (data) {
                    return "<button onclick='BeginEdit(" + data + ")' class='btn btn-warning btn-xs' style='border-width: 0px; width: 65px; margin-right: 5px;'><span class='glyphicon glyphicon-edit'></span> Edit</button>" + "<button onclick='BeginDelete(" + data + ")' class='btn btn-danger btn-xs' style='border-width: 0px;'><span class='glyphicon glyphicon-trash'></span> Delete</button>";
                },
                "width": "130px"
            }
        ],
        destroy: true,
        "order": [[0, "desc"]],
        "info": false
    });
}

function BeginningAction() {
    var action = '';
    action = document.getElementById('btnBegining').innerText;
    if (action === " បង្កើតថ្មី / Add New") {
        document.getElementById('balanceamount').disabled = false;
        document.getElementById('btnBegining').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>រក្សាទុក</span> / Save";
        $('#balancedate').val(moment().format('YYYY-MM-DD'));
        $('#balanceamount').val('0');
        $('#balanceamount').focus();
    }else if(action === " រក្សាទុក / Save") {
        if ($('#balanceamount').val().trim() === "") {
            $('#balanceamount').css('border-color', 'red');
            $('#balanceamount').focus();
            toastr.info("Please enter Pament Method's name", "Required");
        }
        else {
            $('#balanceamount').css('border-color', '#cccccc');
            var data = {
                date: $('#balancedate').val(),
                paymentmethodid: $('#paymentmethodid').val(),
                amount: $('#balanceamount').val()
            };
            $.ajax({
                url: "/api/beginingbalances",
                data: JSON.stringify(data),
                type: "POST",
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                success: function (result) {
                    toastr.success("Save to database successfully.", "Server Response");
                    tableCase.ajax.reload();
                    document.getElementById('balanceamount').disabled = true;
                    document.getElementById('btnBegining').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                    $('#balanceamount').val('');
                },
                error: function (errormessage) {
                    toastr.error("This case is already exists.", "Server Response");
                    document.getElementById('balanceamount').disabled = true;
                    document.getElementById('btnBegining').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                    $('#balanceamount').val('');
                }
            });
        }
    }else if (action === " កែប្រែ / Update") {
        $('#balanceamount').css('border-color', '#cccccc');

        var data = {
            id: $('#balanceid').val(),
            date: $('#balancedate').val(),
            paymentmethodid: $('#paymentmethodid').val(),
            amount: $('#balanceamount').val(),
        };
        $.ajax({
            url: "/api/beginingbalances/" + data.id,
            data: JSON.stringify(data),
            type: "PUT",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                toastr.success("Updated successfully.", "Server Response");
                tableCase.ajax.reload();
                document.getElementById('balanceamount').disabled = true;
                document.getElementById('btnBegining').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                $('#balanceamount').val('');
            },
            error: function (errormessage) {
                toastr.error("This Begining Balance is already exists.", "Server Response");
                document.getElementById('balanceamount').disabled = true;
                document.getElementById('btnBegining').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                $('#balanceamount').val('');
            }
        });
    }
}

function BeginEdit(id) {
    $('#balanceamount').css('border-color', '#cccccc');
    $.ajax({
        url: "/api/beginingbalances/" + id,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            $('#balanceid').val(result.id);
            var dr = moment(result.date).format("YYYY-MM-DD");
            $('#balancedate').val(dr);
            $('#paymentmethodid').val(result.paymentmethodid).change();
            $('#balanceamount').val(result.amount);
            //alert(result.paymentmethodid);
            document.getElementById('btnBegining').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>កែប្រែ</span> / Update";
            document.getElementById('balanceamount').disabled = false;
            document.getElementById('balancedate').disabled = false;

            $('#balanceamount').focus();
        },
        error: function (errormessage) {
            toastr.error("Something unexpected happen.", "Server Response");
        }
    });
    return false;
}

function BeginDelete(id) {
    bootbox.confirm({
        title: "Confirmation",
        message: "Are you sure you want to delete this?",
        buttons: {
            cancel: {
                label: "Cancel",
                className: "btn-default"
            },
            confirm: {
                label: "Delete",
                className: "btn-danger"
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: "/api/beginingbalances/" + id,
                    method: "DELETE",
                    success: function () {
                        tableCase.ajax.reload();
                        toastr.success("Deleted successfully.", "Server Response");
                    },
                    error: function () {
                        toastr.error("This balance is being used, cannot delete this.", "Server Response");
                    }
                });
            }
        }
    });
}