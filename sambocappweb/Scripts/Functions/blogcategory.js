﻿$(document).ready(function () {
    $(document).ajaxStart(function () {
        Pace.start();
        $("div#divLoadingModal").addClass('show');
    }).ajaxStop(function () {
        Pace.stop();
        $("div#divLoadingModal").removeClass('show');
    });

    $('#blogCategoryModal').on('show.bs.modal', function () {
        GetBlogCategories();
        document.getElementById('blogCategoryName').disabled = true;
        document.getElementById('btnBlogCategoryAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
        $('#blogCategoryName').val('');
    });
});

var tableDepartment = [];

function GetBlogCategories() {
    tableDepartment = $('#blogCategoryTable').DataTable({
        ajax: {
            url: "/api/blogcategories",
            dataSrc: ""
        },
        columns: [
            {
                data: "name"
            },
            {
                data: "id",
                render: function (data) {
                    return "<button onclick='BlogCategoryEdit(" + data + ")' class='btn btn-warning btn-xs' style='border-width: 0px; width: 65px; margin-right: 5px;'><span class='glyphicon glyphicon-edit'></span> Edit</button>" + "<button onclick='BlogCategoryDelete(" + data + ")' class='btn btn-danger btn-xs' style='border-width: 0px;'><span class='glyphicon glyphicon-trash'></span> Delete</button>";
                },
                "width": "130px"
            }
        ],
        destroy: true,
        "order": [[0, "desc"]],
        "info": false
    });
}

function BlogCategoryAction() {
    var action = '';
    action = document.getElementById('btnBlogCategoryAction').innerText;
    if (action === " រក្សាទុក / Save") {
        if ($('#blogCategoryName').val().trim() === "") {
            $('#blogCategoryName').css('border-color', 'red');
            $('#blogCategoryName').focus();
            toastr.info("Please enter blog categories's name", "Required");
        }
        else {
            $('#blogCategoryName').css('border-color', '#cccccc');

            var data = {
                Name: $('#blogCategoryName').val()
            };
            $.ajax({
                url: "/api/blogcategories",
                data: JSON.stringify(data),
                type: "POST",
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                success: function (result) {
                    toastr.success("Save to database successfully.", "Server Response");
                    tableDepartment.ajax.reload();
                    document.getElementById('blogCategoryName').disabled = true;
                    document.getElementById('btnBlogCategoryAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                    $('#blogCategoryName').val('');
                },
                error: function (errormessage) {
                    toastr.error("This blog categories is already exists.", "Server Response");
                    document.getElementById('blogCategoryName').disabled = true;
                    document.getElementById('btnBlogCategoryAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                    $('#blogCategoryName').val('');
                }
            });
        }
    }
    else if (action === " បង្កើតថ្មី / Add New") {
        document.getElementById('blogCategoryName').disabled = false;
        document.getElementById('btnBlogCategoryAction').innerHTML = "<span class='glyphicon glyphicon-floppy-save'></span> <span class='kh'>រក្សាទុក</span> / Save";
        $('#blogCategoryName').val('');
        $('#blogCategoryName').focus();
    }
    else if (action === " កែប្រែ / Update") {
        $('#blogCategoryName').css('border-color', '#cccccc');

        var data = {
            Id: $('#BlogCategoryId').val(),
            Name: $('#blogCategoryName').val()
        };
        $.ajax({
            url: "/api/blogcategories/" + data.Id,
            data: JSON.stringify(data),
            type: "PUT",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                toastr.success("Updated successfully.", "Server Response");
                tableDepartment.ajax.reload();
                document.getElementById('blogCategoryName').disabled = true;
                document.getElementById('btnBlogCategoryAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                $('#blogCategoryName').val('');
            },
            error: function (errormessage) {
                toastr.error("This blog categories is already exists.", "Server Response");
                document.getElementById('blogCategoryName').disabled = true;
                document.getElementById('btnBlogCategoryAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                $('#blogCategoryName').val('');
            }
        });
    }
}

function BlogCategoryEdit(id) {

    $('#blogCategoryName').css('border-color', '#cccccc');

    $.ajax({
        url: "/api/blogcategories/" + id,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            $('#BlogCategoryId').val(result.id);
            $('#blogCategoryName').val(result.name);
            document.getElementById('btnBlogCategoryAction').innerHTML = "<span class='glyphicon glyphicon-saved'></span> <span class='kh'>កែប្រែ</span> / Update";
            document.getElementById('blogCategoryName').disabled = false;
            $('#blogCategoryName').focus();
        },
        error: function (errormessage) {
            toastr.error("Something unexpected happen.", "Server Response");
        }
    });
    return false;
}

function BlogCategoryDelete(id) {
    bootbox.confirm({
        title: "Confirmation",
        message: "Are you sure you want to delete this?",
        buttons: {
            cancel: {
                label: "Cancel",
                className: "btn-default"
            },
            confirm: {
                label: "Delete",
                className: "btn-danger"
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: "/api/blogcategories/" + id,
                    method: "DELETE",
                    success: function () {
                        tableDepartment.ajax.reload();
                        toastr.success("Deleted successfully.", "Server Response");
                    },
                    error: function () {
                        toastr.error("This blog categories is being used, cannot delete this.", "Server Response");
                    }
                });
            }
        }
    });
}