﻿$(document).ready(function () {
    $(document).ajaxStart(function () {
        Pace.start();
        $("div#divLoadingModal").addClass('show');
    }).ajaxStop(function () {
        Pace.stop();
        $("div#divLoadingModal").removeClass('show');
    });

    $('#blogPostModal').on('show.bs.modal', function () {
        $('#btnBlogPostUpdate').hide();
        $('#btnBlogPostSave').show();
        $('#blogPostTitle').focus();
    });

    GetBlogPosts();
    
});

var tableBlogPosts = [];

function GetBlogPosts() {
    tableBlogPosts = $('#blogPostsTable').DataTable({
        ajax: {
            url: "/api/blogposts",
            dataSrc: ""
        },
        columns: [
            {
                data: "id"
            },
            {
                data: "blogCategory.name"
            },
            {
                data: "titleKh"
            },
            {
                data: "author"
            },
            {
                data: "postDate",
                render: function (data) {
                    var date = new Date(data);
                    var month = date.getMonth() + 1;
                    return date.getDate() + "-" + (month.length > 1 ? month : month) + "-" + date.getFullYear();
                }
            },
            {
                data: "id",
                render: function (data) {
                    return "<button onclick='onBlogPostEdit(" + data + ")' class='btn btn-warning btn-xs' style='border-width: 0px; width: 65px; margin-right: 5px;'><span class='glyphicon glyphicon-edit'></span> Edit</button>" + "<button onclick='BlogPostDelete(" + data + ")' class='btn btn-danger btn-xs' style='border-width: 0px;'><span class='glyphicon glyphicon-trash'></span> Delete</button>";
                },
                "width": "130px"
            }
        ],
        destroy: true,
        responsive: true,
        "order": [[0, "desc"]],
        "language": {
            "search": "<span class='glyphicon glyphicon-search'></span> <span class='kh'>ស្វែងរក</span> Search:"
        }
    });
}

function SaveBlogPost() {
    
    var res = validate();

    if (res == false) {
        return false;
    }

    var data = {
        title: $('#blogPostTitle').val(),
        titleKh: $('#blogPostTitleKhmer').val(),
        content: tinymce.get('BlogContent').getContent(),
        contentKh: tinymce.get('BlogContentKhmer').getContent(),
        thumbnail: $('#postThumbnailName').val(),
        postStatus: $('#blogPostStatus').val(),
        blogCategoryId: $('#blogPostCategory').val()
    };

    $.ajax({
        url: "/api/blogposts",
        data: JSON.stringify(data),
        type: "POST",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            toastr.success("Save to database successfully.", "Server Response");
            tableBlogPosts.ajax.reload();
            $('#blogPostModal').modal('hide');
        },
        error: function (errormessage) {
            toastr.error("This post is already exists.", "Server Response");
        }
    });
}

function UpdateBlogPost() {
    var res = validate();

    if (res == false) {
        return false;
    }

    var data = {
        id: $('#blogPostId').val(),
        title: $('#blogPostTitle').val(),
        titleKh: $('#blogPostTitleKhmer').val(),
        content: tinymce.get('BlogContent').getContent(),
        contentKh: tinymce.get('BlogContentKhmer').getContent(),
        thumbnail: $('#postThumbnailName').val(),
        postStatus: $('#blogPostStatus').val(),
        blogCategoryId: $('#blogPostCategory').val()
    };

    $.ajax({
        url: "/api/blogposts/" + data.id,
        data: JSON.stringify(data),
        type: "PUT",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            toastr.success("Update to database successfully.", "Server Response");
            tableBlogPosts.ajax.reload();
            $('#blogPostModal').modal('hide');
        },
        error: function (errormessage) {
            toastr.error("This post is already exists.", "Server Response");
        }
    });

}

function onBlogPostEdit(id) {

    clearFormBlogPost();

    $('#btnBlogPostUpdate').show();
    $('#btnBlogPostSave').hide();

    $.ajax({
        url: "/api/blogposts/" + id,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            $('#blogPostId').val(result.id);
            $('#blogPostTitle').val(result.title);
            $('#blogPostTitleKhmer').val(result.titleKh);
            $('#blogPostCategory').val(result.blogCategoryId);
            $('#blogPostStatus').val(result.postStatus);
            tinymce.get('BlogContent').setContent(result.content);
            tinymce.get('BlogContentKhmer').setContent(result.contentKh);
            var img = "<img src='/Uploads/" + result.thumbnail + "' height='100' alt='Uploaded Image' class='img-responsive' />";
            $(img).prependTo('#postThumbnail');
            $('#postThumbnailName').val(result.thumbnail);
            $('#blogPostModal').modal('show');
            $('#btnBlogPostUpdate').show();
            $('#btnBlogPostSave').hide();
        },
        statusCode: {
            400: function () {
                $('#blogPostTitle').focus();
                toastr.error("This post is already exists in database.");
            }

        }
    });
    return false;
}

function BlogPostDelete(id) {

    bootbox.confirm("Are you sure you want to delete this announcement?", function (result) {
        if (result) {
            $.ajax({
                url: "/api/blogposts/" + id,
                method: "DELETE",
                success: function () {
                    tableBlogPosts.ajax.reload();
                    toastr.success("Post has been deleted successfully.");
                },
                error: function () {
                    toastr.error("You don't have permission to delete this announcement.");
                }
            })
        }
    });

}

function validate() {

    var isValid = true;

    var content = tinymce.get('BlogContentKhmer').getContent();
    
    if ($('#blogPostTitleKhmer').val().trim() == "") {
        isValid = false;
        toastr.info('Please enter title.');
    } else {
        if (content == "") {
            isValid = false;
            toastr.info('Please enter content.');
        } else {
            if ($('#postThumbnailName').val().trim() == "") {
                isValid = false;
                toastr.info('Please upload your thumbnail.');
            }
        }
    }
    return isValid;
}

function clearFormBlogPost() {
    $('#blogPostId').val('');
    $('#blogPostTitle').val('');
    $('#blogPostTitleKhmer').val('');
    $('#postThumbnail').text('');
}