﻿$(document).ready(function () {
    //$(document).ajaxStart(function () {
    //    Pace.start();
    //    $("div#divLoadingModal").addClass('show');
    //}).ajaxStop(function () {
    //    Pace.stop();
    //    $("div#divLoadingModal").removeClass('show');
    //}); 

    $('#BuyerModal').on('show.bs.modal', function () {
        GetBuyers();
        DisableControlBuyer();
        ClearControlBuyer();
    });
});

var tableBuyer = [];

//function onPopulateBuyer() {
//    $.ajax({
//        url: "/api/Buyers?selectoption=true",
//        type: "GET",
//        contentType: "application/json;charset=UTF-8",
//        dataType: "json",
//        success: function (data) {
//            console.log(data);
//            $("#BuyerId").select2({
//                data: data,
//                dropdownParent: $("#BuyerModal")
//            });
//        },
//        error: function (errormessage) {
//            toastr.error("Something unexpected happen.", "Server Response");
//        }
//    });
//}

//function onPopulateCaseProccedingBuyer() {
//    $.ajax({
//        url: "/api/Buyers?selectoption=true",
//        type: "GET",
//        contentType: "application/json;charset=UTF-8",
//        dataType: "json",
//        success: function (data) {
//            console.log(data);
//            $("#zcaseprocedingBuyerid").select2({
//                data: data,
//                dropdownParent: $("#civilcaseModal")
//            });
//        },
//        error: function (errormessage) {
//            toastr.error("Something unexpected happen.", "Server Response");
//        }
//    });
//}

//function onPopulateCaseBuyer() {
//    $.ajax({
//        url: "/api/Buyers?selectoption=true",
//        type: "GET",
//        contentType: "application/json;charset=UTF-8",
//        dataType: "json",
//        success: function (data) {
//            console.log(data);
//            $("#zcaseBuyerids").select2({
//                data: data,
//                dropdownParent: $("#civilcaseModal")
//            });
//        },
//        error: function (errormessage) {
//            toastr.error("Something unexpected happen.", "Server Response");
//        }
//    });
//}



function GetBuyers() {
    tableBuyer = $('#BuyerTable').DataTable({
        ajax: {
            url: "/api/Buyers",
            dataSrc: ""
        },
        columns: [
            {
                data: "id"
            },
            {
                data: "buyerName"
            },
            {
                data: "buyerSex"
            },
            {
                data: "buyerAddress"
            },
            {
                data: "buyerIdentityno"
            },
            {
                data: "buyerPhone"
            },
            {
                data: "id",
                render: function (data) {
                    return "<button onclick='BuyerEdit(" + data + ")' class='btn btn-warning btn-xs' style='border-width: 0px; width: 65px; margin-right: 5px;'><span class='glyphicon glyphicon-edit'></span> Edit</button>" +
                        "<button onclick='BuyerDelete(" + data + ")' class='btn btn-danger btn-xs' style='border-width: 0px;'><span class='glyphicon glyphicon-trash'></span> Delete</button>";
                },
                "width": "130px"
            }
        ],
        destroy: true,
        "order": [[0, "desc"]],
        "info": false
    });
}

function BuyerEdit(id) {

    $('#BuyerName').css('border-color', '#cccccc');

    $.ajax({
        url: "/api/Buyers/" + id,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            $('#BuyerId').val(result.id);
            $('#BuyerName').val(result.buyerName);
            $('#BuyerSex').val(result.buyerSex);
            $('#BuyerAge').val(result.buyerAge);
            $("#BuyerPhone").val(result.buyerPhone);
            $("#BuyerAddress").val(result.buyerAddress);
            $('#BuyerIdentityno').val(result.buyerIdentityno);
            $('#Buyerfile_old').val(result.buyerPhoto);

            var dr = moment(result.buyerDob).format("YYYY-MM-DD");
            $('#bdob').val(dr);

            
            if (result.buyerPhoto == "") {
                $('#BuyerPhoto').attr('src', '../Images/no_image.png');
            } else {
                //alert(result.img);
                $('#BuyerPhoto').attr('src', '../Images/' + result.buyerPhoto);
            }
            EnableControlBuyer();
            document.getElementById('btnBuyerAction').innerHTML = "កែប្រែ​ / Update";
            $('#BuyerModel').modal('show');
        },
        error: function (errormessage) {
            toastr.error("No Record Select!", "Service Response");
        }
    });
    return false;
}

function BuyerAction() {
    var action = '';
    action = document.getElementById('btnBuyerAction').innerText;
    if (action === "បង្កើតថ្មី​ / Add New") {
        document.getElementById('btnBuyerAction').innerText = "រក្សាទុក / Save";
        EnableControlBuyer();
        $('#BuyerPhoto').attr('src', '../Images/no_image.png');
        $("#BuyerName").focus();
        $('#bdob').val(moment().format('YYYY-MM-DD'));
        
    } else if (action == "រក្សាទុក / Save") {
        var data = new FormData();
        var files = $("#Buyerfile").get(0).files;
        if (files.length > 0) {
            data.append("buyerphoto", files[0]);
        }

        data.append("buyerName", $("#BuyerName").val());
        data.append("buyersex", $("#BuyerSex").val());
        data.append("buyerage", $("#BuyerAge").val());
        data.append("buyeraddress", $("#BuyerAddress").val());
        data.append("buyeridentityno", $("#BuyerIdentityno").val());
        data.append("buyerphone", $("#BuyerPhone").val());
        data.append("buyerdob", $("#bdob").val());

        //console.log(files[0]);

        $.ajax({
            type: "POST",
            url: "/api/Buyers",
            contentType: false,
            processData: false,
            data: data,
            success: function (result) {
                toastr.success("Save to database successfully.", "Server Response");
                tableBuyer.ajax.reload();
                //onPopulateBuyer();
                //onPopulateCaseBuyer();
                //onPopulateCaseProccedingBuyer();                document.getElementById('BuyerName').disabled = true;
                DisableControlBuyer();
                $('#BuyerPhoto').val('');
                ClearControlBuyer();
                document.getElementById('btnBuyerAction').innerText = "បង្កើតថ្មី​ / Add New";
                
            },
            error: function (errormessage) {
                toastr.error("This Buyer is already exists.", "Server Response");
            }
        });

        //maritalstatus

    } else if (action === "កែប្រែ​ / Update") {

        var data = new FormData();
        var files = $("#Buyerfile").get(0).files;
        if (files.length > 0) {
            data.append("BuyerPhoto", files[0]);
        }
        data.append("id", $('#BuyerId'));
        data.append("buyerName", $("#BuyerName").val());
        data.append("buyerSex", $("#BuyerSex").val());
        data.append("buyerAge", $("#BuyerAge").val());
        data.append("buyerAddress", $("#BuyerAddress").val());
        data.append("buyerDob", $("#bdob").val());
        data.append("buyerIdentityno", $("#BuyerIdentityno").val());
        data.append("buyerPhone", $("#BuyerPhone").val());
        data.append("buyerPhoto", $("#Buyerfile_old").val());

        console.log(data);

        $.ajax({
            type: "PUT",
            url: "/api/Buyers/" + $('#BuyerId').val(),
            contentType: false,
            processData: false,
            data: data,
            success: function (result) {
                toastr.success("Updated successfully.", "Server Response");
                tableBuyer.ajax.reload();
                //onPopulateBuyer();
                //onPopulateCaseBuyer();
                //onPopulateCaseProccedingBuyer();
                DisableControlBuyer();
                $('#BuyerPhoto').val('');
                ClearControlBuyer();
                document.getElementById('btnBuyerAction').innerHTML = "បង្កើតថ្មី​ / Add New";
                
            },
            error: function (errormessage) {
                toastr.error("This Buyer is already exists.", "Server Response");
            }
        });

    }
}

function BuyerDelete(id) {
    bootbox.confirm({
        title: "Confirmation",
        message: "Are you sure you want to delete this?",
        buttons: {
            cancel: {
                label: "Cancel",
                className: "btn-default"
            },
            confirm: {
                label: "Delete",
                className: "btn-danger"
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: "/api/Buyers/" + id,
                    method: "DELETE",
                    success: function () {
                        tableBuyer.ajax.reload();
                        //onPopulateBuyer();
                        //onPopulateCaseBuyer();
                        //onPopulateCaseProccedingBuyer();
                        toastr.success("Deleted successfully.", "Server Response");
                        ClearControlBuyer();
                    },
                    error: function () {
                        toastr.error("This Buyer is being used, cannot delete this.", "Server Response");
                    }
                });
            }
        }
    });
}

function readURLL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#BuyerPhoto').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}


function DisableControlBuyer() {
    document.getElementById('BuyerName').disabled = true;
    document.getElementById('BuyerSex').disabled = true;
    document.getElementById('BuyerAge').disabled = true;
    document.getElementById('BuyerAddress').disabled = true;
    document.getElementById('BuyerIdentityno').disabled = true;
    document.getElementById('BuyerPhone').disabled = true;
    document.getElementById('bdob').disabled = true;
    document.getElementById('Buyerfile').disabled = true;
    //document.getElementById('btnBuyerAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";

}

function EnableControlBuyer() {
    document.getElementById('BuyerName').disabled = false;
    document.getElementById('BuyerSex').disabled = false;
    document.getElementById('BuyerAge').disabled = false;
    document.getElementById('BuyerAddress').disabled = false;
    document.getElementById('BuyerIdentityno').disabled = false;
    document.getElementById('BuyerPhone').disabled = false;
    document.getElementById('bdob').disabled = false;
    document.getElementById('Buyerfile').disabled = false;
}

function ClearControlBuyer() {
    $('#BuyerName').val('');
    $('#BuyerSex').val('');
    $('#BuyerAge').val('');
    $('#BuyerAddress').val('');
    $('#BuyerIdentityno').val('');
    $('#BuyerPhone').val('');
    $('#bdob').val('');
    $('#Buyerfile').val('');
    $('#BuyerPhoto').attr('src', '../Images/no_image.png');

}

function NewBuyerAction() {
    alert('hi');
    document.getElementById('btnBuyerAction').innerText = "បង្កើតថ្មី​ / Add New";
    DisableControlBuyer();
    ClearControlBuyer();
}