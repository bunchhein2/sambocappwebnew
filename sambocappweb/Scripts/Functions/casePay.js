﻿$(document).ready(function () {
    $(document).ajaxStart(function () {
        Pace.start();
        $("div#divLoadingModal").addClass('show');
    }).ajaxStop(function () {
        Pace.stop();
        $("div#divLoadingModal").removeClass('show');
    });
    $('#transferModel').on('show.bs.modal', function () {
        GetCivilcasePayAction();
        document.getElementById('paydate').disabled = true;
        document.getElementById('paidAmount').disabled = true;
        document.getElementById('btnCivilCasePay').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
    });
});

var tablePayment = [];

function GetCivilcasePayAction() {
    //alert('hi');
    //$('#transferModel').modal('show');    
    document.getElementById('paydate').disabled = true;
    document.getElementById('paidAmount').disabled = true;
    document.getElementById('btnCivilCasePay').innerText = "Add New";

    tablePayment = $('#civilCasePayTable').DataTable({
        ajax: {
            url: "/api/civilcasepayments",
            dataSrc: ""
        },
        columns: [
            {
                data: "id"
            },
            {
                data: "date",
                render: function (data) {
                    return moment(new Date(data)).format('DD-MMM-YYYY');
                }
            },
            {
                data: "paidamount"
            },
            {
                data: "id",
                render: function (data) {
                    return "<button onclick='CivilEdit(" + data + ")' class='btn btn-warning btn-xs' style='margin-right:5px;'>Edit</button>" +
                        "<button onclick='CivilDelete(" + data + ")' class='btn btn-danger btn-xs'>Delete</button>";
                }
            }
        ],
        destroy: true,
        "order": [0, "desc"],
        "info": false
    });

}


function CivilCasePayAction() {
    var action = '';
    action = document.getElementById('btnCivilCasePay').innerText;
    //alert(action);
    if (action === " រក្សាទុក / Save") {
        if ($('#paidAmount').val().trim() === "") {
            $('#paidAmount').css('border-color', 'red');
            $('#paidAmount').focus();
            toastr.info("Please enter Paid Amount", "Required");
        }
        else {
            $('#paidAmount').css('border-color', '#cccccc');

            var data = {
                date: $('#paydate').val(),
                paidamount: $('#paidAmount').val()
            };
            $.ajax({
                url: "/api/civilcasepayments",
                data: JSON.stringify(data),
                type: "POST",
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                success: function (result) {
                    toastr.success("Save to database successfully.", "Server Response");
                    tablePayment.ajax.reload();
                    document.getElementById('paydate').disabled = true;
                    document.getElementById('paidAmount').disabled = true;
                    document.getElementById('btnCivilCasePay').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";

                    $('#paydate').val('');
                    $('#paidAmount').val('');
                },
                error: function (errormessage) {
                    toastr.error("This case position is already exists.", "Server Response");
                    document.getElementById('paydate').disabled = true;
                    document.getElementById('paidAmount').disabled = true;
                    document.getElementById('btnCivilCasePay').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";

                    $('#paydate').val('');
                    $('#paidAmount').val('');
                }
            });
        }
    }
    else if (action === " បង្កើតថ្មី / Add New") {
        document.getElementById('paydate').disabled = false;
        document.getElementById('paidAmount').disabled = false;
        document.getElementById('btnCivilCasePay').innerHTML = "<span class='glyphicon glyphicon-floppy-save'></span> <span class='kh'>រក្សាទុក</span> / Save";
        $('#paydate').val(moment().format('YYYY-MM-DD'));
        $('#paidAmount').val('');
        $('#paidAmount').focus();
    }
    else if (action === " កែប្រែ / Update") {
        $('#paidAmount').css('border-color', '#cccccc');

        var data = {
            id: $('#civilCasePayid').val(),
            date: $('#paydate').val(),
            paidamount: $('#paidAmount').val()
        };
        $.ajax({
            url: "/api/civilcasepayments/" + data.id,
            data: JSON.stringify(data),
            type: "PUT",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                toastr.success("Updated successfully.", "Server Response");
                tablePayment.ajax.reload();
                document.getElementById('paydate').disabled = true;
                document.getElementById('paidAmount').disabled = true;
                document.getElementById('btnCivilCasePay').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";

                $('#paydate').val('');
                $('#paidAmount').val('');
            },
            error: function (errormessage) {
                toastr.error("This position is already exists.", "Server Response");
                document.getElementById('paydate').disabled = true;
                document.getElementById('paidAmount').disabled = true;
                document.getElementById('btnCivilCasePay').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";

                $('#paydate').val('');
                $('#paidAmount').val('');
            }
        });
    }
}

function CivilEdit(id) {

    $('#paidAmount').css('border-color', '#cccccc');

    $.ajax({
        url: "/api/civilcasepayments/" + id,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            $('#civilCasePayid').val(result.id);
            $('#paydate').val(result.date);
            $('#paidAmount').val(result.paidamount);
            document.getElementById('btnCivilCasePay').innerHTML = "<span class='glyphicon glyphicon-saved'></span> <span class='kh'>កែប្រែ</span> / Update";
            document.getElementById('paydate').disabled = false;
            document.getElementById('paidAmount').disabled = false;
            $('#paydate').val('');
            $('#paidAmount').val('');

        },
        error: function (errormessage) {
            toastr.error("Something unexpected happen.", "Server Response");
        }
    });
    return false;
}

function CivilDelete(id) {
    bootbox.confirm({
        title: "Confirmation",
        message: "Are you sure you want to delete this?",
        buttons: {
            cancel: {
                label: "Cancel",
                className: "btn-default"
            },
            confirm: {
                label: "Delete",
                className: "btn-danger"
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: "/api/civilcasepayments/" + id,
                    method: "DELETE",
                    success: function () {
                        tablePayment.ajax.reload();
                        toastr.success("Deleted successfully.", "Server Response");
                    },
                    error: function () {
                        toastr.error("This case is being used, cannot delete this.", "Server Response");
                    }
                });
            }
        }
    });
}