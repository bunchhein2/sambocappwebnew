﻿$(document).ready(function () {
    $(document).ajaxStart(function () {
        Pace.start();
        $("div#divLoadingModal").addClass('show');
    }).ajaxStop(function () {
        Pace.stop();
        $("div#divLoadingModal").removeClass('show');
    });
    onPopulateCasePosition();
    //$('#civilcaseModal').on('show.bs.modal', function () {
    //    // Bind Cases Select Options
    //    onPopulateCasePosition();
    //});

    //GetCases();
    $('#casepositionModal').on('show.bs.modal', function () {
        GetPositionCases();
        document.getElementById('casepositionName').disabled = true;
        document.getElementById('casepositionNote').disabled = true;
        document.getElementById('btnCasePositionAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
        $('#casepositionName').val('');
    });
});

var tableCasePosition = [];

function onPopulateCasePosition() {
    $.ajax({
        url: "/api/casepositions?selectoption=true",
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (data) {
            console.log(data);
            $("#zcasepositionid").select2({
                data: data,
                dropdownParent: $("#civilcaseModal")
            });
        },
        error: function (errormessage) {
            toastr.error("Something unexpected happen.", "Server Response");
        }
    });
}

function GetPositionCases() {
    tableCasePosition = $('#CasePositionTable').DataTable({
        ajax: {
            url: "/api/casepositions",
            dataSrc: ""
        },
        columns: [
            {
                data: "casepositionname"
            },
            {
                data: "casepositionnote"
            },
            {
                data: "id",
                render: function (data) {
                    return "<button onclick='CasePositionEdit(" + data + ")' class='btn btn-warning btn-xs' style='border-width: 0px; width: 65px; margin-right: 5px;'><span class='glyphicon glyphicon-edit'></span> Edit</button>" + "<button onclick='CasePositionDelete(" + data + ")' class='btn btn-danger btn-xs' style='border-width: 0px;'><span class='glyphicon glyphicon-trash'></span> Delete</button>";
                },
                "width": "130px"
            }
        ],
        destroy: true,
        "order": [[0, "desc"]],
        "info": false
    });
}

function CasePositionAction() {
    var action = '';
    action = document.getElementById('btnCasePositionAction').innerText;
    if (action === " រក្សាទុក / Save") {
        if ($('#casepositionName').val().trim() === "") {
            $('#casepositionName').css('border-color', 'red');
            $('#casepositionName').focus();
            toastr.info("Please enter Case Position's name", "Required");
        }
        else {
            $('#casepositionName').css('border-color', '#cccccc');

            var data = {
                casepositionname: $('#casepositionName').val(),
                casepositionnote: $('#casepositionNote').val()
            };
            $.ajax({
                url: "/api/casepositions",
                data: JSON.stringify(data),
                type: "POST",
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                success: function (result) {
                    toastr.success("Save to database successfully.", "Server Response");
                    tableCasePosition.ajax.reload();
                    onPopulateCasePosition();
                    document.getElementById('casepositionName').disabled = true;
                    document.getElementById('casepositionNote').disabled = true;
                    document.getElementById('btnCasePositionAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                    $('#casepositionName').val('');
                    $('#casepositionNote').val('');
                },
                error: function (errormessage) {
                    toastr.error("This case position is already exists.", "Server Response");
                    document.getElementById('casepositionName').disabled = true;
                    document.getElementById('casepositionNote').disabled = true;
                    document.getElementById('btnCasePositionAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                    $('#casepositionName').val('');
                    $('#casepositionNote').val('');
                }
            });
        }
    }
    else if (action === " បង្កើតថ្មី / Add New") {
        document.getElementById('casepositionName').disabled = false;
        document.getElementById('casepositionNote').disabled = false;
        document.getElementById('btnCasePositionAction').innerHTML = "<span class='glyphicon glyphicon-floppy-save'></span> <span class='kh'>រក្សាទុក</span> / Save";
        $('#casepositionName').val('');
        $('#casepositionNote').val('');
        $('#casepositionName').focus();
    }
    else if (action === " កែប្រែ / Update") {
        $('#casepositionName').css('border-color', '#cccccc');

        var data = {
            id: $('#casepositionId').val(),
            casepositionname: $('#casepositionName').val(),
            casepositionnote: $('#casepositionNote').val()
        };
        $.ajax({
            url: "/api/casepositions/" + data.id,
            data: JSON.stringify(data),
            type: "PUT",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                toastr.success("Updated successfully.", "Server Response");
                tableCasePosition.ajax.reload();
                onPopulateCasePosition();
                document.getElementById('casepositionName').disabled = true;
                document.getElementById('casepositionNote').disabled = true;
                document.getElementById('btnCasePositionAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                $('#casepositionName').val('');
                $('#casepositionNote').val('');
            },
            error: function (errormessage) {
                toastr.error("This position is already exists.", "Server Response");
                document.getElementById('casepositionName').disabled = true;
                document.getElementById('btnCasePositionAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                $('#casepositionName').val('');
                $('#casepositionNote').val('');
            }
        });
    }
}

function CasePositionEdit(id) {

    $('#casepositionName').css('border-color', '#cccccc');

    $.ajax({
        url: "/api/casepositions/" + id,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            $('#casepositionId').val(result.id);
            $('#casepositionName').val(result.casepositionname);
            $('#casepositionNote').val(result.casepositionnote);
            document.getElementById('btnCasePositionAction').innerHTML = "<span class='glyphicon glyphicon-saved'></span> <span class='kh'>កែប្រែ</span> / Update";
            document.getElementById('casepositionName').disabled = false;
            document.getElementById('casepositionNote').disabled = false;
            $('#casepositionName').focus();
            $('#casepositionNote').focus();

        },
        error: function (errormessage) {
            toastr.error("Something unexpected happen.", "Server Response");
        }
    });
    return false;
}

function CasePositionDelete(id) {
    bootbox.confirm({
        title: "Confirmation",
        message: "Are you sure you want to delete this?",
        buttons: {
            cancel: {
                label: "Cancel",
                className: "btn-default"
            },
            confirm: {
                label: "Delete",
                className: "btn-danger"
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: "/api/casepositions/" + id,
                    method: "DELETE",
                    success: function () {
                        tableCasePosition.ajax.reload();
                        toastr.success("Deleted successfully.", "Server Response");
                    },
                    error: function () {
                        toastr.error("This case is being used, cannot delete this.", "Server Response");
                    }
                });
            }
        }
    });
}