﻿
$(document).ready(function () {
    alert('hi');
    $('#transferModel').on('show.bs.modal', function () {
        GetCivilcasePayAction();
    });
    GetCivilcasePayAction();
});

var tablePayment = [];
function GetCivilcasePayAction() {
    alert('hi');
    //$('#transferModel').modal('show');    
    document.getElementById('paydate').disabled = true;
    document.getElementById('paidAmount').disabled = true;
    document.getElementById('btnCivilCasePay').innerText = "Add New";
    
    tablePayment = $('#civilCasePayTable').DataTable({
        ajax: {
            url: "/api/civilcasepayments",
            dataSrc: ""
        },
        columns: [
            {
                data: "id"
            },
            {
                data: "date",
                render: function (data) {
                    return moment(new Date(data)).format('DD-MMM-YYYY');
                }
            },
            {
                data: "paidamount"
            },
            {
                data: "id",
                render: function (data) {
                    return "<button onclick='CivilEdit(" + data + ")' class='btn btn-success btn-xs pull-center' style='margin-right:5px;'></span> Print</button>" +
                        "<button onclick='CivilDelete(" + data + ")' class='btn btn-warning btn-xs' style='margin-right:5px;'>Edit</button>" + "<button onclick='paymentDelete(" + data + ")' class='btn btn-danger btn-xs'>Delete</button>";
                }
            }
        ],
        destroy: true,
        "order": [0, "desc"],
        "info": false
    });
    
}
