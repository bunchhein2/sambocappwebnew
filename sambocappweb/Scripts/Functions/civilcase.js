﻿$(document).ready(function () {
    $(document).ajaxStart(function () {
        Pace.start();
        $("div#divLoadingModal").addClass('show');
    }).ajaxStop(function () {
        Pace.stop();
        $("div#divLoadingModal").removeClass('show');
    });
    //alert('Hi');
    GetCivilCase();
    $('#civilcaseModal').on('show.bs.modal', function () {
        document.getElementById('btnCivilCase').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
    });
    

});
var billgroup=0;
var tableEmployee = [];
var tablePayment = [];
var tableCTribunal = [];
var tableCProceeding = [];
toastr.optionsOverride = 'positionclass = "toast-bottom-right"';
toastr.options.positionClass = 'toast-bottom-right';

function GetCivilCase() {
    tableEmployee = $('#civilcaseTable').DataTable({
        ajax: {
            url: "/api/civilcases",
            dataSrc: ""
        },
        columns: [
            {
                data: "civilcaseno", render: function (data) {

                    return "VLEX" + ("000000" + data).slice(-6);
                }
            },             
            {
                data: "civilcasedate",
                render: function (data) {
                    return moment(new Date(data)).format('DD-MMM-YYYY');
                }
            },
            {
                data: "casename"
            },
            {
                data: "casetypename"
            },
            {
                data: "plaintiffname"
            },
            {
                data: "defendentname"
            },
            
            
            {
                data: "customername"
            },
            //{
            //    data: "charges"
            //},
            //{
            //    data: "services"
            //},
            {
                data: "id",
                render: function (data) {
                    return "<button onclick='GetPlaintiffByCase(" + data + ")' class='btn btn-success btn-xs pull-center' style='border-width: 0px; margin-right: 5px;'><span class='glyphicon glyphicon-list-alt'></span> ដើមបណ្តឹង</button>" + "<button onclick='GetDefendentByCase(" + data + ")' class='btn btn-success btn-xs pull-center' style='border-width: 0px; margin-right: 5px;'><span class='glyphicon glyphicon-list-alt'></span> ចុងបណ្តឹង</button>";
                },
                "width": "230px"
            }, 
            {
                data: "id",
                render: function (data) {
                    return "<button onclick='GetCaseexpanse(" + data + ")' class='btn btn-success btn-xs pull-center' style='border-width: 0px; margin-right: 5px;'><span class='glyphicon glyphicon-list-alt'></span> ការចំណាយ</button>"
                        + "<button onclick='GetCivilcasePayment(" + data + ")' class='btn btn-success btn-xs pull-center' style='border-width: 0px; margin-right: 5px;'><span class='glyphicon glyphicon-list-alt'></span> បង់ប្រាក់</button>"
                        + "<button onclick='GetCivilTribunal(" + data + ")' class='btn btn-success btn-xs pull-center' style='border-width: 0px; margin-right: 5px;'><span class='glyphicon glyphicon-list-alt'></span> ប្រវត្តិក្តី</button>"
                        + "<button onclick='GetCivilProceeding(" + data + ")' class='btn btn-success btn-xs pull-center' style='border-width: 0px;​​ margin-top: 10px;'><span class='glyphicon glyphicon-list-alt'></span> ចំណាត់ការក្តី</button>";
                },
                "width": "230px"
            },
            //{
            //    data: "status"
            //},
            //{
            //    data: "createby"
            //},
            //{
            //    data: "createdate"
            //},
            {
                data: "id",
                render: function (data) {
                    return "<button onclick='civilcaseEdit(" + data + ")' class='btn btn-warning btn-xs' style='border-width: 0px; width: 65px; margin-right: 5px;'><span class='glyphicon glyphicon-edit'></span> Edit</button>" + "<button onclick='civilcaseDelete(" + data + ")' class='btn btn-danger btn-xs' style='border-width: 0px;'><span class='glyphicon glyphicon-trash'></span> Delete</button>";
                },
                "width": "130px"
            }
        ],
        destroy: true,
        "order": [[0, "desc"]],
        "info": false
    });
}

//function GetCaseexpanse() {
//    $('#caseexpenseModal').on('show.bs.modal', function () {
//    alert('hi');
//    GetCaseexpanse();
//        });
//}
var tableCaseexpanse = [];
function GetCaseexpanse(id) {
    //alert('hi');
    $('#caseexpenseModal').modal('show');
    DisableControl();
    ClearControl();
    $('#civilcaseid').val(id);

    tableCaseexpanse = $('#caseexpenseTable').dataTable({
        ajax: {
            url: "/api/Caseexpenses/" + id,
            dataSrc: ""
        },
        columns:
            [
                {
                    data: "id"
                },
                {
                    data: "datetime",
                    render: function (data) {
                        return moment(data).format("DD-MMM-YYYY");
                    }
                },
                {
                    data: "note"
                },

                {
                    data: "offerby"
                },
                {
                    data: "startdate",
                    render: function (data) {
                        return moment(data).format("DD-MMM-YYYY");
                    }
                },
                {
                    data: "enddate",
                    render: function (data) {
                        return moment(data).format("DD-MMM-YYYY");
                    }
                },
                {
                    data: "id",
                    render: function (data) {
                        return "<button OnClick='CaseexpenseDetail (" + data + ")' class='btn btn-primary btn-xs' style='margin-right:5px'><span class='glyphicon glyphicon-eye-open'></span> Detail</button>" +
                            "<button OnClick='CaseexpenseEdit (" + data + ")' class='btn btn-primary btn-xs' style='margin-right:5px'><span class='glyphicon glyphicon-edit'></span> Edit</button>" +
                            "<button OnClick='CaseexpenseDelete (" + data + ")' class='btn btn-default btn-xs' ><span class='glyphicon glyphicon-remove'></span> Delete</button>";
                    }
                }
            ],
        destroy: true,

    });
}

var tableCaseexpanseDetail = [];
function CaseexpenseDetail(id) {
    
    $('#CaseexpenseDetailModal').modal('show');
    DisableControlDetail();
    ClearControlDetail();
    $('#Caseexpenseid').val(id);

    tableCaseexpanseDetail = $('#CaseexpenseDetailTable').dataTable({
        ajax: {
            url: "/api/CaseexpenseDetails/" + id,
            dataSrc: ""
        },
        columns:
            [
                {
                    data: "id"
                },
                {
                    data: "description"
                },

                {
                    data: "amount"
                },
                {
                    data: "paymentMethod"
                },
                {
                    data: "id",
                    render: function (data) {
                        return "<button OnClick='CaseexpenseDetailEdit (" + data + ")' class='btn btn-primary btn-xs' style='margin-right:5px'><span class='glyphicon glyphicon-edit'></span> Edit</button>" +
                            "<button OnClick='CaseexpenseDetailDelete (" + data + ")' class='btn btn-default btn-xs' ><span class='glyphicon glyphicon-remove'></span> Delete</button>";
                    }
                }
            ],
        destroy: true,

    });
}
function CaseexpenseDetailAction() {

    var action = '';
    action = document.getElementById('btnSaveCaseexpenseDetail').innerText;
    //alert(action);
    if (action == "Add New") {
        document.getElementById('btnSaveCaseexpenseDetail').innerText = 'Save';
        EnableControlDetail();

        $('#description').focus();
    }

    if (action === "Save") {
        //Validate();
        var data = {
            description: $('#description').val(),
            amount: $('#amount').val(),
            caseexpenseid: $('#Caseexpenseid').val(),
            paymentmethodid: $('#paymentmethod').val(),
        };
        $.ajax({
            url: "/api/CaseexpenseDetails",
            data: JSON.stringify(data),
            type: "POST",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                toastr.success("Save to database successfully.", "Server Response");
                $('#CaseexpenseDetailTable').DataTable().ajax.reload();
                document.getElementById('btnSaveCaseexpenseDetail').innerText = 'Add New';
                DisableControlDetail();
                ClearControlDetail();
                //$('#caseexpenseModal').modal('hide');
                //document.getElementById('btnSaveCaseexpense').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";

            },
            error: function (errormessage) {
                toastr.error("This Caseexpense is already exists.", "Server Response");
                //document.getElementById('btnSaveCaseexpense').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";

            }
        });
    }
    else if (action === "Update") {
        //alert('hi');
        var data = {
            id: $('#CaseexpenseDetailid').val(),
            description: $('#description').val(),
            amount: $('#amount').val(),
            caseexpenseid: $('#Caseexpenseid').val(),
            paymentmethodid: $('#paymentmethod').val(),
        };
        $.ajax({
            url: "/api/CaseexpenseDetails/" + data.id,
            data: JSON.stringify(data),
            type: "PUT",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                toastr.success("Updated successfully.", "Server Response");
                $('#CaseexpenseDetailTable').DataTable().ajax.reload();
                document.getElementById('btnSaveCaseexpenseDetail').innerText = 'Add New';
                DisableControlDetail();
                ClearControlDetail();
                //$('#CaseexpenseModal').modal('hide');
                //tableCaseexpenses.ajax.reload();
                //$('#registerModal').modal('hide');

            },
            error: function (errormessage) {
                toastr.error("This CaseexpenseDetail can't Update.", "Server Response");
            }
        });
    }
}


function CaseexpenseDetailEdit(id) {
    ClearControlDetail();
    EnableControlDetail();

    //$('#status').val('');
    action = document.getElementById('btnSaveCaseexpenseDetail').innerText = "Update";

    $.ajax({
        url: "/api/CaseexpenseDetails/?id=" + id+"&b=2",
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {

            $('#CaseexpenseDetailid').val(result.id);
            $('#description').val(result.description);
            $('#amount').val(result.amount);
            $('#paymentmethod').val(result.paymentmethodid);
            $("#CaseexpenseDetailModal").modal('show');
        },
        error: function (errormessage) {
            toastr.error("Something unexpected happen.", "Server Response");
        }
    });
    return false;
}


function CaseexpenseDetailDelete(id) {
    //alert('hi');
    bootbox.confirm({
        title: "",
        message: "Are you sure want to delete this?",
        button: {
            cancel: {
                label: "Cancel",
                ClassName: "btn-default",
            },
            confirm: {
                label: "Delete",
                ClassName: "btn-danger"
            }
        },
        callback: function (result) {
            //alert(id);
            if (result) {
                $.ajax({
                    url: "/api/CaseexpenseDetails/" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        $('#CaseexpenseDetailTable').DataTable().ajax.reload();

                        toastr.success("CaseexpenseDetail Deleted successfully!", "Service Response");
                        ClearControlDetail();
                    },
                    error: function (errormessage) {
                        toastr.error("CaseexpenseDetail Can't be Deleted", "Service Response");
                    }
                });
            }
        }
    });
}

function DisableControlDetail() {
    document.getElementById('description').disabled = true;
    document.getElementById('amount').disabled = true;
    document.getElementById('paymentmethod').disabled = true;
}

function EnableControlDetail() {

    document.getElementById('description').disabled = false;
    document.getElementById('amount').disabled = false;
    document.getElementById('paymentmethod').disabled = false;
}

function ClearControlDetail() {
    $('#description').val('');
    $('#amount').val('');
    $('#paymentmethod').val('');
}

//function CaseexpenseAction() {
//    document.getElementById('btnSaveCaseexpense').innerText = "Add New";
//    DisableControl();
//    ClearControl();
//}

function Validate() {
    var isValid = true;
    var formAddEdit = $("#formTrainingProgramAdd");
    if ($('#CaseexpenseTittle').val().trim() === "") {
        $('#CaseexpenseTittle').css('border-color', 'red');
        $('#CaseexpenseTittle').focus();
        toastr.info("Please enter Caseexpense title", "Required");
    }
    else {
        $('#CaseexpenseTittle').css('border-color', '#cccccc');
        if ($('#createBy').val().trim() === "") {
            $('#createBy').css('border-color', 'red');
            $('#createBy').focus();
            toastr.info("Please enter your phone", "Required");
        }
        else {
            $('#createBy').css('border-color', '#cccccc');
        }
    }
    return isValid;
}

//CaseExpense
function CaseexpenseAction() {
    
    var action = '';
    action = document.getElementById('btnSaveCaseexpense').innerText;
    //alert(action);
    if (action == "Add New") {
        document.getElementById('btnSaveCaseexpense').innerText = 'Save';
        EnableControl();

        $('#note').focus();

        $('#datetime').val(moment().format('YYYY-MM-DD'));
        $('#createdate').val(moment().format('YYYY-MM-DD'));
        $('#startdate').val(moment().format('YYYY-MM-DD'));
        $('#enddate').val(moment().format('YYYY-MM-DD'));

    }

    if (action === "Save") {
        //Validate();
        var data = {
            datetime: $('#datetime').val(),
            note: $('#note').val(),
            offerby: $('#offerby').val(),
            startdate: $('#startdate').val(),
            enddate: $('#enddate').val(),
            createdate: $('#createdate').val(),
            civilcaseid: $('#civilcaseid').val(),

        };
        $.ajax({
            url: "/api/Caseexpenses",
            data: JSON.stringify(data),
            type: "POST",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                toastr.success("Save to database successfully.", "Server Response");
                $('#caseexpenseTable').DataTable().ajax.reload();                
                document.getElementById('btnSaveCaseexpense').innerText = 'Add New';
                DisableControl();
                ClearControl();
                //$('#caseexpenseModal').modal('hide');
                //document.getElementById('btnSaveCaseexpense').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";

            },
            error: function (errormessage) {
                toastr.error("This Caseexpense is already exists.", "Server Response");
                //document.getElementById('btnSaveCaseexpense').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";

            }
        });
    }
    else if (action === "Update") {
        //alert('hi');
        var data = {
            id: $('#caseexpenseid').val(),
            datetime: $('#datetime').val(),
            note: $('#note').val(),
            offerby: $('#offerby').val(),
            startdate: $('#startdate').val(),
            enddate: $('#enddate').val(),
            createdate: $('#createdate').val(),
            civilcaseid: $('#civilcaseid').val(),

        };
        $.ajax({
            url: "/api/Caseexpenses/" + data.id,
            data: JSON.stringify(data),
            type: "PUT",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                toastr.success("Updated successfully.", "Server Response");
                $('#caseexpenseTable').DataTable().ajax.reload();
                document.getElementById('btnSaveCaseexpense').innerText = 'Add New';
                DisableControl();
                ClearControl();
                //$('#CaseexpenseModal').modal('hide');
                //tableCaseexpenses.ajax.reload();
                //$('#registerModal').modal('hide');

            },
            error: function (errormessage) {
                toastr.error("This Caseexpense can't Update.", "Server Response");

            }
        });
    }
}


function CaseexpenseEdit(id) {
    ClearControl();
    EnableControl();

    //$('#status').val('');
    action = document.getElementById('btnSaveCaseexpense').innerText = "Update";

    $.ajax({
        url: "/api/Caseexpenses/?id=" + id + "&b=2",
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {

            $('#caseexpenseid').val(result.id);

            var date = new Date(result.datetime);
            var datetime = moment(date).format('YYYY-MM-DD');
            $('#datetime').val(datetime);
            var date = new Date(result.createdate);
            var datetime = moment(date).format('YYYY-MM-DD');
            $('#createdate').val(datetime);
            
            $('#offerby').val(result.offerby);
            $('#note').val(result.note);
            $('#startdate').val(result.startdate);
            $('#enddate').val(result.enddate);

            $("#CaseexpenseModal").modal('show');
        },
        error: function (errormessage) {
            toastr.error("Something unexpected happen.", "Server Response");
        }
    });
    return false;
}


function CaseexpenseDelete(id) {
    //alert('hi');
    bootbox.confirm({
        title: "",
        message: "Are you sure want to delete this?",
        button: {
            cancel: {
                label: "Cancel",
                ClassName: "btn-default",
            },
            confirm: {
                label: "Delete",
                ClassName: "btn-danger"
            }
        },
        callback: function (result) {
            //alert(id);
            if (result) {
                $.ajax({
                    url: "/api/Caseexpenses/" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        $('#caseexpenseTable').DataTable().ajax.reload();

                        toastr.success("Caseexpense Deleted successfully!", "Service Response");
                    },
                    error: function (errormessage) {
                        toastr.error("Caseexpense Can't be Deleted", "Service Response");
                    }
                });
            }
        }
    });
}



function DisableControl() {
    document.getElementById('datetime').disabled = true;
    document.getElementById('note').disabled = true;
    document.getElementById('offerby').disabled = true;
    document.getElementById('startdate').disabled = true;
    document.getElementById('enddate').disabled = true;
    document.getElementById('createdate').disabled = true;
}

function EnableControl() {

    document.getElementById('datetime').disabled = false;
    document.getElementById('note').disabled = false;
    document.getElementById('offerby').disabled = false;
    document.getElementById('startdate').disabled = false;
    document.getElementById('enddate').disabled = false;
    document.getElementById('createdate').disabled = false;
}

function ClearControl() {
    $('#datetime').val('');
    $('#note').val('');
    $('#offerby').val('');
    $('#startdate').val('');
    $('#enddate').val('');
    $('#createdate').val('');

}

//function CaseexpenseAction() {
//    document.getElementById('btnSaveCaseexpense').innerText = "Add New";
//    DisableControl();
//    ClearControl();
//}

function Validate() {
    var isValid = true;
    var formAddEdit = $("#formTrainingProgramAdd");
    if ($('#CaseexpenseTittle').val().trim() === "") {
        $('#CaseexpenseTittle').css('border-color', 'red');
        $('#CaseexpenseTittle').focus();
        toastr.info("Please enter Caseexpense title", "Required");
    }
    else {
        $('#CaseexpenseTittle').css('border-color', '#cccccc');
        if ($('#createBy').val().trim() === "") {
            $('#createBy').css('border-color', 'red');
            $('#createBy').focus();
            toastr.info("Please enter your phone", "Required");
        }
        else {
            $('#createBy').css('border-color', '#cccccc');
        }
    }
    return isValid;
}

function CivilcaseAction() {
    //alert('hi');
    var action = '';
    action = document.getElementById('btnCivilCase').innerText;
    if (action === " បង្កើតថ្មី / Add New") {
        $('#zcivilcasedate').val(moment().format('YYYY-MM-DD'));
        document.getElementById('btnCivilCase').innerHTML = "<span class='glyphicon glyphicon-floppy-save'></span> <span class='kh'>រក្សាទុក</span> / Save";
        document.getElementById('zcivilcaseno').disabled = false;
        document.getElementById('zdocumentno').disabled = false;
        document.getElementById('zcasetypeid').disabled = false;
        document.getElementById('zcaseid').disabled = false;
        document.getElementById('zdefendentid').disabled = false;
        document.getElementById('zplaintiffid').disabled = false;
        document.getElementById('zlawyerid').disabled = false;
        document.getElementById('zcharges').disabled = false;
        document.getElementById('zservices').disabled = false;
        document.getElementById('zcasestatus').disabled = false;
        document.getElementById('zpaidstatus').disabled = false;
        document.getElementById('zlawrelateid').disabled = false;
        document.getElementById('zcustomerid').disabled = false;
        document.getElementById('zofficeid').disabled = false;
        document.getElementById('zcasepositionid').disabled = false;
        getMaxInvNo();
        $("#zcasetypeid").focus();

        //CaseProcceding
        $('#zcaseproceedingdate').val(moment().format('YYYY-MM-DD'));
        document.getElementById('zcaseprocedinglawyerid').disabled = false;
        document.getElementById('zcaseprocedingtypeid').disabled = false;

        //CaseTribunal
        document.getElementById('zcasetribunalid').disabled = false;
        document.getElementById('zcaselawyerids').disabled = false;
        document.getElementById('zcasetribunaldate').disabled = false;
        document.getElementById('zcasejudgeid').disabled = false;
        document.getElementById('zcaseclerkid').disabled = false;
        document.getElementById('zcaseofficerid').disabled = false;
        document.getElementById('zcaselocation').disabled = false;
        $('#zcasetribunaldate').val(moment().format('YYYY-MM-DD'));
        $("#zcharges").val("0.00");
        $("#zservices").val("0.00");
      
    } else if (action === " រក្សាទុក / Save") {
        //alert('hi');
            var data = new FormData();
            data.append("zcivilcasedate", $("#zcivilcasedate").val());
            data.append("zcivilcaseno", $("#zcivilcaseno").val());
            data.append("zcasetypeid", $("#zcasetypeid").val());
            data.append("zcaseid", $("#zcaseid").val());
            data.append("zdefendentid", $("#zdefendentid").val());
            data.append("zplaintiffid", $("#zplaintiffid").val());
            data.append("zlawyerid", $("#zlawyerid").val());
            data.append("zcharges", $("#zcharges").val());
            data.append("zservices", $("#zservices").val());
            data.append("zcasestatus", $("#zcasestatus").val());
            data.append("zpaidstatus", $("#zpaidstatus").val());
            data.append("zcustomerid", $("#zcustomerid").val());
            data.append("zlawrelateid", $("#zlawrelateid").val());
            data.append("zdocumentno", $("#zdocumentno").val());
            data.append("zofficeid", $("#zofficeid").val());
            data.append("zcasepositionid", $("#zcasepositionid").val());

            //For CaseProceding
            data.append("pdate", $("#zcaseproceedingdate").val());
            data.append("plawyerid", $("#zcaseprocedinglawyerid").val());
            data.append("pcaseprocedingtypeid", $("#zcaseprocedingtypeid").val());

            //For CaseTribunal
            data.append("tdate", $("#zcasetribunaldate").val());
            data.append("ttribunalid", $("#zcasetribunalid").val());
            data.append("tlawyerid", $("#zcaselawyerids").val());
            data.append("tjudgeid", $("#zcasejudgeid").val());
            data.append("tclerkid", $("#zcaseclerkid").val());
            data.append("tofficerid", $("#zcaseofficerid").val());
            data.append("tlocation", $("#zcaselocation").val());
            data.append("tnote", "");
            //console.log(data);
            //alert('hi');
            $.ajax({
                type: "POST",
                url: "/api/civilcases",
                contentType: false,
                processData: false,
                data: data,
                success: function (result) {
                    toastr.success("Civilcase has been created successfully.", "Server Response");
                    tableEmployee.ajax.reload();
                    $('#civilcaseId').val(result.id);
                    $('#civilcaseModel').modal('hide');
                    document.getElementById('btnCivilCase').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                    //DisableControl();
                    //ClearData();

            },
            error: function (error) {
                //console.log(error);
                toastr.error("Please check all selected field!.", "Server Response");
            }
        });

        //maritalstatus

    } else if (action === " កែប្រែ / Update") {
        var data = new FormData();
        data.append("id", $("#zcivilcaseid").val());
        data.append("civilcasedate", $("#zcivilcasedate").val());
        data.append("civilcaseno", $("#zidno").val());
        data.append("casetypeid", $("#zcasetypeid").val());
        data.append("caseid", $("#zcaseid").val());
        data.append("defendentid", $("#zdefendentid").val());
        data.append("plaintiffid", $("#zplaintiffid").val());
        data.append("lawyerid", $("#zlawyerid").val());
        data.append("charges", $("#zcharges").val());
        data.append("services", $("#zservices").val());
        data.append("casestatus", $("#zcasestatus").val());
        data.append("paidstatus", $("#zpaidstatus").val());
        data.append("customerid", $("#zcustomerid").val());
        data.append("lawrelateid", $("#zlawrelateid").val());
        data.append("documentno", $("#zdocumentno").val());
        data.append("officeid", $("#zofficeid").val());
        data.append("casepositionid", $("#zcasepositionid").val());

        //For CaseProceding
        data.append("pid", $("#zpid").val());
        data.append("pcivilcaseid", $("#zcivilcaseid").val());
        data.append("pdate", $("#zcaseproceedingdate").val());
        data.append("plawyerid", $("#zcaseprocedinglawyerid").val());
        data.append("pcaseprocedingtypeid", $("#zcaseprocedingtypeid").val());

        //For CaseTribunal
        data.append("tid", $("#ztid").val());
        data.append("tcivilcaseid", $("#zcivilcaseid").val());
        data.append("tdate", $("#zcasetribunaldate").val());
        data.append("ttribunalid", $("#zcasetribunalid").val());
        data.append("tlawyerid", $("#zcaselawyerids").val());
        data.append("tjudgeid", $("#zcasejudgeid").val());
        data.append("tclerkid", $("#zcaseclerkid").val());
        data.append("tofficerid", $("#zcaseofficerid").val());
        data.append("tlocation", $("#zcaselocation").val());
        data.append("tnote", "");

        $.ajax({
            type: "PUT",
            url: "/api/civilcases/" + $('#zcivilcaseid').val(),
            contentType: false,
            processData: false,
            data: data,
            success: function (result) {
                toastr.success("Civilcase has been created successfully.", "Server Response");
                tableEmployee.ajax.reload();
                $('#civilcaseId').val(result.id);
                $('#civilcaseModel').modal('hide');
                document.getElementById('btnCivilCase').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                //DisableControl();
                //ClearData();

            },
            error: function (error) {
                //console.log(error);
                toastr.error("Civilcase Already Exists!.", "Server Response");
            }
        });
        //var response = Validate();
        //if (response == false) {
        //    return false;
        //}
       
        //var data = {
        //    id: $("civilcaseid").val(),
        //    civilcasedate: $("#civilcasedate").val(),
        //    civilcaseno: $("#idno").val(),
        //    casetypeid: $("#casetypeid").val(),
        //    caseid: $("#caseid").val(),
        //    defendentid: $("#defendentid").val(),
        //    plaintiffid: $("#plaintiffid").val(),
        //    lawyerid: $("#lawyerid").val(),
        //    charges: $("#charges").val(),
        //    services: $("#services").val(),
        //    casestatus: $("#casestatus").val(),
        //    paidstatus: $("#paidstatus").val(),
        //    customerid: $("#customerid").val(),
        //    lawrelateid: $("#lawrelateid").val(),
        //    documentno: $("#documentno").val(),
        //    officeid: $("#officeid").val(),
        //    casepositionid: $("#casepositionid").val(),
        //};

        //$.ajax({
        //    url: "/api/civilcases/" + $('#civilcaseid').val(),
        //    data: JSON.stringify(data),
        //    type: "PUT",
        //    contentType: "application/json;charset=utf-8",
        //    success: function (data) {
        //        //alert('hi');
        //        toastr.success("Civil Case has been updated successfully.", "Server Response");
        //        $('#civilcaseModel').modal('hide');
        //        tableEmployee.ajax.reload();
        //    },
        //    error: function (err) {
        //        //console.log("False")
        //        toastr.warning("You don't have permission to update Record!");
        //    }
        //});

    }
}

function getMaxInvNo() {
    $.get("/api/civilcases/?a=1&b=2", function (data, status) {
        // alert("Data: " + data + "\nStatus: " + status);
        var arrayData = data.split(',');
        //alert(arrayData[0]);
        //alert(arrayData[1]);
        $('#zcivilcaseno').val(arrayData[1]);
        //$('#idnoformat').val(arrayData[0]);

    });
}

function civilcaseEdit(id) {
    $.ajax({
        url: "/api/civilcases/" + id,
        type: "GET",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        success: function (result) {
            console.log(result);
            $('#zcivilcaseid').val(result.id);
            var dr = moment(result.date).format("YYYY-MM-DD");
            $("#zcivilcasedate").val(dr);
            var doc = "VLEX" + ("000000" + result.civilcaseno).slice(-6);
            $('#zcivilcaseno').val(doc);
            $('#zidno').val(result.civilcaseno);
            //$('#zcasetypeid').val(result.casetypeid).trigger("change");
            $('#zcasetypeid').val(result.casetypeid).trigger("change");
            $('#zdefendentid').val(result.defendentid).trigger("change");
            $('#zplaintiffid').val(result.plaintiffid).trigger("change");
            $("#zcaseid").val(result.caseid).change();
            $('#zlawyerid').val(result.lawyerid).change();
            $('#zcharges').val(result.charges);
            $('#zservices').val(result.services);
            $('#zcasestatus').val(result.casestatus);
            $('#zstatus').val(result.status);
            $('#zlawrelateid').val(result.lawrelateid).change();
            $('#zcustomerid').val(result.customerid).change();
            $('#zdocumentno').val(result.documentno);
            $('#zofficeid').val(result.officeid).change();
            $('#zcasepositionid').val(result.casepositionid).change();

            ////CaseProcceding
            $('#zpid').val(result.pid);
            var dr = moment(result.caseproceedingdate).format("YYYY-MM-DD");
            $("#zcaseproceedingdate").val(dr);
            $('#zcaseprocedinglawyerid').val(result.caseprocedinglawyerid).change();
            $('#zcaseprocedingnote').val(result.caseprocedingnote);

            ////CaseTribunal
            $('#ztid').val(result.tid);
            var dr = moment(result.casetribunaldate).format("YYYY-MM-DD");
            $("#zcasetribunaldate").val(dr);
            $('#zcasetribunalid').val(result.casetribunalid).change();
            $('#zcaselawyerids').val(result.caselawyerids).change();
            $('#zcasejudgeid').val(result.casejudgeid).change();
            $('#zcaseclerkid').val(result.caseclerkid).change();
            $('#zcaseofficerid').val(result.caseofficerid).change();
            $('#zcaselocation').val(result.caselocation);

            $('#civilcaseModal').modal('show');
            document.getElementById('btnCivilCase').innerHTML = "<span class='glyphicon glyphicon-saved'></span> <span class='kh'>កែប្រែ</span> / Update";
        },
        error: function (errormessage) {
            toastr.error("No Record Select!", "Service Response");
        }
    });


    

}

function civilcaseDelete(id) {
    bootbox.confirm({
        title: "",
        message: "Are you sure want to delete this?",
        button: {
            cancel: {
                label: "Cancel",
                ClassName: "btn-default",
            },
            confirm: {
                label: "Delete",
                ClassName: "btn-danger"
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: "/api/civilcases/" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        tableEmployee.ajax.reload();
                        toastr.success("Civilcase has been Deleted successfully!", "Service Response");
                    },
                    error: function (errormessage) {
                        toastr.error("This Civilcase is already exists in Database", "Service Response");
                    }
                });
            }
        }
    });
}

function OpenBonusModel(id) {
    GetBonus(id);   // salary.js
    $("#employeeid").val(id);
    $("#bonusModel").modal('show');
}

function employeeSalary(id) {
    //alert(id);
    GetSalary(id);   // salary.js
    //alert(id);
    $("#empid").val(id);
    $("#salaryModel").modal('show');

}


//Payment
function GetCivilcasePayment(id) {
    $('#civilcasepaymentModal').modal('show');
    document.getElementById('civilcasepaymentdate').disabled = true;
    document.getElementById('paymentpaidamount').disabled = true;
    document.getElementById('paymentnote').disabled = true;
    document.getElementById('civilcasepaymentduedate').disabled = true;
    document.getElementById('paymentreferred').disabled = true;
    document.getElementById('paymentpaystatus').disabled = true;
    document.getElementById('civilcasepaymentpaiddate').disabled = true;
    document.getElementById('paymentbillgroup').disabled = true;
    document.getElementById('paymentmethodid').disabled = true;
    document.getElementById('btnCivilPaymentAction').innerText = "Add New";
    $('#civilcaseid').val(id);
    //alert(id);
    tablePayment = $('#paymentTable').DataTable({
        ajax: {
            url: "/api/civilcasepayments/" +id,
            dataSrc: ""
        },
        columns: [
            {
                data: "id"
            },
            {
                data: "date",
                render: function (data) {
                    return moment(new Date(data)).format('DD-MMM-YYYY');
                }
            },
            //{
            //    data: "civilcaseid",
            //},
            //{
            //    data: "paidby",
            //},
            {
                data: "paidamount"
            },
            {
                data: "paymentmethodname"
            },
            {
                data: "paystatus"
            },
             
            {
                data: "id",
                render: function (data) {
                    return "<button onclick='openInvoice(" + data + ")' class='btn btn-success btn-xs pull-center' style='margin-right:5px;'></span> Print</button>" + "<button onclick='paymentEdit(" + data + ")' class='btn btn-warning btn-xs' style='margin-right:5px;'>Edit</button>" + "<button onclick='paymentDelete(" + data + ")' class='btn btn-danger btn-xs'>Delete</button>";
                }
            }
        ],
        destroy: true,
        "order": [0, "desc"],
        "info": false
    });
    //alert('Hi');
}

function CivilCasePaymentAction() {
    if ($("#paymentbillgroup").is(':checked')) {
        billgroup = 1;
    } else {
        billgroup = 0;
    }
    var action = '';
    action = document.getElementById('btnCivilPaymentAction').innerText;
    if (action == "Add New") {
        document.getElementById('civilcasepaymentdate').disabled = false;
        document.getElementById('paymentpaidamount').disabled = false;
        document.getElementById('paymentnote').disabled = false;
        document.getElementById('civilcasepaymentduedate').disabled = false;
        document.getElementById('paymentreferred').disabled = false;
        document.getElementById('paymentpaystatus').disabled = false;
        document.getElementById('civilcasepaymentpaiddate').disabled = false;
        document.getElementById('paymentbillgroup').disabled = false;
        document.getElementById('paymentmethodid').disabled = false;


        document.getElementById('btnCivilPaymentAction').innerText = "Save";
        $('#civilcasepaymentdate').val(moment().format('YYYY-MM-DD'));
        $('#paymentnote').val('');
        $('#paymentpaidamount').val('0.00');
        $('#civilcasepaymentduedate').val(moment().format('YYYY-MM-DD'));
        $('#civilcasepaymentpaiddate').val(moment().format('YYYY-MM-DD'));
        $('#paymentreferred').val('');
        //alert(id);
    } else if (action == "Save") {
        var dataOrder = {
            date: $('#civilcasepaymentdate').val(),
            civilcaseid: $('#civilcaseid').val(),
            paidamount: $('#paymentpaidamount').val(),
            note: $('#paymentnote').val(),
            duedate: $('#civilcasepaymentduedate').val(),
            referred: $('#paymentreferred').val(),
            paystatus: $('#paymentpaystatus').val(),
            paiddate: $('#civilcasepaymentpaiddate').val(),
            paymentmethodid: $('#paymentmethodid').val(),
            billgroup: billgroup,
        }
        //console.log(dataOrder);
        $.ajax({
            url: "/api/CivilcasePayments",
            type: "POST",
            data: JSON.stringify(dataOrder),
            contentType: "application/json",
            success: function (data) {
                toastr.success("Payment has been created successfully.", "Server Response");
                tablePayment.ajax.reload();
                document.getElementById('btnCivilPaymentAction').innerText = "Add New";
            },
            error: function (err) {
                toastr.error("Record Save Error");
            }
        });
    } else if (action == "Update") {

        var dataOrder = {
            id: $('#civilcasepaymentid').val(),
            date: $('#civilcasepaymentdate').val(),
            civilcaseid: $('#civilcaseid').val(),
            paidamount: $('#paymentpaidamount').val(),
            note: $('#paymentnote').val(),
            duedate: $('#civilcasepaymentduedate').val(),
            referred: $('#paymentreferred').val(),
            paystatus: $('#paymentpaystatus').val(),
            paiddate: $('#civilcasepaymentpaiddate').val(),
            paymentmethodid: $('#paymentmethodid').val(),
            billgroup: billgroup,
        }

        console.log(dataOrder);
        $.ajax({
            url: "/api/CivilcasePayments/" + dataOrder.id,
            type: "PUT",
            data: JSON.stringify(dataOrder),
            contentType: "application/json",
            success: function (data) {
                toastr.success("Payment has been updated successfully.", "Server Response");
                tablePayment.ajax.reload();
                document.getElementById('btnCivilPaymentAction').innerText = "Add New";
            },
            error: function (err) {
                toastr.warning("You don't have permission update Record!");
            }
        });
    }

}

function paymentEdit(id) {
    $('#civilcasepaymentModal').modal('show');
    document.getElementById('civilcasepaymentdate').disabled = false;
    document.getElementById('paymentpaidamount').disabled = false;
    document.getElementById('paymentnote').disabled = false;
    document.getElementById('civilcasepaymentduedate').disabled = false;
    document.getElementById('paymentreferred').disabled = false;
    document.getElementById('paymentpaystatus').disabled = false;
    document.getElementById('civilcasepaymentpaiddate').disabled = false;
    document.getElementById('paymentbillgroup').disabled = false;
    document.getElementById('paymentmethodid').disabled = false;
    $.ajax({
        url: "/api/CivilcasePayments/?id=" + id + "&b=2",
        type: "GET",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        success: function (result) {
            $('#civilcasepaymentid').val(result.id);
            var dr = moment(result.date).format("YYYY-MM-DD");
            $("#civilcasepaymentdate").val(dr);
            $("#civilcaseid").val(result.civilcaseid);
            $("#paymentpaidamount").val(result.paidamount);
            $('#paymentnote').val(result.note);
            var due = moment(result.duedate).format("YYYY-MM-DD");
            $("#civilcasepaymentduedate").val(due);
            $("#paymentreferred").val(result.referred);
            $("#paymentpaystatus").val(result.paystatus);
            var paid = moment(result.paiddate).format("YYYY-MM-DD");
            $("#civilcasepaymentpaiddate").val(paid);
            $("#paymentbillgroup").prop('checked', result.billgroup > 0),
            $('#paymentmethodid').val(result.paymentmethodid);
            document.getElementById('btnCivilPaymentAction').innerText = "Update";
        },
        error: function (errormessage) {
            toastr.error("Load Record Error", "Service Response");
        }
    });

}

function paymentDelete(id) {
    bootbox.confirm({
        title: "",
        message: "Are you sure want to delete this?",
        button: {
            cancel: {
                label: "Cancel",
                ClassName: "btn-default",
            },
            confirm: {
                label: "Delete",
                ClassName: "btn-danger"
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: "/api/CivilcasePayments/" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        tablePayment.ajax.reload();
                        toastr.success("Payment has been Deleted successfully!", "Service Response");

                    },
                    error: function (errormessage) {
                        toastr.warning("You don't have permission!", "Service Response");

                    }
                });
            }
        }
    });

}

function openInvoice(id) {
    window.open("/customer-invoice/" + id, "_blank")
}

function openInvoiceBillgrouop(id) {
    var id = $('#civilcaseid').val();
    //alert(id);
    window.open("/customer-invoice-billgroup/" + id, "_blank")
}

//Tribunal
function GetCivilTribunal(id) {
    $('#civilcasetribunalModal').modal('show');
    document.getElementById('ccivilcaseid').disabled = true;
    document.getElementById('ctribunaldate').disabled = true;
    document.getElementById('ctribunalid').disabled = true;
    document.getElementById('clawyerid').disabled = true;
    document.getElementById('cjudgeid').disabled = true;
    document.getElementById('cclerkid').disabled = true;
    document.getElementById('cofficerid').disabled = true;
    document.getElementById('clocation').disabled = true;
    document.getElementById('cnote').disabled = true;
    document.getElementById('btnCTribunalAction').innerText = "Add New";
    $('#ccivilcaseid').val(id);

    tableCTribunal = $('#ctribunalTable').DataTable({
        ajax: {
            url: "/api/civilcasetribunals/" + id,
            dataSrc: ""
        },
        columns: [
            {
                data: "id"
            },
            {
                data: "date",
                render: function (data) {
                    return moment(new Date(data)).format('DD-MMM-YYYY');
                }
            },
            {
                data: "civilcaseno", render: function (data) {

                    return "VLEX" + ("000000" + data).slice(-6);
                }
            },
            {
                data: "tribunalname"
            },
            {
                data: "lawyername"
            },
            {
                data: "judgename",
            },
            {
                data: "clerkname",
            },
             {
                 data: "officername",
             },
             {
                 data: "note",
             },
            {
                data: "id",
                render: function (data) {
                    return "<button onclick='ctribunalEdit(" + data + ")' class='btn btn-warning btn-xs' style='margin-right:5px;'>Edit</button>" + "<button onclick='ctibunalDelete(" + data + ")' class='btn btn-danger btn-xs'>Delete</button>";
                }
            }
        ],
        destroy: true,
        "order": [0, "desc"],
        "info": false
    });
    //alert('Hi');
}

function CTribunalAction() {
    var action = '';
    action = document.getElementById('btnCTribunalAction').innerText;
    if (action == "Add New") {
        document.getElementById('ctribunaldate').disabled = false;
        document.getElementById('ctribunalid').disabled = false;
        document.getElementById('clawyerid').disabled = false;
        document.getElementById('cjudgeid').disabled = false;
        document.getElementById('cclerkid').disabled = false;
        document.getElementById('cofficerid').disabled = false;
        document.getElementById('clocation').disabled = false;
        document.getElementById('cnote').disabled = false;
        document.getElementById('btnCTribunalAction').innerText = "Save";
        $('#ctribunaldate').val(moment().format('YYYY-MM-DD'));
        $('#clocation').val('');
        $('#cnote').val('');
        //alert(id);
    } else if (action == "Save") {
        var dataOrder = {
            date: $('#ctribunaldate').val(),
            civilcaseid: $('#ccivilcaseid').val(),
            tribunalid: $('#ctribunalid').val(),
            lawyerid: $('#clawyerid').val(),
            judgeid: $('#cjudgeid').val(),
            clerkid: $('#cclerkid').val(),
            officerid: $('#cofficerid').val(),
            location: $('#clocation').val(),
            note: $('#cnote').val(),

        }
        //console.log(dataOrder);
        $.ajax({
            url: "/api/civilcasetribunals",
            type: "POST",
            data: JSON.stringify(dataOrder),
            contentType: "application/json",
            success: function (data) {
                toastr.success("Civilcase Tribunal has been created successfully.", "Server Response");
                tableCTribunal.ajax.reload();
                document.getElementById('btnCTribunalAction').innerText = "Add New";
            },
            error: function (err) {
                toastr.error("Record Save Error");
            }
        });
    } else if (action == "Update") {

        var dataOrder = {
            id: $('#cvtribunalid').val(),
            date: $('#ctribunaldate').val(),
            civilcaseid: $('#ccivilcaseid').val(),
            tribunalid: $('#ctribunalid').val(),
            lawyerid: $('#clawyerid').val(),
            judgeid: $('#cjudgeid').val(),
            clerkid: $('#cclerkid').val(),
            officerid: $('#cofficerid').val(),
            location: $('#clocation').val(),
            note: $('#cnote').val(),
        }

        console.log(dataOrder);
        $.ajax({
            url: "/api/civilcasetribunals/" + dataOrder.id,
            type: "PUT",
            data: JSON.stringify(dataOrder),
            contentType: "application/json",
            success: function (data) {
                toastr.success("CivilCase Tribunal has been updated successfully.", "Server Response");
                tableCTribunal.ajax.reload();
                document.getElementById('btnCTribunalAction').innerText = "Add New";
            },
            error: function (err) {
                toastr.warning("You don't have permission update Record!");
            }
        });
    }

}

function ctribunalEdit(id) {
    //$('#civilcasepaymentModal').modal('show');
    document.getElementById('ctribunaldate').disabled = false;
    document.getElementById('ctribunalid').disabled = false;
    document.getElementById('clawyerid').disabled = false;
    document.getElementById('cjudgeid').disabled = false;
    document.getElementById('clocation').disabled = false;
    document.getElementById('cclerkid').disabled = false;
    document.getElementById('cofficerid').disabled = false;
    document.getElementById('cnote').disabled = false;
    $.ajax({
        url: "/api/civilcasetribunals/?id=" + id + "&b=2",
        type: "GET",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        success: function (result) {
            $('#cvtribunalid').val(result.id);
            var dr = moment(result.date).format("YYYY-MM-DD");
            $("#ctribunaldate").val(dr);
            $("#ccivilcaseid").val(result.civilcaseid);
            $("#ctribunalid").val(result.tribunalid);
            $('#clawyerid').val(result.lawyerid);
            $('#cjudgeid').val(result.judgeid);
            $('#cclerkid').val(result.clerkid);
            $('#cofficerid').val(result.officerid);
            $("#clocation").val(result.location);
            $('#cnote').val(result.note);
            document.getElementById('btnCTribunalAction').innerText = "Update";
        },
        error: function (errormessage) {
            toastr.error("Load Record Error", "Service Response");
        }
    });

}

function ctibunalDelete(id) {
    bootbox.confirm({
        title: "",
        message: "Are you sure want to delete this?",
        button: {
            cancel: {
                label: "Cancel",
                ClassName: "btn-default",
            },
            confirm: {
                label: "Delete",
                ClassName: "btn-danger"
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: "/api/civilcasetribunals/" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        tableCTribunal.ajax.reload();
                        toastr.success("Civilcase Tribunal has been Deleted successfully!", "Service Response");

                    },
                    error: function (errormessage) {
                        toastr.warning("You don't have permission!", "Service Response");

                    }
                });
            }
        }
    });

}

//Proceeding
function GetCivilProceeding(id) {
    $('#civilcaseproceedingModal').modal('show');
    document.getElementById('cproceedingdate').disabled = true;
    document.getElementById('cpcivilcaseid').disabled = true;
    document.getElementById('cplawyerid').disabled = true;
    document.getElementById('cproccedingtypeid').disabled = true;
    document.getElementById('btnCProceedingAction').innerText = "Add New";
    $('#cpcivilcaseid').val(id);

    tableCProceeding = $('#cproceedingTable').DataTable({
        ajax: {
            url: "/api/civilcaseproceedings/" + id,
            dataSrc: ""
        },
        columns: [
            {
                data: "id"
            },
            {
                data: "date",
                render: function (data) {
                    return moment(new Date(data)).format('DD-MMM-YYYY');
                }
            },
            {
                data: "civilcaseno", render: function (data) {

                    return "VLEX" + ("000000" + data).slice(-6);
                }
            },
            {
                data: "lawyername"
            },
            {
                data: "proccedingtype",
            },
            {
                data: "id",
                render: function (data) {
                    return "<button onclick='cproceedingEdit(" + data + ")' class='btn btn-warning btn-xs' style='margin-right:5px;'>Edit</button>" + "<button onclick='cproceedingDelete(" + data + ")' class='btn btn-danger btn-xs'>Delete</button>";
                }
            }
        ],
        destroy: true,
        "order": [0, "desc"],
        "info": false
    });
    //alert('Hi');
}

function CProceedingAction() {
    var action = '';
    action = document.getElementById('btnCProceedingAction').innerText;
    if (action == "Add New") {
        document.getElementById('cproceedingdate').disabled = false;
        //document.getElementById('cpcivilcaseid').disabled = false;
        document.getElementById('cplawyerid').disabled = false;
        document.getElementById('cproccedingtypeid').disabled = false;
        document.getElementById('btnCProceedingAction').innerText = "Save";
        $('#cproceedingdate').val(moment().format('YYYY-MM-DD'));
        $('#cproccedingtype').val('');
        //alert(id);
    } else if (action == "Save") {
        var dataOrder = {
            date: $('#cproceedingdate').val(),
            civilcaseid: $('#cpcivilcaseid').val(),
            lawyerid: $('#cplawyerid').val(),
            proccedingtypeid: $('#cproccedingtypeid').val(),

        }
        //console.log(dataOrder);
        $.ajax({
            url: "/api/civilcaseproceedings",
            type: "POST",
            data: JSON.stringify(dataOrder),
            contentType: "application/json",
            success: function (data) {
                toastr.success("Case Proceeding has been created successfully.", "Server Response");
                tableCProceeding.ajax.reload();
                document.getElementById('btnCProceedingAction').innerText = "Add New";
            },
            error: function (err) {
                toastr.error("Record Save Error");
            }
        });
    } else if (action == "Update") {

        var dataOrder = {
            id: $('#cvproceedingid').val(),
            date: $('#cproceedingdate').val(),
            civilcaseid: $('#cpcivilcaseid').val(),
            lawyerid: $('#cplawyerid').val(),
            proccedingtypeid: $('#cproccedingtypeid').val(),
        }

        console.log(dataOrder);
        $.ajax({
            url: "/api/civilcaseproceedings/" + dataOrder.id,
            type: "PUT",
            data: JSON.stringify(dataOrder),
            contentType: "application/json",
            success: function (data) {
                toastr.success("CivilCase Proceeding has been updated successfully.", "Server Response");
                tableCProceeding.ajax.reload();
                document.getElementById('btnCProceedingAction').innerText = "Add New";
            },
            error: function (err) {
                toastr.warning("You don't have permission update Record!");
            }
        });
    }

}

function cproceedingEdit(id) {
    //$('#civilcasepaymentModal').modal('show');
    document.getElementById('cproceedingdate').disabled = false;
    //document.getElementById('cpcivilcaseid').disabled = false;
    document.getElementById('cplawyerid').disabled = false;
    document.getElementById('cproccedingtypeid').disabled = false;
    $.ajax({
        url: "/api/civilcaseproceedings/?id=" + id + "&b=2",
        type: "GET",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        success: function (result) {
            $('#cvproceedingid').val(result.id);
            var dr = moment(result.date).format("YYYY-MM-DD");
            $("#cproceedingdate").val(dr);
            $("#cpcivilcaseid").val(result.civilcaseid);
            $('#cplawyerid').val(result.lawyerid);
            $('#cproccedingtypeid').val(result.proccedingtypeid);
            document.getElementById('btnCProceedingAction').innerText = "Update";
        },
        error: function (errormessage) {
            toastr.error("Load Record Error", "Service Response");
        }
    });

}

function cproceedingDelete(id) {
    bootbox.confirm({
        title: "",
        message: "Are you sure want to delete this?",
        button: {
            cancel: {
                label: "Cancel",
                ClassName: "btn-default",
            },
            confirm: {
                label: "Delete",
                ClassName: "btn-danger"
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: "/api/civilcaseproceedings/" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        tableCProceeding.ajax.reload();
                        toastr.success("Civilcase Proceeding has been Deleted successfully!", "Service Response");

                    },
                    error: function (errormessage) {
                        toastr.warning("You don't have permission!", "Service Response");

                    }
                });
            }
        }
    });

}
