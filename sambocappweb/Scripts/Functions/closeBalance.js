﻿
$(document).ready(function(){
    GetCloseBalance();
    $('#todate').focus().val(moment().format('YYYY-MM-DD'));
    //GetCloseBalanceByDate();
});

$('input[name=todate]').change(function () {
    var selectdate = $('#todate').val();    
    GetCloseBalanceByDate(selectdate);
    document.getElementById('btnCloseBalance').innerText = "Add New";
    document.getElementById('startbalances').disabled = true;
    document.getElementById('civilCasePay').disabled = true;
    document.getElementById('contractPay').disabled = true;
    document.getElementById('otherIncome').disabled = true;
    document.getElementById('otherExpense').disabled = true;
    document.getElementById('endBalance').disabled = true;
    

});

var tableCloseBalance = [];
function GetCloseBalanceByDate(selectdatesd) {

    tableCloseBalance = $('#closeBalanceTable').DataTable({
        ajax: {
            url: "/api/CloseBalance?selectdatesd=" + selectdatesd,
            dataSrc: ""
        },
        columns: [
            {
                data: "startbalance",
            },
            {
                data: "civilcasepaid"
            },
            {
                data: "contractpaid"
            },
            {
                data: "otherincome"
            },
            {
                data: "otherexpend"
            },
            {
                data: "endbalance"
            },
            {
                data: "id",
                render: function (data) {
                    return "<button onclick='CloseBalanceEdit(" + data + ")' class='btn btn-warning btn-xs' style='border-width: 0px; width: 65px; margin-right: 5px;'><span class='glyphicon glyphicon-edit'></span> Edit</button>" +
                        "<button onclick='CloseBalanceDelete(" + data + ")' class='btn btn-danger btn-xs' style='border-width: 0px;'><span class='glyphicon glyphicon-trash'></span> Delete</button>";
                },
                "width": "130px"
            }
        ],
        destroy: true,
        "order": [[0, "desc"]],
        "info": false
    });
}

function GetCloseBalance() {
    document.getElementById('startbalances').disabled = true;
    document.getElementById('civilCasePay').disabled = true;
    document.getElementById('contractPay').disabled = true;
    document.getElementById('otherIncome').disabled = true;
    document.getElementById('otherExpense').disabled = true;
    document.getElementById('endBalance').disabled = true;

    tableCloseBalance = $('#closeBalanceTable').DataTable({
        ajax: {
            url: "/api/CloseBalance",
            dataSrc: ""
        },
        columns: [
            {
                data: "startbalance",
            },
            {
                data: "civilcasepaid"
            },
            {
                data: "contractpaid"
            },
            {
                data: "otherincome"
            },
            {
                data: "otherexpend"
            },
            {
                data: "endbalance"
            },
            {
                data: "id",
                render: function (data) {
                    return "<button onclick='CloseBalanceEdit(" + data + ")' class='btn btn-warning btn-xs' style='border-width: 0px; width: 65px; margin-right: 5px;'><span class='glyphicon glyphicon-edit'></span> Edit</button>" +
                        "<button onclick='CloseBalanceDelete(" + data + ")' class='btn btn-danger btn-xs' style='border-width: 0px;'><span class='glyphicon glyphicon-trash'></span> Delete</button>";
                },
                "width": "130px"
            }
        ],
        destroy: true,
        "order": [[0, "desc"]],
        "info": false
    });
}

function CloseBalanceEdit(id) {
    $('#startbalances').val('');
    $('#civilCasePay').val('');
    $('#contractPay').val('');
    $('#otherIncome').val('');
    $('#otherExpense').val('');
    $('#endBalance').val('');
    document.getElementById('startbalances').disabled = false;
    document.getElementById('civilCasePay').disabled = false;
    document.getElementById('contractPay').disabled = false;
    document.getElementById('otherIncome').disabled = false;
    document.getElementById('otherExpense').disabled = false;
    document.getElementById('endBalance').disabled = false;

    //$('#status').val('');
    action = document.getElementById('btnCloseBalance').innerText = "Update";

    $.ajax({
        url: "/api/CloseBalance/" + id,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {

            $('#closeBalanceid').val(result.id);

            $('#startbalances').val(result.startbalance);
            $('#civilCasePay').val(result.civilcasepaid);
            $('#contractPay').val(result.contractpaid);
            $('#otherIncome').val(result.otherincome);
            $('#otherExpense').val(result.otherexpend);
            $('#endBalance').val(result.endbalance);
            var pd = moment(result.date).format("YYYY-MM-DD");
            $('#todate').val(pd);

            //$("#ContractPositionModal").modal('show');
        },
        error: function (errormessage) {
            toastr.error("Something unexpected happen.", "Server Response");
        }
    });
    return false;
}

function CalAction() {
    document.getElementById('endBalance').disabled = false;

    var startBalances = document.getElementById('startbalances').value;
    var civilCasePays = document.getElementById('civilCasePay').value;
    var contractPay = document.getElementById('contractPay').value;
    var otherIncome = document.getElementById('otherIncome').value;
    var otherExpense = document.getElementById('otherExpense').value;
    //alert(startBalances);

    document.getElementById('endBalance').value = CalEndBalance(parseFloat(startBalances), parseFloat(civilCasePays), parseFloat(contractPay), parseFloat(otherIncome), parseFloat(otherExpense));

    
}

function CloseBalanceAction() {
    
    var action = '';
    action = document.getElementById('btnCloseBalance').innerText;   
    //alert('hi');

    
    
    if (action == "Add New") {
        var date = $('#todate').val();
        //alert(date);s

        document.getElementById('btnCloseBalance').innerText = 'Submit';
        document.getElementById('startbalances').disabled = false;
        document.getElementById('civilCasePay').disabled = false;
        document.getElementById('contractPay').disabled = false;
        document.getElementById('otherIncome').disabled = false;
        document.getElementById('otherExpense').disabled = false;
        document.getElementById('endBalance').disabled = true;
        getMaxCloseBalance();
        getSumCivil(date);        
        getSumContract(date);
        getSumOtherExpend(date);
        getSumIncome(date);
        //$('#civilCasePay').focus();
        $('#todate').val(moment().format('YYYY-MM-DD'));      
        
       

        
        //var re = Sum(parseInt(totalamount), parseInt(totalcarprice), parseInt(totalshipprice));
        
    }

    if (action === "Submit") {
        //Validate();
        var data = {
            startbalance: $('#startbalances').val(),
            civilcasepaid: $('#civilCasePay').val(),
            contractpaid: $('#contractPay').val(),
            otherIncome: $('#otherIncome').val(),
            otherexpend: $('#otherExpense').val(),
            endBalance: $('#endBalance').val(),
            date: $('#todate').val(),
        };
        $.ajax({
            url: "/api/CloseBalance",
            data: JSON.stringify(data),
            type: "POST",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                toastr.success("Save to database successfully.", "Server Response");
                $('#closeBalanceTable').DataTable().ajax.reload();
                
                document.getElementById('btnCloseBalance').innerText = "Add New";
                $('#startbalances').val('');
                $('#civilCasePay').val('');
                $('#contractPay').val('');
                $('#otherIncome').val('');
                $('#otherExpense').val('');
                $('#endBalance').val('');
                document.getElementById('startbalances').disabled = true;
                document.getElementById('civilCasePay').disabled = true;
                document.getElementById('contractPay').disabled = true;
                document.getElementById('otherIncome').disabled = true;
                document.getElementById('otherExpense').disabled = true;
                document.getElementById('endBalance').disabled = true;
                //$('#ContractPositionModal').modal('hide');
                //document.getElementById('btnSaveContractPosition').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";

            },
            error: function (errormessage) {
                toastr.error("This ContractPosition is already exists.", "Server Response");
                //document.getElementById('btnSaveContractPosition').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";

            }
        });
    }
    else if (action === "Update") {
        //alert('hi');
        var data = {
            id: $('#closeBalanceid').val(),
            startbalance: $('#startbalances').val(),
            civilcasepaid: $('#civilCasePay').val(),
            contractpaid: $('#contractPay').val(),
            otherIncome: $('#otherIncome').val(),
            otherexpend: $('#otherExpense').val(),
            endBalance: $('#endBalance').val(),
            date: $('#todate').val(),
        };
        $.ajax({
            url: "/api/CloseBalance/" + data.id,
            data: JSON.stringify(data),
            type: "PUT",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                toastr.success("Updated successfully.", "Server Response");
                $('#closeBalanceTable').DataTable().ajax.reload();
                document.getElementById('btnCloseBalance').innerText = "Add New";
                document.getElementById('startbalances').disabled = true;
                document.getElementById('civilCasePay').disabled = true;
                document.getElementById('contractPay').disabled = true;
                document.getElementById('otherIncome').disabled = true;
                document.getElementById('otherExpense').disabled = true;
                document.getElementById('endBalance').disabled = true;
                $('#startbalances').val('');
                $('#civilCasePay').val('');
                $('#contractPay').val('');
                $('#otherIncome').val('');
                $('#otherExpense').val('');
                $('#endBalance').val('');
                //$('#ContractPositionModal').modal('hide');
                //tableContractPositions.ajax.reload();
                //$('#registerModal').modal('hide');

            },
            error: function (errormessage) {
                toastr.error("This ContractPosition can't Update.", "Server Response");

            }
        });
    }
}

function CalEndBalance(startBalances, civilCasePays, contractPay, otherIncome, otherExpense) {
    return (startBalances + civilCasePays + contractPay + otherIncome) - otherExpense;
}

function getMaxCloseBalance() {

    $.get("/api/CloseBalance/?a=1&b=2", function (data, status) {
        // alert("Data: " + data + "\nStatus: " + status);
        var arrayData = data.split();        
        $('#startbalances').val(arrayData);

    });
}
function getSumCivil(selectdate) {
    $.get("/api/CloseBalanceCopy?selectdate=" + selectdate, function (data, status) {
        // alert("Data: " + data + "\nStatus: " + status);
        var arrayData = data.split(',');
        $('#civilCasePay').val(arrayData);
    });
}
function getSumContract(contractDate) {
    $.get("/api/CloseBalance?contractDate=" + contractDate, function (data, status) {
        // alert("Data: " + data + "\nStatus: " + status);
        var arrayData = data.split(',');
        $('#contractPay').val(arrayData);
    });
}
function getSumOtherExpend(otherExpenddate) {
    $.get("/api/CloseBalance?otherExpenddate=" + otherExpenddate, function (data, status) {
        // alert("Data: " + data + "\nStatus: " + status);
        var arrayData = data.split(',');
        $('#otherExpense').val(arrayData);
    });
}
function getSumIncome(incomedate) {
    $.get("/api/CloseBalance?incomedate=" + incomedate, function (data, status) {
        // alert("Data: " + data + "\nStatus: " + status);
        var arrayData = data.split(',');
        $('#otherIncome').val(arrayData);
    });
}


function CloseBalanceDelete(id) {
   // alert('hi');
    bootbox.confirm({
        title: "",
        message: "Are you sure want to delete this?",
        button: {
            cancel: {
                label: "Cancel",
                ClassName: "btn-default",
            },
            confirm: {
                label: "Delete",
                ClassName: "btn-danger"
            }
        },
        callback: function (result) {
            //alert(id);
            if (result) {
                $.ajax({
                    url: "/api/CloseBalance/" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        $('#closeBalanceTable').DataTable().ajax.reload();

                        toastr.success("Close Balance Deleted successfully!", "Service Response");
                        
                        $('#startbalances').val('');
                        $('#civilCasePay').val('');
                        $('#contractPay').val('');
                        $('#otherIncome').val('');
                        $('#otherExpense').val('');
                        $('#endBalance').val('');
                        document.getElementById('startbalances').disabled = true;
                        document.getElementById('civilCasePay').disabled = true;
                        document.getElementById('contractPay').disabled = true;
                        document.getElementById('otherIncome').disabled = true;
                        document.getElementById('otherExpense').disabled = true;
                        document.getElementById('endBalance').disabled = true;
                        document.getElementById('btnCloseBalance').innerText = "Add New";
                    },
                    error: function (errormessage) {
                        toastr.error("ContractPosition Can't be Deleted", "Service Response");
                    }
                });
            }
        }
    });
}