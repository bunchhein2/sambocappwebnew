﻿$(document).ready(function () {
    $("#contractshopModel").on('show.modal.bs', function () {
        GetContract();
        //oTable = $('#tblContract').DataTable();
        //oTable.columns(0).search($('#shopID').val().trim());
        ////hit search on server
        //oTable.draw();

        oTable = $('#tblContract').DataTable();
        oTable.columns(1).search($('#shopNameCon').val().trim());
        //hit search on server
        oTable.draw();
    });
   

    //oTable = $('#tblContract').DataTable();
    //$("#id").on("change", function () {
    //    var filter = this.value;
    //    if (filter == "") {
    //        GetContract();

    //    } else {
    //        //alert('Well');
    //        oTable.columns(0).search($('#shopName').val().trim());
    //        //hit search on server
    //        oTable.draw();
    //    }
    //});
    PrintPrescription();
});
let Register1 = document.getElementById("btnSaveContract");
function btnSaveContract_load() {
    document.getElementById("btnSaveContract").disabled = true;

}



function Newcontract(id) {

    //GetContracts(id);   // contract.js
    $("#shopid").val(id);

    $("#ContractModal").modal('show');
}

function ContractAction() {
    action = document.getElementById('btnSaveContract').innerText="Save";
    
    if (action == "Save")
    {
        //ValidateShop();
        var data = new FormData();

        data.append("shopid", $('#shopid').val());
        data.append("date", $('#date').val());
        data.append("attachment", $('#attachment').val());
        data.append("note", $('#notetext').val());
        data.append("status", $('#status').val());
        
        var files = $('#attachment').get(0).files;
        if (files.length > 0) {
            data.append("attachment", files[0]);
        }
       
        var ajaxRequest = $.ajax({
            url: "/api/ContractShops",
            data: data,
            type: "POST",
            contentType: false,
            processData: false,
            dataType: "json",
            success: function (result) {
                toastr.success("New record has been Created", "Server Respond");
                $('#tblContract').DataTable().ajax.reload();
                document.getElementById('btnSaveContract').innerText = 'Add New';

                ClearControl();
                $("#ContractModal").modal('hide');
                document.getElementById('btCreatecontrct').disabled = true;
            },
            error: function (errormesage) {
                toastr.error("exist in Database", "Server Respond")
            }

        });

    } else if (action == "Update") {
        var data = new FormData();

        data.append("shopid", $('#shopid').val());
        data.append("date", $('#date').val());
        data.append("attachment", $('#attachment').val());
        data.append("note", $('#notetext').val());
        data.append("status", $('#status').val());

        var files = $('#attachment').get(0).files;
        if (files.length > 0) {
            data.append("attachment", files[0]);
        }

        $.ajax({
            url: "/api/ContractShops/" + $("#id").val(),
            data: data,
            type: "PUT",
            contentType: false,
            processData: false,
            dataType: "json",
            success: function (result) {
                toastr.success("Updated", "Server Respond");
                $('#tblContract').DataTable().ajax.reload();
                $("#ContractModal").modal('hide');
                ClearControl();
                window.DataTable.ajax.reload();
                
            },
            error: function (errormesage) {
                toastr.error("hasn't Updated in Database", "Server Respond")
            }
        });
    }
}

//function Viw(id) {
//    $.ajax({
//        url: "/api/ContractShops/" + id,
//        method: 'GET',
//        xhrFields: {
//            responseType: 'blob'
//        },
//        success: function (data) {
//            var a = document.createElement('a');
//            var url = window.URL.createObjectURL(data);
//            a.href = url;
//            a.download = .attr('src', '../Images/' + result.attachment);
//            document.body.append(a);
//            a.click();
//            a.remove();
//            window.URL.revokeObjectURL(url);
//        }
//    });
//}
function AccEdits(id) {
    EnableControl();
  
    $.ajax({
        url: "/api/ContractShops/" + id,
        type: "GET",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        success: function (result) {
            //$("#id").val(result.contractid);
            $("#date").val(result.date);
            $("#shopid").val(result.shopid);
            $("#attachment").val(result.attachment);
            $("#notetext").val(result.note);
            $("#status").val(result.status);
            
            $("#file_old").val(result.attachment);
            //QRCODE
            var files = $('#attachment').get(0).files;
            if (result.attachment == "") {
                $("#AttPhoto").attr('src', '../Images/company.png');
            } else {
                $("#AttPhoto").attr('src', '../Images/' + result.attachment);

            }
            
            $("#ContractModal").modal('show');
            action = document.getElementById('btnSaveContract').innerText = "Update";
        },
        error: function (errormessage) {
            toastr.error("Something unexpected happenn.", "Server Response");
        }
    });
}

function Delete(id) {
    bootbox.confirm({
        title: "",
        message: "Are you sure want to delete this?",
       
        buttons: {
            cancel: {
                label: "Cancel",
                className: "btn-default"
            },
            confirm: {
                label: "Delete",
                className: "btn-danger"
            }
        },
        callback: function (result) {
            //alert(id);
            if (result) {
                $.ajax({
                    url: "/api/ContractShops/" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        $('#tblContract').DataTable().ajax.reload();
                        toastr.success("Shop Deleted successfully!", "Service Response");
                    },
                    error: function (errormessage) {
                        toastr.error("Can't be deleted", "Service Response");
                    }
                });
            }
        }
    });
}


function DisableControl() {
    document.getElementById('date').disabled = true;
    document.getElementById('shopid').disabled = true;
    document.getElementById('attachment').disabled = true;
    document.getElementById('notetext').disabled = true;
    
}

function EnableControl() {
    document.getElementById('date').disabled = false;
    document.getElementById('shopid').disabled = false;
    document.getElementById('attachment').disabled = false;
    document.getElementById('notetext').disabled = false;

}

function ClearControl() {
    $('#date').val('');
    $('#shopid').val('');
    $('#attachment').val('');
    $('#notetext').val('');

}

function AddnewAction() {
    document.getElementById('btnSaveContract').innerText = "Add New";
    DisableControl();
    ClearControl();
    $('date').val = Date.now();
}

function ValidateShop() {
    var isValid = true;
    var formAddEdit = $("#formTrainingProgramAdd");
    if ($('#shopid').val().trim() == "") {
        $(' #shopid').css('border-color', 'red');
        $(' #shopid').focus();
        isValid = false;
    } else {
        $(' #shopid').css('border-color', '#cccccc');
        $(' #shopid').focus();
    }
    if ($(' #attachment').val().trim() == "") {
        $(' #attachment').css('border-color', 'red');
        isValid = false;
    } else {
        $(' #attachment').css('border-color', '#cccccc');

    }
    if ($(' #notetext').val().trim() == "") {
        $(' #notetext').css('border-color', 'red');
        isValid = false;
    } else {
        $(' #notetext').css('border-color', '#cccccc');
    }
    
    return isValid;
}

//function readURLqrCodeImage(input) {
//    if (input.files && input.files[0]) {
//        var reader = new FileReader();
//        reader.onload = function (e) {
//            $('#AttPhoto').attr('src', e.target.result);
//        };
//        reader.readAsDataURL(input.files[0]);
//    }
//}


function PrintPrescription() {
    $.ajax({
        url: "/api/ContractShops/",
        type: "GET",
        contentType: "application/json;charset=utf-8",
        success: function (data) {
            $("#pname").val(data.shopName);
        }
    })
}