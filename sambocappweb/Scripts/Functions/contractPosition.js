﻿$(document).ready(function () {
    $('#ContractPositionModal').on('show.bs.modal', function () {
        GetContractPosition();
    });
    
});

var tableContractPosition = [];
function GetContractPosition() {
    DisableControlContractP();
    ClearControlContractP();
    tableContractPosition = $('#ContractPositionTable').DataTable({
        ajax: {
            url: "/api/ContractPosition",
            dataSrc: ""
        },
        columns: [
            {
                data: "id",
            },
            {
                data: "name"
            },
            {
                data: "note"
            },           
            {
                data: "id",
                render: function (data) {
                    return "<button onclick='ContractPositionEdit(" + data + ")' class='btn btn-warning btn-xs' style='border-width: 0px; width: 65px; margin-right: 5px;'><span class='glyphicon glyphicon-edit'></span> Edit</button>" + "<button onclick='ContractPositionDelete(" + data + ")' class='btn btn-danger btn-xs' style='border-width: 0px;'><span class='glyphicon glyphicon-trash'></span> Delete</button>";
                },
                "width": "130px"
            }
        ],
        destroy: true,
        "order": [[0, "desc"]],
        "info": false
    });
}


function ContractPositionAction() {
    var action = '';
    action = document.getElementById('btnSaveContractPosition').innerText;
    
    if (action == "Add New") {
        document.getElementById('btnSaveContractPosition').innerText = 'Save';
        EnableControlContractP();

        $('#name').focus();
        //$('#feedBackDate').val(moment().format('YYYY-MM-DD'));
    }

    if (action === "Save") {
        //Validate();
        var data = {
            name: $('#name').val(),
            note: $('#cnote').val(),
        };
        $.ajax({
            url: "/api/ContractPosition",
            data: JSON.stringify(data),
            type: "POST",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                toastr.success("Save to database successfully.", "Server Response");
                $('#ContractPositionTable').DataTable().ajax.reload();
                ClearControlContractP();
                document.getElementById('btnSaveContractPosition').innerText = "Add New";

                //$('#ContractPositionModal').modal('hide');
                //document.getElementById('btnSaveContractPosition').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";

            },
            error: function (errormessage) {
                toastr.error("This ContractPosition is already exists.", "Server Response");
                //document.getElementById('btnSaveContractPosition').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";

            }
        });
    }
    else if (action === "Update") {
        //alert('hi');
        var data = {
            id: $('#ContractPositionid').val(),
            name: $('#name').val(),
            note: $('#cnote').val(),
        };
        $.ajax({
            url: "/api/ContractPosition/" + data.id,
            data: JSON.stringify(data),
            type: "PUT",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                toastr.success("Updated successfully.", "Server Response");
                $('#ContractPositionTable').DataTable().ajax.reload();
                document.getElementById('btnSaveContractPosition').innerText = "Add New";
                ClearControlContractP();
                DisableControlContractP();
                //$('#ContractPositionModal').modal('hide');
                //tableContractPositions.ajax.reload();
                //$('#registerModal').modal('hide');

            },
            error: function (errormessage) {
                toastr.error("This ContractPosition can't Update.", "Server Response");

            }
        });
    }
}


function ContractPositionEdit(id) {
    ClearControlContractP();
    EnableControlContractP();

    //$('#status').val('');
    action = document.getElementById('btnSaveContractPosition').innerText = "Update";

    $.ajax({
        url: "/api/ContractPosition/" + id,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {

            $('#ContractPositionid').val(result.id);

            $('#name').val(result.name);
            $('#cnote').val(result.note);

            //$("#ContractPositionModal").modal('show');
        },
        error: function (errormessage) {
            toastr.error("Something unexpected happen.", "Server Response");
        }
    });
    return false;
}


function ContractPositionDelete(id) {
    //alert('hi');
    bootbox.confirm({
        title: "",
        message: "Are you sure want to delete this?",
        button: {
            cancel: {
                label: "Cancel",
                ClassName: "btn-default",
            },
            confirm: {
                label: "Delete",
                ClassName: "btn-danger"
            }
        },
        callback: function (result) {
            //alert(id);
            if (result) {
                $.ajax({
                    url: "/api/ContractPosition/" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        $('#ContractPositionTable').DataTable().ajax.reload();

                        toastr.success("ContractPosition Deleted successfully!", "Service Response");
                        ClearControlContractP();
                    },
                    error: function (errormessage) {
                        toastr.error("ContractPosition Can't be Deleted", "Service Response");
                    }
                });
            }
        }
    });
}



function DisableControlContractP() {
    document.getElementById('name').disabled = true;
    document.getElementById('cnote').disabled = true;
}

function EnableControlContractP() {
    document.getElementById('name').disabled = false;
    document.getElementById('cnote').disabled = false;
}

function ClearControlContractP() {
    $('#name').val('');
    $('#cnote').val('');

}

function AddnewContractPositionAction() {
    document.getElementById('btnSaveContractPosition').innerText = "Add New";
    DisableControlContractP();
    ClearControlContractP();
}
