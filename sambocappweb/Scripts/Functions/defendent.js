﻿$(document).ready(function () {
    $(document).ajaxStart(function () {
        Pace.start();
        $("div#divLoadingModal").addClass('show');
    }).ajaxStop(function () {
        Pace.stop();
        $("div#divLoadingModal").removeClass('show');
    });

    $('#defendentModal').on('show.bs.modal', function () {
        GetDefendent();
        

        document.getElementById('btnDefendentAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
        $('#defendentName').val('');
    });
});

var tableCase = [];

function GetDefendent() {
    tableCase = $('#DefendentTable').DataTable({
        ajax: {
            url: "/api/defendents",
            dataSrc: ""
        },
        columns: [
            {
                data: "id",
                render: function (data) {

                    return "ID" + ("0000" + data).slice(-4);
                }
            },
            {
                data: "defendentName"
            },
            {
                data: "defendentSex"
            },
            {
                data: "defendentAge"
            },
            {
                data: "defendentAddress"
            },
            {
                data: "defendentDob"
            },
            {
                data: "defendentIdentityno"
            },
            {
                data: "defendentPhone"
            },
            {
                data: "id",
                render: function (data) {
                    return "<button onclick='DefendentEdit(" + data + ")' class='btn btn-warning btn-xs' style='border-width: 0px; width: 65px; margin-right: 5px;'><span class='glyphicon glyphicon-edit'></span> Edit</button>" + "<button onclick='DefendentDelete(" + data + ")' class='btn btn-danger btn-xs' style='border-width: 0px;'><span class='glyphicon glyphicon-trash'></span> Delete</button>";
                },
                "width": "130px"
            }
        ],
        destroy: true,
        "order": [[0, "desc"]],
        "info": false
    });
}

function DefendentEdit(id) {
    $.ajax({
        url: "/api/defendents/" + id,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            $('#defendentId').val(result.id);
            $('#defendentName').val(result.defendentName);
            $('#defendentSex').val(result.defendentSex);
            $('#defendentAge').val(result.defendentAge);
            $('#defendentAddress').val(result.defendentAddress);
            $('#defendentDob').val(result.defendentDob);
            $('#defendentIdentityno').val(result.defendentIdentityno);
            $('#defendentPhone').val(result.defendentPhone);
            
            var dr = moment(result.defendentPhoto).format("YYYY-MM-DD");
            $('#defendentfile_old').val(dr);
            
            $('#defendentWithname').val(result.defendentWithname);
            $('#defendentWithsex').val(result.defendentWithsex);
            $('#defendentWithage').val(result.defendentWithage);
            $('#defendentWithaddress').val(result.defendentWithaddress);
            $('#defendentWithdob').val(result.defendentWithdob);
            $('#defendentWithidentityno').val(result.defendentWithidentityno);
            $('#defendentWithphone').val(result.defendentWithphone);
            //alert(result.defendentWithphoto);
            var wdr = moment(result.defendentWithphoto).format("YYYY-MM-DD");
            $('#defendentwithfile_old').val(wdr);

            //console.log(result);
            //alert(result.photo);
            if (result.defendentPhoto == "") {
                $('#defendentPhoto').attr('src', '../Images/no_image.png');
            } else {
                //alert(result.img);
                $('#defendentPhoto').attr('src', '../Images/' + result.defendentPhoto);
            }
            if (result.defendentWithphoto == "") {
                $('#defendentwithPhoto').attr('src', '../Images/no_image.png');
            } else {
                //alert(result.img);
                $('#defendentwithPhoto').attr('src', '../Images/' + result.defendentWithphoto);
            }
            //alert('hi');

            //$('#defendentModal').modal('show');
        },
        error: function (errormessage) {
        toastr.error("Something unexpected happen.", "Server Response");
    }
    });
    return false;
}

function DefendentAction() {
    var action = '';
    action = document.getElementById('btnDefendentAction').innerText;
    if (action === " បង្កើតថ្មី / Add New") {
        $('#defendentDob').val(moment().format('YYYY-MM-DD'));
        $('#defendentWithdob').val(moment().format('YYYY-MM-DD'));
        document.getElementById('btnDefendentAction').innerHTML = "<span class='glyphicon glyphicon-floppy-save'></span> <span class='kh'>រក្សាទុក</span> / Save";
        $('#defendentPhoto').attr('src', '../Images/no_image.png');
        $('#defendentwithPhoto').attr('src', '../Images/no_image.png');
        document.getElementById('defendentName').disabled = false;
        document.getElementById('defendentSex').disabled = false;
        document.getElementById('defendentAge').disabled = false;
        document.getElementById('defendentAddress').disabled = false;
        document.getElementById('defendentDob').disabled = false;
        document.getElementById('defendentIdentityno').disabled = false;
        document.getElementById('defendentPhone').disabled = false;
        //document.getElementById('defendentPhoto').disabled = false;
        document.getElementById('defendentWithname').disabled = false;
        document.getElementById('defendentWithsex').disabled = false;
        document.getElementById('defendentWithage').disabled = false;
        document.getElementById('defendentWithaddress').disabled = false;
        document.getElementById('defendentWithdob').disabled = false;
        document.getElementById('defendentWithidentityno').disabled = false;
        document.getElementById('defendentWithphone').disabled = false;
        //document.getElementById('defendentwithPhoto').disabled = false;
        $("#defendentName").focus();
    } else if (action === " រក្សាទុក / Save") {
        
        var data = new FormData();
        var files = $("#defendentfile").get(0).files;
        if (files.length > 0) {
            data.append("defendentphoto", files[0]);
        }
        //alert($("#defendentName").val());

        data.append("defendentname", $("#defendentName").val());
        data.append("defendentsex", $("#defendentSex").val());
        data.append("defendentage", $("#defendentAge").val());
        data.append("defendentaddress", $("#defendentAddress").val());
        data.append("defendentdob", $("#defendentDob").val());
        data.append("defendentidentityno", $("#defendentIdentityno").val());
        data.append("defendentphone", $("#defendentPhone").val());

        var fileswith = $("#defendentWithfile").get(0).files;
        if (fileswith.length > 0) {
            data.append("defendentwithphoto", fileswith[0]);
        }
        data.append("defendentwithname", $("#defendentWithname").val());
        data.append("defendentwithsex", $("#defendentWithsex").val());
        data.append("defendentwithage", $("#defendentWithage").val());
        data.append("defendentwithaddress", $("#defendentWithaddress").val());
        data.append("defendentwithdob", $("#defendentWithdob").val());
        data.append("defendentwithidentityno", $("#defendentWithidentityno").val());
        data.append("defendentwithphone", $("#defendentWithphone").val());

        console.log(data);

        $.ajax({
            type: "POST",
            url: "/api/defendents",
            contentType: false,
            processData: false,
            data: data,
            success: function (result) {
                toastr.success("Save to database successfully.", "Server Response");
                tableCase.ajax.reload();
                document.getElementById('plaintiffName').disabled = true;
                //document.getElementById('caseNote').disabled = true;
                document.getElementById('btnDefendentAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                $('#defendentName').val('');
            },
            error: function (errormessage) {
                toastr.error("Record save error !.", "Server Response");
                //document.getElementById('defendentName').disabled = true;
                //document.getElementById('caseNote').disabled = true;
                document.getElementById('btnDefendentAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                $('#defendentName').val('');
            }
        });

        //maritalstatus

    } else if (action === " កែប្រែ / Update") {
        var response = Validate();
        if (response == false) {
            return false;
        }
        var data = new FormData();
        var files = $("#defendentfile").get(0).files;
        if (files.length > 0) {
            data.append("defendentPhoto", files[0]);
        }
        var fileswith = $("#defendentwithfile").get(0).files;
        if (fileswith.length > 0) {
            data.append("defendentwithPhoto", fileswith[0]);
        }
        
        data.append("name", $("#defendentName").val());
        data.append("sex", $("#defendentSex").val());
        data.append("age", $("#defendentAge").val());
        data.append("address", $("#defendentAddress").val());
        data.append("dob", $("#defendentDob").val());
        data.append("identityno", $("#defendentIdentityno").val());
        data.append("phone", $("#defendentPhone").val());
        data.append("withname", $("#defendentWithname").val());
        data.append("withsex", $("#defendentWithsex").val());
        data.append("withage", $("#defendentWithage").val());
        data.append("withaddress", $("#defendentWithaddress").val());
        data.append("withdob", $("#defendentWithdob").val());
        data.append("withidentityno", $("#defendentWithidentityno").val());
        data.append("withphone", $("#defendentWithphone").val());

        //console.log(data);

        $.ajax({
            url: "/api/defendent/" + data.Id,
            data: JSON.stringify(data),
            type: "PUT",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                toastr.success("Updated successfully.", "Server Response");
                tableCase.ajax.reload();
                document.getElementById('defendentName').disabled = true;
                document.getElementById('btnDefendentAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                $('#defendentName').val('');
                //$('#caseNote').val('');
            },
            error: function (errormessage) {
                toastr.error("This defendent is already exists.", "Server Response");
                document.getElementById('defendentName').disabled = true;
                document.getElementById('btnDefendentAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                $('#defendentName').val('');
                //$('#caseNote').val('');
            }
        });

    }
}

function DefendentDelete(id) {
    bootbox.confirm({
        title: "Confirmation",
        message: "Are you sure you want to delete this?",
        buttons: {
            cancel: {
                label: "Cancel",
                className: "btn-default"
            },
            confirm: {
                label: "Delete",
                className: "btn-danger"
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: "/api/defendents/" + id,
                    method: "DELETE",
                    success: function () {
                        tableCase.ajax.reload();
                        toastr.success("Deleted successfully.", "Server Response");
                    },
                    error: function () {
                        toastr.error("This defendent is being used, cannot delete this.", "Server Response");
                    }
                });
            }
        }
    });
}

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#defendentPhoto').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

function readURLWith(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#defendentwithPhoto').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}