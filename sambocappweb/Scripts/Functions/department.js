﻿$(document).ready(function () {
    $(document).ajaxStart(function () {
        Pace.start();
        $("div#divLoadingModal").addClass('show');
    }).ajaxStop(function () {
        Pace.stop();
        $("div#divLoadingModal").removeClass('show');
    });

    $('#departmentModal').on('show.bs.modal', function () {
        GetDepartments();
        document.getElementById('departmentName').disabled = true;
        document.getElementById('btnDepartmentAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
        $('#departmentName').val('');
    });
});

var tableDepartment = [];

function GetDepartments() {
    tableDepartment = $('#departmentTable').DataTable({
        ajax: {
            url: "/api/departments",
            dataSrc: ""
        },
        columns: [
            {
                data: "name"
            },
            {
                data: "id",
                render: function (data) {
                    return "<button onclick='DepartmentEdit(" + data + ")' class='btn btn-warning btn-xs' style='border-width: 0px; width: 65px; margin-right: 5px;'><span class='glyphicon glyphicon-edit'></span> Edit</button>" + "<button onclick='DepartmentDelete(" + data + ")' class='btn btn-danger btn-xs' style='border-width: 0px;'><span class='glyphicon glyphicon-trash'></span> Delete</button>";
                },
                "width": "130px"
            }
        ],
        destroy: true,
        "order": [[0, "desc"]],
        "info": false
    });
}

function DepartmentAction() {
    var action = '';
    action = document.getElementById('btnDepartmentAction').innerText;
    if (action === " រក្សាទុក / Save") {
        if ($('#departmentName').val().trim() === "") {
            $('#departmentName').css('border-color', 'red');
            $('#departmentName').focus();
            toastr.info("Please enter department's name", "Required");
        }
        else {
            $('#departmentName').css('border-color', '#cccccc');

            var data = {
                Name: $('#departmentName').val()
            };
            $.ajax({
                url: "/api/departments",
                data: JSON.stringify(data),
                type: "POST",
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                success: function (result) {
                    toastr.success("Save to database successfully.", "Server Response");
                    tableDepartment.ajax.reload();
                    document.getElementById('departmentName').disabled = true;
                    document.getElementById('btnDepartmentAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                    $('#departmentName').val('');
                },
                error: function (errormessage) {
                    toastr.error("This department is already exists.", "Server Response");
                    document.getElementById('departmentName').disabled = true;
                    document.getElementById('btnDepartmentAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                    $('#departmentName').val('');
                }
            });
        }
    }
    else if (action === " បង្កើតថ្មី / Add New") {
        document.getElementById('departmentName').disabled = false;
        document.getElementById('btnDepartmentAction').innerHTML = "<span class='glyphicon glyphicon-floppy-save'></span> <span class='kh'>រក្សាទុក</span> / Save";
        $('#departmentName').val('');
        $('#departmentName').focus();
    }
    else if (action === " កែប្រែ / Update") {
        $('#departmentName').css('border-color', '#cccccc');

        var data = {
            Id: $('#departmentId').val(),
            Name: $('#departmentName').val()
        };
        $.ajax({
            url: "/api/departments/" + data.Id,
            data: JSON.stringify(data),
            type: "PUT",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                toastr.success("Updated successfully.", "Server Response");
                tableDepartment.ajax.reload();
                document.getElementById('departmentName').disabled = true;
                document.getElementById('btnDepartmentAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                $('#departmentName').val('');
            },
            error: function (errormessage) {
                toastr.error("This department is already exists.", "Server Response");
                document.getElementById('departmentName').disabled = true;
                document.getElementById('btnDepartmentAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                $('#departmentName').val('');
            }
        });
    }
}

function DepartmentEdit(id) {

    $('#departmentName').css('border-color', '#cccccc');

    $.ajax({
        url: "/api/departments/" + id,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            $('#departmentId').val(result.id);
            $('#departmentName').val(result.name);
            document.getElementById('btnDepartmentAction').innerHTML = "<span class='glyphicon glyphicon-saved'></span> <span class='kh'>កែប្រែ</span> / Update";
            document.getElementById('departmentName').disabled = false;
            $('#departmentName').focus();
        },
        error: function (errormessage) {
            toastr.error("Something unexpected happen.", "Server Response");
        }
    });
    return false;
}

function DepartmentDelete(id) {
    bootbox.confirm({
        title: "Confirmation",
        message: "Are you sure you want to delete this?",
        buttons: {
            cancel: {
                label: "Cancel",
                className: "btn-default"
            },
            confirm: {
                label: "Delete",
                className: "btn-danger"
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: "/api/departments/" + id,
                    method: "DELETE",
                    success: function () {
                        tableDepartment.ajax.reload();
                        toastr.success("Deleted successfully.", "Server Response");
                    },
                    error: function () {
                        toastr.error("This department is being used, cannot delete this.", "Server Response");
                    }
                });
            }
        }
    });
}