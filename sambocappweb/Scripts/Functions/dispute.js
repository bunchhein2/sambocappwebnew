﻿$(document).ready(function () {
    $('#DisputeModal').on('show.bs.modal', function () {
        GetDispute();
    });

});

var tableDispute = [];
function GetDispute() {
    DisableControlDispute();
    ClearControlDispute();
    tableDispute = $('#DisputeTable').DataTable({
        ajax: {
            url: "/api/Disputes",
            dataSrc: ""
        },
        columns: [
            {
                data: "id",
            },
            {
                data: "deputenNme"
            },
            {
                data: "deputeNote"
            },
            {
                data: "id",
                render: function (data) {
                    return "<button onclick='DisputeEdit(" + data + ")' class='btn btn-warning btn-xs' style='border-width: 0px; width: 65px; margin-right: 5px;'><span class='glyphicon glyphicon-edit'></span> Edit</button>" + "<button onclick='DisputeDelete(" + data + ")' class='btn btn-danger btn-xs' style='border-width: 0px;'><span class='glyphicon glyphicon-trash'></span> Delete</button>";
                },
                "width": "130px"
            }
        ],
        destroy: true,
        "order": [[0, "desc"]],
        "info": false
    });
}


function DisputeAction() {
    var action = '';
    action = document.getElementById('btnSaveDispute').innerText;

    if (action == "Add New") {
        document.getElementById('btnSaveDispute').innerText = 'Save';
        EnableControlDispute();

        $('#dname').focus();
        //$('#feedBackDate').val(moment().format('YYYY-MM-DD'));
    }

    if (action === "Save") {
        //Validate();
        var data = {
            deputenNme: $('#dname').val(),
            deputeNote: $('#dnote').val(),
        };
        $.ajax({
            url: "/api/Disputes",
            data: JSON.stringify(data),
            type: "POST",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                toastr.success("Save to database successfully.", "Server Response");
                $('#DisputeTable').DataTable().ajax.reload();

                //$('#DisputeModal').modal('hide');
                //document.getElementById('btnSaveDispute').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";

            },
            error: function (errormessage) {
                toastr.error("This Dispute is already exists.", "Server Response");
                //document.getElementById('btnSaveDispute').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";

            }
        });
    }
    else if (action === "Update") {
        //alert('hi');
        var data = {
            id: $('#Disputeid').val(),
            deputenNme: $('#dname').val(),
            deputeNote: $('#dnote').val(),
        };
        $.ajax({
            url: "/api/Disputes/" + data.id,
            data: JSON.stringify(data),
            type: "PUT",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                toastr.success("Updated successfully.", "Server Response");
                $('#DisputeTable').DataTable().ajax.reload();
                document.getElementById('btnSaveDispute').innerText = "Add New";
                ClearControlDispute();
                DisableControlDispute();
                //$('#DisputeModal').modal('hide');
                //tableDisputes.ajax.reload();
                //$('#registerModal').modal('hide');

            },
            error: function (errormessage) {
                toastr.error("This Dispute can't Update.", "Server Response");

            }
        });
    }
}


function DisputeEdit(id) {
    ClearControlDispute();
    EnableControlDispute();

    //$('#status').val('');
    action = document.getElementById('btnSaveDispute').innerText = "Update";

    $.ajax({
        url: "/api/Disputes/" + id,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {

            $('#Disputeid').val(result.id);

            $('#dname').val(result.deputenNme);
            $('#dnote').val(result.deputeNote);

            //$("#DisputeModal").modal('show');
        },
        error: function (errormessage) {
            toastr.error("Something unexpected happen.", "Server Response");
        }
    });
    return false;
}


function DisputeDelete(id) {
    //alert('hi');
    bootbox.confirm({
        title: "",
        message: "Are you sure want to delete this?",
        button: {
            cancel: {
                label: "Cancel",
                ClassName: "btn-default",
            },
            confirm: {
                label: "Delete",
                ClassName: "btn-danger"
            }
        },
        callback: function (result) {
            //alert(id);
            if (result) {
                $.ajax({
                    url: "/api/Disputes/" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        $('#DisputeTable').DataTable().ajax.reload();

                        toastr.success("Dispute Deleted successfully!", "Service Response");
                    },
                    error: function (errormessage) {
                        toastr.error("Dispute Can't be Deleted", "Service Response");
                    }
                });
            }
        }
    });
}



function DisableControlDispute() {
    document.getElementById('dname').disabled = true;
    document.getElementById('dnote').disabled = true;
}

function EnableControlDispute() {
    document.getElementById('dname').disabled = false;
    document.getElementById('dnote').disabled = false;
}

function ClearControlDispute() {
    $('#dname').val('');
    $('#dnote').val('');

}

function AddnewDisputeAction() {
    document.getElementById('btnSaveDispute').innerText = "Add New";
    DisableControlDispute();
    ClearControlDispute();
}
