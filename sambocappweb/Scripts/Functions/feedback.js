﻿$(document).ready(function () {
    $(document).ajaxStart(function () {
        Pace.start();
        $("div#divLoadingModal").addClass('show');
    }).ajaxStop(function () {
        Pace.stop();
        $("div#divLoadingModal").removeClass('show');
    });

    $('#feedbackModal').on('show.bs.modal', function () {
        GetFeedbacks();
        document.getElementById('feedbackName').disabled = true;
        document.getElementById('feedbackEmail').disabled = true;
        document.getElementById('feedbackSubject').disabled = true;
        document.getElementById('feedbackMessage').disabled = true;
        document.getElementById('btnFeedbackAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
        $('#feedbackName').val('');
        $('#feedbackEmail').val('');
        $('#feedbackSubject').val('');
        $('#feedbackMessage').val('');
    });
});

var tableFeedback = [];

function GetFeedbacks() {
    tableFeedback = $('#feedbackTable').DataTable({
        ajax: {
            url: "/api/feedbacks",
            dataSrc: ""
        },
        columns: [
            {
                data: "name"
            },
            {
                data: "email"
            },
            {
                data: "subject"
            },
            {
                data: "message"
            },
            {
                data: "id",
                render: function (data) {
                    return "<button onclick='FeedbackEdit(" + data + ")' class='btn btn-warning btn-xs' style='border-width: 0px; width: 65px; margin-right: 5px;'><span class='glyphicon glyphicon-edit'></span> Edit</button>" + "<button onclick='FeedbackDelete(" + data + ")' class='btn btn-danger btn-xs' style='border-width: 0px;'><span class='glyphicon glyphicon-trash'></span> Delete</button>";
                },
                "width": "130px"
            }
        ],
        destroy: true,
        "order": [[0, "desc"]],
        "info": false
    });
}

function FeedbackAction() {
    var action = '';
    action = document.getElementById('btnFeedbackAction').innerText;
    if (action === " រក្សាទុក / Save") {
        if ($('#feedbackName').val().trim() === "") {
            $('#feedbackName').css('border-color', 'red');
            $('#feedbackName').focus();
            toastr.info("Please enter your name", "Required");
        }
        else {
            $('#feedbackName').css('border-color', '#cccccc');
            if ($('#feedbackEmail').val().trim() === "") {
                $('#feedbackEmail').css('border-color', 'red');
                $('#feedbackEmail').focus();
                toastr.info("Please enter your email", "Required");
            }
            else {
                $('#feedbackEmail').css('border-color', '#cccccc');
                if ($('#feedbackSubject').val().trim() === "") {
                    $('#feedbackSubject').css('border-color', 'red');
                    $('#feedbackSubject').focus();
                    toastr.info("Please enter your subject", "Required");
                }
                else {
                    $('#feedbackSubject').css('border-color', '#cccccc');
                    if ($('#feedbackMessage').val().trim() === "") {
                        $('#feedbackMessage').css('border-color', 'red');
                        $('#feedbackMessage').focus();
                        toastr.info("Please enter your message", "Required");
                    }
                    else {
                        $('#feedbackMessage').css('border-color', '#cccccc');

                        var data = {
                            Name: $('#feedbackName').val(),
                            Email: $('#feedbackEmail').val(),
                            Subject: $('#feedbackSubject').val(),
                            Message: $('#feedbackMessage').val()
                        };
                        $.ajax({
                            url: "/api/feedbacks",
                            data: JSON.stringify(data),
                            type: "POST",
                            contentType: "application/json;charset=utf-8",
                            dataType: "json",
                            success: function (result) {
                                toastr.success("Save to database successfully.", "Server Response");
                                tableFeedback.ajax.reload();
                                document.getElementById('feedbackName').disabled = true;
                                document.getElementById('feedbackEmail').disabled = true;
                                document.getElementById('feedbackSubject').disabled = true;
                                document.getElementById('feedbackMessage').disabled = true;
                                document.getElementById('btnFeedbackAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                                $('#feedbackName').val('');
                                $('#feedbackEmail').val('');
                                $('#feedbackSubject').val('');
                                $('#feedbackMessage').val('');
                            },
                            error: function (errormessage) {
                                toastr.error("This feedback is already exists.", "Server Response");
                                document.getElementById('feedbackName').disabled = true;
                                document.getElementById('feedbackEmail').disabled = true;
                                document.getElementById('feedbackSubject').disabled = true;
                                document.getElementById('feedbackMessage').disabled = true;
                                document.getElementById('btnFeedbackAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                                $('#feedbackName').val('');
                                $('#feedbackEmail').val('');
                                $('#feedbackSubject').val('');
                                $('#feedbackMessage').val('');
                            }
                        });
                    }
                }
            }
        }
    }
    else if (action === " បង្កើតថ្មី / Add New") {
        document.getElementById('feedbackName').disabled = false;
        document.getElementById('feedbackEmail').disabled = false;
        document.getElementById('feedbackSubject').disabled = false;
        document.getElementById('feedbackMessage').disabled = false;
        document.getElementById('btnFeedbackAction').innerHTML = "<span class='glyphicon glyphicon-floppy-save'></span> <span class='kh'>រក្សាទុក</span> / Save";
        $('#feedbackName').val('');
        $('#feedbackEmail').val('');
        $('#feedbackSubject').val('');
        $('#feedbackMessage').val('');
        $('#feedbackName').focus();
    }
    else if (action === " កែប្រែ / Update") {
        $('#feedbackName').css('border-color', '#cccccc');
        $('#feedbackEmail').css('border-color', '#cccccc');
        $('#feedbackSubject').css('border-color', '#cccccc');
        $('#feedbackMessage').css('border-color', '#cccccc');

        var data = {
            Id: $('#feedbackId').val(),
            Name: $('#feedbackName').val(),
            Email: $('#feedbackEmail').val(),
            Subject: $('#feedbackSubject').val(),
            Message: $('#feedbackMessage').val()
        };

        $.ajax({
            url: "/api/feedbacks/" + data.Id,
            data: JSON.stringify(data),
            type: "PUT",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                toastr.success("Updated successfully.", "Server Response");
                tableFeedback.ajax.reload();
                document.getElementById('feedbackName').disabled = true;
                document.getElementById('feedbackEmail').disabled = true;
                document.getElementById('feedbackSubject').disabled = true;
                document.getElementById('feedbackMessage').disabled = true;
                document.getElementById('btnFeedbackAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                $('#feedbackName').val('');
                $('#feedbackEmail').val('');
                $('#feedbackSubject').val('');
                $('#feedbackMessage').val('');
            },
            error: function (errormessage) {
                toastr.error("This department is already exists.", "Server Response");
                document.getElementById('feedbackName').disabled = true;
                document.getElementById('feedbackEmail').disabled = true;
                document.getElementById('feedbackSubject').disabled = true;
                document.getElementById('feedbackMessage').disabled = true;
                document.getElementById('btnFeedbackAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                $('#feedbackName').val('');
                $('#feedbackEmail').val('');
                $('#feedbackSubject').val('');
                $('#feedbackMessage').val('');
            }
        });
    }
}

function FeedbackEdit(id) {

    $('#feedbackName').css('border-color', '#cccccc');
    $('#feedbackEmail').css('border-color', '#cccccc');
    $('#feedbackSubject').css('border-color', '#cccccc');
    $('#feedbackMessage').css('border-color', '#cccccc');

    $.ajax({
        url: "/api/feedbacks/" + id,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            $('#feedbackId').val(result.id);
            $('#feedbackName').val(result.name);
            $('#feedbackEmail').val(result.email);
            $('#feedbackSubject').val(result.subject);
            $('#feedbackMessage').val(result.message);
            document.getElementById('btnFeedbackAction').innerHTML = "<span class='glyphicon glyphicon-saved'></span> <span class='kh'>កែប្រែ</span> / Update";
            document.getElementById('feedbackName').disabled = false;
            document.getElementById('feedbackEmail').disabled = false;
            document.getElementById('feedbackSubject').disabled = false;
            document.getElementById('feedbackMessage').disabled = false;
            $('#feedbackName').focus();
        },
        error: function (errormessage) {
            toastr.error("Something unexpected happen.", "Server Response");
        }
    });
    return false;
}

function FeedbackDelete(id) {
    bootbox.confirm({
        title: "Confirmation",
        message: "Are you sure you want to delete this?",
        buttons: {
            cancel: {
                label: "Cancel",
                className: "btn-default"
            },
            confirm: {
                label: "Delete",
                className: "btn-danger"
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: "/api/feedbacks/" + id,
                    method: "DELETE",
                    success: function () {
                        tableFeedback.ajax.reload();
                        toastr.success("Deleted successfully.", "Server Response");
                    },
                    error: function () {
                        toastr.error("This feedback is being used, cannot delete this.", "Server Response");
                    }
                });
            }
        }
    });
}
