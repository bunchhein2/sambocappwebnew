﻿$(document).ready(function () {
    $(document).ajaxStart(function () {
        Pace.start();
        $("div#divLoadingModal").addClass('show');
    }).ajaxStop(function () {
        Pace.stop();
        $("div#divLoadingModal").removeClass('show');
    });
    onPopulateJudge();
    //$('#civilcaseModal').on('show.bs.modal', function () {
    //    // Bind Cases Select Options
    //    onPopulateJudge();
    //});
    $('#judgeModal').on('show.bs.modal', function () {
        GetJudges();
        document.getElementById('judgeName').disabled = true;
        document.getElementById('judgeSex').disabled = true;
        document.getElementById('judgeAge').disabled = true;
        document.getElementById('judgeAddress').disabled = true;
        document.getElementById('judgeIdentityno').disabled = true;
        document.getElementById('judgePhone').disabled = true;
        document.getElementById('judgePhoto').disabled = true;
        document.getElementById('btnJudgeAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
        $('#judgeName').val("");
        $('#judgeSex').val("");
        $('#judgeAge').val("");
        $("#judgePhone").val("");
        $("#judgeAddress").val("");
        $('#judgeIdentityno').val("");
        $('#lawyerfile_old').val("");
        $('#judgePhoto').attr('src', '../Images/no_image.png');
        $("#judgeName").focus();
    });
});

var tableCase = [];

function onPopulateJudge() {
    $.ajax({
        url: "/api/judges?selectoption=true",
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (data) {
            console.log(data);
            $("#zcasejudgeid").select2({
                data: data,
                dropdownParent: $("#civilcaseModal")
            });
        },
        error: function (errormessage) {
            toastr.error("Something unexpected happen.", "Server Response");
        }
    });
}

function GetJudges() {
    tableCase = $('#JudgeTable').DataTable({
        ajax: {
            url: "/api/judges",
            dataSrc: ""
        },
        columns: [
            {
                data: "id"
            },
            {
                data: "judgename"
            },
            {
                data: "judgesex"
            },
            {
                data: "judgeaddress"
            },
            {
                data: "judgeidentityno"
            },
            {
                data: "judgephone"
            },
            //{
            //    data: "photo"
            //},
            //{
            //    data: "status"
            //},
            {
                data: "id",
                render: function (data) {
                    return "<button onclick='JudgeEdit(" + data + ")' class='btn btn-warning btn-xs' style='border-width: 0px; width: 65px; margin-right: 5px;'><span class='glyphicon glyphicon-edit'></span> Edit</button>" + "<button onclick='JudgeDelete(" + data + ")' class='btn btn-danger btn-xs' style='border-width: 0px;'><span class='glyphicon glyphicon-trash'></span> Delete</button>";
                },
                "width": "130px"
            }
        ],
        destroy: true,
        "order": [[0, "desc"]],
        "info": false
    });
}

function JudgeEdit(id) {

    $('#judgeName').css('border-color', '#cccccc');

    $.ajax({
        url: "/api/judges/" + id,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            $('#judgeId').val(result.id);
            $('#judgeName').val(result.judgename);
            $('#judgeSex').val(result.judgesex);
            $('#judgeAge').val(result.judgeage);
            $("#judgePhone").val(result.judgephone);
            $("#judgeAddress").val(result.judgeaddress);
            $('#judgeIdentityno').val(result.judgeidentityno);
            $('#lawyerfile_old').val(result.judgephoto);
            //console.log(result);
            //alert(result.photo);
            if (result.judgephoto == "") {
                $('#judgePhoto').attr('src', '../Images/no_image.png');
            } else {
                //alert(result.img);
                $('#judgePhoto').attr('src', '../Images/' + result.judgephoto);
            }
            document.getElementById('judgeName').disabled = false;
            document.getElementById('judgeSex').disabled = false;
            document.getElementById('judgeAge').disabled = false;
            document.getElementById('judgeAddress').disabled = false;
            document.getElementById('judgeIdentityno').disabled = false;
            document.getElementById('judgePhone').disabled = false;
            document.getElementById('btnJudgeAction').innerHTML = "<span class='glyphicon glyphicon-saved'></span> <span class='kh'>កែប្រែ</span> / Update";
            $('#lawyerModel').modal('show');
        },
        error: function (errormessage) {
            toastr.error("No Record Select!", "Service Response");
        }
    });
    return false;
}

function JudgeAction() {
    var action = '';
    action = document.getElementById('btnJudgeAction').innerText;
    if (action === " បង្កើតថ្មី / Add New") {
        //$('#dob').val(moment().format('YYYY-MM-DD'));
        document.getElementById('btnJudgeAction').innerHTML = "<span class='glyphicon glyphicon-floppy-save'></span> <span class='kh'>រក្សាទុក</span> / Save";
        document.getElementById('judgeName').disabled = false;
        document.getElementById('judgeSex').disabled = false;
        document.getElementById('judgeAge').disabled = false;
        document.getElementById('judgeAddress').disabled = false;
        document.getElementById('judgeIdentityno').disabled = false;
        document.getElementById('judgePhone').disabled = false;
        $('#judgeName').val("");
        $('#judgeSex').val("");
        $('#judgeAge').val("");
        $("#judgePhone").val("");
        $("#judgeAddress").val("");
        $('#judgeIdentityno').val("");
        $('#lawyerfile_old').val("");
        $('#judgePhoto').attr('src', '../Images/no_image.png');
        $("#judgeName").focus();
    } else if (action === " រក្សាទុក / Save") {
        //var response = Validate();
        //if (response == false) {
        //    return false;
        //}
        var data = new FormData();
        var files = $("#judgefile").get(0).files;
        if (files.length > 0) {
            data.append("judgephoto", files[0]);
        }

        data.append("judgename", $("#judgeName").val());
        data.append("judgesex", $("#judgeSex").val());
        data.append("judgeage", $("#judgeAge").val());
        data.append("judgeaddress", $("#judgeAddress").val());
        data.append("judgeidentityno", $("#judgeIdentityno").val());
        data.append("judgephone", $("#judgePhone").val());

        //console.log(files[0]);

        $.ajax({
            type: "POST",
            url: "/api/judges",
            contentType: false,
            processData: false,
            data: data,
            success: function (result) {
                toastr.success("Save to database successfully.", "Server Response");
                tableCase.ajax.reload();
                onPopulateJudge();
                document.getElementById('judgeName').disabled = true;
                //document.getElementById('caseNote').disabled = true;
                document.getElementById('btnJudgeAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                $('#judgeName').val('');
            },
            error: function (errormessage) {
                toastr.error("This Judge is already exists.", "Server Response");
            }
        });

        //maritalstatus

    } else if (action === " កែប្រែ / Update") {
        //var response = Validate();
        //if (response == false) {
        //    return false;
        //}
        var data = new FormData();
        var files = $("#judgefile").get(0).files;
        if (files.length > 0) {
            data.append("judgePhoto", files[0]);
        }
        data.append("judgeid", $('#judgeId'));
        data.append("judgename", $("#judgeName").val());
        data.append("judgesex", $("#judgeSex").val());
        data.append("judgeage", $("#judgeAge").val());
        data.append("judgeaddress", $("#judgeAddress").val());
        data.append("judgeidentityno", $("#judgeIdentityno").val());
        data.append("judgephone", $("#judgePhone").val());
        data.append("lawyerfile_old", $("#lawyerfile_old").val());

        console.log(data);

        $.ajax({
            type: "PUT",
            url: "/api/judges/" + $('#judgeId').val(),
            contentType: false,
            processData: false,
            data: data,
            success: function (result) {
                toastr.success("Updated successfully.", "Server Response");
                tableCase.ajax.reload();
                onPopulateJudge();
                document.getElementById('judgeName').disabled = true;
                document.getElementById('btnJudgeAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                $('#judgeName').val('');
                //$('#caseNote').val('');
            },
            error: function (errormessage) {
                toastr.error("This Judge is already exists.", "Server Response");
            }
        });

    }
}

function JudgeDelete(id) {
    bootbox.confirm({
        title: "Confirmation",
        message: "Are you sure you want to delete this?",
        buttons: {
            cancel: {
                label: "Cancel",
                className: "btn-default"
            },
            confirm: {
                label: "Delete",
                className: "btn-danger"
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: "/api/judges/" + id,
                    method: "DELETE",
                    success: function () {
                        tableCase.ajax.reload();
                        onPopulateJudge();
                        toastr.success("Deleted successfully.", "Server Response");
                    },
                    error: function () {
                        toastr.error("This lawyer is being used, cannot delete this.", "Server Response");
                    }
                });
            }
        }
    });
}

function readURLJudge(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#judgePhoto').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}