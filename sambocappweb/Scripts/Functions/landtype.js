﻿$(document).ready(function () {
    $('#landTypeModal').on('show.bs.modal', function () {
        GetLandType();
        DisableControlLa();
    });
});

var landTypeTable = [];
function GetLandType() {
    landTypeTable = $('#landTypetbl').dataTable({
        ajax: {
            url: "/api/LandTypes",
            dataSrc: ""
        },
        columns:
            [
                {
                    data: "id"
                },
                {
                    data: "land"
                },
                {
                    data: "id",
                    render: function (data) {
                        return "<button OnClick='LandTypeEdit (" + data + ")' class='btn btn-primary btn-xs' style='margin-right:5px'>Edit</button>" +
                            "<button OnClick='LandTypeDelete (" + data + ")' class='btn btn-default btn-xs' >Delete</button>";
                    }
                }
            ],
        destroy: true,
        "order": [0, "desc"],
        "info": false

    });
}
function LandTypeAction() {
    var action = '';
    action = document.getElementById('btnLandType').innerText;
    //alert(action);

    if (action == "Add New") {
        document.getElementById('btnLandType').innerText = 'Save';
        EnableControlLa();
        ClearControlLa();
        $('#landType').focus();
    }
    else if (action == "Save") {
        if ($('#landType').val().trim() == "") {
            $('#landType').css('border-color', 'red');
            $('#landType').focus();
            toastr.info("Please Input Land type", "Server Respon")
        }
        else {
            $('#landType').css('border-color', '#cccccc');
            var data = {
                land: $('#landType').val(),
                note: $('#landnote').val(),
            };
            $.ajax({
                url: "/api/LandTypes",
                data: JSON.stringify(data),
                type: "POST",
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                success: function (result) {
                    toastr.success("New Data has been Created", "Server Respond");
                    landTypeTable.DataTable().ajax.reload();
                    document.getElementById('btnLandType').innerText = 'Add New';
                    ClearControlLa();
                    DisableControlLa();
                },
                error: function (errormesage) {
                    toastr.error("This Name is exist in Database", "Server Respond")
                }
            });
        }
    } else if (action == "Update") {
        if ($('#landType').val().trim() == "") {
            $('#landType').css('border-color', 'red');
            $('#landType').focus();
            toastr.info("Please Input Land type", "Server Respon")
        }
        else {
            $('#landType').css('border-color', '#cccccc');
            var data = {
                id: $('#ID').val(),
                land: $('#landType').val(),
                note: $('#landnote').val(),
            };
            $.ajax({
                url: "/api/LandTypes/" + data.id,
                data: JSON.stringify(data),
                type: "PUT",
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                success: function (result) {
                    toastr.success("Data has been Updated", "Server Respond");
                    landTypeTable.DataTable().ajax.reload();
                    document.getElementById('btnLandType').innerText = 'Add New';
                    ClearControlLa();
                    DisableControlLa();
                },
                error: function (errormesage) {
                    toastr.error("Data hasn't Updated in Database", "Server Respond")
                }
            });
        }
    }
}

//Delete
function LandTypeDelete(id) {
    bootbox.confirm({
        title: "",
        message: "Are you sure want to delete this?",
        button: {
            cancel: {
                label: "Cancel",
                ClassName: "btn-default",
            },
            confirm: {
                label: "Delete",
                ClassName: "btn-danger"
            }
        },
        callback: function (result) {
            //alert(id);
            if (result) {
                $.ajax({
                    url: "/api/LandTypes/" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        landTypeTable.DataTable().ajax.reload();
                        toastr.success("Data Deleted successfully!", "Service Response");
                    },
                    error: function (errormessage) {
                        toastr.error("This Data hasn't deleted in Database", "Service Response");
                    }
                });
            }
        }
    });
}

//Edit
function LandTypeEdit(id) {
    $.ajax({
        url: "/api/LandTypes/" + id,
        type: "Get",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            $('#landnote').val(result.note);
            $('#landType').val(result.land);
            $('#ID').val(result.id);

            document.getElementById('btnLandType').innerText = 'Update';
            $('#landType').focus();

        },
        error: function (errormesage) {
            toastr.error("Unexpacted ", "Server Respond")
        }
    });
}
function DisableControlLa() {
    document.getElementById('landType').disable = true;
    document.getElementById('landnote').disable = true;
}
function EnableControlLa() {
    document.getElementById('landType').disable = false;
    document.getElementById('landnote').disable = false;
}
function ClearControlLa() {
    $('#landType').val('');
    $('#landnote').val('');
}
function LandTypeModalAction() {
    document.getElementById('btnLandType').innerText = 'Add New';
    DisableControlLa();
    ClearControlLa();
}
