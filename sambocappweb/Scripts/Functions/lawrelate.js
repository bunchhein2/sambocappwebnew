﻿$(document).ready(function () {
    $(document).ajaxStart(function () {
        Pace.start();
        $("div#divLoadingModal").addClass('show');
    }).ajaxStop(function () {
        Pace.stop();
        $("div#divLoadingModal").removeClass('show');
    });
    onPopulateLawyerRelate();
    //$('#civilcaseModal').on('show.bs.modal', function () {
    //    // Bind Cases Select Options
    //    onPopulateLawyerRelate();
    //});

    $('#lawrelateModal').on('show.bs.modal', function () {
        GetLawrelate();
        document.getElementById('lawrelateName').disabled = true;
        document.getElementById('lawrelateNote').disabled = true;
        document.getElementById('btnLawrelateAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
        $('#lawrelateName').val('');
    });
});

var tableCase = [];

function onPopulateLawyerRelate() {
    $.ajax({
        url: "/api/lawrelates?selectoption=true",
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (data) {
            console.log(data);
            $("#zlawrelateid").select2({
                data: data,
                dropdownParent: $("#civilcaseModal")
            });
        },
        error: function (errormessage) {
            toastr.error("Something unexpected happen.", "Server Response");
        }
    });
}

function GetLawrelate() {
    tableCase = $('#LawrelateTable').DataTable({
        ajax: {
            url: "/api/Lawrelates",
            dataSrc: ""
        },
        columns: [
            {
                data: "lawrelatename"
            },
            {
                data: "lawrelatenote"
            },
            {
                data: "id",
                render: function (data) {
                    return "<button onclick='LawrelateEdit(" + data + ")' class='btn btn-warning btn-xs' style='border-width: 0px; width: 65px; margin-right: 5px;'><span class='glyphicon glyphicon-edit'></span> Edit</button>" + "<button onclick='LawrelateDelete(" + data + ")' class='btn btn-danger btn-xs' style='border-width: 0px;'><span class='glyphicon glyphicon-trash'></span> Delete</button>";
                },
                "width": "130px"
            }
        ],
        destroy: true,
        "order": [[0, "desc"]],
        "info": false
    });
}

function LawrelateAction() {
    var action = '';
    action = document.getElementById('btnLawrelateAction').innerText;
    document.getElementById('lawrelateName').disabled = true;
    document.getElementById('lawrelateNote').disabled = true;
    if (action === " រក្សាទុក / Save") {
        if ($('#lawrelateName').val().trim() === "") {
            $('#lawrelateName').css('border-color', 'red');
            $('#lawrelateName').focus();
            toastr.info("Please enter Law Relate's name", "Required");
        }
        else {
            $('#lawrelateName').css('border-color', '#cccccc');

            var data = {
                lawrelatename: $('#lawrelateName').val(),
                lawrelatenote: $('#lawrelateNote').val()
            };
            $.ajax({
                url: "/api/Lawrelates",
                data: JSON.stringify(data),
                type: "POST",
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                success: function (result) {
                    toastr.success("Save to database successfully.", "Server Response");
                    tableCase.ajax.reload();
                    onPopulateLawyerRelate();
                    document.getElementById('lawrelateName').disabled = true;
                    document.getElementById('lawrelateNote').disabled = true;
                    document.getElementById('btnLawrelateAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                    $('#lawrelateName').val('');
                    $('#lawrelateNote').val('');
                },
                error: function (errormessage) {
                    toastr.error("This law's relate is already existss.", "Server Response");
                    document.getElementById('lawrelateName').disabled = true;
                    document.getElementById('lawrelateNote').disabled = true;
                    document.getElementById('btnLawrelateAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                    $('#lawrelateName').val('');
                    $('#lawrelateNote').val('');
                }
            });
        }
    }
    else if (action === " បង្កើតថ្មី / Add New") {
        document.getElementById('lawrelateName').disabled = false;
        document.getElementById('lawrelateNote').disabled = false;
        document.getElementById('btnLawrelateAction').innerHTML = "<span class='glyphicon glyphicon-floppy-save'></span> <span class='kh'>រក្សាទុក</span> / Save";
        $('#lawrelateName').val('');
        $('#lawrelateNote').val('');
        $('#lawrelateName').focus();
    }
    else if (action === " កែប្រែ / Update") {
        $('#lawrelateName').css('border-color', '#cccccc');

        var data = {
            id: $('#lawrelateId').val(),
            lawrelatename: $('#lawrelateName').val(),
            lawrelatenote: $('#lawrelateNote').val()
        };
        $.ajax({
            url: "/api/Lawrelates/" + data.id,
            data: JSON.stringify(data),
            type: "PUT",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                toastr.success("Updated successfully.", "Server Response");
                tableCase.ajax.reload();
                onPopulateLawyerRelate();
                document.getElementById('lawrelateName').disabled = true;
                document.getElementById('lawrelateNote').disabled = true;
                document.getElementById('btnLawrelateAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                $('#lawrelateName').val('');
                $('#lawrelateNote').val('');
            },
            error: function (errormessage) {
                toastr.error("This Relate's Name is already exists.", "Server Response");
                document.getElementById('lawrelateName').disabled = true;
                document.getElementById('lawrelateNote').disabled = true;
                document.getElementById('btnLawrelateAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                $('#lawrelateName').val('');
                $('#lawrelateNote').val('');
            }
        });
    }
}

function LawrelateEdit(id) {

    $('#lawrelateName').css('border-color', '#cccccc');

    $.ajax({
        url: "/api/Lawrelates/" + id,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            $('#lawrelateId').val(result.id);
            $('#lawrelateName').val(result.lawrelatename);
            $('#lawrelateNote').val(result.lawrelatenote);
            document.getElementById('btnLawrelateAction').innerHTML = "<span class='glyphicon glyphicon-saved'></span> <span class='kh'>កែប្រែ</span> / Update";
            document.getElementById('lawrelateName').disabled = false;
            document.getElementById('lawrelateNote').disabled = false;
            $('#lawrelateName').focus();

        },
        error: function (errormessage) {
            toastr.error("Something unexpected happen.", "Server Response");
        }
    });
    return false;
}

function LawrelateDelete(id) {
    bootbox.confirm({
        title: "Confirmation",
        message: "Are you sure you want to delete this?",
        buttons: {
            cancel: {
                label: "Cancel",
                className: "btn-default"
            },
            confirm: {
                label: "Delete",
                className: "btn-danger"
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: "/api/Lawrelates/" + id,
                    method: "DELETE",
                    success: function () {
                        tableCase.ajax.reload();
                        onPopulateLawyerRelate();
                        toastr.success("Deleted successfully.", "Server Response");
                    },
                    error: function () {
                        toastr.error("This case is being used, cannot delete this.", "Server Response");
                    }
                });
            }
        }
    });
}