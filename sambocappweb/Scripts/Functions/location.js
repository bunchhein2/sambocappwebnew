﻿
$(document).ready(function () {
    $("#LocationModel").on('show.modal.bs', function () {
        Getlocation();
    });
    Getlocation();
});
var tbllocation = [];
function Getlocation() {
    tableCase = $('#tbllocation').DataTable({
        ajax: {
            url: "/api/Locations",
            dataSrc: ""
        },
        columns: [
            {

                data: "id",
                render: function (data) {

                    return "" + ("").slice(-4);
                }
            },
            {

                data: "location"
            },
            {
                data: "id",
                render: function (data) {
                    return "<button onclick='Edits(" + data + ")' class='btn btn-warning btn-xs' style='border-width: 0px; width: 65px; margin-right: 5px;'><span class='glyphicon glyphicon-edit'></span> Edit</button>" + "<button onclick='Delete(" + data + ")' class='btn btn-danger btn-xs' style='border-width: 0px;'><span class='glyphicon glyphicon-trash'></span> Delete</button>";
                },
                "width": "130px"
            }
        ],
        destroy: true,
        "order": [[0, "desc"]],
        "info": false,
        "paging": true,
        lengthMenu: [
            [ 50, -1],
            [ 50, 'All'],
        ],
    });
}
function locationAction() {
    var action = '';
    action = $("#txtbtnlocation").val();
    if (action == "Save") {
        if ($('#location').val().trim() === "") {
            $('#location').css('border-color', 'red');
            $('#location').focus();
            toastr.info("Please enter location", "Required");
        }
        else {
            $('#location').css('border-color', '#cccccc');
            var data = new FormData();
            data.append("location", $('#location').val());
            data.append("active", $('#active').val());
            $.ajax({
                url: "/api/Locations",
                data: data,
                type: "POST",
                contentType: false,
                processData: false,
                success: function (result) {
                    toastr.success("New Bank has been Created", "Server Respond");
                    $('#tbllocation').DataTable().ajax.reload();
                    ClearControl();
                    //$("#setupbankModel").modal('hide');

                },
                error: function (errormesage) {
                    toastr.error("Exist in Database", "Server Respond")
                }

            });
        }
    } else if (action == "Update") {
        
        if ($('#location').val().trim() === "") {
            $('#location').css('border-color', 'red');
            $('#location').focus();
            toastr.info("Please enter location", "Required");
        }
        else {
            $('#location').css('border-color', '#cccccc');
            var data = new FormData();
            data.append("location", $('#location').val());
            data.append("active", $('#active').val());
            $.ajax({
                url: "/api/Locations/" + $("#id").val(),
                data: data,
                type: "PUT",
                contentType: false,
                processData: false,
                dataType: "json",
                success: function (result) {
                    toastr.success("Updated", "Server Respond");

                    action = document.getElementById('btnSetupbank').innerText = "Save";
                    $('#txtbtnlocation').val("Save");

                    $('#tbllocation').DataTable().ajax.reload();
                    $("#LocationModel").modal('hide');
                    ClearControl();

                },
                error: function (errormesage) {
                    toastr.error("Exit Updated in Database", "Server Respond")
                }
            });
        }

    }
}

function Edits(id) {
    action = document.getElementById('btnLocation').innerText = "Update";
    $.ajax({
        url: "/api/Locations/" + id,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            $('#id').val(result.id);
            $('#location').val(result.location);
            $('#active').val(result.active);
            $("#LocationModel").modal('show');
        },
        error: function (errormessage) {
            toastr.error("Something unexpected happenn.", "Server Response");
        }
    });
}

function Delete(id) {
    // alert('hi');
    bootbox.confirm({
        title: "Confirmation",
        message: "Are you sure to delete this?",
        buttons: {
            cancel: {
                label: "Cancel",
                className: "btn-default"
            },
            confirm: {
                label: "Delete",
                className: "btn-danger"
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: "/api/Locations/" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        $('#tbllocation').DataTable().ajax.reload();
                        toastr.success("Item Deleted successfully!", "Service Response");

                    },
                    error: function (errormessage) {
                        toastr.error("This Item is already exists in Database", "Service Response");
                    }
                });
            }
        }
    });
}

function ClearControl() {
    $('#location').val('');
    $('#active').val('');
}
