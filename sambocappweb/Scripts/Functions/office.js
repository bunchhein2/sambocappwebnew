﻿$(document).ready(function () {
    $(document).ajaxStart(function () {
        Pace.start();
        $("div#divLoadingModal").addClass('show');
    }).ajaxStop(function () {
        Pace.stop();
        $("div#divLoadingModal").removeClass('show');
    });
    //GetCases();
    $('#officeModal').on('show.bs.modal', function () {
        GetOffice();
        document.getElementById('officeName').disabled = true;
        document.getElementById('officeNote').disabled = true;
        document.getElementById('btnOfficeAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
        $('#officeName').val('');
    });
});

var tableOffice = [];

function GetOffice() {
    tableOffice = $('#OfficeTable').DataTable({
        ajax: {
            url: "/api/offices",
            dataSrc: ""
        },
        columns: [
            {
                data: "officename"
            },
            {
                data: "officenote"
            },
            {
                data: "id",
                render: function (data) {
                    return "<button onclick='OfficeEdit(" + data + ")' class='btn btn-warning btn-xs' style='border-width: 0px; width: 65px; margin-right: 5px;'><span class='glyphicon glyphicon-edit'></span> Edit</button>" + "<button onclick='OfficeDelete(" + data + ")' class='btn btn-danger btn-xs' style='border-width: 0px;'><span class='glyphicon glyphicon-trash'></span> Delete</button>";
                },
                "width": "130px"
            }
        ],
        destroy: true,
        "order": [[0, "desc"]],
        "info": false
    });
}

function OfficeAction() {
    var action = '';
    action = document.getElementById('btnOfficeAction').innerText;
    if (action === " រក្សាទុក / Save") {
        if ($('#officeName').val().trim() === "") {
            $('#officeName').css('border-color', 'red');
            $('#officeName').focus();
            toastr.info("Please enter Case Position's name", "Required");
        }
        else {
            $('#officeName').css('border-color', '#cccccc');

            var data = {
                officename: $('#officeName').val(),
                officenote: $('#officeNote').val()
            };
            $.ajax({
                url: "/api/offices",
                data: JSON.stringify(data),
                type: "POST",
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                success: function (result) {
                    toastr.success("Save to database successfully.", "Server Response");
                    //Load data to Civilcase
                    //console.log(result);
                    //var office = $("#zofficeid");
                    //office.empty();
                    //office.append($('<option></option>').attr('value', result.id).text(result.officename));


                    tableOffice.ajax.reload();
                    document.getElementById('officeName').disabled = true;
                    document.getElementById('officeNote').disabled = true;
                    document.getElementById('btnOfficeAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                    $('#officeName').val('');
                    $('#officeNote').val('');
                    
                       // var status = result.Statuses;

                        //$.each(status, function (key, item) {
                        //   // dropDown.append($('<option></option>').attr('selected', (item.Name == selectedName) ? 'selected' : undefined).attr('value', item.Id).text(item.Name));
                        //    dropDown.append($('<option></option>').attr('value', item.Id).text(item.Name));
                        //    $('option', this).each(function () {
                        //        if ($(this).html() == item.Name) {
                        //            $(this).attr('selected', 'selected')
                        //        };
                        //    });
                        //});
                            
                },
                error: function (errormessage) {
                    toastr.error("This case position is already exists.", "Server Response");
                    document.getElementById('officeName').disabled = true;
                    document.getElementById('officeNote').disabled = true;
                    document.getElementById('btnOfficeAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                    $('#officeName').val('');
                    $('#officeNote').val('');
                }
            });
        }
    }
    else if (action === " បង្កើតថ្មី / Add New") {
        document.getElementById('officeName').disabled = false;
        document.getElementById('officeNote').disabled = false;
        document.getElementById('btnOfficeAction').innerHTML = "<span class='glyphicon glyphicon-floppy-save'></span> <span class='kh'>រក្សាទុក</span> / Save";
        $('#officeName').val('');
        $('#officeNote').val('');
        $('#officeName').focus();
    }
    else if (action === " កែប្រែ / Update") {
        $('#officeName').css('border-color', '#cccccc');

        var data = {
            id: $('#officeId').val(),
            officename: $('#officeName').val(),
            officenote: $('#officeNote').val()
        };
        $.ajax({
            url: "/api/offices/" + data.id,
            data: JSON.stringify(data),
            type: "PUT",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                toastr.success("Updated successfully.", "Server Response");
                tableOffice.ajax.reload();
                document.getElementById('officeName').disabled = true;
                document.getElementById('officeNote').disabled = true;
                document.getElementById('btnOfficeAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                $('#officeName').val('');
                $('#officeNote').val('');
            },
            error: function (errormessage) {
                toastr.error("This position is already exists.", "Server Response");
                document.getElementById('officeName').disabled = true;
                document.getElementById('btnOfficeAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                $('#officeName').val('');
                $('#officeNote').val('');
            }
        });
    }
}

function OfficeEdit(id) {

    $('#officeName').css('border-color', '#cccccc');

    $.ajax({
        url: "/api/offices/" + id,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            $('#officeId').val(result.id);
            $('#officeName').val(result.officename);
            $('#officeNote').val(result.officenote);
            document.getElementById('btnOfficeAction').innerHTML = "<span class='glyphicon glyphicon-saved'></span> <span class='kh'>កែប្រែ</span> / Update";
            document.getElementById('officeName').disabled = false;
            document.getElementById('officeNote').disabled = false;
            $('#officeName').focus();
            $('#officeNote').focus();

        },
        error: function (errormessage) {
            toastr.error("Something unexpected happen.", "Server Response");
        }
    });
    return false;
}

function OfficeDelete(id) {
    bootbox.confirm({
        title: "Confirmation",
        message: "Are you sure you want to delete this?",
        buttons: {
            cancel: {
                label: "Cancel",
                className: "btn-default"
            },
            confirm: {
                label: "Delete",
                className: "btn-danger"
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: "/api/offices/" + id,
                    method: "DELETE",
                    success: function () {
                        tableOffice.ajax.reload();
                        toastr.success("Deleted successfully.", "Server Response");
                    },
                    error: function () {
                        toastr.error("This office is being used, cannot delete this.", "Server Response");
                    }
                });
            }
        }
    });
}

$("#close_office").click(function () {
    if ($('#civilcaseModal').is(':visible')) {
        $.ajax({
            url: "/api/offices",
            type: "GET",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                var office = $("#zofficeid");
                office.empty();
                office.append($('<option></option>').attr('value', "").text("---សូមជ្រើសរើស---"));

                $.each(result, function (key, item) {
                    // dropDown.append($('<option></option>').attr('selected', (item.Name == selectedName) ? 'selected' : undefined).attr('value', item.Id).text(item.Name));
                    office.append($('<option></option>').attr('value', item.id).text(item.officename));
                    $('option', this).each(function () {
                        if ($(this).html() == item.officename) {
                            $(this).attr('selected', 'selected')
                        };
                    });
                });

            },
            error: function (errormessage) {

            }
        });
    }
});