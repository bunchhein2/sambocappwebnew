﻿$(document).ready(function () {
    $(document).ajaxStart(function () {
        Pace.start();
        $("div#divLoadingModal").addClass('show');
    }).ajaxStop(function () {
        Pace.stop();
        $("div#divLoadingModal").removeClass('show');
    });
    onPopulateOfficer();
    //$('#civilcaseModal').on('show.bs.modal', function () {
    //    // Bind Cases Select Options
    //    onPopulateOfficer();
        
    //});

    $('#officerModal').on('show.bs.modal', function () {
        GetOfficer();
        document.getElementById('officerName').disabled = true;
        document.getElementById('officerSex').disabled = true;
        document.getElementById('officerAge').disabled = true;
        document.getElementById('officerAddress').disabled = true;
        document.getElementById('officerIdentityno').disabled = true;
        document.getElementById('officerPhone').disabled = true;
        document.getElementById('officerPhoto').disabled = true;
        document.getElementById('btnOfficerAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";

        $('#officerName').val('');
    });
});

var tableCase = [];

function onPopulateOfficer() {
    $.ajax({
        url: "/api/officers?selectoption=true",
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (data) {
            console.log(data);
            $("#zcaseofficerid").select2({
                data: data,
                dropdownParent: $("#civilcaseModal")
            });
        },
        error: function (errormessage) {
            toastr.error("Something unexpected happen.", "Server Response");
        }
    });
}


function GetOfficer() {
    tableCase = $('#OfficerTable').DataTable({
        ajax: {
            url: "/api/officers",
            dataSrc: ""
        },
        columns: [
            {
                data: "id"
            },
            {
                data: "officername"
            },
            {
                data: "officersex"
            },
            {
                data: "officeraddress"
            },
            {
                data: "officeridentityno"
            },
            {
                data: "officerphone"
            },
            //{
            //    data: "photo"
            //},
            //{
            //    data: "status"
            //},
            {
                data: "id",
                render: function (data) {
                    return "<button onclick='OfficerEdit(" + data + ")' class='btn btn-warning btn-xs' style='border-width: 0px; width: 65px; margin-right: 5px;'><span class='glyphicon glyphicon-edit'></span> Edit</button>" + "<button onclick='OfficerDelete(" + data + ")' class='btn btn-danger btn-xs' style='border-width: 0px;'><span class='glyphicon glyphicon-trash'></span> Delete</button>";
                },
                "width": "130px"
            }
        ],
        destroy: true,
        "order": [[0, "desc"]],
        "info": false
    });
}

function OfficerEdit(id) {

    $('#officerName').css('border-color', '#cccccc');

    $.ajax({
        url: "/api/officers/" + id,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            $('#officerId').val(result.id);
            $('#officerName').val(result.officername);
            $('#officerSex').val(result.officersex);
            $('#officerAge').val(result.officerage);
            $('#officerPhone').val(result.officerphone);
            $('#officerAddress').val(result.officeraddress);
            $('#officerIdentityno').val(result.officeridentityno);
            $('#officerfile_old').val(result.officerphoto);
            //console.log(result);
            //alert($('#officerId').val());
            if (result.officerphoto == "") {
                $('#officerPhoto').attr('src', '../Images/no_image.png');
            } else {
                //alert(result.img);
                $('#officerPhoto').attr('src', '../Images/' + result.officerphoto);
            }
            document.getElementById('officerName').disabled = false;
            document.getElementById('officerSex').disabled = false;
            document.getElementById('officerAge').disabled = false;
            document.getElementById('officerAddress').disabled = false;
            document.getElementById('officerIdentityno').disabled = false;
            document.getElementById('officerPhone').disabled = false;
            document.getElementById('btnOfficerAction').innerHTML = "<span class='glyphicon glyphicon-saved'></span> <span class='kh'>កែប្រែ</span> / Update";
            $('#officerModel').modal('show');
        },
        error: function (errormessage) {
            toastr.error("No Record Select!", "Service Response");
        }
    });
    return false;
}

function OfficerAction() {
    var action = '';
    action = document.getElementById('btnOfficerAction').innerText;
    if (action === " បង្កើតថ្មី / Add New") {
        //$('#dob').val(moment().format('YYYY-MM-DD'));
        document.getElementById('btnOfficerAction').innerHTML = "<span class='glyphicon glyphicon-floppy-save'></span> <span class='kh'>រក្សាទុក</span> / Save";
        document.getElementById('officerName').disabled = false;
        document.getElementById('officerSex').disabled = false;
        document.getElementById('officerAge').disabled = false;
        document.getElementById('officerAddress').disabled = false;
        document.getElementById('officerIdentityno').disabled = false;
        document.getElementById('officerPhone').disabled = false;
        $('#officerPhoto').attr('src', '../Images/no_image.png');
        $("#officerName").focus();
    } else if (action === " រក្សាទុក / Save") {
        //var response = Validate();
        //if (response == false) {
        //    return false;
        //}
        var data = new FormData();
        var files = $("#officerfile").get(0).files;
        if (files.length > 0) {
            data.append("officerphoto", files[0]);
        }

        data.append("officername", $("#officerName").val());
        data.append("officersex", $("#officerSex").val());
        data.append("officerage", $("#officerAge").val());
        data.append("officeraddress", $("#officerAddress").val());
        data.append("officeridentityno", $("#officerIdentityno").val());
        data.append("officerphone", $("#officerPhone").val());

        //console.log(files[0]);

        $.ajax({
            type: "POST",
            url: "/api/officers",
            contentType: false,
            processData: false,
            data: data,
            success: function (result) {
                toastr.success("Save to database successfully.", "Server Response");
                tableCase.ajax.reload();
                onPopulateOfficer();
                document.getElementById('officerName').disabled = true;
                //document.getElementById('caseNote').disabled = true;
                document.getElementById('btnOfficerAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                $('#officerName').val('');
            },
            error: function (errormessage) {
                toastr.error("This clerk is already exists.", "Server Response");
            }
        });

        //maritalstatus

    } else if (action === " កែប្រែ / Update") {
        //var response = Validate();
        //if (response == false) {
        //    return false;
        //}
        var data = new FormData();
        var files = $("#officerfile").get(0).files;
        if (files.length > 0) {
            data.append("officerPhoto", files[0]);
        }
        data.append("officerid", $("#officerId").val());
        data.append("officername", $("#officerName").val());
        data.append("officersex", $("#officerSex").val());
        data.append("officerage", $("#officerAge").val());
        data.append("officeraddress", $("#officerAddress").val());
        data.append("officeridentityno", $("#officerIdentityno").val());
        data.append("officerphone", $("#officerPhone").val());
        data.append("officerfile_old", $("#officerfile_old").val());

        //alert($("#officerId").val());

        $.ajax({
            type: "PUT",
            url: "/api/officers/" + $("#officerId").val(),
            contentType: false,
            processData: false,
            data: data,
            success: function (result) {
                toastr.success("Updated successfully.", "Server Response");
                tableCase.ajax.reload();
                onPopulateOfficer();
                document.getElementById('officerName').disabled = true;
                document.getElementById('btnOfficerAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                $('#officerName').val('');
                //$('#caseNote').val('');
            },
            error: function (errormessage) {
                toastr.error("This Officer is already exists.", "Server Response");
            }
        });

    }
}

function OfficerDelete(id) {
    bootbox.confirm({
        title: "Confirmation",
        message: "Are you sure you want to delete this?",
        buttons: {
            cancel: {
                label: "Cancel",
                className: "btn-default"
            },
            confirm: {
                label: "Delete",
                className: "btn-danger"
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: "/api/officers/" + id,
                    method: "DELETE",
                    success: function () {
                        tableCase.ajax.reload();
                        toastr.success("Deleted successfully.", "Server Response");
                    },
                    error: function () {
                        toastr.error("This clerk is being used, cannot delete this.", "Server Response");
                    }
                });
            }
        }
    });
}

function readURLOfficer(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#officerPhoto').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}