﻿$(document).ready(function () {
    GetOwnerShip();
});

var ownerShipTable = [];
function GetOwnerShip() {
    ownerShipTable = $('#ownerShipTbl').dataTable({
        ajax: {
            url: "/api/OwnerShips",
            dataSrc: ""
        },
        columns:
            [
                {
                    data: "id", render: function (data) {

                      return "W" + ("000" + data).slice(-3);
                  }
                },
                {
                    data: "date",
                    render: function (data) {
                        return moment(new Date(data)).format('DD-MMM-YYYY');
                    }
                },
                {
                    data: "ownermalename"
                },
                {
                    data: "ownermalephone"
                },
                {
                    data: "gotmalename"
                },
                {
                    data: "gotmalephone"
                },
                {
                    data: "letternumber"
                },
                {
                    data: "id",
                    render: function (data) {
                        return "<button OnClick='ShipProccess (" + data + ")' class='btn btn-primary btn-xs' style='margin-right:5px'>ដំណើរការ</button>" +
                            "<button OnClick='ShipPayment (" + data + ")' class='btn btn-primary btn-xs' style='margin-right:5px'>ការចំណាយ</button>" +
                            "<button OnClick='ShipDetail (" + data + ")' class='btn btn-primary btn-xs' style='margin-right:5px'>ឯកសារ</button>" +
                            "<button OnClick='OwnerShipEdit (" + data + ")' class='btn btn-warning btn-xs' style='margin-right:5px'>កែប្រែ</button>" +
                            "<button OnClick='OwnerShipDelete (" + data + ")' class='btn btn-danger btn-xs' style='margin-right:5px'>លុប</button>";
                    }
                }
            ],
        destroy: true,
        "order": [0, "desc"],
        "info": false

    });
}
function OwnerShipAction() {
    var action = '';
    action = document.getElementById('btnOwnerShip').innerText;
    //alert(action);

    if (action == "Add New") {
        document.getElementById('btnOwnerShip').innerText = 'Save';
        EnableControlOw();
        ClearControlOw();
        $('#ownermalename').focus();
        $('#date').val(moment().format('YYYY-MM-DD'));
        $('#letterdate').val(moment().format('YYYY-MM-DD'));
    }
    else if (action == "Save") {
        if ($('#ownermalename').val().trim() == "") {
            $('#ownermalename').css('border-color', 'red');
            $('#ownermalename').focus();
            toastr.info("Please Input Property Name", "Server Respon")
        }
        else {
            $('#ownermalename').css('border-color', '#cccccc');
            var data = {
                date: $('#date').val(),
                shiptype: $('#shiptype').val(),
                ownermalename: $('#ownermalename').val(),
                ownermalephone: $('#ownermalephone').val(),
                ownerfemalename: $('#ownerfemalename').val(),
                ownerfemalephone: $('#ownerfemalephone').val(),
                gotmalename: $('#gotmalename').val(),
                gotmalephone: $('#gotmalephone').val(),
                gotfemalename: $('#gotfemalename').val(),
                gotfemalephone: $('#gotfemalephone').val(),
                propertyid: $('#propertyid').val(),
                letterid: $('#letterid').val(),
                letternumber: $('#letternumber').val(),
                letterdate: $('#letterdate').val(),
                location: $('#location').val(),
                landtypeid: $('#landtypeid').val(),
                landnote: $('#landnote').val(),
                hometype: $('#hometype').val(),
            };
            $.ajax({
                url: "/api/OwnerShips",
                data: JSON.stringify(data),
                type: "POST",
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                success: function (result) {
                    toastr.success("New Data has been Created", "Server Respond");
                    ownerShipTable.DataTable().ajax.reload();
                    document.getElementById('btnOwnerShip').innerText = 'Add New';
                    ClearControlOw();
                    DisableControlOw();
                    $('#ownerShipModal').modal('hide');
                },
                error: function (errormesage) {
                    toastr.error("This Name is exist in Database", "Server Respond")
                }
            });
        }
    } else if (action == "Update") {
        if ($('#ownermalename').val().trim() == "") {
            $('#ownermalename').css('border-color', 'red');
            $('#ownermalename').focus();
            toastr.info("Please Input Property Name", "Server Respon")
        }
        else {
            $('#ownermalename').css('border-color', '#cccccc');
            var data = {
                id: $('#ownerID').val(),
                date: $('#date').val(),
                shiptype: $('#shiptype').val(),
                ownermalename: $('#ownermalename').val(),
                ownermalephone: $('#ownermalephone').val(),
                ownerfemalename: $('#ownerfemalename').val(),
                ownerfemalephone: $('#ownerfemalephone').val(),
                gotmalename: $('#gotmalename').val(),
                gotmalephone: $('#gotmalephone').val(),
                gotfemalename: $('#gotfemalename').val(),
                gotfemalephone: $('#gotfemalephone').val(),
                propertyid: $('#propertyid').val(),
                letterid: $('#letterid').val(),
                letternumber: $('#letternumber').val(),
                letterdate: $('#letterdate').val(),
                location: $('#location').val(),
                landtypeid: $('#landtypeid').val(),
                landnote: $('#landnote').val(),
                hometype: $('#hometype').val(),
            };
            $.ajax({
                url: "/api/OwnerShips/" + data.id,
                data: JSON.stringify(data),
                type: "PUT",
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                success: function (result) {
                    toastr.success("Data has been Updated", "Server Respond");
                    ownerShipTable.DataTable().ajax.reload();
                    document.getElementById('btnOwnerShip').innerText = 'Add New';
                    ClearControlOw();
                    DisableControlOw();
                    $('#ownerShipModal').modal('hide');
                },
                error: function (errormesage) {
                    toastr.error("Data hasn't Updated in Database", "Server Respond")
                }
            });
        }
    }
}

//Delete
function OwnerShipDelete(id) {
    bootbox.confirm({
        title: "",
        message: "Are you sure want to delete this?",
        button: {
            cancel: {
                label: "Cancel",
                ClassName: "btn-default",
            },
            confirm: {
                label: "Delete",
                ClassName: "btn-danger"
            }
        },
        callback: function (result) {
            //alert(id);
            if (result) {
                $.ajax({
                    url: "/api/OwnerShips/" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        ownerShipTable.DataTable().ajax.reload();
                        toastr.success("Data Deleted successfully!", "Service Response");
                    },
                    error: function (errormessage) {
                        toastr.error("This Data hasn't deleted in Database", "Service Response");
                    }
                });
            }
        }
    });
}

//Edit
function OwnerShipEdit(id) {
    $('#ownerShipModal').modal('show');
    $.ajax({
        url: "/api/OwnerShips/" + id,
        type: "Get",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            
            $('#ownerID').val(result.id);
            var dr = moment(result.date).format("YYYY-MM-DD");
            $("#date").val(dr);
            $('#shiptype').val(result.shiptype);
            $('#ownermalename').val(result.ownermalename);
            $('#ownermalephone').val(result.ownermalephone);
            $('#ownerfemalename').val(result.ownerfemalename);
            $('#ownerfemalephone').val(result.ownerfemalephone);
            $('#gotmalename').val(result.gotmalename);
            $('#gotmalephone').val(result.gotmalephone);
            $('#gotfemalename').val(result.gotfemalename);
            $('#gotfemalephone').val(result.gotfemalephone);
            $('#propertyid').val(result.propertyid);
            $('#letterid').val(result.letterid);
            $('#letternumber').val(result.letternumber);
            var da = moment(result.letterdate).format("YYYY-MM-DD");
            $("#letterdate").val(da);
            $('#location').val(result.location);
            $('#landtypeid').val(result.landtypeid);
            $('#landnote').val(result.landnote);
            $('#hometype').val(result.hometype);

            document.getElementById('btnOwnerShip').innerText = 'Update';
            $('#ownermalename').focus();

        },
        error: function (errormesage) {
            toastr.error("Unexpacted ", "Server Respond")
        }
    });
}
function DisableControlOw() {
    document.getElementById('date').disable = true;
    document.getElementById('shiptype').disable = true;
    document.getElementById('ownermalename').disable = true;
    document.getElementById('ownermalephone').disable = true;
    document.getElementById('ownerfemalename').disable = true;
    document.getElementById('ownerfemalephone').disable = true;
    document.getElementById('gotmalename').disable = true;
    document.getElementById('gotmalephone').disable = true;
    document.getElementById('gotfemalename').disable = true;
    document.getElementById('gotfemalephone').disable = true;
    document.getElementById('propertyid').disable = true;
    document.getElementById('letterid').disable = true;
    document.getElementById('letternumber').disable = true;
    document.getElementById('letterdate').disable = true;
    document.getElementById('location').disable = true;
    document.getElementById('landtypeid').disable = true;
    document.getElementById('landnote').disable = true;
    document.getElementById('hometype').disable = true;

}
function EnableControlOw() {
    document.getElementById('date').disable = false;
    document.getElementById('shiptype').disable = false;
    document.getElementById('ownermalename').disable = false;
    document.getElementById('ownermalephone').disable = false;
    document.getElementById('ownerfemalename').disable = false;
    document.getElementById('ownerfemalephone').disable = false;
    document.getElementById('gotmalename').disable = false;
    document.getElementById('gotmalephone').disable = false;
    document.getElementById('gotfemalename').disable = false;
    document.getElementById('gotfemalephone').disable = false;
    document.getElementById('propertyid').disable = false;
    document.getElementById('letterid').disable = false;
    document.getElementById('letternumber').disable = false;
    document.getElementById('letterdate').disable = false;
    document.getElementById('location').disable = false;
    document.getElementById('landtypeid').disable = false;
    document.getElementById('landnote').disable = false;
    document.getElementById('hometype').disable = false;
}
function ClearControlOw() {
    $('#ownermalename').val('');
    $('#date').val('');
    $('#shiptype').val('');
    $('#ownermalephone').val('');
    $('#ownerfemalename').val('');
    $('#ownerfemalephone').val('');
    $('#gotmalename').val('');
    $('#gotmalephone').val('');
    $('#gotfemalename').val('');
    $('#gotfemalephone').val('');
    //$('#propertyid').val('');
    //$('#letterid').val('');
    $('#letternumber').val('');
    $('#location').val('');
    //$('#landtypeid').val('');
    $('#landnote').val('');
    $('#hometype').val('');
}
function OwnerShipModalAction() {
    document.getElementById('btnOwnerShip').innerText = 'Add New';
    DisableControlOw();
    ClearControlOw();
}
function ShipProccess(id) {
    document.getElementById('btnShipProccess').innerText = 'Add New';
    $("#shipProccessModal").modal('show');
    $('#ownershipid').val(id);
    GetShipProccess(id);
    ClearControlPro();
    DisableControlPro();
}
var shipProccessTable = [];
function GetShipProccess(id) {
    shipProccessTable = $('#shipProccesstbl').dataTable({
        ajax: {
            url: "/api/ShipProccessByOwnerShip/" + id + "/2",
            dataSrc: ""
        },
        columns:
            [
                {
                    data: "id",
                },
                
                {
                    data: "type"
                },
                {
                    data: "datein",
                    render: function (data) {
                        return moment(new Date(data)).format('DD-MMM-YYYY');
                    }
                },
                {
                    data: "dateout",
                    render: function (data) {
                        return moment(new Date(data)).format('DD-MMM-YYYY');
                    }
                },
                {
                    data: "note"
                },
                {
                    data: "id",
                    render: function (data) {
                        return "<button OnClick='ShipProccessEdit (" + data + ")' class='btn btn-primary btn-xs' style='margin-right:5px'>Edit</button>" +
                            "<button OnClick='ShipProccessDelete (" + data + ")' class='btn btn-default btn-xs' >Delete</button>";
                    }
                }
            ],
        destroy: true,
        "order": [0, "desc"],
        "info": false

    });
}
function ShipProccessAction() {
    var action = '';
    action = document.getElementById('btnShipProccess').innerText;
    //alert(action);

    if (action == "Add New") {
        document.getElementById('btnShipProccess').innerText = 'Save';
        EnableControlPro();
        ClearControlPro();
        $('#proccesstype').focus();
        $('#datePro').val(moment().format('YYYY-MM-DD'));
        $('#datein').val(moment().format('YYYY-MM-DD'));
        $('#dateout').val(moment().format('YYYY-MM-DD'));
    }
    else if (action == "Save") {
        if ($('#proccesstype').val().trim() == "") {
            $('#proccesstype').css('border-color', 'red');
            $('#proccesstype').focus();
            toastr.info("Please Input Type Name", "Server Respon")
        }
        else {
            $('#proccesstype').css('border-color', '#cccccc');
            var data = {
                date: $('#datePro').val(),
                type: $('#proccesstype').val(),
                datein: $('#datein').val(),
                dateout: $('#dateout').val(),
                note: $('#noteProccess').val(),
                ownershipid: $('#ownershipid').val(),
            };
            $.ajax({
                url: "/api/ShipProccess",
                data: JSON.stringify(data),
                type: "POST",
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                success: function (result) {
                    toastr.success("New Data has been Created", "Server Respond");
                    shipProccessTable.DataTable().ajax.reload();
                    document.getElementById('btnShipProccess').innerText = 'Add New';
                    ClearControlPro();
                    DisableControlPro();
                    //$('#shipProccessModal').modal('hide');
                },
                error: function (errormesage) {
                    toastr.error("This Name is exist in Database", "Server Respond")
                }
            });
        }
    } else if (action == "Update") {
        if ($('#proccesstype').val().trim() == "") {
            $('#proccesstype').css('border-color', 'red');
            $('#proccesstype').focus();
            toastr.info("Please Input Type Name", "Server Respon")
        }
        else {
            $('#proccesstype').css('border-color', '#cccccc');
            var data = {
                id: $('#ID').val(),
                date: $('#datePro').val(),
                type: $('#proccesstype').val(),
                datein: $('#datein').val(),
                dateout: $('#dateout').val(),
                note: $('#noteProccess').val(),
                ownershipid: $('#ownershipid').val(),
            };
            $.ajax({
                url: "/api/ShipProccess/" + data.id,
                data: JSON.stringify(data),
                type: "PUT",
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                success: function (result) {
                    toastr.success("Data has been Updated", "Server Respond");
                    shipProccessTable.DataTable().ajax.reload();
                    document.getElementById('btnShipProccess').innerText = 'Add New';
                    ClearControlPro();
                    DisableControlPro();
                    //$('#shipProccessModal').modal('hide');
                },
                error: function (errormesage) {
                    toastr.error("Data hasn't Updated in Database", "Server Respond")
                }
            });
        }
    }
}
function ShipProccessDelete(id) {
    bootbox.confirm({
        title: "",
        message: "Are you sure want to delete this?",
        button: {
            cancel: {
                label: "Cancel",
                ClassName: "btn-default",
            },
            confirm: {
                label: "Delete",
                ClassName: "btn-danger"
            }
        },
        callback: function (result) {
            //alert(id);
            if (result) {
                $.ajax({
                    url: "/api/ShipProccess/" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        shipProccessTable.DataTable().ajax.reload();
                        toastr.success("Data Deleted successfully!", "Service Response");
                    },
                    error: function (errormessage) {
                        toastr.error("This Data hasn't deleted in Database", "Service Response");
                    }
                });
            }
        }
    });
}
function ShipProccessEdit(id) {
    //$('#ownerShipModal').modal('show');
    $.ajax({
        url: "/api/ShipProccess/" + id,
        type: "Get",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {

            $('#ID').val(result.id);
            var dr = moment(result.date).format("YYYY-MM-DD");
            $("#datePro").val(dr);
            $('#ownershipid').val(result.ownershipid);
            $('#proccesstype').val(result.type);
            $('#noteProccess').val(result.note);
            var da = moment(result.datein).format("YYYY-MM-DD");
            $("#datein").val(da);
            var dat = moment(result.dateout).format("YYYY-MM-DD");
            $("#dateout").val(dat);

            document.getElementById('btnShipProccess').innerText = 'Update';
            $('#proccesstype').focus();

        },
        error: function (errormesage) {
            toastr.error("Unexpacted ", "Server Respond")
        }
    });
}
function DisableControlPro() {
    document.getElementById('datePro').disable = true;
    document.getElementById('proccesstype').disable = true;
    document.getElementById('datein').disable = true;
    document.getElementById('dateout').disable = true;
    document.getElementById('noteProccess').disable = true;

}
function EnableControlPro() {
    document.getElementById('datePro').disable = false;
    document.getElementById('proccesstype').disable = false;
    document.getElementById('datein').disable = false;
    document.getElementById('dateout').disable = false;
    document.getElementById('noteProccess').disable = false;
}
function ClearControlPro() {
    $('#datePro').val('');
    //$('#proccesstype').val('');
    $('#datein').val('');
    $('#dateout').val('');
    $('#noteProccess').val('');
}
function ShipPayment(id) {
    document.getElementById('btnShipPayment').innerText = 'Add New';
    $("#shipPaymentModal").modal('show');
    $('#ownershipid').val(id);
    GetShipPayment(id);
    ClearControlPay();
    DisableControlPay();
}
var shipPaymentTable = [];
function GetShipPayment(id) {
    shipPaymentTable = $('#shipPaymenttbl').dataTable({
        ajax: {
            url: "/api/ShipPaymentByOwnerShip/" + id + "/2",
            dataSrc: ""
        },
        columns:
            [
                {
                    data: "id",
                },
                {
                    data: "date",
                    render: function (data) {
                        return moment(new Date(data)).format('DD-MMM-YYYY');
                    }
                },
                {
                    data: "amount"
                },
                {
                    data: "note"
                },
                {
                    data: "id",
                    render: function (data) {
                        return "<button OnClick='ShipPaymentEdit (" + data + ")' class='btn btn-primary btn-xs' style='margin-right:5px'>Edit</button>" +
                            "<button OnClick='ShipPaymentDelete (" + data + ")' class='btn btn-default btn-xs' style='margin-right:5px'>Delete</button>" +
                            "<button OnClick='ShipPaymentPrint (" + data + ")' class='btn btn-success btn-xs' style='margin-right:5px'>Print</button>";
                    }
                }
            ],
        destroy: true,
        "order": [0, "desc"],
        "info": false

    });
}
function ShipPaymentPrint(id) {
    window.open("/ownershipPay/" + id, "_blank")
}
function ShipPaymentAction() {
    var action = '';
    action = document.getElementById('btnShipPayment').innerText;
    //alert(action);

    if (action == "Add New") {
        document.getElementById('btnShipPayment').innerText = 'Save';
        EnableControlPay();
        ClearControlPay();
        $('#amount').focus();
        $('#amount').val('0.00');
        $('#datePay').val(moment().format('YYYY-MM-DD'));

    }
    else if (action == "Save") {
       
            var data = {
                date: $('#datePay').val(),
                amount: $('#amount').val(),
                note: $('#notePay').val(),
                ownershipid: $('#ownershipid').val(),
            };
            $.ajax({
                url: "/api/ShipPayments",
                data: JSON.stringify(data),
                type: "POST",
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                success: function (result) {
                    toastr.success("New Data has been Created", "Server Respond");
                    shipPaymentTable.DataTable().ajax.reload();
                    document.getElementById('btnShipPayment').innerText = 'Add New';
                    ClearControlPay();
                    DisableControlPay();
                    //$('#shipPaymentModal').modal('hide');
                },
                error: function (errormesage) {
                    toastr.error("This Name is exist in Database", "Server Respond")
                }
            });
        }
         else if (action == "Update") {
        
            var data = {
                id: $('#ID').val(),
                date: $('#datePay').val(),
                amount: $('#amount').val(),
                note: $('#notePay').val(),
                ownershipid: $('#ownershipid').val(),
            };
            $.ajax({
                url: "/api/ShipPayments/" + data.id,
                data: JSON.stringify(data),
                type: "PUT",
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                success: function (result) {
                    toastr.success("Data has been Updated", "Server Respond");
                    shipPaymentTable.DataTable().ajax.reload();
                    document.getElementById('btnShipPayment').innerText = 'Add New';
                    ClearControlPay();
                    DisableControlPay();
                    //$('#shipProccessModal').modal('hide');
                },
                error: function (errormesage) {
                    toastr.error("Data hasn't Updated in Database", "Server Respond")
                }
            });
        }
    }

function ShipPaymentDelete(id) {
    bootbox.confirm({
        title: "",
        message: "Are you sure want to delete this?",
        button: {
            cancel: {
                label: "Cancel",
                ClassName: "btn-default",
            },
            confirm: {
                label: "Delete",
                ClassName: "btn-danger"
            }
        },
        callback: function (result) {
            //alert(id);
            if (result) {
                $.ajax({
                    url: "/api/ShipPayments/" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        shipProccessTable.DataTable().ajax.reload();
                        toastr.success("Data Deleted successfully!", "Service Response");
                    },
                    error: function (errormessage) {
                        toastr.error("This Data hasn't deleted in Database", "Service Response");
                    }
                });
            }
        }
    });
}
function ShipPaymentEdit(id) {
    //$('#ownerShipModal').modal('show');
    $.ajax({
        url: "/api/ShipPayments/" + id,
        type: "Get",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {

            $('#ID').val(result.id);
            var dr = moment(result.date).format("YYYY-MM-DD");
            $("#datePay").val(dr);
            $('#ownershipid').val(result.ownershipid);
            $('#amount').val(result.amount);
            $('#notePay').val(result.note);

            document.getElementById('btnShipPayment').innerText = 'Update';
            $('#amount').focus();

        },
        error: function (errormesage) {
            toastr.error("Unexpacted ", "Server Respond")
        }
    });
}
function DisableControlPay() {
    document.getElementById('datePay').disable = true;
    document.getElementById('amount').disable = true;
    document.getElementById('notePay').disable = true;

}
function EnableControlPay() {
    document.getElementById('datePay').disable = false;
    document.getElementById('amount').disable = false;
    document.getElementById('notePay').disable = false;
}
function ClearControlPay() {
    $('#datePay').val('');
    $('#amount').val('0.00');
    $('#notePay').val('');
}
//Ship Detail
function ShipDetail(id) {
    document.getElementById('btnShipDetail').innerText = 'Add New';
    $("#shipDetailModal").modal('show');
    $('#ownership_id').val(id);
    GetShipDetail(id);
    ClearControlDe();
    DisableControlDe();
}
var shipDetailTable = [];
function GetShipDetail(id) {
    shipDetailTable = $('#shipDetailtbl').DataTable({
        ajax: {
            url: "api/ShipDetailByOwnerShip/" + id + "/2",
            dataSrc: ""
        },
        columns:
            [

                {
                    data: "id",
                },
                {
                    data: "detailtype",
                },
                {
                    data: "note",
                },
                {
                    data: "id",
                    render: function (data) {
                        return "<button OnClick='ShipDetailEdit (" + data + ")' class='btn btn-primary btn-xs' style='margin-right:5px'><span class='glyphicon glyphicon-edit'></span> Edit</button>" +
                            "<button OnClick='ShipDetailDelete (" + data + ")' class='btn btn-default btn-xs' ><span class='glyphicon glyphicon-remove'></span> Delete</button>";
                    }
                }
            ],
        destroy: true,
        "order": [0, "desc"],
        "info": false

    });
}
function readURLCustomer(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $("#myfile").attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
};
function ShipDetailAction() {
    var action = '';
    action = document.getElementById('btnShipDetail').innerText;

    if (action == "Add New") {
        
        document.getElementById('btnShipDetail').innerText = 'Save';
        EnableControlDe();
        ClearControlDe();
        $('#detailtype').focus();
        //$('#dob').val(moment().format('YYYY-MM-DD'));
        $('#myfile').attr('src', '../Images');
    }
    else if (action == "Save") {
        var data = new FormData();

        var files = $('#myfile').get(0).files;

        if (files.length > 0) {
            data.append("attachment", files[0]);
        }

        data.append("detailtype", $('#detailtype').val());
        data.append("note", $('#noteDetail').val());
        data.append("ownership_id", $('#ownership_id').val());



        $.ajax({
            url: "/api/ShipDetails",
            data: data,
            type: "POST",
            contentType: false,
            processData: false,
            dataType: "json",
            success: function (result) {
                toastr.success("Save To Database Successfully", "Server Respond");
                $('#shipDetailtbl').DataTable().ajax.reload();
                document.getElementById('btnShipDetail').innerText = 'Add New';
                DisableControlDe();
                ClearControlDe();
                //$("#customersModal").modal('hide');
            },
            error: function (errormesage) {
                toastr.error("This Name is exist in Database", "Server Respond")
            }

        });

    } else if (action == "Update") {
        var data = new FormData();

        var files = $('#myfile').get(0).files;

        if (files.length > 0) {
            data.append("attachment", files[0]);
        }

        data.append("id", $('#ID').val());

        data.append("detailtype", $('#detailtype').val());
        data.append("note", $('#noteDetail').val());
        data.append("ownership_id", $('#ownership_id').val());
        data.append("file_old", $("#file_old").val());

        $.ajax({
            url: "/api/ShipDetails/" + $("#ID").val(),
            data: data,
            type: "PUT",
            contentType: false,
            processData: false,
            dataType: "json",
            success: function (result) {
                toastr.success("Data has been Updated", "Server Respond");
                $('#shipDetailtbl').DataTable().ajax.reload();
                document.getElementById('btnShipDetail').innerText = 'Add New';
                DisableControlDe();
                ClearControlDe();
                //$("#customersModal").modal('hide');
            },
            error: function (errormesage) {
                toastr.error("Data hasn't Updated in Database", "Server Respond")
            }
        });
    }
}
function ShipDetailEdit(id) {
    //alert('hi');
    ClearControlDe();
    EnableControlDe();
    action = document.getElementById('btnShipDetail').innerText = "Update";

    $.ajax({
        url: "/api/ShipDetails/" + id,
        type: "GET",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        success: function (result) {
            $('#ID').val(result.id);

            $("#detailtype").val(result.detailtype);
            $("#noteDetail").val(result.note);
            $("#ownership_id").val(result.ownership_id);
            $('#file_old').val(result.attachment);
            $('#myfile1').html(result.attachment);

            if (result.attachment == "") {
                $("#myfile").attr('src');
            } else {
                $("#myflie").attr('src', result.attachment);
            }
        },
        error: function (errormessage) {
            toastr.error("No Record Select!", "Service Response");
        }
    });
}
function ShipDetailDelete(id) {
    bootbox.confirm({
        title: "",
        message: "Are you sure want to delete this?",
        button: {
            cancel: {
                label: "Cancel",
                ClassName: "btn-default",
            },
            confirm: {
                label: "Delete",
                ClassName: "btn-danger"
            }
        },
        callback: function (result) {
            //alert(id);
            if (result) {
                $.ajax({
                    url: "/api/ShipDetails/" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        $('#shipDetailtbl').DataTable().ajax.reload();
                        toastr.success("Data Deleted successfully!", "Service Response");
                    },
                    error: function (errormessage) {
                        toastr.error("Data Can't be Deleted", "Service Response");
                    }
                });
            }
        }
    });
}
function DisableControlDe() {
    document.getElementById('detailtype').disabled = true;
    document.getElementById('noteDetail').disabled = true;
}

function EnableControlDe() {
    document.getElementById('detailtype').disabled = false;
    document.getElementById('noteDetail').disabled = false;

}

function ClearControlDe() {
    $('#detailtype').val('');
    $('#noteDetail').val('');
    $("#myfile").val(null);
    document.getElementById('myfile1').innerText = "";
    
}


