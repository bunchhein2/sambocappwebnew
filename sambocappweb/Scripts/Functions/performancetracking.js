﻿
$(document).ready(function () {
    $(document).ajaxStart(function () {
        Pace.start();
        $("div#divLoadingModal").addClass('show');
    }).ajaxStop(function () {
        Pace.stop();
        $("div#divLoadingModal").removeClass('show');
        //document.getElementById('pendingBadge').innerText = tableTasksPending.page.info().recordsTotal;
    });

    loadCurrentTaskChart();
    getJobsPending();

    $(".nav-pills a").click(function () {
        var menu = $(this).attr('data-tab');
       
        if (menu == "pending") {
            getJobsPending();
        }
        else if (menu == "inprogress") {
            getJobsInProgress();
        }
        else if (menu == "suspend") {
            getJobsSuspend();
        }
        else if (menu == "completed") {
            getJobsCompleted();
        }
    });

    //$('#jobModalEdit').on('show.bs.modal', function () {
    //    if ($('#jobPriorityUpdate').val() == "urgent") {
    //        $('#deadlineInputUpdate').show();
    //    }
    //    else {
    //        $('#deadlineInputUpdate').hide();
    //    }
    //});

    $('#jobModalEdit').on('shown.bs.modal', function () {
        $('#jobSubjectUpdate').focus();
    });

    //$("#jobPriorityUpdate").change(function () {
    //    if ($('#jobPriorityUpdate').val() == "urgent") {
    //        $('#deadlineInputUpdate').show();
    //        $('#jobDeadlineUpdate').val(GetCurrentDate());
    //    }
    //    else {
    //        $('#deadlineInputUpdate').hide();
    //    }
    //});
})

var tableTasksPending = [];
var tableTasksInProgress = [];
var tableTasksSuspend = [];
var tableTasksCompleted = [];
var chart = [];
var view = [];
var options = [];

function getJobsPending()
{
    tableTasksPending = $('#jobsPendingTable').DataTable({
        ajax: {
            url: "/api/jobs?status=pending",
            dataSrc: ""
        },
        columns: [
            {
                data: "id",
                "className": "text-center"
            },
            //{
            //    data: "priority"
            //},
            {
                data: function (data) {
                    return "<a href='#' style='text-decoration: none;' onclick='onJobEdit(" + data.id + ")'><span class='glyphicon glyphicon-edit'></span> " + data.subject + "</a>";
                }
            },
            {
                data: "startDate",
                render: function (data) {
                    var date = new Date(data);
                    var month = date.getMonth() + 1;
                    return date.getDate() + "-" + (month.length > 1 ? month : month) + "-" + date.getFullYear();
                }
            },
            {
                data: "endDate",
                render: function (data) {
                    if (data == null) {
                        return "Not Set";
                    }
                    else {
                        var date = new Date(data);
                        var month = date.getMonth() + 1;
                        return date.getDate() + "-" + (month.length > 1 ? month : month) + "-" + date.getFullYear();
                    }
                }
            },
            {
                data: function (data) {
                    var start = moment(data.startDate);
                    var deadline = moment(data.completionDate);
                    var currentDate = moment(getCurrentDateYYDDMM());

                    var endStart = deadline.diff(start, "days");
                    var currentStart = currentDate.diff(start, "days");
                    var deadCurrent = deadline.diff(currentDate, "days");
                    var perPercentage = 100 / endStart;
                    var percentageNow = perPercentage * currentStart;
                    if (percentageNow > 100) {
                        percentageNow = 100;
                    }
                    if (data.completionDate == null) {
                        return "None";
                    }
                    else {
                        return "<div class='progress progress-striped active'><div class='progress-bar progress-bar" + (percentageNow <= 30 ? "-info" : percentageNow <= 60 ? "-warning" : percentageNow < 100 ? "-danger" : "") + "' style='width: " + percentageNow + "%;'></div><div class='progress-bar-title' style='color: " + (percentageNow >= 100 ? "#fff" : "#000") + "'>" + (deadCurrent <= 0 ? "Reached Deadline" : deadCurrent + " day(s) left") + "</div></div>";
                    }
                }
            },
            {
                data: "name"
            },
            {
                data: "id",
                render: function (data) {
                    return "<button onclick='onUpdateStatusToInProgress(" + data + ")' class='btn btn-info btn-xs' style='border-width: 0px; width: 100px;'><span class='glyphicon glyphicon-share-alt'></span> In Progress</button>";
                },
                "width": "100px", "className": "text-center"
            }
        ],
        destroy: true,
        responsive: true,
        "order": [[0, "desc"]],
        "language": {
            "search": "<span class='glyphicon glyphicon-search'></span> <span class='kh'>ស្វែងរក</span> Search:"
        },
        "info": false,
        "createdRow": function (row, data, dataIndex) {
            var start = moment(data.startDate);
            var deadline = moment(data.completionDate);
            var currentDate = moment(getCurrentDateYYDDMM());

            var endStart = deadline.diff(start, "days");
            var currentStart = currentDate.diff(start, "days");
            var deadCurrent = deadline.diff(currentDate, "days");
            var perPercentage = 100 / endStart;
            var percentageNow = perPercentage * currentStart;
            if (percentageNow > 100) {
                $(row).css('color', "#8c0000");
            }
        }
    });
}

function getJobsInProgress()
{
    tableTasksInProgress = $('#jobsInProgressTable').DataTable({
        ajax: {
            url: "/api/jobs?status=in progress",
            dataSrc: ""
        },
        columns: [
            {
                data: "id",
                "className": "text-center"
            },
            //{
            //    data: "priority"
            //},
            {
                data: function (data) {
                    return "<a href='#' style='text-decoration: none;' onclick='onJobEdit(" + data.id + ")'><span class='glyphicon glyphicon-edit'></span> " + data.subject + "</a>";
                }
            },
            {
                data: "startProgress",
                render: function (data) {
                    var date = new Date(data);
                    var month = date.getMonth() + 1;
                    return date.getDate() + "-" + (month.length > 1 ? month : month) + "-" + date.getFullYear() + " (" + moment(data, "YYYYMMDD hh:mm:ss tt").fromNow() + ")";
                }
            },
            {
                data: function (data) {
                    var start = moment(data.startDate);
                    var deadline = moment(data.completionDate);
                    var currentDate = moment(getCurrentDateYYDDMM());

                    var endStart = deadline.diff(start, "days");
                    var currentStart = currentDate.diff(start, "days");
                    var deadCurrent = deadline.diff(currentDate, "days");
                    var perPercentage = 100 / endStart;
                    var percentageNow = perPercentage * currentStart;
                    if (percentageNow > 100) {
                        percentageNow = 100;
                    }
                    if (data.completionDate == null) {
                        return "None";
                    }
                    else {
                        return "<div class='progress progress-striped active'><div class='progress-bar progress-bar" + (percentageNow <= 30 ? "-info" : percentageNow <= 60 ? "-warning" : percentageNow < 100 ? "-danger" : "") + "' style='width: " + percentageNow + "%;'></div><div class='progress-bar-title' style='color: " + (percentageNow >= 100 ? "#fff" : "#000") + "'>" + (deadCurrent <= 0 ? "Reached Deadline" : deadCurrent + " day(s) left") + "</div></div>";
                    }
                }
            },
            {
                data: "name"
            },
            {
                data: "id",
                render: function (data) {
                    return "<button onclick='onUpdateStatusToCompleted(" + data + ")' class='btn btn-primary btn-xs' style='border-width: 0px; width: 100px; margin-right: 5px;'><span class='glyphicon glyphicon-check'></span> Completed</button>" +
                        "<button onclick='onUpdateStatusToSuspend(" + data + ")' class='btn btn-danger btn-xs' style='border-width: 0px;'><span class='glyphicon glyphicon-stop'></span> Suspend</button>";
                }
            },
        ],
        destroy: true,
        responsive: true,
        "order": [[0, "desc"]],
        "language": {
            "search": "<span class='glyphicon glyphicon-search'></span> <span class='kh'>ស្វែងរក</span> Search:"
        },
        "info": false,
        "createdRow": function (row, data, dataIndex) {
            var start = moment(data.startDate);
            var deadline = moment(data.completionDate);
            var currentDate = moment(getCurrentDateYYDDMM());

            var endStart = deadline.diff(start, "days");
            var currentStart = currentDate.diff(start, "days");
            var deadCurrent = deadline.diff(currentDate, "days");
            var perPercentage = 100 / endStart;
            var percentageNow = perPercentage * currentStart;
            if (percentageNow > 100) {
                $(row).css('color', "#8c0000");
            }
        }
    });
}

function getJobsSuspend()
{
    tableTasksSuspend = $('#jobsSuspendTable').DataTable({
        ajax: {
            url: "/api/jobs?status=suspend",
            dataSrc: ""
        },
        columns: [
            {
                data: "id",
                "className": "text-center"
            },
            //{
            //    data: "priority"
            //},
            {
                data: function (data) {
                    return "<a href='#' style='text-decoration: none;' onclick='onJobEdit(" + data.id + ")'><span class='glyphicon glyphicon-edit'></span> " + data.subject + "</a>";
                }
            },
            {
                data: "closeOn",
                render: function (data) {
                    var date = new Date(data);
                    var month = date.getMonth() + 1;
                    return date.getDate() + "-" + (month.length > 1 ? month : month) + "-" + date.getFullYear();
                }
            },
            {
                data: function (data) {
                    var start = moment(data.startDate);
                    var deadline = moment(data.completionDate);
                    var currentDate = moment(getCurrentDateYYDDMM());

                    var endStart = deadline.diff(start, "days");
                    var currentStart = currentDate.diff(start, "days");
                    var deadCurrent = deadline.diff(currentDate, "days");
                    var perPercentage = 100 / endStart;
                    var percentageNow = perPercentage * currentStart;
                    if (percentageNow > 100) {
                        percentageNow = 100;
                    }
                    if (data.completionDate == null) {
                        return "None";
                    }
                    else {
                        return "<div class='progress progress-striped active'><div class='progress-bar progress-bar" + (percentageNow <= 30 ? "-info" : percentageNow <= 60 ? "-warning" : percentageNow < 100 ? "-danger" : "") + "' style='width: " + percentageNow + "%;'></div><div class='progress-bar-title' style='color: " + (percentageNow >= 100 ? "#fff" : "#000") + "'>" + (deadCurrent <= 0 ? "Reached Deadline" : deadCurrent + " day(s) left") + "</div></div>";
                    }
                }
            },
            {
                data: "name"
            },
            {
                data: "id",
                render: function (data) {
                    return "<button onclick='onUpdateStatusToInProgressForSuspend(" + data + ")' class='btn btn-info btn-xs' style='border-width: 0px; width: 100px; margin-right: 5px;'><span class='glyphicon glyphicon-share-alt'></span> In Progress</button>";
                },
                "width": "100px"
            }
        ],
        destroy: true,
        responsive: true,
        "order": [[0, "desc"]],
        "language": {
            "search": "<span class='glyphicon glyphicon-search'></span> <span class='kh'>ស្វែងរក</span> Search:"
        },
        "info": false,
        "createdRow": function (row, data, dataIndex) {
            var start = moment(data.startDate);
            var deadline = moment(data.completionDate);
            var currentDate = moment(getCurrentDateYYDDMM());

            var endStart = deadline.diff(start, "days");
            var currentStart = currentDate.diff(start, "days");
            var deadCurrent = deadline.diff(currentDate, "days");
            var perPercentage = 100 / endStart;
            var percentageNow = perPercentage * currentStart;
            if (percentageNow > 100) {
                $(row).css('color', "#8c0000");
            }
        }
    });
}

function getJobsCompleted() {
    tableTasksCompleted = $('#jobsCompletedTable').DataTable({
        ajax: {
            url: "/api/jobs?status=completed",
            dataSrc: ""
        },
        columns: [
            {
                data: "id",
                "className": "text-center"
            },
            //{
            //    data: "priority"
            //},
            {
                data: function (data) {
                    return data.subject;
                }
            },
            {
                data: "startDate",
                render: function (data) {
                    var date = new Date(data);
                    var month = date.getMonth() + 1;
                    return date.getDate() + "-" + (month.length > 1 ? month : month) + "-" + date.getFullYear();
                }
            },
            {
                data: "endProgress",
                render: function (data) {
                    var date = new Date(data);
                    var month = date.getMonth() + 1;
                    return date.getDate() + "-" + (month.length > 1 ? month : month) + "-" + date.getFullYear();
                }
            },
            {
                data: function (data) {
                    var startDate = moment(data.startDate, "YYYY-MM-DD");
                    var endDate = moment(data.endProgress, "YYYY-MM-DD");

                    return moment.duration(endDate.diff(startDate)).asDays() + " day(s)";
                }
            },
            {
                data: "name"
            },
            {
                data: "id",
                render: function (data) {
                    return "<button onclick='onUpdateStatusToPending(" + data + ")' class='btn btn-warning btn-xs' style='border-width: 0px; width: 80px; margin-right: 5px;'><span class='glyphicon glyphicon-retweet'></span> Reopen</button>";
                },
                "width": "100px"
            }
        ],
        destroy: true,
        responsive: true,
        "order": [[0, "desc"]],
        "language": {
            "search": "<span class='glyphicon glyphicon-search'></span> <span class='kh'>ស្វែងរក</span> Search:"
        },
        "info": false
    });
}

function loadCurrentTaskChart()
{
    google.charts.load("current", { packages: ['corechart'] });
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
        $.ajax({
            type: "GET",
            dataType: "json",
            url: '/api/jobs?currentTaskOverView=true',
            success: function (jsondata) {

                var data = new google.visualization.DataTable();
                data.addColumn('string', 'status');
                data.addColumn('number', 'current task');
                data.addColumn('string', 'color');

                $.each(jsondata, function (i, item) {
                    data.addRow([item.status, item.total, "#000"]);
                });

                view = new google.visualization.DataView(data);
                view.setColumns([0, 1,
                                 {
                                     calc: "stringify",
                                     sourceColumn: 1,
                                     type: "string",
                                     role: "annotation"
                                 }]);

                options = {
                    'title': 'Current Task Overview',
                    'chartArea': { 'width': '100%', 'height': '90%' },
                    'legend': { 'position': 'bottom' },
                    sliceVisibilityThreshold: 0,
                    pieSliceText: 'value-and-percentage',
                    colors: ['#18bc9c', '#3498db', '#e74c3c', '#2c3e50', '#f6c7b6'],
                    is3D: true,
                    pieSliceTextStyle: { fontSize: 13 }
                };
                chart = new google.visualization.PieChart(document.getElementById("currentTaskChart"));
                chart.draw(view, options);

                function resize() {
                    chart.draw(view, options);
                }
                if (window.addEventListener) {
                    window.addEventListener('resize', resize);
                }
                else {
                    window.attachEvent('onresize', resize);
                }

            }
        });
    }
}

function onUpdateStatusToInProgress(id)
{
    $.ajax({
        url: "/api/jobs?jobId=" + id + "&status=In Progress",
        type: "PUT",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            toastr.success("Task has been updated to <In Progress Status> successfully.", "Server Response");
            tableTasksPending.ajax.reload();
            loadCurrentTaskChart();
        },
        error: function (errormessage) {
            toastr.error("Cannot proceed this.", "Server Response");
        }
    });
}

function onUpdateStatusToInProgressForSuspend(id) {
    $.ajax({
        url: "/api/jobs?jobId=" + id + "&status=In Progress",
        type: "PUT",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            toastr.success("Task has been updated to <In Progress Status> successfully.", "Server Response");
            tableTasksSuspend.ajax.reload();
            loadCurrentTaskChart();
        },
        error: function (errormessage) {
            toastr.error("Cannot proceed this.", "Server Response");
        }
    });
}

function onUpdateStatusToCompleted(id) {
    $.ajax({
        url: "/api/jobs?jobId=" + id + "&status=Completed",
        type: "PUT",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            toastr.success("Task has been updated to <Completed Status> successfully.", "Server Response");
            tableTasksInProgress.ajax.reload();
            loadCurrentTaskChart();
        },
        error: function (errormessage) {
            toastr.error("Cannot proceed this.", "Server Response");
        }
    });
}

function onUpdateStatusToSuspend(id) {
    $.ajax({
        url: "/api/jobs?jobId=" + id + "&status=Suspend",
        type: "PUT",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            toastr.success("Task has been updated to <Suspend Status> successfully.", "Server Response");
            tableTasksInProgress.ajax.reload();
            loadCurrentTaskChart();
        },
        error: function (errormessage) {
            toastr.error("Cannot proceed this.", "Server Response");
        }
    });
}

function onUpdateStatusToPending(id) {
    $.ajax({
        url: "/api/jobs?jobId=" + id + "&status=Pending",
        type: "PUT",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            toastr.success("Task has been updated to <Pending Status> successfully.", "Server Response");
            tableTasksCompleted.ajax.reload();
            loadCurrentTaskChart();
        },
        error: function (errormessage) {
            toastr.error("Cannot proceed this.", "Server Response");
        }
    });
}

// Report
function getIndividualPerformanceReport() {
    var start = $('#jobFilterStartDate').val();
    var end = $('#jobFilterEndDate').val();

    if (start > end) {
        toastr.error("Start Date cannot be bigger than End Date.");
    }
    else if (start == '' || end == '') {
        toastr.error("Please enter required field.");
        $('#jobFilterStartDate').css('border-color', 'red');
        $('#jobFilterEndDate').css('border-color', 'red');
    }
    else {
        $('#jobFilterStartDate').css('border-color', '#cccccc');
        $('#jobFilterEndDate').css('border-color', '#cccccc');
        var url = "/performance-individual-report/startDate=" + moment($('#jobFilterStartDate').val()).format('MM-DD-YYYY') + "/endDate=" + moment($('#jobFilterEndDate').val()).format('MM-DD-YYYY');
        //window.location.href = url;
        window.open(url);
    }
}

// For Edit Job
function onJobUpdate() {
    if ($('#jobStartDateUpdate').val().trim() === "") {
        $('#jobStartDateUpdate').css('border-color', 'red');
        $('#jobStartDateUpdate').focus();
        toastr.info("Please enter start date.", "Required");
    }
    else {
        $('#jobStartDateUpdate').css('border-color', '#cccccc');
        if ($('#jobSubjectUpdate').val().trim() === "") {
            $('#jobSubjectUpdate').css('border-color', 'red');
            $('#jobSubjectUpdate').focus();
            toastr.info("Please enter subject.", "Required");
        }
        else {
            $('#jobSubjectUpdate').css('border-color', '#cccccc');
            var data = {
                Id: $('#jobIdUpdate').val(),
                Priority: 'urgent',
                DepartmentId: $('#jobDepartmentUpdate').val(),
                Subject: $('#jobSubjectUpdate').val(),
                Description: $('#jobDescriptionUpdate').val(),
                StartDate: $('#jobStartDateUpdate').val(),
                EndDate: $('#jobDeadlineUpdate').val()
            };
            $.ajax({
                url: "/api/jobs/" + data.Id,
                data: JSON.stringify(data),
                type: "PUT",
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                success: function (result) {
                    toastr.success("Updated successfully.", "Server Response");
                    if (result.status == "Pending") {
                        tableTasksPending.ajax.reload();
                    }
                    else if (result.status == "In Progress") {
                        tableTasksInProgress.ajax.reload();
                    }
                    else if (result.status == "Suspend") {
                        tableTasksSuspend.ajax.reload();
                    }
                    $('#jobModalEdit').modal('hide');
                },
                error: function (errormessage) {
                    toastr.error("This job is already exists.", "Server Response");
                }
            });
        }
    }
}

function onJobEdit(id) {
    $('#jobSubjectUpdate').css('border-color', '#cccccc');
    $('#jobDeadlineUpdate').css('border-color', '#cccccc');
    $('#jobStartDateUpdate').css('border-color', '#cccccc');

    $.ajax({
        url: "/api/jobs/" + id,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            $('#jobIdUpdate').val(result[0].id);
            $('#jobDepartmentUpdate').val(result[0].departmentId);
            $('#jobSubjectUpdate').val(result[0].subject);
            $('#jobDescriptionUpdate').val(result[0].description);
            $('#jobStartDateUpdate').val(moment(result[0].startDate).format('MM/DD/YYYY HH:mm A'));
            $('#jobDeadlineUpdate').val(moment(result[0].endDate).format('MM/DD/YYYY HH:mm A'));
            $('#jobModalEdit').modal('show');
        },
        error: function (errormessage) {
            toastr.error("Something unexpected happen.", "Server Response");
        }
    });
    return false;
}
 
function GetCurrentDate() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    var time = new Date($.now());

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }

    return moment(time).format("MM/DD/YYYY HH:mm A");
}

function GetCurrentDateEnd() {
    var time = new Date($.now());
    var hour = time.getHours();
    time.setHours(hour + 1);

    return moment(time).format("MM/DD/YYYY HH:mm A");
}

function getCurrentDateYYDDMM() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }

    return yyyy + '-' + mm + '-' + dd;
}

function formatDateMMDDYY(parDate) {
    var date = new Date(parDate);
    var dd = date.getDate();
    var mm = date.getMonth() + 1;
    var yyyy = date.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    return mm + '/' + dd + '/' + yyyy;
}

$('#jobStartDateUpdate, #jobDeadlineUpdate').datetimepicker({
    format: 'MM/DD/YYYY HH:mm A'
});

$('#jobFilterStartDate, #jobFilterEndDate').datetimepicker({
    format: 'MM/DD/YYYY'
});