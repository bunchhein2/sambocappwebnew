﻿var tablePrinciple = [];

function onGetPrincipleTab() {
    tablePrinciple = $('#principleTable').DataTable({
        ajax: {
            url: "/api/principles",
            dataSrc: ""
        },
        columns: [
            {
                data: function (data) {
                    return "<a href='#' onclick='PrincipleEdit(" + data.id + ")' style='text-decoration: none;'>" + data.objective + "</a>";
                }
            },
            {
                data: "id",
                render: function (data) {
                    return "<a href='/administrative-format/principle=" + data + "' class='btn btn-primary btn-xs' style='border-width: 0px; width: 70px; margin-right: 5px;'><span class='glyphicon glyphicon-search'></span> Display</a>" + "<button onclick='PrincipleDelete(" + data + ")' class='btn btn-danger btn-xs' style='border-width: 0px;'><span class='glyphicon glyphicon-trash'></span> Delete</button>";
                },
                "width": "130px"
            }
        ],
        destroy: true,
        "order": [[0, "desc"]],
        "info": false
    });
}

$('#btnSavePrinciple').click(function () {
    if (principleValidation() == false) {
        $('#objectivePrinciple').css('border-color', 'red');
        $('#objectivePrinciple').focus();
        return;
    }
    $.ajax({
        url: "/api/principles",
        data: JSON.stringify(setPrincipleObject()),
        type: "POST",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            toastr.success("Save toPrinciple database successfully.", "Server Response");
            tablePrinciple.ajax.reload();
            $('#principleModal').modal('hide');
        },
        error: function (errormessage) {
            toastr.error("This principle is already exists.", "Server Response");
        }
    });
})

$('#btnUpdatePrinciple').click(function () {
    if (principleValidationEdit() == false) {
        $('#objectivePrincipleEdit').css('border-color', 'red');
        $('#objectivePrincipleEdit').focus();
        return;
    }
    $.ajax({
        url: "/api/principles/" + $('#principleIdEdit').val(),
        data: JSON.stringify(setPrincipleObjectEdit()),
        type: "PUT",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            toastr.success("Save toPrinciple database successfully.", "Server Response");
            tablePrinciple.ajax.reload();
            $('#principleEditModal').modal('hide');
        },
        error: function (errormessage) {
            toastr.error("This principle is already exists.", "Server Response");
        }
    });
})

function PrincipleEdit(id) {
    $.ajax({
        url: "/api/principles/" + id,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            $('#principleIdEdit').val(result.id);
            $('#fromPrincipleEdit').val(result.civilServantFromId).trigger('change');
            $('#toPrincipleEdit').val(result.civilServantToId).trigger('change');
            $('#objectivePrincipleEdit').val(result.objective);
            $('#viaPrincipleEdit').val(result.via);
            $('#contentPrincipleEdit').val(result.content);
            $('#referencePrincipleEdit').val(result.reference);
            $('#locationPrincipleEdit').val(result.location);
            $('#principleEditModal').modal('show');
        },
        error: function (errormessage) {
            toastr.error("Something unexpected happen.", "Server Response");
        }
    });
    return false;
}

function PrincipleDelete(id) {
    bootbox.confirm({
        title: "Confirmation",
        message: "Are you sure you want toPrinciple delete this?",
        buttons: {
            cancel: {
                label: "Cancel",
                className: "btn-default"
            },
            confirm: {
                label: "Delete",
                className: "btn-danger"
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: "/api/principles/" + id,
                    method: "DELETE",
                    success: function () {
                        tablePrinciple.ajax.reload();
                        toastr.success("Deleted successfully.", "Server Response");
                    },
                    error: function () {
                        toastr.error("This principle is being used, cannot delete this.", "Server Response");
                    }
                });
            }
        }
    });
}

function setPrincipleObject() {
    var data = {
        Id: $('#principleId').val(),
        Objective: $('#objectivePrinciple').val(),
        Via: $('#viaPrinciple').val(),
        Content: $('#contentPrinciple').val(),
        Location: $('#locationPrinciple').val(),
        Reference: $('#referencePrinciple').val(),
        CivilServantFromId: $('#fromPrinciple').val(),
        CivilServantToId: $('#toPrinciple').val()
    };
    return data;
}

function setPrincipleObjectEdit() {
    var data = {
        Id: $('#principleIdEdit').val(),
        Objective: $('#objectivePrincipleEdit').val(),
        Via: $('#viaPrincipleEdit').val(),
        Content: $('#contentPrincipleEdit').val(),
        Location: $('#locationPrincipleEdit').val(),
        Reference: $('#referencePrincipleEdit').val(),
        CivilServantFromId: $('#fromPrincipleEdit').val(),
        CivilServantToId: $('#toPrincipleEdit').val()
    };
    return data;
}

function principleValidation() {
    var objectivePrinciple = $('#objectivePrinciple').val();
    return (objectivePrinciple == "") ? false : true;
}

function principleValidationEdit() {
    var objectivePrincipleEdit = $('#objectivePrincipleEdit').val();
    return (objectivePrincipleEdit == "") ? false : true;
}

function clearInput() {
    $('#principleId').val('');
    $("#fromPrinciple").val($("#fromPrinciple option:first").val()).trigger('change');
    $("#toPrinciple").val($("#toPrinciple option:first").val());
    $('#objectivePrinciple').val('');
    $('#contentPrinciple').val('');
    $('#viaPrinciple').val('');
    $('#referencePrinciple').val('');
    $('#locationPrinciple').val('');
    $('#objectivePrinciple').css('border-color', '#dce4ec');
}

$("#fromPrinciple, #toPrinciple").select2({
    dropdownParent: $('#principleModal .modal-content'),
    placeholder: "--- Please Select ---",
    allowClear: true
});

$("#fromPrincipleEdit, #toPrincipleEdit").select2({
    dropdownParent: $('#principleEditModal .modal-content'),
    placeholder: "--- Please Select ---",
    allowClear: true
});