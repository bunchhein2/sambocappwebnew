﻿
$(document).ready(function () {
    $("#privacyModel").on('show.modal.bs', function () {
        //tinymce.init({
        //    selector: '#mytextarea',
            
        //    plugins: [
        //        'a11ychecker', 'advlist', 'advcode', 'advtable', 'autolink', 'checklist', 'export',
        //        'lists', 'link', 'image', 'charmap', 'preview', 'anchor', 'searchreplace', 'visualblocks',
        //        'powerpaste', 'fullscreen', 'formatpainter', 'insertdatetime', 'media', 'table', 'help', 'wordcount'
        //    ],
        //    toolbar: 'undo redo | formatpainter casechange blocks | bold italic backcolor | ' +
        //        'alignleft aligncenter alignright alignjustify | ' +
        //        'bullist numlist checklist outdent indent | removeformat | a11ycheck code table help'
        //});
        
    });
    GetPrivacy();
    
});
//function get(id) {

//    $.ajax({
//        url: "/api/privacys/" + id,
//        type: "GET",
//        contentType: "application/json;charset=UTF-8",
//        dataType: "json",
//        success: function (result) {
//            $('#descriptiona').text(result.description);
//            $("#privacyModel").modal('show');
//        },

//    });
//}
var tableCase = [];

function GetPrivacy(id) {
    tableCase = $('#privacyTable').DataTable({
        ajax: {
            url: "/api/Privacys",
            dataSrc: ""
        },
        columns: [
            {

                data: "id",
                render: function (data) {

                    return "" + ("").slice(-4);
                }
            },
            {

                data: "description"
            },
            {
                data: "id",
                render: function (data) {
                    return "<button onclick='PrivacyEdit(" + data + ")' class='btn btn-warning btn-xs' style='border-width: 0px; width: 65px; margin-right: 5px;'><span class='glyphicon glyphicon-edit'></span> Edit</button>" + "<button onclick='PrivacyDelete(" + data + ")' class='btn btn-danger btn-xs' style='border-width: 0px;'><span class='glyphicon glyphicon-trash'></span> Delete</button>";
                },
                "width": "130px"
            }
        ],
        destroy: true,
        "order": [[0, "desc"]],
        "info": false,
        "paging": false,
        
    });

}
    function PrivacyAction() {
        var action = '';
        action = document.getElementById('btnPrivacy').innerText;
        if (action === "Save") {
            if ($('#privacydescription').val().trim() === "") {
                $('#privacydescription').css('border-color', 'red');
                $('#privacydescription').focus();
                toastr.info("Please enter Privacy", "Required");
            }
            else {
                $('#privacydescription').css('border-color', '#cccccc');

                var data = {
                    description: $('#privacydescription').val()
                };
                $.ajax({
                    url: "/api/privacys",
                    data: JSON.stringify(data),
                    type: "POST",
                    contentType: "application/json;charset=utf-8",
                    dataType: "json",
                    success: function (result) {
                        toastr.success("Save to database successfully.", "Server Response");
                        tableCase.ajax.reload();
                        document.getElementById('privacydescription').disabled = true;
                        document.getElementById('btnPrivacy').innerHTML = "Add New";
                        $('#privacydescription').val('');
                        $("#privacyModel").modal('hide');
                    },
                    error: function (errormessage) {
                        toastr.error("This privacy is error.", "Server Response");
                    }
                });
            }
        }
        else if (action === "Add New") {
            document.getElementById('privacydescription').disabled = false;
            document.getElementById('btnPrivacy').innerHTML = "Save";
            $('#privacydescription').val('');
            $('#privacydescription').focus();
        }
        else if (action === "Update") {
            $('#privacydescription').css('border-color', '#cccccc');

            var data = {
                id: $('#privacyid').val(),
                description: $('#privacydescription').val()
            };
            $.ajax({
                url: "/api/privacys/" + data.id,
                data: JSON.stringify(data),
                type: "PUT",
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                success: function (result) {
                    toastr.success("Updated successfully.", "Server Response");
                    tableCase.ajax.reload();
                    document.getElementById('privacydescription').disabled = true;
                    document.getElementById('btnPrivacy').innerHTML = "Add New";
                    $('#privacydescription').val('');
                    $("#privacyModel").modal('hide');
                },
                error: function (errormessage) {
                    toastr.error("This privacy cannot update.", "Server Response");
                }
            });
        }
    }

    function PrivacyEdit(id) {

        $('#privacydescription').css('border-color', '#cccccc');

        $.ajax({
            url: "/api/privacys/" + id,
            type: "GET",
            contentType: "application/json;charset=UTF-8",
            dataType: "json",
            success: function (result) {
                $('#privacyid').val(result.id);
                $('#privacydescription').val(result.description);
                $('#tinymce').text(result.description);
                //$('#descriptiona').text(result.description);
                document.getElementById('btnPrivacy').innerHTML = "Update";
                document.getElementById('privacydescription').disabled = false;
                $("#privacyModel").modal('show');
                $('#privacydescription').focus();

            },
            error: function (errormessage) {
                toastr.error("Something unexpected happen.", "Server Response");
            }
        });
        return false;
    }

function PrivacyDelete(id) {
    //alert(id);
    bootbox.confirm({
        title: "Confirmation",
        message: "Are you sure you want to delete this?",
        buttons: {
            cancel: {
                label: "Cancel",
                className: "btn-default"
            },
            confirm: {
                label: "Delete",
                className: "btn-danger"
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: "/api/privacys/" + id,
                    method: "DELETE",
                    success: function () {
                        tableCase.ajax.reload();
                        toastr.success("Deleted successfully.", "Server Response");
                    },
                    error: function () {
                        toastr.error("This case is being used, cannot delete this.", "Server Response");
                    }
                });
            }
        }
    });
}
