﻿$(document).ready(function () {
    $('#propertyModal').on('show.bs.modal', function () {
        GetProperty();
        DisableControlProper();
    });
});

var propertyTable = [];
function GetProperty() {
    propertyTable = $('#propertytbl').dataTable({
        ajax: {
            url: "/api/Propertys",
            dataSrc: ""
        },
        columns:
            [
                {
                    data: "id"
                },
                {
                    data: "property"
                },
                {
                    data: "id",
                    render: function (data) {
                        return "<button OnClick='PropertyEdit (" + data + ")' class='btn btn-primary btn-xs' style='margin-right:5px'>Edit</button>" +
                            "<button OnClick='PropertyDelete (" + data + ")' class='btn btn-default btn-xs' >Delete</button>";
                    }
                }
            ],
        destroy: true,
        "order": [0, "desc"],
        "info": false

    });
}
function PropertyAction() {
    var action = '';
    action = document.getElementById('btnProperty').innerText;
    //alert(action);

    if (action == "Add New") {
        document.getElementById('btnProperty').innerText = 'Save';
        EnableControlProper();
        ClearControlProper();
        $('#propertyName').focus();
    }
    else if (action == "Save") {
        if ($('#propertyName').val().trim() == "") {
            $('#propertyName').css('border-color', 'red');
            $('#propertyName').focus();
            toastr.info("Please Input Property Name", "Server Respon")
        }
        else {
            $('#propertyName').css('border-color', '#cccccc');
            var data = {
                property: $('#propertyName').val(),
                note: $('#note').val(),
            };
            $.ajax({
                url: "/api/Propertys",
                data: JSON.stringify(data),
                type: "POST",
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                success: function (result) {
                    toastr.success("New Data has been Created", "Server Respond");
                    propertyTable.DataTable().ajax.reload();
                    document.getElementById('btnProperty').innerText = 'Add New';
                    ClearControlProper();
                    DisableControlProper();
                },
                error: function (errormesage) {
                    toastr.error("This Name is exist in Database", "Server Respond")
                }
            });
        }
    } else if (action == "Update") {
        if ($('#propertyName').val().trim() == "") {
            $('#propertyName').css('border-color', 'red');
            $('#propertyName').focus();
            toastr.info("Please Input Property Name", "Server Respon")
        }
        else {
            $('#propertyName').css('border-color', '#cccccc');
            var data = {
                id: $('#ID').val(),
                property: $('#propertyName').val(),
                note: $('#note').val(),
            };
            $.ajax({
                url: "/api/Propertys/" + data.id,
                data: JSON.stringify(data),
                type: "PUT",
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                success: function (result) {
                    toastr.success("Data has been Updated", "Server Respond");
                    propertyTable.DataTable().ajax.reload();
                    document.getElementById('btnProperty').innerText = 'Add New';
                    ClearControlProper();
                    DisableControlProper();
                },
                error: function (errormesage) {
                    toastr.error("Data hasn't Updated in Database", "Server Respond")
                }
            });
        }
    }
}

//Delete
function PropertyDelete(id) {
    bootbox.confirm({
        title: "",
        message: "Are you sure want to delete this?",
        button: {
            cancel: {
                label: "Cancel",
                ClassName: "btn-default",
            },
            confirm: {
                label: "Delete",
                ClassName: "btn-danger"
            }
        },
        callback: function (result) {
            //alert(id);
            if (result) {
                $.ajax({
                    url: "/api/Propertys/" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        propertyTable.DataTable().ajax.reload();
                        toastr.success("Data Deleted successfully!", "Service Response");
                    },
                    error: function (errormessage) {
                        toastr.error("This Data hasn't deleted in Database", "Service Response");
                    }
                });
            }
        }
    });
}

//Edit
function PropertyEdit(id) {
    $.ajax({
        url: "/api/Propertys/" + id,
        type: "Get",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            $('#note').val(result.note);
            $('#propertyName').val(result.property);
            $('#ID').val(result.id);

            document.getElementById('btnProperty').innerText = 'Update';
            $('#propertyName').focus();

        },
        error: function (errormesage) {
            toastr.error("Unexpacted ", "Server Respond")
        }
    });
}
function DisableControlProper() {
    document.getElementById('propertyName').disable = true;
    document.getElementById('note').disable = true;
}
function EnableControlProper() {
    document.getElementById('propertyName').disable = false;
    document.getElementById('note').disable = false;
}
function ClearControlProper() {
    $('#propertyName').val('');
    $('#note').val('');
}
function PropertyModalAction() {
    document.getElementById('btnProperty').innerText = 'Add New';
    DisableControlProper();
    ClearControlProper();
}
