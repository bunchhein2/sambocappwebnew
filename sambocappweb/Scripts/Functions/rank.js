﻿$(document).ready(function () {
    $(document).ajaxStart(function () {
        Pace.start();
        $("div#divLoadingModal").addClass('show');
    }).ajaxStop(function () {
        Pace.stop();
        $("div#divLoadingModal").removeClass('show');
    });

    $('#rankModal').on('show.bs.modal', function () {
        GetRanks();
        document.getElementById('rankName').disabled = true;
        document.getElementById('ranking').disabled = true;
        document.getElementById('btnRankAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
        $('#rankName').val('');
    });
});

var tableRank = [];

function GetRanks() {
    tableRank = $('#rankTable').DataTable({
        ajax: {
            url: "/api/ranks",
            dataSrc: ""
        },
        columns: [
            {
                data: "name"
            },
            {
                data: "ranking"
            },
            {
                data: "id",
                render: function (data) {
                    return "<button onclick='RankEdit(" + data + ")' class='btn btn-warning btn-xs' style='border-width: 0px; width: 65px; margin-right: 5px;'><span class='glyphicon glyphicon-edit'></span> Edit</button>" + "<button onclick='RankDelete(" + data + ")' class='btn btn-danger btn-xs' style='border-width: 0px;'><span class='glyphicon glyphicon-trash'></span> Delete</button>";
                },
                "width": "130px"
            }
        ],
        destroy: true,
        "order": [[1, "asc"]],
        "info": false
    });
}

function onRankAction() {
    var action = '';
    action = document.getElementById('btnRankAction').innerText;
    if (action === " រក្សាទុក / Save") {
        if ($('#rankName').val().trim() === "") {
            $('#rankName').css('border-color', 'red');
            $('#rankName').focus();
            toastr.info("Please enter department's name", "Required");
        }
        else {
            $('#rankName').css('border-color', '#cccccc');

            var data = {
                Name: $('#rankName').val(),
                Ranking: $('#ranking').val()
            };
            $.ajax({
                url: "/api/ranks",
                data: JSON.stringify(data),
                type: "POST",
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                success: function (result) {
                    toastr.success("Save to database successfully.", "Server Response");
                    tableRank.ajax.reload();
                    document.getElementById('rankName').disabled = true;
                    document.getElementById('ranking').disabled = true;
                    document.getElementById('btnRankAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                    $('#rankName').val('');
                },
                error: function (errormessage) {
                    toastr.error("This rank is already exists.", "Server Response");
                    document.getElementById('rankName').disabled = true;
                    document.getElementById('ranking').disabled = true;
                    document.getElementById('btnRankAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                    $('#rankName').val('');
                }
            });
        }
    }
    else if (action === " បង្កើតថ្មី / Add New") {
        document.getElementById('rankName').disabled = false;
        document.getElementById('ranking').disabled = false;
        document.getElementById('btnRankAction').innerHTML = "<span class='glyphicon glyphicon-floppy-save'></span> <span class='kh'>រក្សាទុក</span> / Save";
        $('#rankName').val('');
        $('#rankName').focus();
    }
    else if (action === " កែប្រែ / Update") {
        $('#rankName').css('border-color', '#cccccc');

        var data = {
            Id: $('#rankId').val(),
            Name: $('#rankName').val(),
            Ranking: $('#ranking').val()
        };
        $.ajax({
            url: "/api/ranks/" + data.Id,
            data: JSON.stringify(data),
            type: "PUT",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                toastr.success("Updated successfully.", "Server Response");
                tableRank.ajax.reload();
                document.getElementById('rankName').disabled = true;
                document.getElementById('ranking').disabled = true;
                document.getElementById('btnRankAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                $('#rankName').val('');
            },
            error: function (errormessage) {
                toastr.error("This rank is already exists.", "Server Response");
                document.getElementById('rankName').disabled = true;
                document.getElementById('ranking').disabled = true;
                document.getElementById('btnRankAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                $('#rankName').val('');
            }
        });
    }
}

function RankEdit(id) {

    $('#rankName').css('border-color', '#cccccc');

    $.ajax({
        url: "/api/ranks/" + id,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            $('#rankId').val(result.id);
            $('#rankName').val(result.name);
            $('#ranking').val(result.ranking);
            document.getElementById('btnRankAction').innerHTML = "<span class='glyphicon glyphicon-saved'></span> <span class='kh'>កែប្រែ</span> / Update";
            document.getElementById('rankName').disabled = false;
            document.getElementById('ranking').disabled = false;
            $('#rankName').focus();
        },
        error: function (errormessage) {
            toastr.error("Something unexpected happen.", "Server Response");
        }
    });
    return false;
}

function RankDelete(id) {
    bootbox.confirm({
        title: "Confirmation",
        message: "Are you sure you want to delete this?",
        buttons: {
            cancel: {
                label: "Cancel",
                className: "btn-default"
            },
            confirm: {
                label: "Delete",
                className: "btn-danger"
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: "/api/ranks/" + id,
                    method: "DELETE",
                    success: function () {
                        tableRank.ajax.reload();
                        toastr.success("Deleted successfully.", "Server Response");
                    },
                    error: function () {
                        toastr.error("This rank is being used, cannot delete this.", "Server Response");
                    }
                });
            }
        }
    });
}