﻿var tableReport = [];

function onGetReportTab() {
    tableReport = $('#reportTable').DataTable({
        ajax: {
            url: "/api/reports",
            dataSrc: ""
        },
        columns: [
            {
                data: function (data) {
                    return "<span class='glyphicon glyphicon-eyes'></span> <a href='#' onclick='ReportEdit(" + data.id + ")' style='text-decoration: none;'>" + data.objective + "</a>";
                }
            },
            {
                data: "id",
                render: function (data) {
                    return "<a href='/administrative-format/report=" + data + "' class='btn btn-primary btn-xs' style='border-width: 0px; width: 70px; margin-right: 5px;'><span class='glyphicon glyphicon-search'></span> Display</a>" + "<button onclick='ReportDelete(" + data + ")' class='btn btn-danger btn-xs' style='border-width: 0px;'><span class='glyphicon glyphicon-trash'></span> Delete</button>";
                },
                "width": "130px"
            }
        ],
        destroy: true,
        "order": [[0, "desc"]],
        "info": false
    });
}

$('#btnSaveReport').click(function () {
    if (reportValidation() == false) {
        $('#objective').css('border-color', 'red');
        $('#objective').focus();
        return;
    }
    $.ajax({
        url: "/api/reports",
        data: JSON.stringify(setReportObject()),
        type: "POST",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            toastr.success("Save to database successfully.", "Server Response");
            tableReport.ajax.reload();
            $('#reportModal').modal('hide');
        },
        error: function (errormessage) {
            toastr.error("This report is already exists.", "Server Response");
        }
    });
})

$('#btnUpdateReport').click(function () {
    if (reportValidationEdit() == false) {
        $('#objectiveEdit').css('border-color', 'red');
        $('#objectiveEdit').focus();
        return;
    }
    $.ajax({
        url: "/api/reports/" + $('#reportIdEdit').val(),
        data: JSON.stringify(setReportObjectEdit()),
        type: "PUT",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            toastr.success("Save to database successfully.", "Server Response");
            tableReport.ajax.reload();
            $('#reportEditModal').modal('hide');
        },
        error: function (errormessage) {
            toastr.error("This report is already exists.", "Server Response");
        }
    });
})

function ReportEdit(id) {
    $.ajax({
        url: "/api/reports/" + id,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            $('#reportIdEdit').val(result.id);
            $('#fromEdit').val(result.civilServantFromId).trigger('change');
            $('#toEdit').val(result.civilServantToId).trigger('change');
            $('#reportTypeIdsEdit').val(result.reportTypeId).trigger('change');
            $('#objectiveEdit').val(result.objective);
            $('#viaEdit').val(result.via);
            $('#contentEdit').val(result.content);
            $('#referenceEdit').val(result.reference);
            $('#locationEdit').val(result.location);
            $('#durationEdit').val(result.durations);
            $('#reportStartDateEdit').val(moment(result.startDate).format('MM/DD/YYYY'));
            $('#reportEndDateEdit').val(moment(result.endDate).format('MM/DD/YYYY'));
            var duration = $('#durationEdit').val();
            if (duration > 1) {
                $("#reportEndDateEdit").show();
                $("#reportEndDateLabelEdit").show();
            }
            else {
                $("#reportEndDateEdit").hide();
                $("#reportEndDateLabelEdit").hide();
            }
            $('#reportEditModal').modal('show');
        },
        error: function (errormessage) {
            toastr.error("Something unexpected happen.", "Server Response");
        }
    });
    return false;
}

function ReportDelete(id) {
    bootbox.confirm({
        title: "Confirmation",
        message: "Are you sure you want to delete this?",
        buttons: {
            cancel: {
                label: "Cancel",
                className: "btn-default"
            },
            confirm: {
                label: "Delete",
                className: "btn-danger"
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: "/api/reports/" + id,
                    method: "DELETE",
                    success: function () {
                        tableReport.ajax.reload();
                        toastr.success("Deleted successfully.", "Server Response");
                    },
                    error: function () {
                        toastr.error("This job is being used, cannot delete this.", "Server Response");
                    }
                });
            }
        }
    });
}

function setReportObject() {
    var data = {
        Id: $('#reportId').val(),
        ReportTypeId: $('#reportTypeIds').val(),
        Objective: $('#objective').val(),
        Via: $('#via').val(),
        Content: $('#content').val(),
        Durations: $('#duration').val(),
        StartDate: $('#reportStartDate').val(),
        EndDate: $('#reportEndDate').val(),
        Location: $('#location').val(),
        Reference: $('#reference').val(),
        CivilServantFromId: $('#from').val(),
        CivilServantToId: $('#to').val()
    };
    return data;
}

function setReportObjectEdit() {
    var data = {
        Id: $('#reportIdEdit').val(),
        ReportTypeId: $('#reportTypeIdsEdit').val(),
        Objective: $('#objectiveEdit').val(),
        Via: $('#viaEdit').val(),
        Content: $('#contentEdit').val(),
        Durations: $('#durationEdit').val(),
        StartDate: $('#reportStartDateEdit').val(),
        EndDate: $('#reportEndDateEdit').val(),
        Location: $('#locationEdit').val(),
        Reference: $('#referenceEdit').val(),
        CivilServantFromId: $('#fromEdit').val(),
        CivilServantToId: $('#toEdit').val()
    };
    return data;
}

function reportValidation() {
    var objective = $('#objective').val();
    return (objective == "") ? false : true;
}

function reportValidationEdit() {
    var objectiveEdit = $('#objectiveEdit').val();
    return (objectiveEdit == "") ? false : true;
}

function clearInput() {
    $('#reportId').val('');
    $("#from").val($("#from option:first").val()).trigger('change');
    $("#to").val($("#to option:first").val());
    $("#reportTypeIds").val($("#reportTypeIds option:first").val());
    $('#objective').val('');
    $('#content').val('');
    $('#via').val('');
    $('#reference').val('');
    $('#location').val('');
    $('#duration').val(1);
    $('#reportStartDate').val(moment(new Date()).format("MM/DD/YYYY"));
    $('#reportEndDate').val(moment(new Date()).format("MM/DD/YYYY"));
    $('#objective').css('border-color', '#dce4ec');
}

$('input[name=duration]').change(function () {
    var duration = $(this).val();
    if (duration > 1) {
        $("#reportEndDate").show();
        $("#reportEndDateLabel").show();
        var date = new Date($("#reportStartDate").val());
        date.setTime(date.getTime() + (duration - 1) * 86400000);
        $("#reportEndDate").val(moment(date).format("MM/DD/YYYY"));
    }
    else {
        $("#reportEndDate").hide();
        $("#reportEndDateLabel").hide();
    }
});

$('input[name=durationEdit]').change(function () {
    var duration = $(this).val();
    if (duration > 1) {
        $("#reportEndDateEdit").show();
        $("#reportEndDateLabelEdit").show();
        var date = new Date($("#reportStartDateEdit").val());
        date.setTime(date.getTime() + (duration - 1) * 86400000);
        $("#reportEndDateEdit").val(moment(date).format("MM/DD/YYYY"));
    }
    else {
        $("#reportEndDateEdit").hide();
        $("#reportEndDateLabelEdit").hide();
    }
});

$('#reportStartDate, #reportEndDate, #reportStartDateEdit, #reportEndDateEdit').datetimepicker({
    format: 'MM/DD/YYYY',
    defaultDate: new Date()
});

$("#from, #to, #reportTypeIds").select2({
    dropdownParent: $('#reportModal .modal-content'),
    placeholder: "--- Please Select ---",
    allowClear: true
});

$("#fromEdit, #toEdit, #reportTypeIdsEdit").select2({
    dropdownParent: $('#reportEditModal .modal-content'),
    placeholder: "--- Please Select ---",
    allowClear: true
});
