﻿$(document).ready(function () {
    $(document).ajaxStart(function () {
        Pace.start();
        $("div#divLoadingModal").addClass('show');
    }).ajaxStop(function () {
        Pace.stop();
        $("div#divLoadingModal").removeClass('show');
    });

    $('#reportTypeModal').on('show.bs.modal', function () {
        GetReportTypes();
        document.getElementById('reportType').disabled = true;
        document.getElementById('btnReportTypeAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
        $('#reportType').val('');
    });
});

var tableReportType = [];

function GetReportTypes() {
    tableReportType = $('#reportTypeTable').DataTable({
        ajax: {
            url: "/api/reporttypes",
            dataSrc: ""
        },
        columns: [
            {
                data: "name"
            },
            {
                data: "id",
                render: function (data) {
                    return "<button onclick='ReportTypeEdit(" + data + ")' class='btn btn-warning btn-xs' style='border-width: 0px; width: 65px; margin-right: 5px;'><span class='glyphicon glyphicon-edit'></span> Edit</button>" + "<button onclick='ReportTypeDelete(" + data + ")' class='btn btn-danger btn-xs' style='border-width: 0px;'><span class='glyphicon glyphicon-trash'></span> Delete</button>";
                },
                "width": "130px"
            }
        ],
        destroy: true,
        "order": [[0, "desc"]],
        "info": false
    });
}

function onReportTypeAction() {
    var action = '';
    action = document.getElementById('btnReportTypeAction').innerText;
    if (action === " រក្សាទុក / Save") {
        if ($('#reportType').val().trim() === "") {
            $('#reportType').css('border-color', 'red');
            $('#reportType').focus();
            toastr.info("Please enter report type's name", "Required");
        }
        else {
            $('#reportType').css('border-color', '#cccccc');

            var data = {
                Name: $('#reportType').val()
            };
            $.ajax({
                url: "/api/reporttypes",
                data: JSON.stringify(data),
                type: "POST",
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                success: function (result) {
                    toastr.success("Save to database successfully.", "Server Response");
                    tableReportType.ajax.reload();
                    document.getElementById('reportType').disabled = true;
                    document.getElementById('btnReportTypeAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                    $('#reportType').val('');
                },
                error: function (errormessage) {
                    toastr.error("This report type is already exists.", "Server Response");
                    document.getElementById('reportType').disabled = true;
                    document.getElementById('btnReportTypeAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                    $('#reportType').val('');
                }
            });
        }
    }
    else if (action === " បង្កើតថ្មី / Add New") {
        document.getElementById('reportType').disabled = false;
        document.getElementById('btnReportTypeAction').innerHTML = "<span class='glyphicon glyphicon-floppy-save'></span> <span class='kh'>រក្សាទុក</span> / Save";
        $('#reportType').val('');
        $('#reportType').focus();
    }
    else if (action === " កែប្រែ / Update") {
        $('#reportType').css('border-color', '#cccccc');

        var data = {
            Id: $('#reportTypeId').val(),
            Name: $('#reportType').val()
        };
        $.ajax({
            url: "/api/reporttypes/" + data.Id,
            data: JSON.stringify(data),
            type: "PUT",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                toastr.success("Updated successfully.", "Server Response");
                tableReportType.ajax.reload();
                document.getElementById('reportType').disabled = true;
                document.getElementById('btnReportTypeAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                $('#reportType').val('');
            },
            error: function (errormessage) {
                toastr.error("This report type is already exists.", "Server Response");
                document.getElementById('reportType').disabled = true;
                document.getElementById('btnReportTypeAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                $('#reportType').val('');
            }
        });
    }
}

function ReportTypeEdit(id) {

    $('#reportType').css('border-color', '#cccccc');

    $.ajax({
        url: "/api/reporttypes/" + id,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            $('#reportTypeId').val(result.id);
            $('#reportType').val(result.name);
            document.getElementById('btnReportTypeAction').innerHTML = "<span class='glyphicon glyphicon-saved'></span> <span class='kh'>កែប្រែ</span> / Update";
            document.getElementById('reportType').disabled = false;
            $('#reportType').focus();
        },
        error: function (errormessage) {
            toastr.error("Something unexpected happen.", "Server Response");
        }
    });
    return false;
}

function ReportTypeDelete(id) {
    bootbox.confirm({
        title: "Confirmation",
        message: "Are you sure you want to delete this?",
        buttons: {
            cancel: {
                label: "Cancel",
                className: "btn-default"
            },
            confirm: {
                label: "Delete",
                className: "btn-danger"
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: "/api/reporttypes/" + id,
                    method: "DELETE",
                    success: function () {
                        tableReportType.ajax.reload();
                        toastr.success("Deleted successfully.", "Server Response");
                    },
                    error: function () {
                        toastr.error("This report type is being used, cannot delete this.", "Server Response");
                    }
                });
            }
        }
    });
}