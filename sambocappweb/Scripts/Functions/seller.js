﻿$(document).ready(function () {
    //$(document).ajaxStart(function () {
    //    Pace.start();
    //    $("div#divLoadingModal").addClass('show');
    //}).ajaxStop(function () {
    //    Pace.stop();
    //    $("div#divLoadingModal").removeClass('show');
    //}); 

    $('#SellerModal').on('show.bs.modal', function () {
        GetSellers();
        DisableControlSeller();
        ClearControlSeller();
    });
});

var tableCase = [];

//function onPopulateSeller() {
//    $.ajax({
//        url: "/api/Sellers?selectoption=true",
//        type: "GET",
//        contentType: "application/json;charset=UTF-8",
//        dataType: "json",
//        success: function (data) {
//            console.log(data);
//            $("#SellerId").select2({
//                data: data,
//                dropdownParent: $("#SellerModal")
//            });
//        },
//        error: function (errormessage) {
//            toastr.error("Something unexpected happen.", "Server Response");
//        }
//    });
//}

//function onPopulateCaseProccedingSeller() {
//    $.ajax({
//        url: "/api/Sellers?selectoption=true",
//        type: "GET",
//        contentType: "application/json;charset=UTF-8",
//        dataType: "json",
//        success: function (data) {
//            console.log(data);
//            $("#zcaseprocedingSellerid").select2({
//                data: data,
//                dropdownParent: $("#civilcaseModal")
//            });
//        },
//        error: function (errormessage) {
//            toastr.error("Something unexpected happen.", "Server Response");
//        }
//    });
//}

//function onPopulateCaseSeller() {
//    $.ajax({
//        url: "/api/Sellers?selectoption=true",
//        type: "GET",
//        contentType: "application/json;charset=UTF-8",
//        dataType: "json",
//        success: function (data) {
//            console.log(data);
//            $("#zcaseSellerids").select2({
//                data: data,
//                dropdownParent: $("#civilcaseModal")
//            });
//        },
//        error: function (errormessage) {
//            toastr.error("Something unexpected happen.", "Server Response");
//        }
//    });
//}



function GetSellers() {
    tableCase = $('#SellerTable').DataTable({
        ajax: {
            url: "/api/Sellers",
            dataSrc: ""
        },
        columns: [
            {
                data: "id"
            },
            {
                data: "sellerName"
            },
            {
                data: "sellerSex"
            },
            {
                data: "sellerAddress"
            },
            {
                data: "sellerIdentityno"
            },
            {
                data: "sellerPhone"
            },
            {
                data: "id",
                render: function (data) {
                    return "<button onclick='SellerEdit(" + data + ")' class='btn btn-warning btn-xs' style='border-width: 0px; width: 65px; margin-right: 5px;'><span class='glyphicon glyphicon-edit'></span> Edit</button>" + "<button onclick='SellerDelete(" + data + ")' class='btn btn-danger btn-xs' style='border-width: 0px;'><span class='glyphicon glyphicon-trash'></span> Delete</button>";
                },
                "width": "130px"
            }
        ],
        destroy: true,
        "order": [[0, "desc"]],
        "info": false
    });
}

function SellerEdit(id) {

    $('#SellerName').css('border-color', '#cccccc');

    $.ajax({
        url: "/api/Sellers/" + id,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            $('#SellerId').val(result.id);
            $('#SellerName').val(result.sellerName);
            $('#SellerSex').val(result.sellerSex);
            $('#SellerAge').val(result.sellerAge);
            $("#SellerPhone").val(result.sellerPhone);
            $("#SellerAddress").val(result.sellerAddress);
            $('#SellerIdentityno').val(result.sellerIdentityno);
            $('#Sellerfile_old').val(result.sellerPhoto);

            var dr = moment(result.sellerDob).format("YYYY-MM-DD");
            $('#sdob').val(dr);

            
            if (result.sellerPhoto == "") {
                $('#SellerPhoto').attr('src', '../Images/no_image.png');
            } else {
                //alert(result.img);
                $('#SellerPhoto').attr('src', '../Images/' + result.sellerPhoto);
            }
            EnableControlSeller();
            document.getElementById('btnSellerAction').innerHTML = "កែប្រែ​ / Update";
            $('#SellerModel').modal('show');
        },
        error: function (errormessage) {
            toastr.error("No Record Select!", "Service Response");
        }
    });
    return false;
}

function SellerAction() {
    var action = '';
    action = document.getElementById('btnSellerAction').innerText;
    if (action === "បង្កើតថ្មី​ / Add New") {
        document.getElementById('btnSellerAction').innerText = "រក្សាទុក / Save";
        EnableControlSeller();
        $('#SellerPhoto').attr('src', '../Images/no_image.png');
        $("#SellerName").focus();
        $('#sdob').val(moment().format('YYYY-MM-DD'));
        
    } else if (action == "រក្សាទុក / Save") {
        var data = new FormData();
        var files = $("#Sellerfile").get(0).files;
        if (files.length > 0) {
            data.append("Sellerphoto", files[0]);
        }

        data.append("sellerName", $("#SellerName").val());
        data.append("sellersex", $("#SellerSex").val());
        data.append("sellerage", $("#SellerAge").val());
        data.append("selleraddress", $("#SellerAddress").val());
        data.append("selleridentityno", $("#SellerIdentityno").val());
        data.append("sellerphone", $("#SellerPhone").val());
        data.append("sellerdob", $("#sdob").val());

        //console.log(files[0]);

        $.ajax({
            type: "POST",
            url: "/api/Sellers",
            contentType: false,
            processData: false,
            data: data,
            success: function (result) {
                toastr.success("Save to database successfully.", "Server Response");
                tableCase.ajax.reload();
                //onPopulateSeller();
                //onPopulateCaseSeller();
                //onPopulateCaseProccedingSeller();                document.getElementById('SellerName').disabled = true;
                DisableControlSeller();
                $('#SellerPhoto').val('');
                ClearControlSeller();
                document.getElementById('btnSellerAction').innerText = "បង្កើតថ្មី​ / Add New";
                
            },
            error: function (errormessage) {
                toastr.error("This Seller is already exists.", "Server Response");
            }
        });

        //maritalstatus

    } else if (action === "កែប្រែ​ / Update") {

        var data = new FormData();
        var files = $("#Sellerfile").get(0).files;
        if (files.length > 0) {
            data.append("SellerPhoto", files[0]);
        }
        data.append("id", $('#SellerId'));
        data.append("sellerName", $("#SellerName").val());
        data.append("sellerSex", $("#SellerSex").val());
        data.append("sellerAge", $("#SellerAge").val());
        data.append("sellerAddress", $("#SellerAddress").val());
        data.append("sellerDob", $("#sdob").val());
        data.append("sellerIdentityno", $("#SellerIdentityno").val());
        data.append("sellerPhone", $("#SellerPhone").val());
        //data.append("sellerfile_old", $("#Sellerfile_old").val());

        console.log(data);

        $.ajax({
            type: "PUT",
            url: "/api/Sellers/" + $('#SellerId').val(),
            contentType: false,
            processData: false,
            data: data,
            success: function (result) {
                toastr.success("Updated successfully.", "Server Response");
                tableCase.ajax.reload();
                //onPopulateSeller();
                //onPopulateCaseSeller();
                //onPopulateCaseProccedingSeller();
                DisableControlSeller();
                $('#SellerPhoto').val('');
                ClearControlSeller();
                document.getElementById('btnSellerAction').innerHTML = "បង្កើតថ្មី​ / Add New";
                
            },
            error: function (errormessage) {
                toastr.error("This Seller is already exists.", "Server Response");
            }
        });

    }
}

function SellerDelete(id) {
    bootbox.confirm({
        title: "Confirmation",
        message: "Are you sure you want to delete this?",
        buttons: {
            cancel: {
                label: "Cancel",
                className: "btn-default"
            },
            confirm: {
                label: "Delete",
                className: "btn-danger"
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: "/api/Sellers/" + id,
                    method: "DELETE",
                    success: function () {
                        tableCase.ajax.reload();
                        //onPopulateSeller();
                        //onPopulateCaseSeller();
                        //onPopulateCaseProccedingSeller();
                        toastr.success("Deleted successfully.", "Server Response");
                        ClearControlSeller();
                    },
                    error: function () {
                        toastr.error("This Seller is being used, cannot delete this.", "Server Response");
                    }
                });
            }
        }
    });
}

function readURLLSeller(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#SellerPhoto').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}


function DisableControlSeller() {
    document.getElementById('SellerName').disabled = true;
    document.getElementById('SellerSex').disabled = true;
    document.getElementById('SellerAge').disabled = true;
    document.getElementById('SellerAddress').disabled = true;
    document.getElementById('SellerIdentityno').disabled = true;
    document.getElementById('SellerPhone').disabled = true;
    document.getElementById('sdob').disabled = true;
    document.getElementById('Sellerfile').disabled = true;
    //document.getElementById('btnSellerAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";

}

function EnableControlSeller() {
    document.getElementById('SellerName').disabled = false;
    document.getElementById('SellerSex').disabled = false;
    document.getElementById('SellerAge').disabled = false;
    document.getElementById('SellerAddress').disabled = false;
    document.getElementById('SellerIdentityno').disabled = false;
    document.getElementById('SellerPhone').disabled = false;
    document.getElementById('sdob').disabled = false;
    document.getElementById('Sellerfile').disabled = false;
}

function ClearControlSeller() {
    $('#SellerName').val('');
    $('#SellerSex').val('');
    $('#SellerAge').val('');
    $('#SellerAddress').val('');
    $('#SellerIdentityno').val('');
    $('#SellerPhone').val('');
    $('#sdob').val('');
    $('#Sellerfile').val('');
    $('#SellerPhoto').attr('src', '../Images/no_image.png');

}

function NewSellerAction() {
    alert('hi');
    document.getElementById('btnSellerAction').innerText = "បង្កើតថ្មី​ / Add New";
    DisableControlSeller();
    ClearControlSeller();
}