﻿$(document).ready(function () {
   
    $(document).ajaxStart(function () {
        $('#loadingGif').addClass('show');
    }).ajaxStop(function () {
        $('#loadingGif').removeClass('show');
    });
    GetShop("all");
    GetPrivacy();
    GetPrivacys()
    var cleave = new Cleave('#phone', {
        phone: true,
        phoneRegionCode: 'KH'
    });
});


let generatbtn = document.getElementById("btCreateShop");
function hidcontract() {
    generatbtn.
    $("#SelectModelA").modal('show');
    $("#ContractModal").modal('hide');
    $("#contractshopModel").modal('hide');
}


let Register = document.getElementById("btnfilter_Register");
let filter = document.getElementById("btn_filter");
function FilterClickRigister() {
        var shopId = this.value;
        //alert(id);
        if (shopId == "---Select_Status---") {
            GetShop("all");
        } else {
            GetShop(shopId);
        }
    Register.style.display = "none";
    filter.style.display = "block";
    filter.style.display = "auto";
        
}
function FilterClick() {
    GetShop("all");
    filter.style.display = "none";
    Register.style.display = "block";
    Register.style.display = "auto";

}
let None1 = document.getElementById("btn_None");
let NoneALL = document.getElementById("btn_NoneAll");
function FilterNone() {
    var None = this.value;
    GetShopNone(None);
    None1.style.display = "none";
    NoneALL.style.display = "block";
    NoneALL.style.display = "auto";
}
function FilterNoneAll() {
    GetShop("all");
    NoneALL.style.display = "none";
    None1.style.display = "block";
    None1.style.display = "auto";
}
var tableShop = [];


function GetShopNone(shopId) {
    tableShop = $('#tblShop').dataTable({
        ajax: {
            //url: "/api/Shops?status=Register",
            //url: (None == "None_active") ? "/api/Shops/0/Inactive" : "/api/Shops/0/" + None,
            url: (shopId == "all") ? "/api/Shops/all/none" : "/api/Shops/" + shopId + "/Inactive",
            dataSrc: ""
        },
        columns:
            [
                {
                    data: "id",
                    render: function (data) {

                        return "" + ("").slice(-4);
                    }
                },
                {
                    data: "shopid"
                },
                {
                    data: "shopName"
                },
                //{
                //    data: "ownerName"
                //},
                //{
                //    data: "phone"
                //},

                {
                    data: "location"
                },

                {
                    data: "paymentType"
                },

                //{
                //    data: "methodName"
                //},
                {
                    data: "accountNumber"
                },
                {
                    data: "accountName"
                },
                {
                    data: "status",
                    render: function (data, type, meta, full) {

                        if (meta.status === 'Inactive') {
                            return "<button OnClick='Aprove(" + data + ")' class='btn btn-success btn-xs' style='margin-left: 20px;' id='Aprove'><span class='glyphicon glyphicon-ok'></span> Aprove</button> ";
                        } else if (meta.status === 'Active') {
                            return "<button OnClick='Disable(" + data + ")' class='btn btn-warning btn-xs' style='margin-left: 20px;'><span class='glyphicon glyphicon-ban-circle'></span> Disable</button> ";
                        }

                    }
                },
                {
                    data: "id",
                    render: function (data, type, meta, full) {

                        if (meta.status === 'Inactive') {
                            return "<div class='btn-group '><button type='button' class='btn btn-primary dropdown-toggle btn-xs ' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>Choose Action <span class='caret'></span></button> " + " " +
                                "<ul class='dropdown-menu animate slideIn' style='width:100px'><li> " + " " +
                                "</li><hr /><li><button OnClick='ShopDelete(" + data + ")' class='btn btn-danger btn-xs' style='margin-left: 20px;'><span class='glyphicon glyphicon-remove'></span> Reject</button> " + " " +
                                "</li><hr /><li><button OnClick='ShopEdit(" + data + ")' class='btn btn-info btn-xs' style='margin-left: 20px;'><span class='glyphicon glyphicon-edit'></span> Edit</button> " + " " +
                                //"</li><hr /><li><button OnClick='Aprove(" + data + ")' class='btn btn-warning btn-xs' style='margin-left: 20px;'><span class='glyphicon glyphicon-ban-circle'></span> Disable</button> " + " " +
                                "</li><hr /><li><button OnClick='Contract(" + data + ")' class='btn btn-info btn-xs ' style='margin-left: 20px;'><span class='glyphicon glyphicon-plus'></span> Contract</button></li></ul></div>"
                        } else if (meta.status === 'Active') {
                            return "<div class='btn-group '><button type='button' class='btn btn-primary dropdown-toggle btn-xs ' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>Choose Action <span class='caret'></span></button> " + " " +
                                "<ul class='dropdown-menu animate slideIn' style='width:100px'><li> " + " " +
                                //"<button OnClick='Aprove(" + data + ")' class='btn btn-success btn-xs' style='margin-left: 20px;' id='Aprove'><span class='glyphicon glyphicon-ok'></span> Aprove</button> " + " " +
                                "</li><hr /><li><button OnClick='ShopDelete(" + data + ")' class='btn btn-danger btn-xs' style='margin-left: 20px;'><span class='glyphicon glyphicon-remove'></span> Reject</button> " + " " +
                                "</li><hr /><li><button OnClick='ShopEdit(" + data + ")' class='btn btn-info btn-xs' style='margin-left: 20px;'><span class='glyphicon glyphicon-edit'></span> Edit</button> " + " " + 
                                "</li><hr /><li><button OnClick='Contract(" + data + ")' class='btn btn-info btn-xs ' style='margin-left: 20px;'><span class='glyphicon glyphicon-plus'></span> Contract</button></li></ul></div>"
                        }

                        
                    }
                    
                }
                
            ],
        destroy: true,
        "order": [[0, "desc"]],
        "info": false,
        "paging": true,
        lengthMenu: [
            [50, -1],
            [50,100, 'All'],
        ],
    });
}

function GetShop(shopId) {
    tableShop = $('#tblShop').dataTable({
        ajax: {
            //url: "/api/Shops?status=Register",
            url: (shopId == "all") ? "/api/Shops/all/none" : "/api/Shops/" + shopId +"/Active",
            dataSrc: ""
        },
        columns:
            [
                {
                    data: "id",
                    render: function (data) {

                        return "" + ("" ).slice(-4);
                    }
                },
                {
                    data: "shopid"
                },
                {
                    data: "shopName"
                },
                //{
                //    data: "ownerName"
                //},
                //{
                //    data: "phone"
                //},

                {
                    data: "location"
                },

                {
                    data: "paymentType"
                },

                //{
                //    data: "methodName"
                //},
                {
                    data: "accountNumber"
                },
                {
                    data: "accountName"
                },
                {
                    data: "id",
                    render: function (data, type, meta, full) {

                        if (meta.status === 'Inactive') {
                            return "<button OnClick='Aprove(" + data + ")' class='btn btn-success btn-xs' style='margin-left: 20px;' id='Aprove'><span class='glyphicon glyphicon-ok'></span> Aprove</button> ";
                        } else if (meta.status === 'Active') {
                            return "<button OnClick='Disable(" + data + ")' class='btn btn-warning btn-xs' style='margin-left: 20px;'><span class='glyphicon glyphicon-ban-circle'></span> Disable</button> ";
                        }
                    }
                },
                {
                    data: "id",
                    render: function (data, type, meta, full) {

                        if (meta.status === 'Inactive') {
                            return "<div class='btn-group '><button type='button' class='btn btn-primary dropdown-toggle btn-xs ' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>Choose Action <span class='caret'></span></button> " + " " +
                                "<ul class='dropdown-menu animate slideIn' style='width:100px'><li> " + " " +
                                "</li><hr /><li><button OnClick='ShopDelete(" + data + ")' class='btn btn-danger btn-xs' style='margin-left: 20px;'><span class='glyphicon glyphicon-remove'></span> Reject</button> " + " " +
                                "</li><hr /><li><button OnClick='ShopEdit(" + data + ")' class='btn btn-info btn-xs' style='margin-left: 20px;'><span class='glyphicon glyphicon-edit'></span> Edit</button> " + " " +
                                //"</li><hr /><li><button OnClick='Aprove(" + data + ")' class='btn btn-warning btn-xs' style='margin-left: 20px;'><span class='glyphicon glyphicon-ban-circle'></span> Disable</button> " + " " +
                                "</li><hr /><li><button OnClick='Contract(" + data + ")' class='btn btn-info btn-xs ' style='margin-left: 20px;'><span class='glyphicon glyphicon-plus'></span> Contract</button></li></ul></div>"
                        } else if (meta.status === 'Active') {
                            return "<div class='btn-group '><button type='button' class='btn btn-primary dropdown-toggle btn-xs ' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>Choose Action <span class='caret'></span></button> " + " " +
                                "<ul class='dropdown-menu animate slideIn' style='width:100px'><li> " + " " +
                                //"<button OnClick='Aprove(" + data + ")' class='btn btn-success btn-xs' style='margin-left: 20px;' id='Aprove'><span class='glyphicon glyphicon-ok'></span> Aprove</button> " + " " +
                                "</li><hr /><li><button OnClick='ShopDelete(" + data + ")' class='btn btn-danger btn-xs' style='margin-left: 20px;'><span class='glyphicon glyphicon-remove'></span> Reject</button> " + " " +
                                "</li><hr /><li><button OnClick='ShopEdit(" + data + ")' class='btn btn-info btn-xs' style='margin-left: 20px;'><span class='glyphicon glyphicon-edit'></span> Edit</button> " + " " +
                                "</li><hr /><li><button OnClick='Contract(" + data + ")' class='btn btn-info btn-xs ' style='margin-left: 20px;'><span class='glyphicon glyphicon-plus'></span> Contract</button></li></ul></div>"
                        }

                        
                    }

                }
            ],
        destroy: true,
        "order": [[0, "desc"]],
        "info": false,
        "paging": true,
        lengthMenu: [
            [50, -1],
            [50, 'All'],
        ],
    });
}
function GetPrivacy() {
    $.ajax({
        url: "/api/Privacys/",
        type: "GET",
        contentType: "application/json;charset=utf-8",
        success: function (data) {
            $("#privacys").text(data.description);
        }
    })      
}
function closefunctin() {
    $("#ContractModal").modal('show');
    $("#SelectModel").modal('hide');
    $("#ReportShopModel").modal('hide');
}
function PrivacyEditAct(id) {

    $.ajax({
        url: "/api/privacys/" + id,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            $('#privacyDes').text(result.description);;
            $("#ReportShopModel").modal('show');

        },
        error: function (errormessage) {
            toastr.error("Something unexpected happen.", "Server Response");
        }
    });
    return false;
}
function GetPrivacys() {
    tableCase = $('#privacyTable').DataTable({
        ajax: {
            url: "/api/Privacys",
            dataSrc: ""
        },
        columns: [
            
            {

                data: "description"
            },
            {
                data: "id",
                render: function (data) {
                    return "<button onclick='PrivacyEditAct(" + data + ")' class='btn btn-warning btn-xs' style='border-width: 0px; width: 75px; margin:0px 0px 0px 55px;'><span class='glyphicon glyphicon-edit'> </span> Select </button>" ;
                },
                "width": "130px"
            }
        ],
        destroy: true,
        "order": [[0, "asc"]],
        "info": true
    });

}


function ShopAction() {
    var action = '';
    action = document.getElementById('btnSaveShop').innerText;
    //alert(action);

    if (action == "Add New") {
        document.getElementById('btnSaveShop').innerText = 'Save';
        EnableControl();
        $('#shopname').focus();
    }
    else if (action == "Save") {
        //ValidateShop();
        var data = new FormData();        
        data.append("shopidm", $('#shopidm').val());
        data.append("shopName", $('#shopname').val());
        data.append("ownerName", $('#ownername').val());
        data.append("gender", $('#gender').val());
        data.append("dob", $('#dob').val());
        data.append("nationality", $('#nationality').val());
        data.append("phone", $('#phone').val());
        data.append("password", $('#password').val());
        data.append("tokenid", $('#tokenid').val());
        data.append("facebookPage", $('#facebookpage').val());
        data.append("location", $('#location').val());
        data.append("paymentType", $('#paymenttype').val());
        var files = $('#qrCodeImage').get(0).files;
        if (files.length > 0) {
            data.append("qrCodeImage", files[0]);
        }
        var files = $('#logoShop').get(0).files;
        if (files.length > 0) {
            data.append("logoShop", files[0]);
        }
        data.append("bankNameid", $('#bankname').val());
        data.append("accountNumber", $('#accountnumber').val());
        data.append("accountName", $('#accountname').val());
        data.append("feetype", $('#feetype').val());
        data.append("feecharge", $('#feecharge').val());
        data.append("shophistorydate", $('#shophistorydate').val());
        data.append("note", $('#note').val());
        data.append("status", $('#status').val());

        var ajaxRequest = $.ajax({
            url: "/api/Shops",
            data: data,
            type: "POST",
            contentType: false,
            processData: false,
            dataType: "json",
            success: function (result) {
                toastr.success("New Shop has been Created", "Server Respond");
                $('#tblShop').DataTable().ajax.reload();
                document.getElementById('btnSaveShop').innerText = 'Add New';
                $('#shopname').val('');
                ClearControl();
                $("#shopModal").modal('hide');
            },
            error: function (errormesage) {
                $('#shopname').focus();
                toastr.error("This ShopName is exist in Database", "Server Respond")
            }

        });

    } else if (action == "Update") {
        var data = new FormData();        
        data.append("shopidm", $('#shopidm').val());
        data.append("id", $('#shopID').val());
        data.append("shopName", $('#shopname').val());
        data.append("gender", $('#gender').val());
        data.append("dob", $('#dob').val());
        data.append("nationality", $('#nationality').val());
        data.append("ownerName", $('#ownername').val());
        data.append("phone", $('#phone').val());
        data.append("password", $('#password').val());
        data.append("tokenid", $('#tokenid').val());
        data.append("facebookPage", $('#facebookpage').val());
        data.append("location", $('#location').val());
        data.append("paymentType", $('#paymenttype').val());
        var files = $('#qrCodeImage').get(0).files;
        if (files.length > 0) {
            data.append("qrCodeImage", files[0]);
        }
        var files = $('#logoShop').get(0).files;
        if (files.length > 0) {
            data.append("logoShop", files[0]);
        }
        data.append("bankNameid", $('#bankname').val());
        data.append("accountNumber", $('#accountnumber').val());
        data.append("accountName", $('#accountname').val());
        data.append("feetype", $('#feetype').val());
        data.append("feecharge", $('#feecharge').val());
        data.append("shophistorydate", $('#shophistorydate').val());
        data.append("note", $('#note').val());
        data.append("status", $('#status').val());
        data.append("qrcode_old", $('#qrcode_old').val());
        data.append("logoShop_old", $('#logoShop_old').val());


        $.ajax({
            url: "/api/Shops/" + $("#shopID").val(),
            data: data,
            type: "PUT",
            contentType: false,
            processData: false,
            dataType: "json",
            success: function (result) {
                toastr.success("Shop has been Updated", "Server Respond");
                $('#tblShop').DataTable().ajax.reload();
                $('#tblShops').DataTable().ajax.reload();
                $("#shopModal").modal('hide');
                ClearItem();
                ClearControl();
                window.location.reload();
                $('#shopname').val('');
                $('#gender').val('');
                $('#dob').val('');
                $('#nationality').val('');
                $('#ownername').val('');
                $('#phone').val('');
                $('#password').val('');
                $('#tokenid').val('');
                $('#facebookpage').val('');
                $('#location').val('');
                $('#paymenttype').val('');
                $('#QrPhoto').val('');
                $('#bankname').val('');
                $('#accountnumber').val('');
                $('#accountname').val('');
                $('#QrlogoShop').val('');
                $('#feetype').val('');
                $('#feecharge').val('');
                $('#shophistorydate').val('');
                $('#note').val('');
                $('#status').val('');
            },
            error: function (errormesage) {
                toastr.error("Shop hasn't Updated in Database", "Server Respond")
            }
        });      
            }
        }  

function ShopEdit(id) {
    /*ClearControl();*/
    EnableControl();
    action = document.getElementById('btnSaveShop').innerText = "Update";
   

    $.ajax({
        url: "/api/Shops/" + id,
        type: "GET",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        success: function (result) {
            $('#shopID').val(result.id);
            $('#shopidm').val(result.shopid);
            ///console.log(result.id);            
            $("#shopname").val(result.shopName);
            $("#gender").val(result.gender);
            var date = new Date(result.dob);
            var datetime = moment(date).format('YYYY-MM-DD');
            $("#dob").val(datetime);
            $("#nationality").val(result.nationality);
            $("#ownername").val(result.ownerName);
            $("#phone").val(result.phone);
            $("#password").val(result.password);
            $("#tokenid").val(result.tokenid);
            $("#facebookpage").val(result.facebookPage);
            $("#location").val(result.location).change();
            $("#paymenttype").val(result.paymentType).change();

            $("#qrcode_old").val(result.qrCodeImage);
            $("#logoShop_old").val(result.logoShop);
            //QRCODE
            var files = $('#qrCodeImage').get(0).files;
            if (result.qrcodeimage == "") {
                $("#QrPhoto").attr('src', '../Images/company.png');
            } else {
                $("#QrPhoto").attr('src', '../Images/' + result.qrCodeImage);
               
            }
            //Logo Shop
            var files = $('#logoShop').get(0).files;
            if (result.logoShop == "") {
                $("#QrlogoShop").attr('src', '../Images/company.png');
                
            } else {
                $("#QrlogoShop").attr('src', '../Images/' + result.logoShop);
                
                
            }
                
                $("#bankname").val(result.bankNameid).change();
                $("#accountnumber").val(result.accountNumber);
            $("#accountname").val(result.accountName);
            $("#feetype").val(result.feetype);
                $("#feecharge").val(result.feecharge);
                var date = new Date(result.shophistorydate);
                var datetime = moment(date).format('YYYY-MM-DD');
                $("#shophistorydate").val(datetime);
                $("#note").val(result.note);
                $("#status").val(result.status).change();
                $("#shopModal").modal('show');
            },
            error: function (errormessage) {
                toastr.error("Something unexpected happenn.", "Server Response");
            }
        });
}

function ShopDelete(id) {
    bootbox.confirm({
        title: "",
        message: "Are you sure want to delete this?",
        buttons: {
            cancel: {
                label: "Cancel",
                className: "btn-default"
            },
            confirm: {
                label: "Delete",
                className: "btn-danger"
            }
        },

        callback: function (result) {
            //alert(id);
            if (result) {
                $.ajax({
                    url: "/api/Shops/" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        $('#tblShop').DataTable().ajax.reload();
                        toastr.success("Shop Deleted successfully!", "Service Response");
                    },
                    error: function (errormessage) {
                        toastr.error("Shop Can't be deleted", "Service Response");
                    }
                });
            }
        }
    });
}
function Aprove(id) {
    bootbox.confirm({
        title: "Confirmation",
        message: "Are you sure to Disable this?",
        buttons: {
            cancel: {
                label: "Cancel",
                className: "btn-default input-sm"
            },
            confirm: {
                label: "Disable",
                className: "btn-danger input-sm"
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: "/api/Shops/" + id,
                    type: "POST",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {

                        $('#tblShop').DataTable().ajax.reload();
                        toastr.success("Item Aprove successfully!", "Service Response");
                    },
                    error: function (errormessage) {
                        toastr.error("This Item is already exists in Database", "Service Response");
                    }
                });
            }
        }
    });
}
function Disable(id) {
    bootbox.confirm({
        title: "Confirmation",
        message: "Are you sure to Aprove this?",
        buttons: {
            cancel: {
                label: "Cancel",
                className: "btn-default input-sm"
            },
            confirm: {
                label: "Aprove",
                className: "btn-danger input-sm"
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: "/api/Shops/" + id,
                    type: "POST",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {

                        $('#tblShop').DataTable().ajax.reload();
                        toastr.success("Item Disable successfully!", "Service Response");
                    },
                    error: function (errormessage) {
                        toastr.error("This Item is already exists in Database", "Service Response");
                    }
                });
            }
        }
    });
}
function DisableControl() {
    document.getElementById('shopidm').disabled = true;
    document.getElementById('shopname').disabled = true;
    document.getElementById('ownername').disabled = true;
    document.getElementById('phone').disabled = true;
    document.getElementById('password').disabled = true;
    document.getElementById('tokenid').disabled = true;
    document.getElementById('facebookpage').disabled = true;
    document.getElementById('location').disabled = true;
    document.getElementById('paymenttype').disabled = true;
    document.getElementById('accountname').disabled = true;
    document.getElementById('bankname').disabled = true;
    document.getElementById('accountnumber').disabled = true;
    document.getElementById('feetype').disabled = true;
    document.getElementById('feecharge').disabled = true;
    document.getElementById('status').disabled = true;
    document.getElementById('shophistorydate').disabled = true;
    document.getElementById('note').disabled = true;
    //document.getElementById('qrcodeimage').disabled = true;
    

}

function EnableControl() {
    document.getElementById('shopidm').disabled = false;
    document.getElementById('shopname').disabled = false;
    document.getElementById('ownername').disabled = false;
    document.getElementById('phone').disabled = false;
    document.getElementById('password').disabled = false;
    document.getElementById('tokenid').disabled = false;
    document.getElementById('facebookpage').disabled = false;
    document.getElementById('location').disabled = false;
    document.getElementById('paymenttype').disabled = false;
    document.getElementById('bankname').disabled = false;
    document.getElementById('accountnumber').disabled = false;
    document.getElementById('accountname').disabled = false;
    document.getElementById('feetype').disabled = false;
    document.getElementById('feecharge').disabled = false;
    document.getElementById('shophistorydate').disabled = false;
    document.getElementById('note').disabled = false;
    document.getElementById('status').disabled = false;
    //document.getElementById('qrcodeimage').disabled = false;

}
function ClearItem() {
    $('#shopidm').val('');
    $('#shopname').val('');
    $('#gender').val('');
    $('#dob').val('');
    $('#nationality').val('');
    $('#ownername').val('');
    $('#phone').val('');
    $('#password').val('');
    $('#tokenid').val('');
    $('#facebookpage').val('');
    $('#location').val('');
    $('#paymenttype').val('');
    $('#QrPhoto').val('');
    $('#bankname').val('');
    $('#accountnumber').val('');
    $('#accountname').val('');
    $('#QrlogoShop').val('');
    $('#feetype').val('');
    $('#feecharge').val('');
    $('#shophistorydate').val('');
    $('#note').val('');
    $('#status').val('');
    window.location.reload();
}
function ClearControl() {
    $('#shopidm').val('');
    $('#shopname').val('');
    $('#gender').val('');
    $('#dob').val('');
    $('#nationality').val('');
    $('#ownername').val('');
    $('#phone').val('');
    $('#password').val('');
    $('#tokenid').val('');
    $('#facebookpage').val('');
    $('#location').val('');
    $('#paymenttype').val('');
    $('#QrPhoto').val('');
    $('#bankname').val('');
    $('#accountnumber').val('');
    $('#accountname').val('');
    $('#QrlogoShop').val('');
    $('#feetype').val('');
    $('#feecharge').val('');
    $('#shophistorydate').val('');
    $('#note').val('');
    $('#status').val('');


}

function AddnewAction() {
    document.getElementById('btnSaveShop').innerText = "Add New";
    DisableControl();
    ClearControl();
}


function ValidateShop() {
    var isValid = true;
    var formAddEdit = $("#formTrainingProgramAdd");
    if ($('#shopname').val().trim() == "") {
        $(' #shopname').css('border-color', 'red');
        $(' #shopname').focus();
        isValid = false;
    } else {
        $(' #shopname').css('border-color', '#cccccc');
        $(' #shopname').focus();
    }
    if ($(' #ownername').val().trim() == "") {
        $(' #ownername').css('border-color', 'red');
        isValid = false;
    } else {
        $(' #ownername').css('border-color', '#cccccc');

    }
    if ($(' #phone').val().trim() == "") {
        $(' #phone').css('border-color', 'red');
        isValid = false;
    } else {
        $(' #phone').css('border-color', '#cccccc');
    }
    if ($(' #password').val().trim() == "") {
        $(' #password').css('border-color', 'red');
        isValid = false;
    } else {
        $(' #password').css('border-color', '#cccccc');
    }
    if ($(' #tokenid').val().trim() == "") {
        $(' #tokenid').css('border-color', 'red');
        isValid = false;
    } else {
        $(' #tokenid').css('border-color', '#cccccc');
    }
    if ($(' #phone').val().trim() == "") {
        $(' #phone').css('border-color', 'red');
        isValid = false;
    } else {
        $(' #phone').css('border-color', '#cccccc');
    }
    if ($(' #shophistorydate').val().trim() == "") {
        $(' #shophistorydate').css('border-color', 'red');
        isValid = false;
    } else {
        $(' #shophistorydate').css('border-color', '#cccccc');
    }
    if ($(' #logoShop').val().trim() == "") {
        $(' #logoShop').css('border-color', 'red');
        isValid = false;
    } else {
        $(' #logoShop').css('border-color', '#cccccc');
    }
    if ($(' #qrCodeImage').files().trim() == "") {
        $(' #qrCodeImage').css('border-color', 'red');
        isValid = false;
    } else {
        $(' #qrCodeImage').css('border-color', '#cccccc');
    }
    return isValid;
}

function readURLqrCodeImage(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#QrPhoto').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}
function readURlogoShop(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#QrlogoShop').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}