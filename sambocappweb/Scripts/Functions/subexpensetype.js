﻿$(document).ready(function () {

    $(document).ajaxStart(function () {
        $('#loadingGif').addClass('show');
    }).ajaxStop(function () {
        $('#loadingGif').removeClass('show');
    });

    $('#subexpensetypeModel').on('show.bs.modal', function () {

        GetSubExpenseType();
        document.getElementById('subtypename').disabled = true;
        document.getElementById('btnsubExpenseType').innerText = "Add New";

    })
})

var tableDepartment = [];
toastr.optionsOverride = 'positionclass = "toast-bottom-right"';
toastr.options.positionClass = 'toast-bottom-right';

function GetSubExpenseType() {
    //alert('Hello');
    tableDepartment = $('#subexpensetypeTable').DataTable({
        ajax: {
            url: "/api/SubExpenseType",
            dataSrc: ""
        },
        columns: [
            {
                data:"id"
            },
            {
                data: "subtypename"
            },
            {
                data: "id",
                render: function (data) {
                    return "<button onclick='subexpensetypeEdit(" + data + ")' class='btn btn-warning btn-xs' style='margin-right:5px;'>Edit</button>" + "<button onclick='subexpensetypeDelete(" + data + ")' class='btn btn-danger btn-xs'>Delete</button>";
                }
            }
        ],
        destroy: true,
        "order": [0, "desc"],
        "info": false
    });
}

function SubExpenseTypeAction() {
    var action = '';
    action = document.getElementById('btnsubExpenseType').innerText;
    if (action == "Add New") {
        document.getElementById('btnsubExpenseType').innerText = "Save";
        document.getElementById('subtypename').disabled = false;
        $('#subtypename').focus();
        //alert('Hi');
    } else if (action == "Save") {
        if ($('#subtypename').val().trim() == "") {
            $('#subtypename').css('border-color', 'red');
            $('#subtypename').focus();
            toastr.info('Please enter Sub ExpenseType.', "Server Response")
        } else {
            $('#subtypename').css('border-color', '#cccccc');

            var dataSave = {
                subexpensetypeid: $('#subexpensetypeid').val(),
                subtypename: $('#subtypename').val()
            };

            $.ajax({
                url: "/api/SubExpenseType",
                data: JSON.stringify(dataSave),
                type: "POST",
                contentType: "application/json;charset=utf-8",
                datatype: "json",
                success: function (result) {
                    toastr.success("ExpenseType has been created successfully.", "Server Response");
                    tableDepartment.ajax.reload();
                    document.getElementById('btnsubExpenseType').innerText = "Add New";
                    $('#subtypename').val('');
                    document.getElementById('subtypename').disabled = true;
                },
                error: function (errormessage) {
                    toastr.error("This ExpenseType is already exists in Database", "Service Response");
                }
            })
        }
    } else if (action == "Update") {
        if ($('#subtypename').val().trim() == "") {
            $('#subtypename').css('border-color', 'red');
            $('#subtypename').focus();
            toastr.info('Please enter Category Name.', "Server Response")
        } else {
            $('#subtypename').css('border-color', '#cccccc');

            var data = {
                id: $('#subidtype').val(),
                subexpensetypeid: $('#subexpensetypeid').val(),
                subtypename: $('#subtypename').val()
            };

            //console.log(data);

            $.ajax({
                url: "/api/SubExpenseType/" + data.id,
                data: JSON.stringify(data),
                type: "PUT",
                contentType: "application/json;charset=utf-8",
                datatype: "json",
                success: function (result) {
                    toastr.success("ExpenseType has been update successfully.", "Server Response");
                    tableDepartment.ajax.reload();
                    document.getElementById('btnsubExpenseType').innerText = "Add New";
                    $('#subtypename').val('');
                    document.getElementById('subtypename').disabled = true;
                },
                error: function (errormessage) {
                    toastr.error("This ExpenseType is already exists in Database", "Service Response");
                }
            })
        }
    }

}

function subexpensetypeEdit(id) {
    $.ajax({
        url: "/api/SubExpenseType/" + id,
        type: "GET",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        success: function (result) {
            $('#subidtype').val(result.id);
            $('#subexpensetypeid').val(result.subexpensetypeid);
            $('#subtypename').val(result.subtypename);
            document.getElementById('btnsubExpenseType').innerText = "Update";
            document.getElementById('subtypename').disabled = false;
            $('#subtypename').focus();
        },
        error: function (errormessage) {
            toastr.error("This ExpenseType is already exists in Database", "Service Response");
        }
    });

}

function subexpensetypeDelete(idtype) {
    bootbox.confirm({
        title: "",
        message: "Are you sure want to delete this?",
        button: {
            cancel: {
                label: "Cancel",
                ClassName: "btn-default",
            },
            confirm: {
                label: "Delete",
                ClassName: "btn-danger"
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: "/api/SubExpenseType/" + idtype,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        tableDepartment.ajax.reload();
                        toastr.success("ExpenseType has been Deleted successfully!", "Service Response");
                        document.getElementById('subtypename').disabled = true;
                        document.getElementById('btnsubExpenseType').innerText = "Add New";
                    },
                    error: function (errormessage) {
                        toastr.error("This ExpenseType cannot delete it already use in Database", "Service Response");
                    }
                });
            }
        }
    });
}