﻿$(document).ready(function () {

    $(document).ajaxStart(function () {
        $('#loadingGif').addClass('show');
    }).ajaxStop(function () {
        $('#loadingGif').removeClass('show');
    });

    $('#subincometypeModel').on('show.bs.modal', function () {

        GetSubExpenseType();
        document.getElementById('subincometypename').disabled = true;
        document.getElementById('btnsubIncomeType').innerText = "Add New";

    })
})

var tableDepartment = [];
toastr.optionsOverride = 'positionclass = "toast-bottom-right"';
toastr.options.positionClass = 'toast-bottom-right';

function GetSubExpenseType() {
    //alert('Hello');
    tableDepartment = $('#subincometypeTable').DataTable({
        ajax: {
            url: "/api/SubIncomeType",
            dataSrc: ""
        },
        columns: [
            {
                data: "subincometypeid"
            },
            {
                data: "subincometypename"
            },
            {
                data: "id",
                render: function (data) {
                    return "<button onclick='subincometypeEdit(" + data + ")' class='btn btn-warning btn-xs' style='margin-right:5px;'>Edit</button>" + "<button onclick='subincometypeDelete(" + data + ")' class='btn btn-danger btn-xs'>Delete</button>";
                }
            }
        ],
        destroy: true,
        "order": [0, "desc"],
        "info": false
    });
}

function SubIncomeTypeAction() {
    var action = '';
    action = document.getElementById('btnsubIncomeType').innerText;
    if (action == "Add New") {
        document.getElementById('btnsubIncomeType').innerText = "Save";
        document.getElementById('subincometypename').disabled = false;
        $('#subincometypename').focus();
        //alert('Hi');
    } else if (action == "Save") {
        if ($('#subincometypename').val().trim() == "") {
            $('#subincometypename').css('border-color', 'red');
            $('#subincometypename').focus();
            toastr.info('Please enter Sub IncomeType.', "Server Response")
        } else {
            $('#subincometypename').css('border-color', '#cccccc');

            var dataSave = {
                subincometypeid: $('#subincometypeid').val(),
                subincometypename: $('#subincometypename').val()
            };

            //alert($('#subincometypeid').val());

            $.ajax({
                url: "/api/SubIncomeType",
                data: JSON.stringify(dataSave),
                type: "POST",
                contentType: "application/json;charset=utf-8",
                datatype: "json",
                success: function (result) {
                    toastr.success("IncomeType has been created successfully.", "Server Response");
                    tableDepartment.ajax.reload();
                    document.getElementById('btnsubIncomeType').innerText = "Add New";
                    $('#subincometypename').val('');
                    document.getElementById('subincometypename').disabled = true;
                },
                error: function (errormessage) {
                    toastr.error("This IncomeType is already exists in Database", "Service Response");
                }
            })
        }
    } else if (action == "Update") {
        if ($('#subincometypename').val().trim() == "") {
            $('#subincometypename').css('border-color', 'red');
            $('#subincometypename').focus();
            toastr.info('Please enter Category Name.', "Server Response")
        } else {
            $('#subincometypename').css('border-color', '#cccccc');

            var data = {
                id: $('#subidtypes').val(),
                subincometypeid: $('#subincometypeid').val(),
                subincometypename: $('#subincometypename').val()
            };

            //console.log(data);

            $.ajax({
                url: "/api/SubIncomeType/" + data.id,
                data: JSON.stringify(data),
                type: "PUT",
                contentType: "application/json;charset=utf-8",
                datatype: "json",
                success: function (result) {
                    toastr.success("IncomeType has been update successfully.", "Server Response");
                    tableDepartment.ajax.reload();
                    document.getElementById('btnsubIncomeType').innerText = "Add New";
                    $('#subincometypename').val('');
                    document.getElementById('subincometypename').disabled = true;
                },
                error: function (errormessage) {
                    toastr.error("This IncomeType is already exists in Database", "Service Response");
                }
            })
        }
    }

}

function subincometypeEdit(id) {
    $.ajax({
        url: "/api/SubIncomeType/" + id,
        type: "GET",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        success: function (result) {
            $('#subidtypes').val(result.id);
            $('#subincometypeid').val(result.subincometypeid);
            $('#subincometypename').val(result.subincometypename);
            document.getElementById('btnsubIncomeType').innerText = "Update";
            document.getElementById('subincometypename').disabled = false;
            $('#subincometypename').focus();
        },
        error: function (errormessage) {
            toastr.error("This IncomeType is already exists in Database", "Service Response");
        }
    });

}

function subincometypeDelete(idtype) {
    bootbox.confirm({
        title: "",
        message: "Are you sure want to delete this?",
        button: {
            cancel: {
                label: "Cancel",
                ClassName: "btn-default",
            },
            confirm: {
                label: "Delete",
                ClassName: "btn-danger"
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: "/api/SubIncomeType/" + idtype,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        tableDepartment.ajax.reload();
                        toastr.success("IncomeType has been Deleted successfully!", "Service Response");
                        document.getElementById('subincometypename').disabled = true;
                        document.getElementById('btnsubIncomeType').innerText = "Add New";
                    },
                    error: function (errormessage) {
                        toastr.error("This IncomeType cannot delete it already use in Database", "Service Response");
                    }
                });
            }
        }
    });
}