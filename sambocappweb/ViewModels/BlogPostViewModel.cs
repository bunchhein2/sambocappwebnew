﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using sambocappweb.Models;

namespace sambocappweb.ViewModels
{
    public class BlogPostViewModel
    {
        public IEnumerable<BlogCategory> BlogCategories { get; set; }
        public IEnumerable<BlogPost> BlogPosts { get; set; }
        public BlogPost BlogPost { get; set; }
        public IEnumerable<string> Images { get; set; }
    }
}