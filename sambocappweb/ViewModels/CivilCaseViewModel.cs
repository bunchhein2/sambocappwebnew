﻿using sambocappweb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sambocappweb.ViewModels
{
    public class CivilCaseViewModel
    {
        public IEnumerable<PaymentMethod> PaymentMethods { get; set; }
        public IEnumerable<Case> Cases { get; set; }
        public IEnumerable<CaseType> CaseTypes { get; set; }
        public IEnumerable<Lawyer> Lawyers { get; set; }
        public IEnumerable<Plaintiff> Plaintiffs { get; set; }
        public IEnumerable<Defendent> Defendents { get; set; }
        public IEnumerable<Tribunal> Tribunals { get; set; }
        public IEnumerable<CivilCase> Civilcases { get; set; }
        public IEnumerable<CivilcaseProceeding> CivilcaseProceedings { get; set; }
        public IEnumerable<Lawrelate> Lawrelates { get; set; }
        public IEnumerable<Customer> Customers { get; set; }
        public IEnumerable<Judge> Judges { get; set; }
        public IEnumerable<PaymentMethod> Paymentmethods { get; set; }
        public IEnumerable<Caseposition> Casepositions { get; set; }
        public IEnumerable<Office> Offices { get; set; }
        public IEnumerable<Clerk> Clerks { get; set; }
        public IEnumerable<Officer> Officers { get; set; }
        public IEnumerable<CivilcaseproccedingType> CaseproccedingTypes { get; set; }
        public int Id { get; set; }
        public DateTime civilcasedate { get; set; }
        public int civilcaseno { get; set; }
        public string documentno { get; set; }
        public int casetypeid { get; set; }
        public int defendentid { get; set; }
        public int plaintiffid { get; set; }
        public int caseid { get; set; }
        public int lawyerid { get; set; }
        public decimal charges { get; set; }
        public decimal services { get; set; }
        public string casestatus { get; set; }
        public string paidstatus { get; set; }
        public Boolean status { get; set; }
        public string createby { get; set; }
        public DateTime createdate { get; set; }
        public int customerid { get; set; }
        public int lawrelateid { get; set; }
        public int casepositionid { get; set; }
        public int officeid { get; set; }

    }
}