﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using sambocappweb.Models;

namespace sambocappweb.helper
{
    public class CheckPermissionUser
    {
        ApplicationDbContext context;

        public CheckPermissionUser()
        {
            context = new ApplicationDbContext();
        }
        public Boolean isSuperAdmin(string emailUser)
        {
            
                //var usersign = User.Identity;

                var user = context.Users.SingleOrDefault(u=>u.Email==emailUser);
                if (user.IsSuperUser == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            
           
        }

    }
}